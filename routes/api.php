<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Igaster\LaravelCities\Geo;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// geo packages routes
Geo::ApiRoutes();

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

