<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BizController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ServiceController;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AddProductController;
use App\Http\Controllers\UpdateBusinessAdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

/*Route::get('/new_sign_up', function () {
    return view('new_signup_thank_you');
});*/

Route::get('/new_login', function () {
    return view('auth/new_login');
});

Route::get('/new_register', function () {
    return view('auth/new_register');
});
Route::get('/new_admin_register', function () {
    return view('admin/admin_registration');
});


Route::get('/biz-registration', function () {
    return view('new_create_business_profile');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('/admin/usermanagement',[UserController::class,'index'])->middleware('admin');

Route::get('/admin/businesses',[BizController::class,'list'])->middleware('admin');

Route::get('/admin/orders',[OrderController::class,'showAllOrders'])->middleware('admin');

Route::get('/admin/order/{id}',[OrderController::class,'showOrderLine'])->middleware('admin');
Route::get('/admin/order/delete/{id}',[OrderController::class,'deleteAdminOrder'])->middleware('admin');
Route::get('/admin/orders/delete/{id}',[OrderController::class,'deleteAdminOrders'])->middleware('admin');

//Route::view('/biz-registration', 'biz-registration')->middleware('auth:sanctum');
//Route::post('/biz-registration', [BizController::class,'save']);

Route::middleware(['auth:sanctum', 'verified'])->get('/sign-up-thank-you', function (){ return view('new_signup_thank_you'); })->name('signup-thanks');

Route::middleware(['auth:sanctum', 'verified'])->get('/contacts', [ContactController::class, 'show_contact'])->name('contact');

Route::post('contacts/add_contact', [ContactController::class,'addContact']);
Route::get('contacts/delete/{id}',[ContactController::class,'delete_contact']);

Route::get('business-contacts', [BizController::class,'show_BizContact']);
Route::get('business-owner-profile', [BizController::class,'business_owner_profile']);

Route::middleware(['auth:sanctum'])->post('business-contacts/add_bizcontact', [BizController::class,'addBizContact']);
Route::get('business-contacts/delete/{id}',[BizController::class,'delete_bizContact']);

Route::get('biz-profile/{id}', [BizController::class,'show_data']);
Route::post('biz-profile/{id}/add_product', [ProductController::class,'addData']);
Route::post('biz-profile/{id}/update_banner', [BizController::class,'updateBanner']);
Route::post('biz-profile/{id}/update_logo', [BizController::class,'updateLogo']);
Route::post('biz-profile/{id}/add_service', [ServiceController::class,'save_service']);

Route::post('biz-profile/{id}/add_photo', [BizController::class,'addPhoto']);

Route::match(['get','post'],'/cart/add-item', [CartController::class,'addItem'])->name('become-a-customer');
Route::post('/cart/update_quantity', [CartController::class,'updateQuantity']);
Route::post('biz-profile/{id}/add_tags', [BizController::class,'addCategoryTags']);
Route::get('/biz-profile/{business_id}/remove-tag/{category_tag}', [BizController::class, 'removeCategoryTag']);

Route::middleware(['auth:sanctum', 'verified'])->get('/business-contacts', function () {
    return view('business-contacts');
})->name('business-contacts');

Route::middleware(['auth:sanctum', 'verified'])->get('/purchase-history', function () {
    return view('purchase-history');
})->name('purchase-history');

/*Route::middleware(['auth:sanctum', 'verified'])->get('/posts', function () {
    return view('posts');
})->name('posts');*/
Route::middleware(['auth:sanctum', 'verified'])->get('/posts', [PostController::class, 'showPosts'])->name('post');
Route::middleware(['auth:sanctum', 'verified'])->post('/posts/add_post', [PostController::class, 'addPost']);

Route::middleware(['auth:sanctum', 'verified'])->get('/my_feed', function () {
    return view('my_feed');
})->name('my_feed');

Route::get('/order_confirmation/{id}', [OrderController::class, 'orderConfirmation']);


Route::get('/biz-location/search', [BizController::class, 'searchLocation']);
Route::get('/location/search', [LocationController::class, 'searchLocation']);
Route::get('/location/children/{location_id}', [LocationController::class, 'getChildren']);
Route::get('/location/states/{country_code}', [LocationController::class, 'getStates']);

Route::get('/location/children/{location_id}', [LocationController::class, 'getChildren']);
Route::get('/location/states/{country_code}', [LocationController::class, 'getStates']);

/*Route::middleware(['auth:sanctum', 'verified'])->get('/cart', [CartController::class, 'showCart'])->name('cart');*/

Route::get('/cart', [CartController::class, 'showCart'])->name('new_cart');


Route::post('cart/remove', [CartController::class,'removeCart']);

Route::middleware(['auth:sanctum', 'verified'])->get('/my-travel-plans', [MyTravelPlansController::class, 'show_travel_plan'])->name('my-travel-plan');
Route::post('my-travel-plans/add_travel_plan', [MyTravelPlansController::class,'addTravelPlan']);
Route::get('my-travel-plans/delete/{id}',[MyTravelPlansController::class,'delete_travelPlan']);

Route::get('/biz-search',[BizController::class,'showSearch']);

Route::get('/order/{id}', [OrderController::class, 'showOrders']);
/*Route::middleware(['auth:sanctum', 'verified'])->post('/save_orders', [OrderController::class,'saveOrders']);*/

Route::get('/save_guest_orders', [OrderController::class,'saveOrders']);
Route::middleware(['auth:sanctum', 'verified'])->get('/save_shoppers_orders', [OrderController::class,'saveOrders']);

Route::middleware(['auth:sanctum', 'verified'])->get('/biz_owner/dashboard', [BizController::class, 'showDashboard'])->name('business-owner-dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/biz_owner/contacts', function () {
    return view('/biz_owner/contacts');
})->name('business-owner-contacts');

Route::middleware(['auth:sanctum', 'verified'])->get('/biz_owner/feed', function () {
    return view('/biz_owner/feed');
})->name('business-owner-feed');

Route::middleware(['auth:sanctum', 'verified'])->get('/biz_owner/suppliers', function () {
    return view('/biz_owner/suppliers');
})->name('business-owner-suppliers');

Route::middleware(['auth:sanctum', 'verified'])->get('/biz_owner/orders', function () {
    return view('/biz_owner/orders');
})->name('business-owner-orders');

Route::middleware(['auth:sanctum', 'verified'])->get('/biz_owner/sales', [OrderController::class, 'showSales'])->name('business-owner-sales-history');

Route::get('/', [HomeController::class, 'show_count']);

Route::get('/business-order', [OrderController::class,'showBusinessOrders']);

Route::get('/demo', function () {
        return view('demo');
});

Route::get('/search',[UserController::class,'search']);
Route::get('/sendrequest',[UserController::class,'sendrequest']);

Route::get('/business_records',[UserController::class,'getBusinessRecords']);
Route::get('/business_few_records',[UserController::class,'getFewBusinessRecords']);

/*Route::post('/biz-registration',[BizController::class,'ssssss']);*/

Route::get('/suggested_contacts',[UserController::class,'getSuggestedContacts']);
Route::get('/suggested_few_contacts',[UserController::class,'getFewSuggestedContacts']);  

Route::get('/get_contact_count',[UserController::class,'geContactCount']);

Route::get('/notification_details',[UserController::class,'getNotificationDetails']);


Route::get('/accept_contact_request',[UserController::class,'AcceptContactRequest']);

Route::get('/get_currency',[UserController::class,'getCurrency']);

Route::middleware(['auth:sanctum', 'verified'])->get('/old_contacts', [ContactController::class, 'show_old_contact'])->name('contact');

Route::get('business-contacts', [BizController::class,'show_BizContact']);

Route::get('/accepted_contacts', [UserController::class,'getAcceptedContacts']);
Route::get('/product/search', [ProductController::class, 'searchProduct']);
Route::get('/search_result', [ProductController::class, 'searchResult'])->name('search_result');;

Route::middleware(['auth:sanctum', 'verified'])->get('/search-results', function () {
    return view('search-results');
})->name('search-results');

Route::post('register', [UserController::class,'register']);
Route::post('user_register', [UserController::class,'user_register']);
Route::get('/thank-you-shopper', [UserController::class, 'thank_you_shopper']);

Route::get('check_emailaddress', [UserController::class,'check_emailaddress']);
Route::get('check_email_availability', [UserController::class,'check_email_availability']);

Route::get('save_business_personal_data', [BizController::class,'save_business_personal_data']);
Route::match(['get','post'],'save_product_data', [BizController::class,'save_product_data']);
Route::match(['get','post'],'save_video_data', [BizController::class,'save_video_data']);


Route::match(['get','post'],'update_product_data', [BizController::class,'update_product_data']);
Route::post('/biz-profile/change_tab_session', [BizController::class,'change_tab_session']);



Route::post('/remove_perticular_item', [OrderController::class,'removePerticularItem']);

Route::post('/place_order', [OrderController::class,'removePerticularItem']);

Route::post('/final_order', [OrderController::class,'finalOrder']);
Route::get('/guest_user', [UserController::class,'add_guest_user']);

Route::post('/update_perticular_item', [OrderController::class,'updatePerticularItem']);


Route::get('service_product_list',[BizController::class,'add_dropdown_list']);


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



Route::get('admin/admin_dashboard', function () {
    return view('admin/admin_dashboard');
});
//oute::middleware(['auth:sanctum', 'verified'])->get('/biz_owner/dashboard', [BizController::class, 'showDashboard'])->name('business-owner-dashboard');

Route::get('admin/dashboard',[AdminDashboarController::class, 'showAdminDashboard'])->name('admin-dashboard');

Route::get('admin/login',function(){
    return view('admin/admin_login');
})->name('admin-login');

Route::resource('/addProduct', AddProductController::class);
Route::resource('/deleteBusiness', UpdateBusinessAdminController::class);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
