var specialCharacterValidateFunc = function (obj, value, element) {
    return obj.optional(element) || /^[a-zA-Z\u0080-\u024F\s\-\)\(\`\.\"\'\_\‘\’\”\“]+$/i.test(jQuery.trim(value));
}

$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

$(document).ready(function () {
    $('#register').validate({
        wrapper: "p", 
        rules: {
            locations_search:"required",
            name:"reqired",
            phone:'required',
            email: {
                        required: true,
                        email: true,    
                        remote: {
                            url: "check_emailaddress",
                            type: "get"
                         }
                    },
            password : {
                    minlength : 5
                },
            password_confirmation:{
                    minlength : 5,
                    equalTo : "#password"
                }        
        },
        messages: {
            locations_search:"Please enter location",
            name:"Please enter name",
            phone:"Please enter the phone",
            email: {
            required: "Please enter your email address.",
            email: "Please enter a valid email address.",
            remote: "Email already in use!"
            },
            password:"Please enter the valid password",
            password_confirmation:"Please enter the valid confirm password"
        },
        errorPlacement: function(error, element) {
            var sequence = $(element).data('error');
            error.insertAfter(element);
        }
    });
});


$(document).ready(function () {
    $('#login').validate({
        wrapper: "p", 
        rules: {
            email: {
                        required: true,
                        email: true,    
                        remote: {
                            url: "check_email_availability",
                            type: "get"
                         }
                    },
            password : {
                    minlength : 5
                }        
        },
        messages: {
            
            email: {
            required: "Please enter your email address.",
            email: "Please enter a valid email address.",
            remote: "Email address does not exist!"
            },
            password:"Please enter the valid password"
        },
        errorPlacement: function(error, element) {
            var sequence = $(element).data('error');
            error.insertAfter(element);
        }
    });
});

$(document).ready(function () {
    $('#section_1').validate({
        wrapper: "p", 
        rules: {
            business_name:'required',
       //     specify_product_service_type:'required',
            locations_search:'required',
            address:'required',
            contact1:'required',
          //  contact2:'required',
            email: {
                        required: true,
                        email: true,    
                        remote: {
                            url: "check_emailaddress",
                            type: "get"
                         }
                    },
            passd: {
                    required: true,
                    minlength : 5
                }        
        },
        messages: {
            business_name:"Please enter the business name.",
           // specify_product_service_type:"Please enter the specify product service type.",
            locations_search:"Please enter the town.",
            address:"Please enter the address.",
            contact1:'Please enter the business contact.',
           // contact2:'Please enter the other contact.',
            email: {
            required: "Please enter your email address.",
            email: "Please enter a valid email address.",
            remote: "Email address does not exist!"
            },
            passd: "Please enter the valid password"
        },
        errorPlacement: function(error, element) {
            var sequence = $(element).data('error');
            error.insertAfter(element);
        },

        submitHandler: function (form) {
        // add a loading image in place of your returning outcome
        // serialize/combine all submitted fields into an array
        var postData = $("#section_1").serialize();
        // set url based of action
        var formURL = "save_business_personal_data";
        $.ajax({
                type: "GET",
                url: formURL,
                data: postData,
                success:function(data, textStatus, jqXHR) {

                    /*$("#myTabs form").on('submit', function (e) {
                      e.preventDefault();
                      var linkHref = $(this).parents('.tab-pane').attr('id');*/
                      $('#myLinks li a').removeClass('active');
                      $('#myLinks li')
                        .find('a[href="#Section1"]')
                        .parent()
                        .next()
                        .find('a')
                        .tab('show')
                        .addClass('active')
                        .attr('data-toggle', 'tab');
                        $('a.nav-link').not('.active').css('pointer-events', 'none');
                    /*});*/
                        $('a.nav-link').not('.active').css('pointer-events', 'none'); 
            },
                error: function(jqXHR, textStatus, errorThrown) {
                   
                }
            });
        }

    });
});

/*......................................................................*/
/*
$(document).ready(function () {
    $('#section_2').validate({
      
        rules: {
            image1:'required',
            description1:'required',
            rate1:'required',
            image2:'required',
            description2:'required',
            rate2:'required',
            image3:'required',
            description3:'required',
            rate3:'required',
            image4:'required',
            description4:'required',
            rate4:'required'
        },
        messages: {
            image1:"Please select the image.",
            description1:'Please enter the description',
            rate1:'Please enter the rate',
            image2:"Please select the image.",
            description2:"Please enter the description.",
            rate2:"Please enter the rate.",
            image3:"Please select the image.",
            description3:"Please enter the description.",
            rate3:"Please enter the rate",
            image4:"Please select the image.",
            description4:"Please enter the description.",
            rate4:"Please enter the rate"
        },
        errorPlacement: function(error, element) {
            var sequence = $(element).data('error');
            error.insertAfter(element);
        },

        submitHandler: function (form) {
            form.submit();
        }

    });
});*/
/*......................................................................*/
$(document).ready(function(){

    $('#section_2').submit(function(e){
        e.preventDefault(e);
      //  var isvalid = $("#section_2").valid();
      //  if(isvalid){
            
            var form_data = new FormData(this);
            /*var form_data = new FormData($('form').get(0));*/
            var formURL = "save_product_data";

           var token = $('meta[name="csrf-token"]').attr('content');
           

        $.ajax({
                method: "POST",
                url: formURL,
                data:form_data,
                headers: { 'X-CSRF-TOKEN': token },
                processData: false,
                contentType: false,
                success:function(data) {
                   // alert(data);
                   // window.location.href = "biz-profile/"+data;
                   // return false;
                   $('#myLinks li a').removeClass('active');
                   $('#myLinks li')
                     .find('a[href="#Section3"]')
                     .parent()
                     .next()
                     .find('a')
                     .tab('show')
                     .addClass('active')
                     .attr('data-toggle', 'tab');
                     $('a.nav-link').not('.active').css('pointer-events', 'none');
                 $('a.nav-link').not('.active').css('pointer-events', 'none'); 
                 $("#Section2").removeClass("active in show");
                 $("#Section3").addClass("active in show");
            },
                error: function(jqXHR, textStatus, errorThrown) {
                   alert("Error");
                }
            });
        return false;
     //   }else{
            /*alert("Not valid yet");*/
      //  }
    });
});
/*.......................................................................*/


$(document).ready(function () {




    $('#section_3').submit(function(e){
        e.preventDefault(e);
      //  var isvalid = $("#section_3").valid();
       // if(isvalid){
            
            var form_data = new FormData(this);
            /*var form_data = new FormData($('form').get(0));*/
            var formURL = "save_video_data";

           var token = $('meta[name="csrf-token"]').attr('content');
           

        $.ajax({
                method: "POST",
                url: formURL,
                data:form_data,
                headers: { 'X-CSRF-TOKEN': token },
                processData: false,
                contentType: false,
                success:function(data) {
                   // alert(data);
                    window.location.href = "biz-profile/"+data;
                    return false;
         
            },
                error: function(jqXHR, textStatus, errorThrown) {
                   alert("Error");
                }
            });
        return false;
     //   }else{
            /*alert("Not valid yet");*/
       // }
    });

    // $('#section_3').validate({
    //     wrapper: "p", 
    //     rules: {
    //         video_code:'required',
    //         video_description:'required'       
    //     },
    //     messages: {
    //         video_code:"Please enter the video code.",
    //         video_description:"Please enter the video description."
    //     },
    //     errorPlacement: function(error, element) {
    //         var sequence = $(element).data('error');
    //         error.insertAfter(element);
    //     },

    //     submitHandler: function (form) {

    //     var postDatas = $("#section_3").serialize();
    //     var formURL = "save_video_data";
    //     $.ajax({
    //             type: "GET",
    //             url: formURL,
    //             data: postDatas,
    //             success:function(data, textStatus, jqXHR) {
    //                 alert(data);
    //               // window.location.href = "biz-profile/"+data;
    //                //return false;
    //         },
    //             error: function(jqXHR, textStatus, errorThrown) {
                   
    //             }
    //         });
    //     }

    // });
});

$(document).ready(function(){
   $(document).on("click",".login-button",function(){
     var form = $(this).closest("form");
     //console.log(form);
     form.submit();
   });
});


function getComboA(selectObject) {
  var value = selectObject.value;  
  alert(value);
}

/*..........................................................*/

$(document).ready(function () {
    $('#biling_details').validate({
        wrapper: "p", 
        rules: {
            first_name:"required",
            last_name:"reqired",
            phone:'required',
            address:'required',
            country:'required',
            state:'required',
            zip_code:'required',
            email: {
                        required: true,
                        email: true,    
                        remote: {
                            url: "check_emailaddress",
                            type: "get"
                         }
                    }
        },
        messages: {
            
            first_name:"Please enter the first name.",
            last_name:"Please enter the last name.",
            phone:"Please enter the phone",
            address:"Please enter the address",
            country:"Please enter the country",
            state:"Please enter the state",
            zip_code:"Please enter the zip code",
            email: {
            required: "Please enter your email address.",
            email: "Please enter a valid email address.",
            remote: "Email already in use!"
            }
        },
        errorPlacement: function(error, element) {
            var sequence = $(element).data('error');
            error.insertAfter(element);
        }
    });
});




$(document).ready(function(){

   $(document).on("click","#tab1-tab",function(){

     var formURL = "change_tab_session";
     var token = $('meta[name="csrf-token"]').attr('content');
     $.ajax({
                method: "POST",
                url: formURL,
                data:{'tab':'1'},
                headers: { 'X-CSRF-TOKEN': token },
                success:function(data) {        
            },
                error: function(jqXHR, textStatus, errorThrown) {
                   alert("Error");
                }
            });
        return false;

   });

   $(document).on("click","#tab2-tab",function(){
   
     var formURL = "change_tab_session";
     var token = $('meta[name="csrf-token"]').attr('content');
     $.ajax({
                method: "POST",
                url: formURL,
                data:{'tab':'2'},
                headers: { 'X-CSRF-TOKEN': token },
                success:function(data) { 
                      
            },
                error: function(jqXHR, textStatus, errorThrown) {
                   alert("Error");
                }
            });
        return false;

   });

   $(document).on("click","#tab3-tab",function(){
  
     var formURL = "change_tab_session";
     var token = $('meta[name="csrf-token"]').attr('content');
     $.ajax({
                method: "POST",
                url: formURL,
                data:{'tab':'3'},
                headers: { 'X-CSRF-TOKEN': token },
                success:function(data) {  
  
                   
            },
                error: function(jqXHR, textStatus, errorThrown) {
                   alert("Error");
                }
            });
        return false;

   });

   $(document).on("click","#tab4-tab",function(){
    
     var formURL = "change_tab_session";
     var token = $('meta[name="csrf-token"]').attr('content');
     $.ajax({
                method: "POST",
                url: formURL,
                data:{'tab':'4'},
                headers: { 'X-CSRF-TOKEN': token },
                success:function(data) {   
                   
            },
                error: function(jqXHR, textStatus, errorThrown) {
                   alert("Error");
                }
            });
        return false;

   });

   $(document).on("click","#tab5-tab",function(){
   
     var formURL = "change_tab_session";
     var token = $('meta[name="csrf-token"]').attr('content');
     $.ajax({
                method: "POST",
                url: formURL,
                data:{'tab':'5'},
                headers: { 'X-CSRF-TOKEN': token },
                success:function(data) { 
                   
            },
                error: function(jqXHR, textStatus, errorThrown) {
                   alert("Error");
                }
            });
        return false;

   });
});





