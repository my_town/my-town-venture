<?php
namespace App\Http\Responses;
use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;
class LoginResponse implements LoginResponseContract
{
    /**
     * @inheritDoc
     */
    public function toResponse($request)
    {
		$home = \Auth::user()->hasRole('admin') ? '/dashboard' : '/my_feed';
		if(\Auth::user()->businesses->count() > 0){
			$biz = \Auth::user()->businesses->first();
			$home = '/biz_owner/dashboard';
		}
        return redirect()->intended($home);
    }
}

