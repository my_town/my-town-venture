<?php

namespace App\Http\Responses;

use Illuminate\Support\Facades\Auth;
use Laravel\Fortify\Contracts\RegisterResponse as RegisterResponseContract;

class RegisterResponse implements RegisterResponseContract
{
    public function toResponse($request)
    {
		$home = '/sign-up-thank-you';
		$referer = request()->headers->get('referer');echo $referer;
		if(preg_match('/biz=1/', $referer)){
			$home = '/biz-registration';
		}
        return redirect()->intended($home);
    }
}

