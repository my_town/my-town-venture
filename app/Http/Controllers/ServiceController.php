<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Service;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ServiceController extends Controller
{
    //
    function save_service(Request $req){
        $service=new Service;
        $service->business_id=$req->business_id;
        $service->name=$req->name;
        $service->description=$req->description;
        $service->price_base_currency=$req->price_base_currency=1;
        
        if($req->hasFile('service_image')){
            $service_file = $req->file('service_image');
            Storage::disk('public')->put('/service/'.time().'_'.$service_file->getClientOriginalName(), File::get($service_file));
            $service->service_image=time().'_'.$service_file->getClientOriginalName();
        }

        $service->save();
        return redirect('biz-profile/'.$service->business_id);
    }
}
