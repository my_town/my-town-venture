<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use DB;


class ProductController extends Controller
{
    function addData(Request $req){
        $product= new Product;
        $product->business_id=$req->business_id;
        $product->name= $req->name;
        $product->productId= $req->sku;
        $product->description= $req->description;
        $product->price_base_currency= $req->price_base_currency;
        if($req->hasFile('product_image')){
            $product_image_file = $req->file('product_image');
            Storage::disk('public')->put('/products/'.time().'_'.$product_image_file->getClientOriginalName(),  File::get($product_image_file));
            $product->product_image=time().'_'.$product_image_file->getClientOriginalName();
        }
        $product->save();
        return redirect('biz-profile/'.$product->business_id);
    }

    public function searchProduct(Request $request){

    $search = $request->search;
      if($search == ''){
    $products = DB::table('businesses')->orderByRaw("RAND()")->distinct()->limit(10)->get();
      }else{
    $products = Db::table('businesses')->where('business_offering_type','LIKE','%'.$request->search."%")->orderByRaw("RAND()")->distinct()->limit(1)->get();
      }
      $response = array();
      foreach($products as $row){
         $response[] = array("value"=>$row->id,"label"=>$row->business_offering_type);
      }
      return response()->json($response);
   }


   public function searchResult(Request $request){

    $location_search = $request->location_search;
    $location_id = $request->place_id;
    $product_search = $request->product_search;
    $productid = $request->productid;

    if(isset($location_search) && empty($product_search)){
        
             $data = DB::table('businesses')
        ->select('businesses.id as businesses_id',
            'businesses.owner as business_owner',
            'businesses.name as business_name',
            'businesses.phone_number1 as phone_number1',
            'businesses.phone_number2 as phone_number2',
            'businesses.email_address as email_address',
            'businesses.logo as business_logo',
            'businesses.banner_image as banner_image',
            'businesses.description as business_description',
            'businesses.address as business_address',
            'businesses.categories as business_categories')->where('businesses.place_id', 'like', '%' .$location_id. '%')->paginate(5);

    }else if(isset($product_search) && empty($location_search)){
     
         $data = DB::table('businesses')
        ->select('businesses.id as businesses_id',
            'businesses.owner as business_owner',
            'businesses.name as business_name',
            'businesses.phone_number1 as phone_number1',
            'businesses.phone_number2 as phone_number2',
            'businesses.email_address as email_address',
            'businesses.logo as business_logo',
            'businesses.banner_image as banner_image',
            'businesses.description as business_description',
            'businesses.address as business_address',
            'businesses.categories as business_categories')->where('businesses.business_offering_type', 'like', '%' .$product_search. '%')->paginate(5);

    }else if(Auth::user()){
       
        $data = DB::table('businesses')
        ->select('businesses.id as businesses_id',
            'businesses.owner as business_owner',
            'businesses.name as business_name',
            'businesses.phone_number1 as phone_number1',
            'businesses.phone_number2 as phone_number2',
            'businesses.email_address as email_address',
            'businesses.logo as business_logo',
            'businesses.banner_image as banner_image',
            'businesses.description as business_description',
            'businesses.address as business_address',
            'businesses.categories as business_categories')->where('businesses.place_id', 'like', '%' .Auth::user()->place_id. '%')->where('businesses.business_offering_type', 'like', '%' .$product_search. '%')->paginate(5);

    }else{

        $data = DB::table('businesses')
        ->select('businesses.id as businesses_id',
            'businesses.owner as business_owner',
            'businesses.name as business_name',
            'businesses.phone_number1 as phone_number1',
            'businesses.phone_number2 as phone_number2',
            'businesses.email_address as email_address',
            'businesses.logo as business_logo',
            'businesses.banner_image as banner_image',
            'businesses.description as business_description',
            'businesses.address as business_address',
            'businesses.categories as business_categories')->where('businesses.place_id', 'like', '%' .$location_id. '%')->where('businesses.business_offering_type', 'like', '%' .$product_search. '%')->paginate(5);
    }

        return view('/search-results',['information'=>$data]);

   }
    
}
