<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Business;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Melihovv\ShoppingCart\Facades\ShoppingCart as Cart;
use Session;
use DB;

class CartController extends Controller
{
    function addItem(Request $req){
	/*Cart::restore(\Auth::user()->id);*/
        
        if(Auth::user()){

            Session::forget('uuid');
            Session::put('uuid', Auth::user()->id);
            $uuid = Session::get('uuid');

        }else{
            //$uuid = Str::uuid()->toString();
            $user_record = DB::table('users')
                           ->where('email', '=','guest@mytown.com')
                           ->get();

            foreach ($user_record as $key => $user) {
                        $user_id = $user->id;   
                }

                echo $user_id;
            Session::put('uuid', $user_id);
            $uuid = Session::get('uuid');
            }

            //echo $uuid; die();
        
        Cart::restore((string)$uuid);
        $product_id=$req->product_id;
        $business_id = $req->business_id;
        $product=Product::find($product_id);
        $name=$product->name;
        $price=$product->price_base_currency;
        $quantity= (int)$req->quantity;
        /*echo $quantity; die();*/
        $cartItem = Cart::add($product_id, $name, $price, $quantity);   
        //Cart::store(\Auth::user()->id);
        Cart::store((string)$uuid);
        Session::forget('tab_count');
        $tab_count = "3";
        Session::put('tab_count', $tab_count);
        return redirect('biz-profile/'.$business_id);
	/*return redirect('/biz-profile/141');*/
    }

    function showCart(){
	   // Cart::restore(\Auth::user()->id);
         Cart::restore((string)Session::get('uuid'));
    return view('new_cart',['cart'=>Cart::content()->sort(), 'total'=>Cart::getTotal()]);
    }

    function removeCart(Request $req){
        //Cart::restore(\Auth::user()->id);
        Cart::restore((string)Session::get('uuid'));
        $item_id=$req->item_id;
        Cart::remove($item_id);
        //Cart::store(\Auth::user()->id);
        Cart::store((string)Session::get('uuid'));
        return redirect('/cart');
    }

    function updateQuantity(Request $req){

        //Remove 
        //Cart::restore(\Auth::user()->id);
        Cart::restore((string)Session::get('uuid'));
        $item_id=$req->item_id;
        Cart::remove($item_id);
        //Cart::store(\Auth::user()->id);
        Cart::store((string)Session::get('uuid'));

        //Add
        //Cart::restore(\Auth::user()->id);
        Cart::restore((string)Session::get('uuid'));
        $product_id=$req->product_id;
        $name=$req->name;
        $quantity=(int)$req->quantity;
        $price=$req->price;
        $cartItem = Cart::add($product_id, $name, $price, $quantity);
        //Cart::store(\Auth::user()->id);
        Cart::store((string)Session::get('uuid'));
        return redirect('/cart');

    }
}
