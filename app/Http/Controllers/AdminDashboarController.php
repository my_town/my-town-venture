<?php

namespace App\Http\Controllers;

use App\Models\Business;
use App\Models\Order;
use App\Models\order_shopper_details;
use App\Models\Product;
use App\Models\shopper_details;
use App\Models\User;
use DB;
use Illuminate\Http\Request;

class AdminDashboarController extends Controller
{
    //
    public function showAdminDashboard()
    {
        //DB::enableQueryLog();
        //business owner details
        //name id email phone address monthly sale

        $business = Business::where('status', '<>', 3)->get(['id', 'name', 'phone_number1', 'email_address', 'address']);
        //dd(DB::getQueryLog());

        $products = DB::table('products')->get(['business_id', 'productId', 'name', 'description', 'product_count', 'actual_rate']);

       
        $products= Product::join('businesses', 'products.business_id', '=', 'businesses.id')
            ->where('businesses.status', '<>', 3)
            ->get(['products.business_id', 'products.productId', 'products.name', 'products.description', 'products.product_count', 'products.actual_rate']);
        
        
            $shoppers = DB::table('shopper_details')->get(['shopperId', 'name', 'email', 'phone_number', 'address']);
      
      
            $order = order_shopper_details::join('order_line_items', 'order_line_items.order_id', '=', 'order_shopper_details.order_id')
            ->join('products', 'products.id', '=', 'order_line_items.product_id')
            ->join('businesses', 'businesses.id', '=', 'order_line_items.business_id')
            ->orderBy('order_shopper_details.order_id')
            ->get(['order_shopper_details.order_id', 'order_shopper_details.first_name', 'order_shopper_details.last_name', 'order_shopper_details.address as shAddress', 'order_line_items.cost', 'order_line_items.product_count', 'order_line_items.created_at', 'products.productId', 'products.name as productName', 'businesses.id', 'businesses.name', 'businesses.address']);

        
            $shipment = Order::join('order_shopper_details', 'order_shopper_details.order_id', '=', 'orders.id')
            ->join('order_line_items', 'order_line_items.order_id', '=', 'order_shopper_details.order_id')
            ->join('businesses', 'businesses.id', '=', 'order_line_items.business_id')
            ->orderBy('orders.id')
            ->get(['orders.id as orderId', 'orders.created_at', 'businesses.id', 'businesses.address', 'order_shopper_details.first_name', 'order_shopper_details.last_name', 'order_shopper_details.address as shAddress']);

            // <th>BusinessOwner Id</th>
            // <th>BusinessOwner Name</th>
            // <th>BusinessOwner: Town Name</th>
            // <th>Product Quentity</th>
            // <th>Monthly Sale</th>
            // <th>Shoper Name</th>
            // <th>Shopper:Town Name</th>
            // <th>Order ID</th>
            // <th>Total Order Cost</th>
           // DB::enableQueryLog();
            $businessOwner=Business::where('status', '<>', 3)->get(['name','id','address']);
            //dd(DB::getQueryLog());
            $summaryArr=[];
            $ownerArr=[];
            $shopperArr=[];
            $orderArr=[];
            foreach($businessOwner as $owner)
            {
                $businessId=  $owner->id;
                $businessName= $owner->name;
                $businessTown= $owner->address;
                $productQty= Product::where('business_id',$businessId)->sum('product_count');
                $monthlySale=0;
                $mSale=  DB::table('order_line_items')->where('business_id',$businessId)->get(['product_count' , 'cost']);
                foreach($mSale as $sale)
                {
                    $total =$sale->product_count * $sale->cost;
                    $monthlySale +=$total;
                }
                
                

                $ownerArr['businessId']=$businessId;
                $ownerArr['businessName']=$businessName;
                $ownerArr['businessTown']=$businessTown;
                $ownerArr['productQty']=$productQty;
                $ownerArr['monthlySale']=$monthlySale;
                //shopper details
                $arr=[];
                $sDetails=DB::table('final_order')
                ->join('order_line_items' ,'order_line_items.order_id', '=', 'final_order.order_id')
                ->where('order_line_items.business_id', $businessId)
                ->distinct('order_line_items.order_id')
                ->get(['final_order.first_name', 'final_order.last_name' ,'final_order.address']);

                foreach($sDetails as $shopperDetails)
                {
                    $shopperArr['shoperName']= $shopperDetails->first_name.' '.$shopperDetails->last_name;
                    $shopperArr['address']=$shopperDetails->address;
                  
                    array_push($arr, $shopperArr);
                }
               // $ownerArr['shopper']=$arr;
            array_push($summaryArr,$ownerArr);
            }
           // print_r();
//dd($summaryArr);

        $shoppersCount = shopper_details::all()->count();



        $businessCount = Business::all()->count();

        $productCount = Product::count();

        $townCount = DB::table('users')->distinct('place')->count('place_id');

        //$countryCount =DB::table('final_order')->distinct('country')->count('country');
        $townCount = User::distinct()->where('role','<>','3')->count('town');
        $cityCount  = User::distinct()->where('role','<>','3')->count('city');
        $countryCount = User::distinct()->where('role','<>','3')->count('country');
        return view('admin/admin_dashboard', ['business' => $business, 'products' => $products, 'shoppers' => $shoppers, 'orders' => $order, 'orderShipment' => $shipment, 'shopperCount' => $shoppersCount, 'businessCount' => $businessCount , 'productCount'=>$productCount,  'cityCount'=>$cityCount, 'townCount'=>$townCount, 'countryCount'=>$countryCount, 'summaries'=>$summaryArr]);
    }

    public function deleteBusinessOwner(Request $req)
    {
        $businessId = $req->id;

        $ownerId = Business::find($businessId)->value('owner');

        $deleteUser = DB::table('user')->where('id', $ownerId)->update(['email' => ' ']);

        $updateBusiness = Business::where('id', $businessId)->update(['status' => 3]);

    }

}
