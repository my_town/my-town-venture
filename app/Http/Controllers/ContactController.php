<?php

namespace App\Http\Controllers;
use App\Models\Contact;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;

class ContactController extends Controller
{
    function addContact(Request $req){
        $array = explode(", ",$req->name);
        $name=$array[0];
        if(count($array)>1){
            $place=$array[1];
        }else{
            $place="";
        }
        for($i=2; $i<count($array);$i++){
            $place=$place.", ".$array[$i];
        }
        $place=trim($place);
        $user=User::where('name','=',$name)->where('place','=',$place)->first();
        if($user){
            $contact = new Contact;
            $contact->user_id=Auth::user()->id;
            $contact->linked_user_id=$user->id;
            $contact->save();
        }
        else{
            echo "No such user";
        }
        return redirect('contacts/');
    }

    function show_contact(){

        $data = DB::table('contacts')
        ->join('users', 'users.id', '=', 'contacts.linked_user_id')
        ->select('contacts.id as contact_id','users.name as user_name','users.place as user_place')->where('contacts.user_id','=',Auth::user()->id)->get();

        return view('new_contacts',['contacts'=>$data]);
        /*return view('contacts',['contacts'=>$data]);*/
    }
    
    function delete_contact($id) 
       {
          $contact = Contact::find($id);
          $contact->delete();
          return redirect('contacts');
       }

       function show_old_contact(){

        $data = DB::table('contacts')
        ->join('users', 'users.id', '=', 'contacts.linked_user_id')
        ->select('contacts.id as contact_id','users.name as user_name','users.place as user_place')->where('contacts.user_id','=',Auth::user()->id)->get();

        return view('contacts',['contacts'=>$data]);
        }
        
    }
