<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use App\Models\Business;
use App\Models\OrderLineItem;
use Melihovv\ShoppingCart\Facades\ShoppingCart as Cart;
use Session;
use DB;

class OrderController extends Controller
{
    function saveOrders(Request $req){

        //echo Session::get('uuid'); die();
        //Cart::restore(\Auth::user()->id);
        Cart::restore(Session::get('uuid'));
        $cart_content=Cart::content();
        $order=new Order;
        //$order->user_id=Auth::user()->id;
        $order->user_id=Session::get('uuid');
        $order->items=$cart_content;
        $order->total_price=Cart::getTotal();
        $order->save();
        
        $this->saveOrderLineItems($order->id);
        return redirect('order/'.$order->id);

    }
    function showOrders($id){
        $orders=Order::find($id);
        if(Auth::user()){
        $users = User::find(Auth::user()->id);
    }else{
        $users = "";
    }
        /*return view('order',['orders'=>$orders]);*/
        return view('new_order',['orders'=>$orders,'users'=>$users]);
    }
    function showAllOrders(){
        $orders = Order::orderBy('created_at','desc')->get();
        return view('admin_order',['orders'=>$orders]);
    }
    function showOrderLine($id){
        $orders=OrderLineItem::where('order_id',$id)->get();
        return view('admin_order_line',['orders'=>$orders]);
    }
    function saveOrderLineItems($id){
        $order=Order::where('id',$id)->first();
        $items=json_decode($order->items);
        foreach ($items as $item) {
            $product=Product::where('id',$item->id)->first();
            $order_line=new OrderLineItem;
            $order_line->business_id=$product->business_id;
            $order_line->order_id=$order->id;
            $order_line->product_id=$item->id;
            $order_line->product_count=$item->quantity;
            $order_line->cost=$item->price;
            $order_line->status="";
            $order_line->save();
        }
    }
    function showBusinessOrders(){
        $business=Business::where('owner',\Auth::user()->id)->first();
        $orders=OrderLineItem::where('business_id',$business->id)->get();
        return view('business-orders',['orders'=>$orders]);
    }
    function deleteAdminOrder($id){
        $order=OrderLineItem::find($id);
        OrderLineItem::find($id)->delete();
        return redirect('/admin/order/'.$order->order_id);
    }
    function deleteAdminOrders($id){
        $order=Order::find($id);
        Order::find($id)->delete();
        OrderLineItem::where('order_id',$order->id)->delete();
        return redirect('/admin/orders');
    }
    function showSales(){
        $business=Business::where('owner',\Auth::user()->id)->first();
        $orders=OrderLineItem::where('business_id',$business->id)->get();
        return view('biz_owner/sales-history',['orderlines'=>$orders]);
    }

    function removePerticularItem(Request $req){

        $orders=Order::find($req->order_id);
        $data = json_decode($orders->items, true);
        foreach($data as &$item){
            if($req->item == $item['id']){
                echo "Success";
                unset($data[$item['unique_id']]);
            }
        }
        unset($item);
        $json_string_modified = json_encode($data,JSON_PRETTY_PRINT);
        $affected = DB::table('orders')->where('id', $req->order_id)->update(['items' =>$json_string_modified ]);

        Cart::restore((string)Session::get('uuid'));
        Cart::remove($req->item_id);
        Cart::store((string)Session::get('uuid'));

         return redirect('/order/'.$req->order_id);
        
    }


    function finalOrder(Request $req){

        if(Auth::user()){

            $users = User::find(Auth::user()->id);
            $name = preg_split('/\s+/', $users->name, -1, PREG_SPLIT_NO_EMPTY);

            $first_name = $name[0];
            if(isset($name[1]))
            {
                $last_name = $name[1];
            }else{
                $last_name = '';
            }
           
            $phone = $users->phone;
            $email = $users->email;
            $address = $users->place;
            $country = "";
            $state = "";
            $zip_code = "";
            $user_id = Session::get('uuid');
            $order_id = $req->order_id;
            $card_type = $req->optradio;
            $month = $req->month;
            $year = $req->year;
            $card_number = $req->card_number;
            $security_code = $req->security_code;
            $expiry_date = $month."/".$year;



        }else{
        
        $first_name = $req->first_name;
        $last_name = $req->last_name;
        $phone = $req->phone;
        $email = $req->email;
        $address = $req->address;
        $country = $req->country;
        $state = $req->state;
        $zip_code = $req->zip_code;
        //$user_id = Auth::user()->id;
        $user_id = Session::get('uuid');
        $order_id = $req->order_id;
        $card_type = $req->optradio;
        $month = $req->month;
        $year = $req->year;
        $card_number = $req->card_number;
        $security_code = $req->security_code;
        $expiry_date = $month."/".$year;

    }
    $last = DB::table('users')->latest()->first();      

    $shopperLastId= DB::table('shopper_details')
    ->orderBy('shopperId', 'desc')
    ->limit(1)
    ->get('shopperId');
   // dd(DB::getQueryLog());


   
    if(count($shopperLastId)>0)
    {
      foreach($shopperLastId as $shoId)
      $shoppersId= $shoId->shopperId + 1;
    }else{
      $shoppersId =$last->id;
    }

$statusShopper=2; 
    
        $data1 = [
                   [
                    'user_id'=>$user_id,
                    'order_id'=>$order_id,
                    'address_type'=>'1',
                    'first_name'=> $first_name,
                    'last_name'=>$last_name,
                    'phone'=>$phone,
                    'email'=>$email,
                    'address'=>$address,
                    'country'=>$country,
                    'state'=> $state,
                    'zip_code'=>$zip_code,
                    'card_type'=>$card_type,
                    'expiry_date'=>$expiry_date,
                    'card_number'=>$card_number,
                    'security_code'=>$security_code,
                    'created_at'=>now(),
                    'updated_at'=>now()
                   ] 
                ];
            $dataShopper1 = [[
                'order_id'=>$order_id,
                'first_name'=> $first_name,
                'last_name'=>$last_name,
                'phone'=>$phone,
                'email'=>$email,
                'address'=>$address,
                'country'=>$country,
                'state'=> $state,
                'zip_code'=>$zip_code,
                

            ]];
            
            $shopper = [
                [
                  
                 'shopperId'=> $shoppersId,
                 'status'=>$statusShopper,
                  'name'=>$first_name.' '.$last_name,
                 'email'=>$email,
                 'address'=>$address,
                 'phone_number'=>$phone,
                 'created_at'=>now(),
                 'profile_image' =>'',
                 'updated_at'=>now()
                 ] 
             ];
        $result1 = DB::table('final_order')->insert($data1);
     $total_amount = $req->total_amount;
    $affected2 = DB::table('orders')->where('id', $order_id)->update(['total_price' =>$total_amount]);

                
        if($req->first_name2){
            $statusShopper=1;
            $first_name2 = $req->first_name2;
            $last_name2 = $req->last_name2;
            $phone2 = $req->phone2;
            $email2 = $req->email2;
            $address2 = $req->address2;
            $country2 = $req->country2;
            $state2 = $req->state2;
            $zip_code2 = $req->zip_code2;

            $data2 = [
                   [
                    'user_id'=>$user_id,
                    'order_id'=>$order_id,
                    'address_type'=>'2',
                    'first_name'=> $first_name2,
                    'last_name'=>$last_name2,
                    'phone'=>$phone2,
                    'email'=>$email2,
                    'address'=>$address2,
                    'country'=>$country2,
                    'state'=> $state2,
                    'zip_code'=>$zip_code2,
                    'card_type'=>$card_type,
                    'expiry_date'=>$expiry_date,
                    'card_number'=>$card_number,
                    'security_code'=>$security_code,
                    'created_at'=>now(),
                    'updated_at'=>now()
                   ] 
                ];
            $dataShopper1 = [
                [
                'order_id'=>$order_id,
                    'first_name'=> $first_name2,
                    'last_name'=>$last_name2,
                    'phone'=>$phone2,
                    'email'=>$email2,
                    'address'=>$address2,
                    'country'=>$country2,
                    'state'=> $state2,
                    'zip_code'=>$zip_code2,
                  
            ]
        ];
        $result2 = DB::table('final_order')->insert($data2);
        $shopper = [
            [
              
             'shopperId'=> $shoppersId,
             'status'=>$statusShopper,
              'name'=>$first_name2.' '.$last_name2,
             'email'=>$email2,
             'address'=>$address2,
             'phone_number'=>$phone2,
             'created_at'=>now(),
             'profile_image' =>'',
             'updated_at'=>now()
             ] 
         ];

        }
        $result3=DB::table('order_shopper_details')->insert($dataShopper1);
        $result4=DB::table('shopper_details')->insert($shopper);
       
        if($result1){

            Cart::destroy($user_id);
            Session::forget('uuid');
        
        /*$orders=Order::find($id);
        return view('new_order',['orders'=>$orders]);
        return redirect('order_confirmation');*/

            return redirect('order_confirmation/'.$order_id);
        }

    }

    function orderConfirmation($id){

        $orders=Order::find($id);
$billing_address = DB::table('final_order')->select('*')->where('order_id','=',$id)->where('address_type','=','1')->get();
$shipping_address = DB::table('final_order')->select('*')->where('order_id','=',$id)->where('address_type','=','2')->get();

return view('order_confirmation',['orders'=>$orders,'billing_address'=>$billing_address,'shipping_address'=>$shipping_address]);
    }


    function updatePerticularItem(Request $req){

        $orders=Order::find($req->order_id);
        $data = json_decode($orders->items, true);
        foreach($data as &$item){
            if($req->item_id == $item['id']){
                $data[$item['unique_id']]['quantity']=$req->quantity;
                //$data[$item['unique_id']]['price']=$req->price;
                //echo $req->quantity;
                //$data['quantity'] = "5";
            }

        }
        unset($item);
        $json_string_modified = json_encode($data,JSON_PRETTY_PRINT);
        $affected1 = DB::table('orders')->where('id', $req->order_id)->update(['items' =>$json_string_modified ]);

        /*Cart::restore((string)Session::get('uuid'));
        Cart::remove($req->item_id);
        Cart::store((string)Session::get('uuid'));
         return redirect('/order/'.$req->order_id); */
    }
}
