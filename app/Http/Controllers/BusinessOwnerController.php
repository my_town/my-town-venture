<?php

namespace App\Http\Controllers;

use App\Models\BusinessOwner;
use Illuminate\Http\Request;

class BusinessOwnerController extends Controller
{
    //

    public function index()//display all the data from table
    {
        $businessOwnerProfile= BusinessOwner::all();
        return view('businessowner.index', compact($businessOwnerProfile));

    }


    public function create()
    {
        return view('businessowner.create');
    
    }

    public function storeProfile()//save form data in database table
    {
        $bProfile= new BusinessOwner();

        $bProfile->business_name = request('business_name');
        $bProfile->service_type = request('service_type');
        $bProfile->product_type = request('product_type');
        $bProfile->email = request('email');
        $bProfile->phone_num = request('phone_num');
        $bProfile->phone_num_alter = request('phone_num_alter');
        $bProfile->address = request('address');

        $bProfile->save();

        return redirect('/businessOwner');
    }
}
