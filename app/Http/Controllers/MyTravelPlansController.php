<?php

namespace App\Http\Controllers;
use App\Models\MyTravelPlan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class MyTravelPlansController extends Controller
{
    function addTravelPlan(Request $req)
    {
        $my_travel_plan = new MyTravelPlan;
        $my_travel_plan->user_id=Auth::user()->id;
        $my_travel_plan->date=$req->date;
        $my_travel_plan->from=$req->from;
        $my_travel_plan->to=$req->to;
        $my_travel_plan->save();
        return redirect('my-travel-plans/');
    }

    function show_travel_plan(){
        $my_travel_plan = MyTravelPlan::where('user_id',\Auth::user()->id)->get();
        return view('my-travel-plans',['my_travel_plans'=>$my_travel_plan]);
        }

     function delete_travelPlan($id) {
            $my_travel_plan=MyTravelPlan::find($id);
            $my_travel_plan->delete();
            return redirect('my-travel-plans');
         }
}
