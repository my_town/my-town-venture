<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Business;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    function addPost(Request $req){
        $array = explode(", ",$req->name);
        $name=$array[0];
        if(count($array)>1){
            $place=$array[1];
        }else{
            $place="";
        }
        for($i=2; $i<count($array);$i++){
            $place=$place.", ".$array[$i];
        }
        $place=trim($place);
        $business=Business::where('name','=',$name)->where('address','=',$place)->first();
        if($business){
            $post = new Post;
            $post->business_id=$business->id;
            $post->user_id=Auth::user()->id;
            $post->experience=$req->experience;
            $post->save();
        }
        else{
            echo "No such user";
        }
        return redirect('/posts');
    }
    function showPosts(){
        $data = Post::all();
        return view('posts',['posts'=>$data]);
    }
}
