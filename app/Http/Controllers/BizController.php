<?php

namespace App\Http\Controllers;

use App\Helpers\Location;
use App\Models\Business;
use App\Models\BusinessContact;
use App\Models\BusinessOwnerNotify;
use App\Models\Hit;
use App\Models\OrderLineItem;
use App\Models\Photo;
use App\Models\Post;
use App\Models\Product;
use App\Models\User;
use DB;
//use SKAgarwal\GoogleApi\PlacesApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Session;
use View;

class BizController extends Controller
{

    public function save(Request $req)
    {

        $business = new Business;
        $business->owner = Auth::user()->id;
        $business->name = $req->name;
        $business->address = $req->location_search;
        $business->place_id = $req->place_id;
        $business->phone_number1 = $req->phone_number1;
        $business->phone_number2 = $req->phone_number2;
        $business->email_address = $req->email_address;
        $business->description = $req->description;

        if ($req->hasFile('logo')) {
            $logo_file = $req->file('logo');
            Storage::disk('public')->put('/logo/' . time() . '_' . $logo_file->getClientOriginalName(), File::get($logo_file));
            $business->logo = time() . '_' . $logo_file->getClientOriginalName();
        }
        if ($req->hasFile('banner_image')) {
            $banner_file = $req->file('banner_image');
            Storage::disk('public')->put('/banner/' . time() . '_' . $banner_file->getClientOriginalName(), File::get($banner_file));
            $business->banner_image = time() . '_' . $banner_file->getClientOriginalName();
        }

        $business->save();
        return redirect('biz-profile/' . $business->id);
    }

    public function show_data($id)
    {
        $business = Business::find($id);
        //$business = Business::where('id', $id)->first();
        $posts = Post::where('business_id', $business->id)->get();
        $products = Product::where('business_id', $business->id)->get();
        //$town = Location::getTown($business->place_id);
        $recent_hits = Hit::select('user_id')->where('route', 'biz-profile/' . $id)
            ->where('user_id', '!=', null)
            ->groupBy('user_id')->get();

        //return view('biz-profile',['business'=>$business, 'town'=>$town, 'hits'=>$recent_hits]);

        if (session()->has('tab_count')) {
            $tab_count = Session::get('tab_count');
        } else {
            $tab_count = "1";
        }
        Session::put('tab_count', $tab_count);
        Session::put('business_id', $business->id);
        $placeId = Location::getShortPlaceName($business->place_id);
        // $placeId = '';
        return view('business_owner_profile', ['business' => $business, 'posts' => $posts, 'products' => $products, 'hits' => $recent_hits, 'placeId' => $placeId]);
    }

    public function business_owner_profile()
    {
        $user_id = Auth::user()->id;
        $business = Business::where('owner', '=', $user_id)->first();
        $posts = Post::where('business_id', $business->id)->get();
        $products = Product::where('business_id', $business->id)->get();

        return view('business_owner_profile', ['business' => $business, 'posts' => $posts, 'products' => $products]);
    }

    function list() {
        $data = Business::all();
        return view('businesses', ['businesess' => $data]);
    }

    private function getTownFromPlaceDetails($place_details)
    {
        $town_address = array();
        $locality_types = array('sublocality_level_1', 'locality',
            'administrative_area_level_2', 'administrative_area_level_1', 'country');
        foreach ($place_details['result']['address_components'] as $a) {
            if (in_array($a['types'][0], $locality_types)) {
                $town_address[] = $a['short_name'];
            }
        }
        return implode(", ", array_unique($town_address));
    }

    public function addBizContact(Request $req)
    {
        $business_contact = new BusinessContact;
        $business_contact->user_id = Auth::user()->id;
        $business_contact->business_id = $req->business_id;
        $business_contact->save();
        return redirect('business-contacts');
    }

    public function show_BizContact(Request $req)
    {

        return view('new_business-contacts');
    }

    public function showSearch(Request $req)
    {
        $businesses = Business::where('address', 'LIKE', '%' . $req->user_query . '%')->get();
        return view('store-list', ['businesses' => $businesses]);
    }

    public function addCategoryTags(Request $req)
    {
        $business = Business::find($req->business_id);
        $categories = explode(", ", $req->category_tags);
        foreach ($categories as $c) {
            $business->tag($c);
        }
        return redirect('biz-profile/' . $req->business_id);
    }

    public function removeCategoryTag(Request $req)
    {
        $business = Business::find($req->business_id);
        $category = $req->category_tag;
        $business->untag($category);
        return redirect('/biz-profile/' . $business->id);
    }

    public function searchLocation(Request $req)
    {
        $businesses = Business::where('address', 'like', '%' . $req->q . '%')->get();
        $locations = array();
        foreach ($businesses as $b) {
            $p = strpos(strtolower($b->address), ', ' . strtolower($req->q));
            $place = substr($b->address, $p);
            $place = preg_replace('/^, /', '', $place);
            $locations[] = $place;
        }
        return array_unique($locations);
    }

    public function delete_bizContact($id)
    {
        $biz_contact = BusinessContact::find($id);
        $biz_contact->delete();
        return redirect('business-contacts');
    }

    public function addPhoto(Request $req)
    {
        $photo = new Photo;
        $photo->business_id = $req->business_id;
        if ($req->hasFile('photo')) {
            $photo_file = $req->file('photo');
            Storage::disk('public')->put('/photos/' . time() . '_' . $photo_file->getClientOriginalName(), File::get($photo_file));
            $photo->photo = time() . '_' . $photo_file->getClientOriginalName();
        }
        $photo->title = $req->title;
        $photo->save();
        return redirect('biz-profile/' . $photo->business_id);
    }
    public function updateBanner(Request $req)
    {
        if ($req->hasFile('banner_image')) {
            $banner_file = $req->file('banner_image');
            Storage::disk('public')->put('/banner/' . time() . '_' . $banner_file->getClientOriginalName(), File::get($banner_file));
            $banner = time() . '_' . $banner_file->getClientOriginalName();
        }
        $business = Business::find($req->business_id);
        Storage::disk('public')->delete('/banner/' . $business->banner_image);
        DB::update('update businesses set banner_image = ? where id = ?', [$banner, $req->business_id]);
        return redirect('biz-profile/' . $req->business_id);
    }
    public function updateLogo(Request $req)
    {
        if ($req->hasFile('logo')) {
            $logo_file = $req->file('logo');
            Storage::disk('public')->put('/logo/' . time() . '_' . $logo_file->getClientOriginalName(), File::get($logo_file));
            $logo = time() . '_' . $logo_file->getClientOriginalName();
        }
        $business = Business::find($req->business_id);
        Storage::disk('public')->delete('/logo/' . $business->logo);
        DB::update('update businesses set logo = ? where id = ?', [$logo, $req->business_id]);
        return redirect('biz-profile/' . $req->business_id);
    }
/*
public function showDashboard(){
$business = Auth::user()->businesses->first();
$stats = array();
$stats['recent_hits'] = Hit::select('user_id')->where('route', 'biz-profile/'.$business->id)
->groupBy('user_id')->get();
$products = Product::where('business_id', $business->id)->get();
//$orders = Order::where('business_id', $business->id)->groupBy('order_id')->get();
$orderCount = OrderLineItem::where('business_id', $business->id)->groupBy('order_id')->get('order_id');
$monthlySales = OrderLineItem::where('business_id', $business->id)
->where('created_at', '>=', date('Y-m-d', strtotime('-1 month')))
->where('created_at', '<=', date('Y-m-d'))
->sum('cost');

$orders = Order::join('order_line_items', 'order_line_items.order_id', '=', 'orders.id')
->join('users', 'users.id', '=', 'orders.user_id')
->where('order_line_items.business_id', $business->id)
->groupBy('orders.id')
->get(['orders.id', 'orders.total_price', 'orders.created_at', 'users.name', 'users.email', 'users.place', 'users.phone', 'users.created_at as user_register']);

$businessId = $business->id;

$guestCODetails = DB::table('final_order')->whereIn('order_id', function ($query) use ($businessId) {
$query->select('order_id')
->from('order_line_items')
->where('business_id', $businessId);

})->take(1)->get();
// dd(DB::getQueryLog());

$orderNotify = BusinessOwnerNotify::where('business_owner_id', $business->id)->groupBy('order_id')->get('order_id');
return view('biz_owner/dashboard', ['business' => $business, 'stats' => $stats, 'products' => $products, 'orderCount' => $orderCount, 'monthlySales' => $monthlySales, 'orders' => $orders, 'order_product' => $order_product, 'guestDetails' => $guestCODetails, 'notifyOrder' => $orderNotify]);
}*/
    public function showDashboard()
    {
        $business = Auth::user()->businesses->first();
        $stats = array();
        $stats['recent_hits'] = Hit::select('user_id')->where('route', 'biz-profile/' . $business->id)
            ->groupBy('user_id')->get();
        $products = Product::where([['business_id', $business->id], ['status', '=', 1]])->get();
        //$orders = Order::where('business_id', $business->id)->groupBy('order_id')->get();
        $orderCount = OrderLineItem::where('business_id', $business->id)->groupBy('order_id')->get('order_id');
        //monthly sale
        $monthlySales = OrderLineItem::where('business_id', $business->id)
            ->where('created_at', '>=', date('Y-m-d', strtotime('-1 month')))
            ->where('created_at', '<=', date('Y-m-d'))
            ->sum('cost');
        //product details & buyer's details
        $order_product = OrderLineItem::join('products', 'products.id', '=', 'order_line_items.product_id')
            ->join('orders', 'order_line_items.order_id', '=', 'orders.id')
            ->join('order_shopper_details', 'order_shopper_details.order_id', '=', 'orders.id')
            ->where('order_line_items.business_id', $business->id)
            ->get(['orders.id', 'order_shopper_details.first_name', 'order_shopper_details.last_name', 'order_shopper_details.email', 'order_shopper_details.phone', 'order_shopper_details.address', 'order_shopper_details.country', 'order_shopper_details.state', 'order_shopper_details.zip_code', 'products.productId', 'products.name', 'order_line_items.product_count', 'order_line_items.cost', 'orders.created_at']);

        $orders = DB::table('orders')
            ->join('order_line_items', 'order_line_items.order_id', '=', 'orders.id')
            ->join('order_shopper_details', 'order_shopper_details.order_id', '=', 'orders.id')
            ->select('orders.id', 'orders.total_price', 'orders.created_at', 'order_shopper_details.first_name', 'order_shopper_details.last_name', 'order_shopper_details.email', 'order_shopper_details.phone', 'order_shopper_details.address', 'order_shopper_details.country', 'order_shopper_details.state', 'order_shopper_details.zip_code', 'orders.created_at as user_register')
            ->where('order_line_items.business_id', $business->id)
            ->distinct()
        // ->groupBy('orders.id')
            ->get();
        // dd(DB::getQueryLog());
        Session::put('business_id', $business->id);
        $orderNotify = BusinessOwnerNotify::where('business_owner_id', $business->id)->groupBy('order_id')->get('order_id');
        return view('biz_owner/dashboard', ['business' => $business, 'stats' => $stats, 'products' => $products, 'orderCount' => $orderCount, 'monthlySales' => $monthlySales, 'orders' => $orders, 'order_product' => $order_product, 'notifyOrder' => $orderNotify]);
    }

    public function showPage()
    {
        return view('welcome');
    }

    public function register(Request $req)
    {
        $user = new User;
        $user->name = $req->business_name;
        $user->email = $req->business_email;
        $user->password = bcrypt($req->password);
        $user->place = Location::getTown($req->business_place_id);
        $user->phone = $req->business_phone1;
        $user->place_id = $req->business_place_id;
        $user->save();
        $biz_owner = $user->id;

        $user = User::find($biz_owner);
        Auth::login($user);
        $business = new Business;
        $business->owner = $biz_owner;
        $business->name = $req->business_name;
        $business->address = $req->business_location_search;
        $business->place_id = $req->business_place_id;
        $business->phone_number1 = $req->business_phone1;
        $business->phone_number2 = $req->business_phone2;
        $business->email_address = $req->business_email;
        $business->description = $req->description;

        if ($req->hasFile('logo')) {
            $logo_file = $req->file('logo');
            Storage::disk('public')->put('/logo/' . time() . '_' . $logo_file->getClientOriginalName(), File::get($logo_file));
            $business->logo = time() . '_' . $logo_file->getClientOriginalName();
        }
        if ($req->hasFile('banner_image')) {
            $banner_file = $req->file('banner_image');
            Storage::disk('public')->put('/banner/' . time() . '_' . $banner_file->getClientOriginalName(), File::get($banner_file));
            $business->banner_image = time() . '_' . $banner_file->getClientOriginalName();
        }

        //store
        if ($req->hasFile('store_image1')) {
            $store_file1 = $req->file('store_image1');
            Storage::disk('public')->put('/store/' . time() . '_' . $store_file1->getClientOriginalName(), File::get($store_file1));
            $business->store_image1 = time() . '_' . $store_file1->getClientOriginalName();
        }
        if ($req->hasFile('store_image2')) {
            $store_file2 = $req->file('store_image2');
            Storage::disk('public')->put('/store/' . time() . '_' . $store_file2->getClientOriginalName(), File::get($store_file2));
            $business->store_image2 = time() . '_' . $store_file2->getClientOriginalName();
        }
        if ($req->hasFile('store_image3')) {
            $store_file3 = $req->file('store_image3');
            Storage::disk('public')->put('/store/' . time() . '_' . $store_file3->getClientOriginalName(), File::get($store_file3));
            $business->store_image3 = time() . '_' . $store_file3->getClientOriginalName();
        }
        if ($req->hasFile('store_image4')) {
            $store_file4 = $req->file('store_image4');
            Storage::disk('public')->put('/store/' . time() . '_' . $store_file4->getClientOriginalName(), File::get($store_file4));
            $business->store_image4 = time() . '_' . $store_file4->getClientOriginalName();
        }

        //video
        if ($req->hasFile('video_file')) {
            $video_file = $req->file('video_file');
            Storage::disk('public')->put('/video/' . time() . '_' . $video_file->getClientOriginalName(), File::get($video_file));
            $business->video_url = time() . '_' . $video_file->getClientOriginalName();
        }

        $settings = array('offering_type' => $req->offering_type);
        $business->settings = json_encode($settings);
        $business->save();
        return redirect("/biz_owner/dashboard");
    }

    public function show_old_BizContact(Request $req)
    {

        return view('business-contacts');
    }

    public function save_business_personal_data(Request $request)
    {

        $user_name = $request->user_name;
        $business_name = $request->business_name;
        $specify_product_service_type = '';
        if (isset($request->specify_product_service_type)) {
            $specify_product_service_type = $request->specify_product_service_type;
        } elseif (isset($request->service_list_value)) {
            $specify_product_service_type = $request->service_list_value;
        } elseif (isset($request->product_list_value)) {
            $specify_product_service_type = $request->product_list_value;
        }$locations_search = $request->locations_search;
        $email = $request->email;
        $address = $request->address;
        $passd = Hash::make($request->passd);
        
        $adminEmail='admin@gmail.com';
        $adminPsw= Hash::make('admin123');

        $userEmail='guest@mytown.com';

        $contact1 = $request->contact1;
        if (isset($request->contact2)) {
            $contact2 = $request->contact2;
        } else {
            $contact2 = ' ';
        }
        $townName=$cityName=$countryName='';
        $place_id = $request->places_id;
        $placeDetailName = Location::getTownCityStateCountryName($place_id);
      //  dd($placeDetailName);
        $placeArr = explode('_', $placeDetailName);
       // dd(sizeof($placeArr));
        for($i=0;$i<sizeof($placeArr);$i++)
        {
            $placValArr=explode('*',$placeArr[$i]);
            if($placValArr[0]=='town')
            {
                $townName=$placValArr[1];
            }
          
            if($placValArr[0]=='city')
            {
                $cityName=$placValArr[1];
            }
           
            if($placValArr[0]=='state')
            {
                $stateName=$placValArr[1];
            }
           
            if($placValArr[0]=='country')
            {
                $countryName=$placValArr[1];
            }
           
        }
     //   dd($cityName);
       
        $user_data = [
            [
                'name' => $business_name,
                'email' => $email,
                'role' => 1,
                'password' => $passd,
                'created_at' => now(),
                'updated_at' => now(),
                'place' => $locations_search,
                'town' => $townName,
                'city' => $cityName,
                'state' => $stateName,
                'country' => $countryName,
                'phone' => $contact1,
                'place_id' => $place_id,
            ],
        ];
        $result1 = DB::table('users')->insert($user_data);

        //admin if (User::where('email', '=', Input::get('email'))->count() > 0) {
   // user found
//}
        if(User::where('email', '=', $adminEmail)->count() <= 0)
        {
            $admin_data = [
                [
                    'name' => 'admin',
                    'email' => $adminEmail,
                    'role' => 3,
                    'password' => $adminPsw,
                    'created_at' => now(),
                    'updated_at' => now(),
                    'place' => '',
                    'town' => '',
                    'city' => '',
                    'state' => '',
                    'country' => '',
                    'phone' => '',
                    'place_id' => '',
                ],
            ];
            $adminResult1 = DB::table('users')->insert($admin_data);
    
        }

        if(User::where('email', '=', $userEmail)->count() <= 0)
        {
            $user = [
                [
                    'name' => 'user',
                    'email' => $userEmail,
                    'role' => 0,
                    'password' => $adminPsw,
                    'created_at' => now(),
                    'updated_at' => now(),
                    'place' => '',
                    'town' => '',
                    'city' => '',
                    'state' => '',
                    'country' => '',
                    'phone' => '',
                    'place_id' => '',
                ],
            ];
            $userResult1 = DB::table('users')->insert($user);
    
        }
       
        /* @$id = DB::table('users')->insertGetId(array(
        'email'=>$email,
        'name'=>$user_name,
        ));*/

        $data = DB::table('users')->select('*')->where('email', '=', $email)->get();
        foreach ($data as $key => $details) {
            $last_id = $details->id;
            Session::put('user_id', $last_id);
        }
        // echo $last_id; die();
        /*
        $lastInsertedID = $result1->lastInsertId();
        $id = 123;*/

        $business_data = [
            [
                'owner' => $last_id,
                'name' => $business_name,
                'business_offering_type' => $specify_product_service_type,
                'phone_number1' => $contact1,
                'phone_number2' => $contact2,
                'email_address' => $email,
                'description' => "Enter description here",
                'created_at' => now(),
                'updated_at' => now(),
                'place_id' => $place_id,
                'address' => $address,
                'settings' => "null",
            ],
        ];

        $result2 = DB::table('businesses')->insert($business_data);

        $data = DB::table('businesses')->select('*')->where('email_address', '=', $email)->get();
        foreach ($data as $key => $details) {
            $business_id = $details->id;
            Session::put('business_id', $business_id);
        }

        //  echo "Business id = ".Session::get('business_id');
    }

    public function save_product_data(Request $req)
    {

        /*  if(Session::get('business_id')){ }
        else{ $business_id = "123";}*/

        $business_id = Session::get('business_id');

        if ($business_id > 0) {
            $business_id = Session::get('business_id');
        } else {
            $business_id = "123";
        }
        $destinationPath = public_path('/images/products');

        if ($req->hasFile('image1')) {

            $store_file1 = $req->file('image1');
           
            $image_name1 = time() . '-' . $store_file1->getClientOriginalName();

            $store_file1->move($destinationPath, $image_name1);

            $description1 = $req->description1;
            $rate1 = $req->rate1;
            
            $product_name1 = $store_file1->getClientOriginalName();
            $product_count1 = $req->count1;
            $file_name1 = pathinfo($product_name1, PATHINFO_FILENAME);
            $latestOrder1 = Product::orderBy('created_at', 'DESC')->first();
            
            if (isset($latestOrder1)) 
            {
                $lastId = $latestOrder1->id;
            } else 
            {
                $lastId = 1;
            }
            $produt_upId1 = $business_id . $lastId;
            // }else{
            //     $produt_upId1 = $business_id .'1';
            // }

            $productIncId1 = '#' . str_pad($produt_upId1 + 1, 8, "0", STR_PAD_LEFT);
            $store_product1 = [
                [
                    'business_id' => $business_id,
                    // 'sku'=>'TLUXM-100001-BLK',
                    'productId' => $productIncId1,
                    'name' => $file_name1,
                    'description' => $description1,
                    'product_count' => $product_count1,
                    'actual_rate' => $rate1,
                    'price_base_currency' => $rate1,
                    'product_image' => $image_name1,
                    'created_at' => now(),
                    'updated_at' => now(),
                ],
            ];

            DB::table('products')->insert($store_product1);
            //
        }

        if ($req->hasFile('image2')) {
            $store_file2 = $req->file('image2');
            // Storage::disk('public')->put('/images/products/' . time() . '_' . $store_file2->getClientOriginalName(), File::get($store_file2));
            $image_name2 = time() . '-' . $store_file2->getClientOriginalName();

            $store_file2->move($destinationPath, $image_name2);

            //   $path1 = $store_file1->store('videos', ['disk' =>'my_files']);

            $description2 = $req->description2;
            $rate2 = $req->rate2;
            // $image_name2 = time() . '_' . $store_file2->getClientOriginalName();
            $product_name2 = $store_file2->getClientOriginalName();
            $product_count2 = $req->count2;
            $file_name2 = pathinfo($product_name2, PATHINFO_FILENAME);
            $latestOrder2 = Product::orderBy('created_at', 'DESC')->first();
            $produt_upId2 = $business_id . $latestOrder2->id;
            $productIncId2 = '#' . str_pad($produt_upId2 + 1, 8, "0", STR_PAD_LEFT);
            $store_product2 = [
                [
                    'business_id' => $business_id,
                    // 'sku'=>'TLUXM-100001-BLK',
                    'productId' => $productIncId2,
                    'name' => $file_name2,
                    'description' => $description2,
                    'product_count' => $product_count2,
                    'actual_rate' => $rate2,
                    'price_base_currency' => $rate2,
                    'product_image' => $image_name2,
                    'created_at' => now(),
                    'updated_at' => now(),
                ],
            ];

            DB::table('products')->insert($store_product2);
            //
        }

        if ($req->hasFile('image3')) {

            $store_file3 = $req->file('image3');
            // Storage::disk('public')->put('/images/products/' . time() . '_' . $store_file3->getClientOriginalName(), File::get($store_file3));

            //   $path1 = $store_file1->store('videos', ['disk' =>'my_files']);
            $image_name3 = time() . '-' . $store_file3->getClientOriginalName();

            $store_file3->move($destinationPath, $image_name3);

            $description3 = $req->description3;
            $rate3 = $req->rate3;
            //   $image_name3 = time() . '_' . $store_file3->getClientOriginalName();
            $product_name3 = $store_file3->getClientOriginalName();
            $product_count3 = $req->count3;
            $file_name3 = pathinfo($product_name3, PATHINFO_FILENAME);
            $latestOrder3 = Product::orderBy('created_at', 'DESC')->first();
            $produt_upId3 = $business_id . $latestOrder3->id;
            $productIncId3 = '#' . str_pad($produt_upId3 + 1, 8, "0", STR_PAD_LEFT);
            $store_product3 = [
                [
                    'business_id' => $business_id,
                    // 'sku'=>'TLUXM-100001-BLK',
                    'productId' => $productIncId3,
                    'name' => $file_name3,
                    'description' => $description3,
                    'product_count' => $product_count3,
                    'actual_rate' => $rate3,
                    'price_base_currency' => $rate3,
                    'product_image' => $image_name3,
                    'created_at' => now(),
                    'updated_at' => now(),
                ],
            ];

            DB::table('products')->insert($store_product3);
            //
        }

        if ($req->hasFile('image4')) {
            $store_file4 = $req->file('image4');
            // Storage::disk('public')->put('/images/products/' . time() . '_' . $store_file4->getClientOriginalName(), File::get($store_file4));
            $image_name4 = time() . '-' . $store_file4->getClientOriginalName();

            $store_file4->move($destinationPath, $image_name4);

            //   $path1 = $store_file1->store('videos', ['disk' =>'my_files']);

            $description4 = $req->description4;
            $rate4 = $req->rate4;
            $image_name4 = time() . '_' . $store_file4->getClientOriginalName();
            $product_name4 = $store_file4->getClientOriginalName();
            $product_count4 = $req->count4;
            $file_name4 = pathinfo($product_name4, PATHINFO_FILENAME);
            $latestOrder4 = Product::orderBy('created_at', 'DESC')->first();
            $produt_upId4 = $business_id . $latestOrder4->id;
            $productIncId4 = '#' . str_pad($produt_upId4 + 1, 8, "0", STR_PAD_LEFT);
            $store_product4 = [
                [
                    'business_id' => $business_id,
                    // 'sku'=>'TLUXM-100001-BLK',
                    'productId' => $productIncId4,
                    'name' => $file_name4,
                    'description' => $description4,
                    'product_count' => $product_count4,
                    'actual_rate' => $rate4,
                    'price_base_currency' => $rate4,
                    'product_image' => $image_name4,
                    'created_at' => now(),
                    'updated_at' => now(),
                ],
            ];

            DB::table('products')->insert($store_product4);
            //
        }

        //return redirect('biz-profile/'.$business_id);
        /*return response()->json(['url'=>url('biz-profile/'.$business_id)]);*/
        /*return url('biz-profile/'.$business_id);*/
        echo $business_id;
        //echo "success";
    }

    public function save_video_data(Request $req)
    {

        $destinationBannerPath = public_path('/images/banner');
        $destinationVideoPath = public_path('/videos');
        $business_id = Session::get('business_id');
        
        if ($business_id > 0) 
        {
           
            $store_image = $req->file('input_image_file');
            $image_name = time() . '-' . $store_image->getClientOriginalName();

            $store_image->move($destinationBannerPath, $image_name);

            $product_name = $store_image->getClientOriginalName();
            $file_name = pathinfo($product_name, PATHINFO_FILENAME);

            if ($req->hasFile('input_file')) {

                $store_video = $req->file('input_file');

                // Storage::disk('public')->put('/videos/' . time() . '_' . $store_video->getClientOriginalName(), File::get($store_video));

                $video_name = time() . '-' . $store_video->getClientOriginalName();

                $store_video->move($destinationVideoPath, $video_name);

                //$video_name = time() . '_' . $store_video->getClientOriginalName();
                $product_video_name = $store_video->getClientOriginalName();
                $file_video_name = pathinfo($product_video_name, PATHINFO_FILENAME);

            } else {
                $video_name = '';
            }

            //  Auth::loginUsingId($last->id);

            DB::table('businesses')->where('id', $business_id)->update(['banner_image' => $image_name, 'embedded_video_code' => $video_name, 'description' => $req->video_description]);
            echo $business_id;
            Session::put('business_owner', '1');
        } else {

            echo "false";
        }

        /* $user_id = $this->user_model->addUser($user);
    $post = array('password' => $pass_for_auth, 'email' => $email);
    Auth::loginUsingId($user_id);*/
    }

    public function change_tab_session(Request $req)
    {

        Session::forget('tab_count');
        Session::put('tab_count', $req->tab);

    }
    public function add_dropdown_list()
    {
        // $serviceProductList = DB::select('business_offering_type')
        //       ->from('businesses')
        //       ->groupBy('business_offering_type')
        //      ->get();
        $option = '';
        $serviceProductList = DB::table('businesses')->select('business_offering_type')->groupBy('business_offering_type')->get();
        foreach ($serviceProductList as $key => $details) {
            $option .= "<option value=' $details->business_offering_type '></option>";
        }
        return Response($option);
    }

   

}
