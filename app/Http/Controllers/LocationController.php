<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Igaster\LaravelCities\Geo;
use SKAgarwal\GoogleApi\PlacesApi;

class LocationController extends Controller
{
	public function searchLocation(Request $request){
		//return Geo::searchNames($request->q);
		
		$googlePlaces = new PlacesApi('AIzaSyC3GjzFyn7dpm6VywF9buvkOuiHaCFm5wE');
	//	dd($googlePlaces);
		return $googlePlaces->placeAutocomplete($request->q);
	}

	public function getPlaceDetails($place_id){
		$googlePlaces = new PlacesApi('AIzaSyC3GjzFyn7dpm6VywF9buvkOuiHaCFm5wE');
		return $googlePlaces->placeDetails($place_id);
	}

	public function getChildren(Request $request){
		//$geo = Geo::find($request->location_id);
		$children = array();
		try{
			$children = Geo::where('parent_id', $request->location_id)
			->orderBy('name')
			->get();
		}
		catch(\Exception $e){
		}
		return $children;	
	}

	public function getStates(Request $request){
		return Geo::where('country', $request->country_code)
		->where('level', 'ADM1')->orderBy('name')->get();
	}
}
