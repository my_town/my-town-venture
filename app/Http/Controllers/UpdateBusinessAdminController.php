<?php

namespace App\Http\Controllers;
use App\Models\Business;
use Illuminate\Http\Request;
use DB;
class UpdateBusinessAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $ownerId = DB::table('businesses')->where('id', $id)->get('owner');
       foreach($ownerId as $owner_id)
       {
        $oId=$owner_id->owner;
        $deleteUser = DB::table('users')->where('id', $oId)->update(['email' => 'abc']);
        $updateBusiness = DB::table('businesses')->where('id', $id)->update(['status' => 3]);
       }
        
       // DB::enableQueryLog();
       // 
        
        //
      // dd(DB::getQueryLog());
        return back();
    }
}
