<?php
namespace App\Http\Controllers;
use App\Models\Contact;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use AmrShawky\LaravelCurrency\Facade\Currency;
use Illuminate\Support\Facades\Hash;
use DB;
use View;
use App\Helpers\Location;

class UserController extends Controller
{
    public function index(){
        $data = User::all();
        return view('/usermanagement',['users'=>$data]);
        }


    public function search(Request $request){

                if($request->ajax())
                {

                $output="";
                $checkbox_value = $request->checkbox_val;
                if(!empty($request->search)){

                    if($checkbox_value == "1"){


                        $users=DB::table('users')->where('id','!=',Auth::user()->id)->where('name','LIKE','%'.$request->search."%")->orWhere('place','LIKE','%'.$request->search."%")->orderByRaw("RAND()")->limit(10)->get();


                    }else if($checkbox_value == "0"){


                $users = DB::table('contacts')
                        ->join('users','users.id', '=','contacts.linked_user_id')
                        ->select('users.id as id','contacts.id as contact_id','users.name as name','users.place as place')
                        ->where('users.name','LIKE','%'.$request->search."%")
                        ->orWhere('users.place','LIKE','%'.$request->search."%")
                        ->orderByRaw("RAND()")->limit(10)->get();


                    }

                    

                if($users)
                {

                foreach ($users as $key => $user) {

                    $notification_status = DB::table('notification')->where('receiver_id','=',$user->id)->where('sender_id','=',Auth::user()->id);
                    $notification_count = $notification_status->count();
                    $cityName = strtok($user->place, ",");

                    $text = ($notification_count < 1) ? 
                            '<span id="request_title'.$user->id.'" class="rt-bt6"><button id="someUinqueId" value="'.$user->id.'" class="btn btn-blue text-center">Connect</button></span>':
                            '<span id="request_title'.$user->id.'">Request Sent</span>';


                    $output.= '<div class="col-lg-3 col-md-6">
                                <div class="member">
                                  <div class="member-img">
                                    <img src="images/team-1.jpg" class="img-fluid contacts-img-sm" alt="">
                                    <div class="social">
                                      <a href=""><i class="icofont-twitter"></i></a>
                                      <a href=""><i class="icofont-facebook"></i></a>
                                      <a href=""><i class="icofont-instagram"></i></a>
                                      <a href=""><i class="icofont-linkedin"></i></a>
                                    </div>
                                  </div>
                                  <div class="member-info">
                                    <h4>'.$user->name.'</h4>
                                    <span><b>'.$cityName.'</b></span>'.$text.'</div>
                                </div>
                              </div>';
                
                }

                //$output.='</tbody>';

                return Response($output);
                   }

                }else{

                   // $output = '<div style="text-align: center;">Please enter the details in above search box to get proper results.</div>';

                    $business_query = DB::table('users')
                        ->where('place','LIKE','%'.Auth::user()->place.'%')
                        ->where('id','!=',Auth::user()->id)
                        ->limit(10)->get();

            $res = array();
            $output="";

            foreach ($business_query as $key => $details) {

            $last_id = $details->id;

            $notification_status = DB::table('notification')->where('receiver_id','=',$details->id);
             $notification_count = $notification_status->count();
             $cityName = strtok($user->place, ",");


             $text = ($notification_count < 1) ? 
                            '<span id="request_title'.$user->id.'" class="rt-bt6"><button id="someUinqueId" value="'.$user->id.'" class="btn btn-blue text-center">Connect</button></span>':
                            '<span id="request_title'.$user->id.'"><b>Request Sent</b></span>';


             if($notification_count<1){


                $output.= '<div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="member-img">
                <img src="images/team-1.jpg" class="img-fluid contacts-img-sm" alt="">
                <div class="social">
                  <a href=""><i class="icofont-twitter"></i></a>
                  <a href=""><i class="icofont-facebook"></i></a>
                  <a href=""><i class="icofont-instagram"></i></a>
                  <a href=""><i class="icofont-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>'.$details->name.'</h4>
               <span><b>'.$cityName.'</b></span>'.$text.'</div>
              </div>
            </div>
          </div>';

             
             }
        }

                    return Response($output);
                }
                
            }

    }


    public function sendrequest(Request $request){


    $notificationCount = DB::table('notification')->where('sender_id', '=',Auth::user()->id)->where('receiver_id',$request->id)->get();
    $count = $notificationCount->count();

    if($count<1){

             if($request->ajax())
                {
                $receiver_id = $request->id;
                $message = "You are successfully get notification.";
                $notification_type = "contact_request";
                $data = [
                   [
                    'receiver_id'=>$receiver_id,
                    'sender_id'=>Auth::user()->id,
                    'message'=> $message,
                    'notification_type'=>$notification_type,
                    'send_at'=>now(),
                    'created_at'=>now(),
                    'updated_at'=>now() 
                   ] 
                ];

        $result = DB::table('notification')->insert($data);
           } 

        }        
    }

    public static function getNotificationCount($user_id){

        $notificationCount = DB::table('notification')->where('receiver_id', '=', $user_id)->get();

        $wordCount = $notificationCount->count();
        return $wordCount;
    }

    public static function getOnlineContacts($user_id){

        return false;

    }

    public static function pendingRequests($user_id){


        $pendingRequests = DB::table('users')
            ->leftJoin('notification', 'users.id', '=', 'notification.receiver_id')
            ->where('notification.sender_id','=',$user_id)
            ->where('notification.notification_type','=','contact_request')
            ->get();

                if($pendingRequests->count()>0)
                {
                    return $pendingRequests;
                    }
                    return false;

    }


    public static function geContactCount(){

    $contactCount = DB::table('contacts')->where('user_id', '=', Auth::user()->id)->get();
        $count = $contactCount->count();
        return $count;

        if($count>0){
            return false;
        }
        return true;

    }

    public static function getBusinessRecords(Request $request){


    $business_details = DB::table('businesses')->get();
    $actual_count = $business_details->count();

    $last_id = $request->id;
    $showLimit = 2; 

    /*$business_query = DB::table('businesses')->where('id','>',$last_id)->limit(2)->get();*/

    $business_query = DB::table('businesses')
        ->join('users', 'users.id', '=', 'businesses.owner')
        ->select('businesses.id as business_id','businesses.name as business_name','users.name as user_name','businesses.address as business_address','businesses.categories as business_categories','users.id as user_id')
        ->where('businesses.id','>',$last_id)->limit(2)->get();

         $res = array();
         $output="";

     
    foreach ($business_query as $key => $details) {

        $notification_status = DB::table('notification')->where('receiver_id','=',$details->user_id);

        $notification_count = $notification_status->count();
        $last_id = $details->business_id;

        if($notification_count<1){

        /*$output.= '<div class="whitebg padding2" style="margin-bottom:10px">'.
                   '<h5>'.$details->business_name.','.$notification_count.'</h5>'.
                    '<div class="orange">'.
                        '<div><b>
                        <i style="font-size:14px" class="fas">&#xf2bb;</i></b> '.$details->business_address.'</div>'.
                        '<div align="right">'.
                '<span id="request_title'.$details->user_id.'"><button id="someUinqueId" value="'.$details->user_id.'" class="btn btn-light">Connect</button></span>'.
                '</div>'.
                    '</div>'.
                '</div>';*/

                 $output.= '<div class="col-lg-3 col-md-6">
                            <div class="member">
                              <div class="member-img">
                                <img src="images/team-1.jpg" class="img-fluid contacts-img-sm" alt="">
                                <div class="social">
                                  <a href=""><i class="icofont-twitter"></i></a>
                                  <a href=""><i class="icofont-facebook"></i></a>
                                  <a href=""><i class="icofont-instagram"></i></a>
                                  <a href=""><i class="icofont-linkedin"></i></a>
                                </div>
                              </div>
                              <div class="member-info">
                                <h4>'.$details->business_name.'</h4>
                                <span>'.$details->business_address.'</span>
                                <span id="request_title'.$details->user_id.'" class="rt-bt6"><button id="someUinqueId" value="'.$details->user_id.'" class="btn btn-blue text-center">Connect</button></span>
                              </div>
                            </div>
                          </div>';

                    }

                }

        $output.= '<div class="load-more" lastID="'.$last_id.'" style="display: none;"><img src="/images/loading.gif"/>';

            return Response($output);
    }

    public static function getFewBusinessRecords(){

         //$business_query = DB::table('businesses')->limit(4)->get();


         $business_query = DB::table('businesses')
        ->join('users', 'users.id', '=', 'businesses.owner')
        ->select('businesses.id as business_id','businesses.name as business_name','users.name as user_name','businesses.address as business_address','businesses.categories as business_categories','users.id as user_id')
        ->limit(4)->get();


         $res = array();
         $output="";

         $actual_count = $business_query->count();

         if($actual_count>0){

        
         foreach ($business_query as $key => $details) {

            $notification_status = DB::table('notification')->where('receiver_id','=',$details->user_id);
             $notification_count = $notification_status->count();

            $last_id = $details->business_id;


    if($notification_count<1){

        /*$output.= '<div class="whitebg padding2" style="margin-bottom:10px">'.
                   '<h5>'.$details->business_name.','.$notification_count.'</h5>'.
                    '<div class="orange">'.
                      
                        '<div><b>
                        <i style="font-size:14px" class="fas">&#xf2bb;</i></b> '.$details->business_address.'</div>'.
                    '<div align="right">'.
                '<span id="request_title'.$details->user_id.'"><button id="someUinqueId" value="'.$details->user_id.'" class="btn btn-light">Connect</button></span>'.
                '</div>'.
                    '</div>'.
                '</div>';*/

                $output.= '<div class="col-lg-3 col-md-6">
                            <div class="member">
                              <div class="member-img">
                                <img src="images/team-1.jpg" class="img-fluid contacts-img-sm" alt="">
                                <div class="social">
                                  <a href=""><i class="icofont-twitter"></i></a>
                                  <a href=""><i class="icofont-facebook"></i></a>
                                  <a href=""><i class="icofont-instagram"></i></a>
                                  <a href=""><i class="icofont-linkedin"></i></a>
                                </div>
                              </div>
                              <div class="member-info">
                                <h4>'.$details->business_name.'</h4>
                                <span>'.$details->business_address.'</span>
                                <span id="request_title'.$details->user_id.'" class="rt-bt6"><button id="someUinqueId" value="'.$details->user_id.'" class="btn btn-blue text-center">Connect</button></span>
                              </div>
                            </div>
                          </div>'; 

            }



                }

           $output.= '<div class="load-more" lastID="'.$last_id.'" style="display: none;" align = "center"><img src="/images/loading.gif"/>'; 

           }else{

            $output.= '<div class="col-lg-12 col-md-12">
                         <div class="member">     
                        <div class="member-info">
                        <span>Records are not available.</span>
                    </div>
                    </div>
                </div>'; 

           }     

            return Response($output);
    }

     public static function getSuggestedContacts(Request $request){

      
        $last_id = $request->id;
        $checkbox_value = $request->checkbox_val;
        $showLimit = 4;

        if($checkbox_value=="1"){

        $business_query = DB::table('users')
            ->where('place','LIKE','%'.Auth::user()->place.'%')
            ->where('id','!=',Auth::user()->id)
            ->where('id','>',$last_id)
            ->limit(4)->get();

       
        }else if($checkbox_value == "0"){

        $business_query = DB::table('contacts')
            ->join('users', 'users.id', '=', 'contacts.linked_user_id')
            ->select('users.id as id','contacts.id as contact_id','users.name as name','users.place as place')
            ->where('contacts.user_id','=',Auth::user()->id)
            ->where('id','>',$last_id)
            ->limit(4)->get();

        }

            $res = array();
            $output="";

            foreach ($business_query as $key => $details) {

            $lasts_id = $details->id;
            $cityName = strtok($details->place, ",");

            $notification_status = DB::table('notification')->where('receiver_id','=',$details->id)->where('sender_id','=',Auth::user()->id);
             $notification_count = $notification_status->count();



             if($checkbox_value=="1"){

                $text = ($notification_count < 1 && $checkbox_value > 0) ? 
                            '<span id="request_title'.$details->id.'" class="rt-bt6"><button id="someUinqueId" value="'.$details->id.'" class="btn btn-blue text-center">Connect</button></span>':
                            '<span id="request_title'.$details->id.'">Request Sent</span>';

            }else if($checkbox_value=="0"){

                $text = ($notification_count < 1 && $checkbox_value > 0) ? '':
                            '<span id="request_title'.$details->id.'"></span>';

            }


             /*$text = ($notification_count < 1) ? 
                            '<span id="request_title'.$details->id.'"><button id="someUinqueId" value="'.$details->id.'" class="btn btn-primary">Connect</button></span>':
                            '<span id="request_title'.$details->id.'"><b>Request Sent</b></span>';*/

             if($notification_count<1){


                $output.= '<div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="member-img">
                <img src="images/team-1.jpg" class="img-fluid contacts-img-sm" alt="" title = "'.$details->name.'">
                <div class="social">
                  <a href=""><i class="icofont-twitter"></i></a>
                  <a href=""><i class="icofont-facebook"></i></a>
                  <a href=""><i class="icofont-instagram"></i></a>
                  <a href=""><i class="icofont-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>'.substr($details->name, 0, 20).'..</h4>
                <span><b>'.$cityName.'</b></span>'.$text.'
              </div>
            </div>
          </div>';


             }
        }

           $output.= '<div class="load-more-more" lastID="'.$lasts_id.'" style="display: none;" align = "center"><img src="/images/loading.gif"/>'; 

            return Response($output);

    }

    public static function getFewSuggestedContacts(Request $request){
/*
         $suggestedRequests = DB::table('users')
            ->where('place','LIKE','%'.Auth::user()->place.'%')
            ->where('id','!=',Auth::user()->id)
            ->limit(12)->get();*/

        $checkbox_value = $request->checkbox_val;
        $showLimit = 12;

        if($checkbox_value=="1"){

        $suggestedRequests = DB::table('users')
            ->where('place','LIKE','%'.Auth::user()->place.'%')
            ->where('id','!=',Auth::user()->id)
            ->limit(12)->get();


        }else if($checkbox_value == "0"){

        $suggestedRequests = DB::table('contacts')
            ->join('users', 'users.id', '=', 'contacts.linked_user_id')
            ->select('users.id as id','contacts.id as contact_id','users.name as name','users.place as place')
            ->where('contacts.user_id','=',Auth::user()->id)
            ->limit($showLimit)->get();

        }
        
         $actual_count = $suggestedRequests->count();
         $res = array();
         $output="";

        //  $output.= '<ul class="list-group">';
         if($actual_count>0){
         foreach ($suggestedRequests as $key => $details) {
            $last_id = $details->id;
            
            $notification_status = DB::table('notification')->where('receiver_id','=',$details->id)->where('sender_id','=',Auth::user()->id);
             $notification_count = $notification_status->count();
             $cityName = strtok($details->place, ",");



            if($checkbox_value=="1"){

                $text = ($notification_count < 1 && $checkbox_value > 0) ? 
                            '<span id="request_title'.$details->id.'" class="rt-bt6"><button id="someUinqueId" value="'.$details->id.'" class="btn btn-blue text-center">Connect</button></span>':
                            '<span id="request_title'.$details->id.'">Request Sent</span>';

            }else if($checkbox_value=="0"){

                $text = ($notification_count < 1 && $checkbox_value > 0) ? '':
                            '<span id="request_title'.$details->id.'"></span>';

            }

               
            

             if($notification_count<1){
                $output.= '<div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="member-img">
                <img src="images/team-1.jpg" class="img-fluid contacts-img-sm"  alt="" 
                title = "'.$details->name.'">
                <div class="social">
                  <a href=""><i class="icofont-twitter"></i></a>
                  <a href=""><i class="icofont-facebook"></i></a>
                  <a href=""><i class="icofont-instagram"></i></a>
                  <a href=""><i class="icofont-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>'.substr($details->name, 0, 20).'</h4>
                <span><b>'.$cityName.'</b></span>'.$text.'
              </div>
            </div>
          </div>';

           
           }

        }

            /*$output.= '</ul>';*/
            $output.= '<div class="load-more-more" lastID="'.$last_id.'" style="display: none;" align = "center"><img src="/images/loading.gif"/>'; 

        }else{

            $output.= '<div class="col-lg-12 col-md-12">
                         <div class="member">
                        
              <div class="member-info">
                <span>Records are not available.</span>
              </div>
            </div>
          </div>'; 
        }

            return Response($output);
    }

    public function getNotificationDetails(){

    $data = DB::table('notification')
        ->join('users', 'users.id', '=', 'notification.sender_id')
        ->select('notification.id as notification_id','notification.sender_id as sender_id','notification.message as notification_msg','notification.send_at as send_at','notification.notification_status as notification_status','users.name as user_name','users.place as user_place','users.id as user_id')
        ->where('notification.receiver_id','=',Auth::user()->id)->get();

        /*return view('/notification-details',['notifications'=>$data]);*/
        return view('/new_notification-details',['notifications'=>$data]);


    }

    public static function AcceptContactRequest(Request $request){

         if($request->ajax())
                {

                $ids = $request->id;

                $myArray = explode(',', $ids);

                $notification_id = $myArray[0];
                $sender_id = $myArray[1];
                $status = $myArray[2];
                $receiver_id = Auth::user()->id;

            $is_business = DB::table("businesses")->where('owner',$receiver_id)->get();
            $count = $is_business->count();

            if($count>0){

                /*.................................................................*/

                foreach ($is_business as $key => $value) {
                    $business_id = $value->id;
                }

                if($status=="1"){
                    $data = [
                   [
                    'user_id'=>$sender_id,
                    'linked_user_id'=>Auth::user()->id,
                    'business_id'=>$business_id,
                    'created_at'=>now(),
                    'updated_at'=>now() 
                   ] 
                ];

                $result = DB::table('business_contacts')->insert($data);
                $delete_records = DB::table('notification')->where('id', $notification_id)->delete();
                }else if($status=="0"){
                $delete_records = DB::table('notification')->where('id', $notification_id)->delete();
                }
            /*..................................................................*/

            }else{


                /*.................................................................*/
                if($status=="1"){
                    $data = [
                   [
                    'user_id'=>$sender_id,
                    'linked_user_id'=>Auth::user()->id,
                    'created_at'=>now(),
                    'updated_at'=>now() 
                   ] 
                ];

                $result = DB::table('contacts')->insert($data);
                $delete_records = DB::table('notification')->where('id', $notification_id)->delete();
                }else if($status=="0"){
                $delete_records = DB::table('notification')->where('id', $notification_id)->delete();
                }
            /*..................................................................*/

            }

        
        if($delete_records){
            return 1;} return 0;

           }                        
        }

        public function getCurrency(){

            $result = Currency::convert()
                                ->from('USA')
                                ->to('INR')
                                ->get();

            print_r($result);
        }

        public function getAcceptedContacts(){

    $data = DB::table('contacts')
        ->join('users', 'users.id', '=', 'contacts.linked_user_id')
        ->select('users.id as user_id','contacts.id as contact_id','users.name as user_name','users.place as user_place')->where('contacts.user_id','=',Auth::user()->id)->get();


        $actual_count = $data->count();
            
            $res = array();
            $output="";

            if($actual_count>0){

            foreach ($data as $key => $details) {
            
            $last_id = $details->contact_id;
            $cityName = strtok($details->user_place, ",");


            $notification_status = DB::table('notification')->where('receiver_id','=',$details->user_id)->where('sender_id','=',Auth::user()->id);
             $notification_count = $notification_status->count();
             

             if($notification_count<1){


                $output.= '<div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="member-img">
                <img src="images/team-1.jpg" class="img-fluid contacts-img-sm" alt="">
                <div class="social">
                  <a href=""><i class="icofont-twitter"></i></a>
                  <a href=""><i class="icofont-facebook"></i></a>
                  <a href=""><i class="icofont-instagram"></i></a>
                  <a href=""><i class="icofont-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>'.$details->user_name.'</h4>
                <span><b>'.$cityName.'</b></span>
              </div>
            </div>
          </div>';
             }
        }

           $output.= '<div class="load-more-more" lastID="'.$last_id.'" style="display: none;" align = "center"><img src="/images/loading.gif"/>'; 

    }else{

        $output.= '<div class="col-lg-12 col-md-12">
                         <div class="member">                
              <div class="member-info">
                <span>Records are not available.</span>
              </div>
            </div>
          </div>';
                 }
           return Response($output);

        }

        public function user_register(Request $request)
        {
          $location_search = $request->locations_search;
            $location_id = $request->places_id;
            $name = $request->name;
            $phone = $request->phone;
            $email = $request->email;
            $password = Hash::make($request->password);
            $confirm_password = $request->confirm_password;

            $data = [
                   [
                    'name'=>$name,
                    'email'=>$email,
                    'role'=>3,
                    'password'=> $password,
                    'created_at'=>now(),
                    'updated_at'=>now(),
                    'place'=>$location_search,
                    'phone'=>$phone,
                    'place_id'=>$location_id
                   ] 
                ];
               

            $result = DB::table('users')->insert($data);
            //return View::make("/auth/new_login"); 
            $last = DB::table('users')->latest()->first();
            Auth::loginUsingId($last->id);
          return  redirect('admin/dashboard');
        }

        public function register(Request $request){

            $location_search = $request->locations_search;
            $location_id = $request->places_id;
            $name = $request->name;
            $phone = $request->phone;
            $email = $request->email;
            $password = Hash::make($request->password);
            $confirm_password = $request->confirm_password;
            $townName=$cityName=$countryName='';
            $place_id = $request->places_id;
            $placeDetailName = Location::getTownCityStateCountryName($place_id);
            $placeArr = explode('_', $placeDetailName);
            
            for($i=0;$i<sizeof($placeArr);$i++)
            {
                $placValArr=explode('*',$placeArr[$i]);
                if($placValArr[0]=='town')
                {
                    $townName=$placValArr[1];
                }
              
                if($placValArr[0]=='city')
                {
                    $cityName=$placValArr[1];
                }
               
                if($placValArr[0]=='state')
                {
                    $stateName=$placValArr[1];
                }
               
                if($placValArr[0]=='country')
                {
                    $countryName=$placValArr[1];
                }
               
            }
            $data = [
                   [
                    'name'=>$name,
                    'email'=>$email,
                    'role'=>2,
                    'password'=> $password,
                    'created_at'=>now(),
                    'updated_at'=>now(),
                    'place'=>$location_search,
                    'town' => $townName,
                    'city' => $cityName,
                    'state' => $stateName,
                    'country' => $countryName,
                    'phone'=>$phone,
                    'place_id'=>$location_id
                   ] 
                ];
               

            $result = DB::table('users')->insert($data);
            //return View::make("/auth/new_login"); 
            $last = DB::table('users')->latest()->first();
          /*  print_r($last->id); die();
          
          $users = DB::table('users')
                ->orderBy('name', 'desc')
                ->limit(1)
                ->get();
          */
         // DB::enableQueryLog();
          $shopperLastId= DB::table('shopper_details')
          ->orderBy('shopperId', 'desc')
          ->limit(1)
          ->get('shopperId');
         // dd(DB::getQueryLog());


         
          if(count($shopperLastId)>0)
          {
            foreach($shopperLastId as $shoId)
            $shoppersId= $shoId->shopperId + 1;
          }else{
            $shoppersId =$last->id;
          }

          $shopper = [
            [
              
             'shopperId'=>  $shoppersId,
             'status'=>2,
              'name'=>$name,
             'email'=>$email,
             'address'=>$location_search,
             'phone_number'=>$request->phone,
             'created_at'=>now(),
             'profile_image' =>'',
             'updated_at'=>now()
             ] 
         ];
         $result1 = DB::table('shopper_details')->insert($shopper);  
         Auth::loginUsingId($last->id);
            return redirect('thank-you-shopper');
        }

        function thank_you_shopper() {
            return view('thank-you-shopper');
        }

        public function check_emailaddress(Request $request){

        $email = $request->email;
        $data = DB::table('users')->select('*')->where('email','=',$email)->get();
        $count = $data->count();
        if($count>0){
            echo "false";
        }else{
            echo "true";
         }

        }

         public function check_email_availability(Request $request){

        $email = $request->email;
        $data = DB::table('users')->select('*')->where('email','=',$email)->get();
        $count = $data->count();
        if($count>0){
            echo "true";
        }else{
            echo "false";
         }

        }


        public function add_guest_user(){

            $name  = "Guest User";
            $email = "guest@mytown.com";
            $password = "guest@123";
            $place = "Unknown";

             $data = [
                   [
                    'name'=>$name,
                    'email'=>$email,
                    'password'=> $password,
                    'created_at'=>now(),
                    'updated_at'=>now(),
                    'place'=>$place
                   ] 
                ];

            $result = DB::table('users')->insert($data);
            if($result){
                echo "Records added successfully";
            }else{
                echo "Records not added successfully";
            }

        }

}
