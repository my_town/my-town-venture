<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Session;
use DB;
use Facade\FlareClient\Stacktrace\File;

class AddProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('/biz_owner/dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        //
       // dd($_FILES["product_image"]["tmp_name"]);
       //
        $this->validate($request,[
            'product_image' => 'required',
            'desc' => 'required',
            'qty' => 'required',
            'rate' => 'required',
          

        ]);

        $store_file = $request->product_image;
        // Storage::disk('public')->put('/images/products/' . time() . '_' . $store_file->getClientOriginalName(), File::get($store_file));
        // $image_name = time() . '_' . $store_file->getClientOriginalName();
        // $product_name = $store_file->getClientOriginalName();
        // $file_name = pathinfo($product_name, PATHINFO_FILENAME);
         $business_id=Session::get('business_id');
 //dd($business_id);
        //`productId` `business_id` `name` `description` `product_count` `actual_rate` `product_image`
        $latestOrder = Product::orderBy('created_at', 'DESC')->first();
        $produt_upId = $business_id . $latestOrder->id;
        $productIncId = '#' . str_pad($produt_upId + 1, 8, "0", STR_PAD_LEFT);
        $description =$request->desc;
        $qty =$request->qty;
        $rate =$request->rate;

        $store_product = [
            [
                'business_id' => $business_id,
                // 'sku'=>'TLUXM-100001-BLK',
                'productId' => $productIncId,
                'name' => $store_file,
                'description' => $description,
                'product_count' => $qty,
                'actual_rate' => $rate,
                'price_base_currency' => $rate,
                'product_image' => $store_file,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ];
//dd($store_product);
        DB::table('products')->insert($store_product);

        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       dd($request);
    //     $this->validate($request,[
    //         'product_image' => 'required',
    //         'product_desc' => 'required',
    //         'product_qty' => 'required',
    //         'product_rate' => 'required',
          

    //     ]);
 
    //     $store_product = [
    //         [

    //     'name' => $request->product_image,
    //     'description' => $request->product_desc,
    //     'product_count' => $request->product_qty,
    //     'actual_rate' => $request->product_rate,
    //     'product_image' => $request->product_image,
    //      'updated_at' =>now()
    //         ]
    //         ];
    //         dd($store_product);
    //     $pro = DB::table('products')
    //    ->where('id',$id)
    //    ->update($store_product);

       return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
       
       $pro = DB::table('products')
       ->where('id',$id)
       ->update(['status'=>2]);

        return back();
    }
}
