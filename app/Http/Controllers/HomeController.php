<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Business;
use App\Models\Product;
use App\Models\User;
use App\Models\shopper_details;

class HomeController extends Controller
{
    //

    public function show_count(){
        $businesses = Business::count();
        $products = Product::count();
        $shoppers = shopper_details::count();
        $towns = User::distinct()->where('role','<>','3')->count('town');
        $cities = User::distinct()->where('role','<>','3')->count('city');
        $countries = User::distinct()->where('role','<>','3')->count('country');
        return view('welcome',compact('businesses', 'products', 'shoppers', 'towns','cities','countries'));
    }

    
}
