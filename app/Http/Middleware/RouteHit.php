<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Hit;
use Illuminate\Support\Facades\Auth;

class RouteHit
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
		// remove all hits older than the configured time
		$date = new \DateTime;
		$date->modify('-1440 minutes');
		$formatted = $date->format('Y-m-d H:i:s');
		Hit::where('updated_at', '<=', $formatted)->delete();
		// save a new hit
		$hit = new Hit;
		$hit->ip_address = $request->ip();
		$hit->user_id = @Auth::user()->id;
		$hit->route = $request->path();
		$hit->save();
        return $next($request);
    }
}
