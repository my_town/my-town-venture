<?php
namespace App\Helpers;

use SKAgarwal\GoogleApi\PlacesApi;
class Location{

	public static function placeDetails($place_id){
		$googlePlaces = new PlacesApi(env('GOOGLE_MAPS_KEY'));
		$place_details = $googlePlaces->placeDetails($place_id);
		return $place_details;
	}

	public static function getTown($place_id){
		$place_details = \App\Helpers\Location::placeDetails($place_id);
		$town_address = array();
		$locality_types = array('sublocality_level_1', 'locality',
			'administrative_area_level_2','administrative_area_level_1','country');
		foreach($place_details['result']['address_components'] as $a){
			if(in_array($a['types'][0], $locality_types)){
				$town_address[] = $a['short_name'];
			}
		}
		return implode(", ", array_unique($town_address));
	}

	public static function getShortPlaceName($place_id){
		$place_details = \App\Helpers\Location::placeDetails($place_id);
		$town_address = array();
		$locality_types = array('sublocality_level_1', 'locality',
			'administrative_area_level_2','administrative_area_level_1','country');
		foreach($place_details['result']['address_components'] as $a){
			if(in_array($a['types'][0], $locality_types)){
				$town_address[] = $a['short_name'];
			}
		}
		return $town_address[0];
	}
	
	
	////town city state country

public static function getTownCityStateCountryName($place_id){
	$placeName ='';
	$place_details = \App\Helpers\Location::placeDetails($place_id);
	//dd($place_details['result']['address_components']);
	$town_address = array();
	$locality_types = array('sublocality_level_1', 'locality',
		'administrative_area_level_2','administrative_area_level_1','country');
	foreach($place_details['result']['address_components'] as $a){
		//dd($a[0]);
		if($a['types'][0]=='sublocality_level_1'){//town
			$town = $a['short_name'];
			$placeName=$placeName.'_'.$town;
		}
		if($a['types'][0]=='locality'){//city
			$city = $a['short_name'];
			$placeName=$placeName.'_'.$city;
		}
		if($a['types'][0]=='administrative_area_level_1'){//state
			$state = $a['long_name'];
			$placeName=$placeName.'_'.$state;
		}
		if($a['types'][0]=='country'){//country
			$country = $a['long_name'];
			$placeName=$placeName.'_'.$country;
		}
	}
//	$placeName=$town.'_'.$city.'_'.$state.'_'.$country;
	//dd($placeName);
	return $placeName;
}

}
