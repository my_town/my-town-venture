<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessContact extends Model
{
    use HasFactory;
    function business(){
        return $this->belongsTo('App\Models\Business');
    }
    function user(){
        return $this->belongsTo('App\Models\User');
        //
    }
}
