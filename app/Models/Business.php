<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Conner\Tagging\Taggable;

class Business extends Model
{
    use HasFactory;
    use Taggable;
    function products(){
        return $this->hasMany('App\Models\Product');
    }
    function services(){
        return $this->hasMany('App\Models\Service');
    }
    function photos(){
        return $this->hasMany('App\Models\Photo');
    }

	function contacts(){
		return $this->hasMany('App\Models\BusinessContact');
	}
}
