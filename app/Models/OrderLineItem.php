<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderLineItem extends Model
{
    use HasFactory;
    use SoftDeletes;
    public function order(){
        return $this->belongsTo(Order::class);
    }
}
