<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;
    function user(){
        return $this->belongsTo('App\Models\User');
        //
    }
	
	function contact_person(){
        return $this->hasOne('App\Models\User', 'id', 'linked_user_id');
	}
}
