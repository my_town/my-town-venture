<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessOwnerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_owner', function (Blueprint $table) {
            $table->increments('id');
            $table->string('business_name');
            $table->string('service_type');
            $table->string('product_type');
            $table->string('email')->nullable();
            $table->string('phone_num')->nullable();
            $table->string('phone_num_alter')->nullable();
            $table->string('address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_owner');
    }
}
