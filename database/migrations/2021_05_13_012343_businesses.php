<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Businesses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('owner');
            $table->string('name');
            $table->string('country');
            $table->string('state_province');
            $table->string('town');
            $table->string('street_address1');
            $table->string('street_address2');
            $table->string('phone_number1');
            $table->string('phone_number2');
            $table->string('email_address');
            $table->string('logo')->nullable();
            $table->string('banner_image')->nullable();
            $table->text('description');
            $table->timestamps();
			$table->foreign('owner')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('businesses');
    }
}
