<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->unsignedBigInteger('business_id');
        $table->string('sku');
        $table->string('name');
        $table->text('description');
        $table->double('price_base_currency');
        $table->string('product_image')->nullable();
        $table->timestamps();
		// set foreign key
		$table->foreign('business_id')->references('id')->on('businesses');
		}); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('products');
    }
}
