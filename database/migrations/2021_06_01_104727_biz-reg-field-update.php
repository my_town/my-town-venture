<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BizRegFieldUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('businesses', function (Blueprint $table) {
			$table->string('place_id')->nullable();
			$table->string('address')->nullable();
			$table->dropColumn('country');
			$table->dropColumn('state_province');
			$table->dropColumn('town');
			$table->dropColumn('street_address1');
			$table->dropColumn('street_address2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
