<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStoreImagesVideoToBusiness extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('businesses', function (Blueprint $table) {
            $table->string('store_image1')->nullable();
            $table->string('store_image2')->nullable();
            $table->string('store_image3')->nullable();
            $table->string('store_image4')->nullable();
            $table->string('video_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('businesses', function (Blueprint $table) {
            //
            $table->dropColumn('store_image1');
            $table->dropColumn('store_image2');
            $table->dropColumn('store_image3');
            $table->dropColumn('store_image4');
            $table->dropColumn('video_url');
            
        });
    }
}
