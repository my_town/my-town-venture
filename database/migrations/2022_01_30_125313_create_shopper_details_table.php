<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopperDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopper_details', function (Blueprint $table) {
            $table->id();
            $table->integer('shopperId');
            $table->integer('status')->comment('1:guest 2:registred');
            $table->string('name');
            $table->string('email');
            $table->integer('phone_number');
            $table->string('address');
            $table->string('profile_image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopper_details');
    }
}
