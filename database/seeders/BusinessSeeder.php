<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Integer;
use DB;

class BusinessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('businesses')->insert([
            'owner' => '5',
            'name' => Str::random(10),
            'phone_number1' => '9766962674',
            'phone_number2' => '8530055138',
            'email_address' => Str::random(10).'@gmail.com',
            'logo'=> '1625143111_meeting.png',
            'banner_image'=> '1625143111_meeting.png',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and
            typesetting industry.',
            'created_at' => '2021-07-01 12:38:31',
            'updated_at' => '2021-07-01 12:38:31',
            'place_id' => '',
            'address' => 'Mumbai',
            'categories' => 'Share Market'
        ]);
        //
    }
}
