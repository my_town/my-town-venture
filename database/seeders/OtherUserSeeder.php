<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Integer;
use DB;

class OtherUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // add users
        DB::table('users')->insert(
            [
                'name'=>Str::random(10),
                'email'=>Str::random(10).'@gmail.com',
                'password'=>bcrypt('MyTown!@#'),
                'remember_token'=>0,
                'created_at'=>NOW(),
                'updated_at'=>NOW(),
                'place'=>'Mumbai'
            ]
        );
    }
}
