@extends('layouts.my_Town_Business')

@section('content')
<div class="right-sldier">
    <div class="top-header">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ul class="left-links">
                    <li>
                        <a href="#" class="mainSidebarToggle"><i class="far fa-bars"></i></a>
                    </li>
                    <li>
                        <img src="{{ url('/') }}/images/dashboard/admin-icon.png" alt="" />
                        Business Owner Dashboard
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-sm-12">
                <ul class="admin-info">
                    <li>
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control" />
                                <button type="submit"><i class="far fa-search"></i></button>
                            </div>
                        </form>
                    </li>

                    <li class="dropdown notification-drop">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-bell"></i>
                            {{--  @if ($notifyOrder->count()>0)
                            {{ $notifyOrder->count() }}
                           
                            @endif
                           --}}
                        </button>
                        
                    </li>
                    <li class="dropdown user-drop">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <img src="{{ url('/') }}/images/dashboard/user.jpg" align="" />
                        </button>
                      
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="center-main-blocker">
        <div class="tab-content" id="nav-tabContent">
            
            <div class="tab-pane fade show active" id="tab0" role="tabpanel" aria-labelledby="tab1-tab">
                <div class="dash-action-lst">
                    <ul>
                        <li>
                            <a id="tab11-tab" data-toggle="tab" href="#tab1" role="tab" aria-controls="tab0" aria-selected="true">
                                <div class="dash-box-in">
                                    <h3>Shoppers</h3>
                                    <h2>0</h2>
                                    <div class="dash-icbx">
                                        <img src="{{ url('/') }}/images/dashboard/dash-ic1.png" alt="" />
                                    </div>
                                    <img src="{{ url('/') }}/images/dashboard/img-1.png" alt=""
                                        class="dash-shp" />
                                </div>
                            </a>
                        </li>
                        <li>
                            <a id="tab12-tab" data-toggle="tab" href="#tab2" role="tab" aria-controls="tab0" aria-selected="true">
                                <div class="dash-box-in">
                                    <h3>Orders</h3>
                                    <h2>{{ $order_product->count() }}</h2>
                                    <div class="dash-icbx">
                                        <img src="{{ url('/') }}/images/dashboard/dash-ic2.png" alt="" />
                                    </div>
                                    <img src="{{ url('/') }}/images/dashboard/img-1.png" alt=""
                                        class="dash-shp" />
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="dash-box-in">
                                    <h3>Shipment</h3>
                                    <h2>{{ $orderCount->count() }}</h2>
                                    <div class="dash-icbx">
                                        <img src="{{ url('/') }}/images/dashboard/dash-ic3.png" alt="" />
                                    </div>
                                    <img src="{{ url('/') }}/images/dashboard/img-1.png" alt=""
                                        class="dash-shp" />
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="dash-box-in">
                                    <h3>Products</h3>
                                    <h2>{{ $products->count() }}</h2>
                                    <div class="dash-icbx">
                                        <img src="{{ url('/') }}/images/dashboard/dash-ic4.png" alt="" />
                                    </div>
                                    <img src="{{ url('/') }}/images/dashboard/img-1.png" alt=""
                                        class="dash-shp" />
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="dash-box-in">
                                    <h3>Monthly Sales</h3>
                                    <h2>{{ $monthlySales }}</h2><!-- 1,50,000 -->
                                    <div class="dash-icbx">
                                        <img src="{{ url('/') }}/images/dashboard/dash-ic5.png" alt="" />
                                    </div>
                                    <img src="{{ url('/') }}/images/dashboard/img-1.png" alt=""
                                        class="dash-shp" />
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="dash-box-in">
                                    <h3>People in Store</h3>
                                    <h2>0</h2>
                                    <div class="dash-icbx">
                                        <img src="{{ url('/') }}/images/dashboard/dash-ic6.png" alt="" />
                                    </div>
                                    <img src="{{ url('/') }}/images/dashboard/img-1.png" alt=""
                                        class="dash-shp" />
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="prolst-wrpbx">
                    <div class="pro-had">
                        <div class="row">
                            <div class="col-md-5 col-sm-5">
                                <div class="pro-hedbing">
                                    <h3>Shopper's Order Summary</h3>
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <div class="sort-lst-bx">
                                    <ul>
                                        <li>
                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option>1-10 Entries</option>
                                                    <option>1-50 Entries</option>
                                                </select>
                                                <div class="sort-ic">
                                                    <img src="{{ url('/') }}/images/dashboard/exprot.png"
                                                        alt="" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option>Filter Name List</option>
                                                    <option>1-50 Entries</option>
                                                </select>
                                                <div class="sort-ic">
                                                    <img src="{{ url('/') }}/images/dashboard/filter.png"
                                                        alt="" />
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="protable-wrp">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>
                                            <p>Order Id</p>
                                        </th>
                                        <th>
                                            <p>Shopper's name</p>
                                        </th>
                                       
                                        <th>
                                            <p>To : Town name</p>
                                        </th>
                                        <th>
                                            <p>Total Price</p>
                                        </th>
                                        <th>
                                            <p>Order Placed Date & Time</p>
                                        </th>
                                        <th>
                                            <p>Order Delivery Date & Time</p>
                                        </th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($orders->count() > 0)
                                        @foreach ($orders as $order)
                                            <tr>
                                                <td>
                                                    <p>{{ '#'.$order->id }}</p>
                                                </td>
                                                <td>
                                                    <p>{{ $order->first_name.' '.$order->last_name }}
                                                    </p>   
                                                    
                                                </td>
                                              {{-- done --}}
                                                <td>
                                                    <p>{{ $order->address. ',' .$order->country. ',' .$order->state }}
                                                        </p> 
                                                      
                                                </td>{{-- done --}}
                                                <td>
                                                    {{ $order->total_price }}
                                                </td>{{-- done --}}
                                                <td>
                                                    <p>{{ $order->created_at }}</p>
                                                </td>
                                                <td>
                                                    <p><?php
                                                        $time = strtotime($order->created_at);
                                                     echo   $final = date("Y-m-d h:i:s", strtotime("+1 month", $time));
                                                        ?></p>

                                                </td>
                                               
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4">
                                                <p>No records found</p>
                                            </td>
                                        </tr>
                                    @endif

                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <div class="tab-pane fade show" id="tab1" role="tabpanel" aria-labelledby="tab1-tab">
               
                <div class="prolst-wrpbx">
                    <div class="pro-had">
                        <div class="row">
                            <div class="col-md-5 col-sm-5">
                                <div class="pro-hedbing">
                                    <h3>Shopper's Order Summary</h3>
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <div class="sort-lst-bx">
                                    <ul>
                                        <li>
                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option>1-10 Entries</option>
                                                    <option>1-50 Entries</option>
                                                </select>
                                                <div class="sort-ic">
                                                    <img src="{{ url('/') }}/images/dashboard/exprot.png"
                                                        alt="" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option>Filter Name List</option>
                                                    <option>1-50 Entries</option>
                                                </select>
                                                <div class="sort-ic">
                                                    <img src="{{ url('/') }}/images/dashboard/filter.png"
                                                        alt="" />
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="protable-wrp">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>
                                            <p>Order Id</p>
                                        </th>
                                        <th>
                                            <p>Shopper's name</p>
                                        </th>
                                        {{-- <th>
                                            <p>From : Town name</p>
                                        </th> --}}
                                        <th>
                                            <p>To : Town name</p>
                                        </th>
                                        <th>
                                            <p>Total Price</p>
                                        </th>
                                        <th>
                                            <p>Order Placed Date</p>
                                        </th>
                                        <th>
                                            <p>Order Delivery Date</p>
                                        </th>

                                    </tr>
                                </thead>
                                <tbody>
                                      @if ($orders->count() > 0)
                                        @foreach ($orders as $order)
                                            <tr>
                                                <td>
                                                    <p>{{ '#'.$order->id }}</p>
                                                </td>
                                                <td>
                                                    <p>{{ $order->first_name.' '.$order->last_name }}
                                                    </p>   
                                                    
                                                </td>
                                              {{-- done --}}
                                                <td>
                                                    <p>{{ $order->address. ',' .$order->country. ',' .$order->state }}
                                                        </p> 
                                                      
                                                </td>{{-- done --}}
                                                <td>
                                                  <p> {{ $order->total_price }}</p>
                                                </td>{{-- done --}}
                                                <td>
                                                    <p>{{ $order->created_at }}</p>
                                                </td>
                                                <td>
                                                    <p><?php
                                                        $time = strtotime($order->created_at);
                                                     echo   $final = date("Y-m-d h:i:s", strtotime("+1 month", $time));
                                                        ?></p>

                                                </td>
                                                {{-- <td>
                                          <p>
                                              <a href="#" class="edit-table" order-id="{{$order->id}}"><i class="fas fa-edit"></i></a><a href="#" class="delete-table" order-id="{{$order->id}}"><i class="far fa-trash-alt"></i></a>
                                          </p>
                                      </td> --}}
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4">
                                                <p>No records found</p>
                                            </td>
                                        </tr>
                                    @endif

                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab2-tab">
               

                <div class="prolst-wrpbx">
                    <div class="pro-had">
                        <div class="row">
                            <div class="col-md-5 col-sm-5">
                                <div class="pro-hedbing">
                                    <h3>Order Details</h3>
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <div class="sort-lst-bx">
                                    <ul>
                                        <li>
                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option>1-10 Entries</option>
                                                    <option>1-50 Entries</option>
                                                </select>
                                                <div class="sort-ic">
                                                    <img src="{{ url('/') }}/images/dashboard/exprot.png"
                                                        alt="" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option>Filter Name List</option>
                                                    <option>1-50 Entries</option>
                                                </select>
                                                <div class="sort-ic">
                                                    <img src="{{ url('/') }}/images/dashboard/filter.png"
                                                        alt="" />
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="protable-wrp">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>
                                            <p>Order Id</p>
                                        </th>
                                        <th>
                                            <p>Product Id</p>
                                        </th>
                                        <th>
                                            <p>Product Name</p>
                                        </th>
                                        <th>
                                            <p>Product Quantity</p>
                                        </th>
                                        <th>
                                            <p>Product Price</p>
                                        </th>
                                        <th>
                                            <p>Shopper's Name</p>
                                        </th>
                                        <th>
                                            <p>From : Town name</p>
                                        </th>
                                        <th>
                                            <p>To : Town name</p>
                                        </th>
                                        <th>
                                            <p>Order Placed DateTime</p>
                                        </th> 
                                        <th>
                                            <p>Order Delivery DateTime</p>
                                        </th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($order_product->count() > 0)
                                        @foreach ($order_product as $productDetail)
                                            <tr>
                                                <td>
                                                    <p>{{ '#'.$productDetail->id }}</p>
                                                </td>
                                                <td>
                                                    <p>{{ $productDetail->productId }}</p>
                                                </td>
                                                <td>
                                                    <p>{{ $productDetail->name }}</p>
                                                </td>
                                                <td>
                                                    <p>{{ $productDetail->product_count }}</p>
                                                </td>
                                                <td>
                                                    <p>{{ $productDetail->cost * $productDetail->product_count }}</p>
                                                </td>
                                                <td>
                                                    <p>{{ $productDetail->first_name.' '.$productDetail->last_name }}</p>
                                                </td>
                                                  
                                               
                                                <td>
                                                    <p>{{ $business->address }}</p>
                                                </td>
                                                <td>
                                                    <p>{{ $productDetail->address.','.$productDetail->country.','. $productDetail->state }}</p>
                                                </td>
                                                <td>
                                                    <p>{{ $productDetail->created_at }}</p>
                                                </td>
                                                <td>
                                                    <p><?php
                                                        $time = strtotime($productDetail->created_at);
                                                     echo   $final = date("Y-m-d h:i:s", strtotime("+1 month", $time));
                                                        ?></p>

                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6">
                                                <p>No records found</p>
                                            </td>
                                        </tr>
                                    @endif
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">
                

                <div class="prolst-wrpbx">
                    <div class="pro-had">
                        <div class="row">
                            <div class="col-md-5 col-sm-5">
                                <div class="pro-hedbing">
                                    <h3>Shipment Details</h3>
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <div class="sort-lst-bx">
                                    <ul>
                                        <li>
                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option>1-10 Entries</option>
                                                    <option>1-50 Entries</option>
                                                </select>
                                                <div class="sort-ic">
                                                    <img src="{{ url('/') }}/images/dashboard/exprot.png"
                                                        alt="" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option>Filter Name List</option>
                                                    <option>1-50 Entries</option>
                                                </select>
                                                <div class="sort-ic">
                                                    <img src="{{ url('/') }}/images/dashboard/filter.png"
                                                        alt="" />
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="protable-wrp">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>
                                            <p>Order Id</p>
                                        </th>
                                        <th>
                                            <p>Shopper's Name</p>
                                        </th>
                                        <th>
                                            <p>Shopper's Email</p>
                                        </th>
                                        <th>
                                            <p>Shopper's Phone</p>
                                        </th>
                                        <th>
                                            <p>From : Town name</p>
                                        </th>
                                        <th>
                                            <p>To : Town name</p>
                                        </th>
                                        <th>
                                            <p>Shipping Charges</p>
                                        </th>
                                        <th>
                                            <p>Total Price</p>
                                        </th>
                                        <th>
                                            <p>Order Delivery Date & Time</p>
                                        </th>
                                        {{--  <th></th>  --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($orders->count() > 0)
                                        @foreach ($orders as $order)
                                            <tr>
                                                <td>
                                                    <p>{{ $order->id }}</p>
                                                </td>
                                               
                                                    <td>
                                                        <p>{{ $order->first_name.' '.$order->last_name }}
                                                        </p>
                                                       
                                                    </td>
                                                    <td>
                                                        <p>{{ $order->email }}</p>
                                                    </td>
                                                    <td>
                                                        <p>{{ $order->phone }}</p>
                                                    </td>
                                                 <td>
                                                    <p>{{ $business->address }}</p>
                                                </td>{{-- done --}}
                                                <td>
                                                   <p>{{ $order->address.', '.$order->state.', '.$order->country }}
                                                        </p> 
                                                </td>
                                                <td>
                                                    <p>80</p>
                                                </td>
                                                <td>
                                                    <p>{{ $order->total_price }}</p>
                                                </td>
                                                <td>

                                                
                                                <p>
                                                    <?php
                                                    $time = strtotime($productDetail->created_at);
                                                 echo   $final = date("Y-m-d h:i:s", strtotime("+1 month", $time));
                                                    ?>
                                                    </p>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6">
                                                <p>No records found</p>
                                            </td>
                                        </tr>
                                    @endif
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="tab4" role="tabpanel" aria-labelledby="tab4-tab">
               

                <div class="prolst-wrpbx">
                    <div class="pro-had">
                        <div class="row">
                            <div class="col-md-5 col-sm-5">
                                <div class="pro-hedbing">
                                    <h3>Product Details</h3>
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <div class="sort-lst-bx">
                                    <ul>
                                        <li>
                                            <button type="button" id="custom-button" class="int-btn" data-toggle="modal" data-target="#addProduct">
                                               Add Product 
                                              </button>
                                        </li>
                                      
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="protable-wrp information-tbl">
                       
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id='datatable'>
                                <thead>
                                    <tr>
                                        <th>
                                            <p>Sr No</p>
                                        </th>
                                        <th>
                                            <p>Product Id</p>
                                        </th>
                                         <th>
                                            <p>Products</p>
                                        </th>
                                        <th>
                                            <p>Name</p>
                                        </th>
                                        <th>
                                            <p>Quantity</p>
                                        </th>
                                        <th>
                                            <p>Rate</p>
                                        </th>
                                        
                                        <th>
                                            <p>Actions</p>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($products->count() > 0)
                                        @foreach ($products as $product)
                                            <tr>
                                            <td>
                                                   
                                                        {{ $product->id }}
                                                   
                                                </td>
                                                 <td>
                                                   
                                                        {{ $product->productId }}
                                                   
                                                </td>
                                                <td>
                                                    <p class="tab-img"><img
                                                            src="http://localhost/mytown/storage/app/public/images/products/{{ $product->product_image }}"
                                                            alt="img" border='3' height='100' width='100' overflow: hidden; /></p>
                                                </td>
                                                <td>
                                                   
                                                        {{ $product->name }}
                                                   
                                                </td>
                                                 <td>
                                                   {{ $product->product_count }}
                                                </td>
                                                <td>
                                                    {{ $product->actual_rate }}
                                                </td>
                                               
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table edit"><i
                                                                class="fas fa-edit"></i></a>
                                                                
                                                                <a href="#"  class="delete-table delete"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5">
                                                <p>No records found</p>
                                            </td>
                                        </tr>
                                    @endif


                                    <!-- <tr>
                                          <td>
                                              <p class="tab-img"><img src="{{ url('/') }}/images/dashboard/tab-img-01.jpg" alt="img" /></p>
                                          </td>
                                          <td>
                                              <p>
                                                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                                                  took a galley of type and scrambled it to make a type specimen book.
                                              </p>
                                          </td>
                                          <td><p>Rs.200</p></td>
                                          <td><p>Rs.20</p></td>
                                          <td>
                                              <p>
                                                  <a href="#" class="edit-table" data-toggle="modal" data-target="#myModal"><i class="fas fa-edit"></i></a><a href="#" class="delete-table"><i class="far fa-trash-alt"></i></a>
                                              </p>
                                          </td>
                                      </tr> -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">...</div>
            <div class="tab-pane fade" id="tab5" role="tabpanel" aria-labelledby="tab5-tab">...</div>
            <div class="tab-pane fade" id="tab6" role="tabpanel" aria-labelledby="tab6-tab">...</div>
            <div class="tab-pane fade" id="tab7" role="tabpanel" aria-labelledby="tab7-tab">...</div>
        </div>
    </div>

    <!-- ======= Footer ======= -->
    <footer id="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 footer-contact">
                        <h3>MyTown</h3>
                        <p>
                            A108 Adam Street <br />
                            New York, NY 535022<br />
                            United States <br />
                            <br />
                            <strong>Phone:</strong> +1 5589 55488 55<br />
                            <strong>Email:</strong> info@example.com<br />
                        </p>
                    </div>

                    <div class="col-lg-2 col-md-6 footer-links">
                        <h4>Useful Links</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Our Services</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-4 col-md-6 footer-newsletter">
                        <h4>Join Our Newsletter</h4>
                        <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                        <form action="" method="post"><input type="email" name="email" /><input type="submit"
                                value="Subscribe" /></form>
                    </div>
                </div>
            </div>
        </div>

        <div class="container d-lg-flex py-4">
            <div class="mr-lg-auto text-center text-lg-left">
                <div class="copyright">
                    &copy; Copyright <strong><span>MyTown</span></strong>. All Rights Reserved
                </div>
            </div>
            <div class="social-links text-center text-lg-right pt-3 pt-lg-0">
                <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
        </div>
    </footer>
    <!-- End Footer -->

    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
</div>

@endsection

@section('modal')

{{--start edit product--}}
  <div class="modal fade products-pop" id="Edit_Product">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-body">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="prd-pop-details">
                      <h4 class="modal-title">Edit Products</h4>
                      <p>Products</p>

                      <form  id="editProductForm" action="\addProduct">
                      {{ csrf_field() }}
                      {{ method_field('PUT') }}
                      <div class="form-group" id="image_div">
                       {{--  <p class="tab-img"><img src="http://localhost/mytown/storage/app/public/images/products/{{ $product->product_image }}"
                                                            alt="img" border='3' height='100' width='100' overflow: hidden;/></p>  --}}
                      </div>
                      <input type="hidden" id="product_id"/>
                       <div class="form-group">
                            <div class="file-upd">
                                <input type="file" name="product_image" id="product_image"
                                class="int-file form-control" accept="image/*" />
                            </div>
                        </div> 
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" id="product_desc" name="desc" type="text"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Product Quantity</label>
                            <input type="text" class="form-control" id="product_qty" name="qty" type="text" />
                        </div>
                        <div class="form-group">
                            <label>Rate</label>
                            <input type="text" class="form-control" id="product_rate" name="rate" type="text" />
                        </div>
                        <input type="hidden" name="businessId" value="{{  Session::get('business_id')}}">
                          <div class="form-group">
                              <button href="#" class="btn-main" type="submit" id="updateProduct">Update</button>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
{{--  end edit product  --}}
  {{-- start add products  --}}
  <div class="modal fade products-pop" id="addProduct">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="prd-pop-details">
                    <h4 class="modal-title">Add Product</h4>
                    <p>Products</p>

                    <form action="{{ route('addProduct.store') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="file-upd">
                                <input type="file" name="product_image" id="product_image"
                                class="int-file form-control" accept="image/*" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" name="desc" type="text"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Product Quantity</label>
                            <input type="text" class="form-control" name="qty" type="text" />
                        </div>
                        <div class="form-group">
                            <label>Rate</label>
                            <input type="text" class="form-control" name="rate" type="text" />
                        </div>
                        <input type="hidden" name="businessId" value="{{  Session::get('business_id')}}">
                        <div class="form-group">
                            {{--  <button type="button" class="btn-main" data-dismiss="modal">Cancel</button>
                              --}}
                            <button href="#" class="btn-main" type="submit">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- end add products  --}}
{{--  start Delete product  --}}
 <div class="modal fade products-pop" id="Delete_Product">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-body">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="prd-pop-details">
                      <h4 class="modal-title">Delete Products</h4>
                     
                      <form  id="deleteProductForm" action="\addProduct" method="POST">
                      
                      {{ csrf_field() }}
                      {{ method_field('DELETE') }}
                      <input type="hidden" name="_method" value="DELETE"/>
                      <p>Are you sure?.. You want to delete data</p>
                        <input type="hidden" name="businessId" value="{{  Session::get('business_id')}}">
                          <div class="form-group">
                              <button href="#" class="btn-main" type="submit" id="updateProduct">Delete</button>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
{{-- end Delete Product--}}
@endsection
