@extends('layouts.my-town-business')

@section('content')

<!-- Div for product addition -->
<div class="box1" id="product-box">
    <div class="jumbotron"> 
        <div class="card">
            <label id="hide_product" class="close-btn fas fa-times"></label>
            <div class="card-body" >
                <h5 class="card-title">Product Details</h5>
                <form method="POST" action="" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="business_id" id="business_id" value= >
                    <div class="mb-3">
                        <label class="form-label">Name:</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">SKU:</label>
                        <input type="text" class="form-control" id="sku" name="sku">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Price in USD:</label>
                        <input type="text" class="form-control" id="price_base_currency" name="price_base_currency">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Product Image:</label>
                        <input type="file" class="form-control" id="product_image" name="product_image" >
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Description:</label>
                        <textarea class="form-control" id="description" name="description" rows="4" cols="50"></textarea>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Div for services addition-->
<div class="box1" id="service-box">
    <div class="jumbotron"> 
        <div class="card">
            <label id="hide_service" class="close-btn fas fa-times"></label>
            <div class="card-body" >
                <h5 class="card-title">Services</h5>
                <form method="POST" action="" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="business_id" id="business_id" value= >
                    <div class="mb-3">
                        <label class="form-label">Title:</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Price:</label>
                        <input type="text" class="form-control" id="price_base_currency" name="price_base_currency">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Service Image:</label>
                        <input type="file" class="form-control" id="service_image" name="service_image" >
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Description:</label>
                        <textarea class="form-control" id="description" name="description" rows="4" cols="50"></textarea>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="greybg content-box">
    <div class="container">
        <div class="row paddingbottom3">
            <div class="col-8 col-12-medium"><br><br>
                <div class="tab margintop3-minus">
                    <button class="tablinks active" onclick="openCity(event, 'Tab1')">Overview</button>
                    <button class="tablinks" onclick="openCity(event, 'Tab2')">Products</button>
                    <button class="tablinks" onclick="openCity(event, 'Tab3')">Photos</button>
                    <button class="tablinks" onclick="openCity(event, 'Tab4')">Feed</button>
                    <button class="tablinks" onclick="openCity(event, 'Tab5')">Services</button>
                </div>
                <div id="Tab1" class="tabcontent" style="display:block;">
                    <h3>About this place</h3>
                    <p></p>

                   
                    <form method="POST" action="" enctype="multipart/form-data" id="addTagForm">
                        @csrf
                        <input type="hidden" name="business_id" id="business_id" value= >
                        <textarea placeholder="Enter category tags..." id="category_tags" name="category_tags" rows="10" cols="20" style="width:100%;height:100px;"></textarea>
                        <a href="javascript:$({{'addTagForm'}}).submit();" class="button-outline-sm marginleft1" style="margin-top:10px;margin-left:-2px;">Add tags</a>
                    </form>
                   
                    <h3>Categories</h3>
                    <p>Big Bazaar is an Indian retail chain of hypermarkets, discount department stores, and grocery stores</p>
                   

                    <!--<table width="90%" border="1" class="table">
                        <tbody>
                            <tr>
                                <td width="31%"><strong>Store Type</strong></td>
                                <td width="69%">Grocery</td>
                            </tr>
                            <tr>
                                <td><strong>Store Type </strong></td>
                                <td>Grocery</td>
                            </tr>
                            <tr>
                                <td><strong>Store Type </strong></td>
                                <td>Grocery</td>
                            </tr>
                        </tbody>
                    </table>-->
                    <p>&nbsp;</p>
                </div>
                <div id="Tab2" class="tabcontent">
                    
                    <label class="show-btn"  id="show_product"><i class="fa fa-plus" aria-hidden="true"></i></label>
                   
                    <div class="row marginbottom1">
                       
                        
                       
                        <!-- <div class="col-4 col-12-medium">
                            <div class="border1">
                                <img src="/images/bakarwadi.jpg" width="170" height="137" alt=""/>
                                <div class="small bolder marginleft1 marginbottom1"> Bakarwadi, 150gm</div>
                                <div class="row textcenter">
                                    <div class="marginbottom1">₹ 55<a href="#" class="button-outline-sm marginleft1"><i class="fas fa-shopping-cart marginright1" aria-hidden="true"></i>Add</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4 col-12-medium">
                            <div class="border1">
                                <img src="/images/bakarwadi.jpg" width="170" height="137" alt=""/>
                                <div class="small bolder marginleft1 marginbottom1"> Bakarwadi, 150gm</div>
                                <div class="row textcenter">
                                    <div class="marginbottom1">₹ 55<a href="#" class="button-outline-sm marginleft1"><i class="fas fa-shopping-cart marginright1" aria-hidden="true"></i>Add</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4 col-12-medium">
                            <div class="border1">
                                <img src="/images/bakarwadi.jpg" width="170" height="137" alt=""/>
                                <div class="small bolder marginleft1 marginbottom1"> Bakarwadi, 150gm</div>
                                <div class="row textcenter">
                                    <div class="marginbottom1">₹ 55<a href="#" class="button-outline-sm marginleft1"><i class="fas fa-shopping-cart marginright1" aria-hidden="true"></i>Add</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4 col-12-medium">
                            <div class="border1">
                                <img src="/images/bakarwadi.jpg" width="170" height="137" alt=""/>
                                <div class="small bolder marginleft1 marginbottom1"> Bakarwadi, 150gm</div>
                                <div class="row textcenter">
                                    <div class="marginbottom1">₹ 55<a href="#" class="button-outline-sm marginleft1"><i class="fas fa-shopping-cart marginright1" aria-hidden="true"></i>Add</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4 col-12-medium">
                            <div class="border1">
                                <img src="/images/bakarwadi.jpg" width="170" height="137" alt=""/>
                                <div class="small bolder marginleft1 marginbottom1"> Bakarwadi, 150gm</div>
                                <div class="row textcenter">
                                    <div class="marginbottom1">₹ 55<a href="#" class="button-outline-sm marginleft1"><i class="fas fa-shopping-cart marginright1" aria-hidden="true"></i>Add</a></div>
                                </div>
                            </div>
                        </div>-->
                    </div>
                </div>
                <div id="Tab3" class="tabcontent">
                    <div class="row marginbottom1 paddingtop2">
                        <div class="col-4 col-12-medium paddingtop1">
                            <img src="/images/store-photo1.jpg" width="194" height="160" alt=""/>
                        </div>
                        <div class="col-4 col-12-medium paddingtop1">
                            <img src="/images/store-photo1.jpg" width="194" height="160" alt=""/>
                        </div>
                        <div class="col-4 col-12-medium paddingtop1">
                            <img src="/images/store-photo1.jpg" width="194" height="160" alt=""/>
                        </div>
                        <div class="col-4 col-12-medium paddingtop1">
                            <img src="/images/store-photo1.jpg" width="194" height="160" alt=""/>
                        </div>
                        <div class="col-4 col-12-medium paddingtop1">
                            <img src="/images/store-photo1.jpg" width="194" height="160" alt=""/>
                        </div>
                        <div class="col-4 col-12-medium paddingtop1">
                            <img src="/images/store-photo1.jpg" width="194" height="160" alt=""/>
                        </div>
                    </div>
                </div>
                <div id="Tab4" class="tabcontent">
                    <div class="row marginbottom1 padding-0">
                        <div class="col-6 col-12-medium">
                            <h3>Customer reviews</h3>
                        </div>
                        <div class="col-6 col-12-medium">
                            <div class="floatright"><span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                    </div>
                    <p class="text-muted">56 reviews</p>
                    <hr class="marginbottom2 margintop2">
                    <p>The best sweet shop in Kothrud. The food quality and taste is just beyond words. Absolute value for money. Anytime good for parties and occasion.</p>
                    <div class="marginbottom1"><img src="/images/profile-pic-review.jpg" width="36" height="36" alt="" class="marginright2 vmiddle"><span class="bolder">Amit Sharma</span></div>
                    <div class="text-muted"><span class="marginright2 small bolder">11 Likes</span><span class="marginright2 small bolder">3 Comments</span></div>
                    <div class="text-muted marginbottom1"><span class="marginright2 small bolder"><i class="far fa-thumbs-up marginright1"></i>Likes</span><span class="marginright2 small bolder"><i class="far fa-comment marginright1"></i>Comments</span></div>
                    <div class="row">
                        <div class="col-1"><img src="/images/profile-pic-review.jpg" width="36" height="36" alt="" class="marginright2 vmiddle"></div>
                        <div class="col-11 col-12-medium">
                            <textarea rows="1" placeholder="Write your comment" tabindex="1" width="100%"></textarea>
                        </div>
                    </div>
                    <hr class="marginbottom2 margintop2">
                </div>

                <div id="Tab5" class="tabcontent">
                    
                    <label class="show-btn" id="show_service"><i class="fa fa-plus" aria-hidden="true"></i></label>
                    
                    
                </div>
                
                <script>
                    function openCity(evt, cityName) {
                        var i, tabcontent, tablinks;
                        tabcontent = document.getElementsByClassName("tabcontent");
                        for (i = 0; i < tabcontent.length; i++) {
                            tabcontent[i].style.display = "none";
                        }
                        tablinks = document.getElementsByClassName("tablinks");
                        for (i = 0; i < tablinks.length; i++) {
                            tablinks[i].className = tablinks[i].className.replace(" active", "");
                        }
                        document.getElementById(cityName).style.display = "block";
                        evt.currentTarget.className += " active";
                    }
                </script>
            </div>
            <div class="col-4 col-12-medium"><br><br>
                <div class="floatright margintop3-minus"><a href="#" class="button-outline marginright1"><i class="far fa-bookmark marginright1" aria-hidden="true"></i>Bookmark</a><a href="#" class="button-outline floatright"><i class="fa fa-map-marker-alt marginright1" aria-hidden="true"></i>Direction</a></div>
                <div class="whitebg padding2 margintop3">
                    <h5>4 Visitors also viewing this</h5>
                    <div class="colored">
                        <ul>
                            <li>Pratiksha</li>
                            <li>John Doe</li>
                        </ul>
                    </div>
                </div>
                <div class="whitebg padding2 margintop2">
                    <h5><img src="/images/ad-300x250.jpg" width="300" height="250" alt=""/></h5>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
