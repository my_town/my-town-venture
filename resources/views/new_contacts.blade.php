@extends('layouts.new_MyTown')
@section('content')
@push('head')

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>

<style type="text/css">
    .suggested-modal-body, .business-modal-body{
    height: 60vh;
    overflow-y: auto;
}

</style>

@endpush

<!-- ======= Breadcrumbs ======= -->
<section id="breadcrumbs" class="breadcrumbs">
    <div class="container">
        <ol>
          <li><a href="index.html">Home</a></li>
          <li><a href="feed.html">Contacts</a></li>
        </ol>
    </div>
</section><!-- End Breadcrumbs -->

<main id="main">

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container">

        <div class="row">
        

          <div class="col-lg-8 entries">
          <br>

            <div class="col-md-12 text-center" id="allocation" style="margin-bottom: 30px;"> 


                <div class="form-check-inline">
              <label class="form-check-label">
                <input type="checkbox" class="form-check-input chb" id="all_contacts" value="1"><b>Suggestions</b>
              </label>
            </div>

                      <div class="form-check-inline">
              <label class="form-check-label">
                <input type="checkbox" class="form-check-input chb" id="accepted_contacts" value="0" checked><b>My Contacts</b>
              </label>
            </div>

            

</div>
          

         <div class="col-md-12">


             <?php
        $count = App\Http\Controllers\UserController::geContactCount();
       if($count<1){
        ?>

         <div class="gedf-card" style="text-align:center;">
                <div class="card-header" style="height: 100px;">

            <div><p>You do not have any contacts. Would you like to add any contact?</p>
            </div>

            <div class="rt-bt6">
          <button type="submit" class="btn btn-blue text-center">Yes</button>
                    <button onclick="window.location.href='{{ url('/my_feed') }}'" class="btn btn-blue text-center">No</button>
                </div>
                </div>
            </div>
    

                    <?php
                }else{
                    ?>
               
           

            <?php
                }
            ?>
        </div>

         
     

            <!-- ======= Team Section ======= -->
    <section id="team" class="team section-bg-team">
      <div class="container">

        <div class="section-title">
         <h4>Shoppers from your town</h4>
           <ul style="list-style-type:none; padding: 0;"><li>

            <input type="text" class="search-form" id="search" placeholder="Search Contacts by Location" style="width: 90%;"> <i class="icofont-search"></i></li> 
            </ul>
         <!-- <p data-aos="fade-up">Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>-->
        </div>

         <div class="suggested-contacts">
           <div class="row" id="postSuggest">
                </div>
         </div>


      </div>
    </section><!-- End Team Section -->
            <br>
            <!-- End blog comments -->

          </div><!-- End blog entries list -->

          <div class="col-lg-4">

            <div class="sidebar">

              <h3 class="sidebar-title">Search</h3>
              <div class="sidebar-item search-form">
                <form action="">
                  <input type="text" placeholder="Search Business Contacts">
                  <button type="submit"><i class="icofont-search"></i></button>
                </form>

              </div><!-- End sidebar search formn-->


              <h3 class="sidebar-title">Business Contacts</h3>
              <div class="sidebar-item recent-posts">
                <div class="post-item clearfix">
                  <img src="images/blog-recent-1.jpg" alt="">
                  <h4><a href="#">Nihil blanditiis at in nihil autem</a></h4>
                  <time datetime="2020-01-01">Town, City</time>
                </div>

                <div class="post-item clearfix">
                  <img src="images/blog-recent-2.jpg" alt="">
                  <h4><a href="#">Quidem autem et impedit</a></h4>
                  <time datetime="2020-01-01">Town, City</time>
                </div>

                <div class="post-item clearfix">
                  <img src="images/blog-recent-3.jpg" alt="">
                  <h4><a href="#">Id quia et et ut maxime similique occaecati ut</a></h4>
                  <time datetime="2020-01-01">Town, City</time>
                </div>

                <div class="post-item clearfix">
                  <img src="images/blog-recent-4.jpg" alt="">
                  <h4><a href="#">Laborum corporis quo dara net para</a></h4>
                  <time datetime="2020-01-01">Town, City</time>
                </div>

                <div class="post-item clearfix">
                  <img src="images/blog-recent-5.jpg" alt="">
                  <h4><a href="#">Et dolores corrupti quae illo quod dolor</a></h4>
                  <time datetime="2020-01-01">Town, City</time>
                </div>

              </div><!-- End sidebar recent posts-->

              

            </div><!-- End sidebar -->

          </div><!-- End blog sidebar -->

      </div>
      </div>
    </section><!-- End Blog Section -->

  </main><!-- End #main -->

  <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


   <script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
  
    $('#search').on('keyup',function(){

            var value=$(this).val();

            if ($('#all_contacts').is(":checked"))
                {
                var checkbox_value = "1";
                    }else{
                        var checkbox_value = "0";
                    }

            $.ajax({
            type : 'get',
            url : '{{URL::to('search')}}',
            data:{'search':value,'checkbox_val':checkbox_value},
            success:function(data){

           // $('#search_result').html(data);
            $('#postSuggest').html(data);
            
            $("button").click(function(e) {
                    e.preventDefault();

                        var me = $(this);
                        var user_id = me.val();
                            

                    $.ajax({
                        type: "get",
                        url: '{{URL::to('sendrequest')}}',
                        data: { 
                            'id': user_id
                        },
                        success: function(result) {
                          me.hide();
                        $('#request_title'+user_id).html("<b>Request Sent</b>");
                           
                        },
                        error: function(result) {
                            alert('error');
                        }
                    });
                });
            }
        });
    });

/*................Suggested Contacts....................................*/

$(document).ready(function(){

    $('.suggested-contacts').scroll(function(){

        
        if ($('#all_contacts').is(":checked"))
            {
                var checkbox_value = "1";
                }else{
                    var checkbox_value = "0";
                }

    
        var lastID = $('.load-more-more').attr('lastID');

        if(($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight) && (lastID != 0)){

            $.ajax({
                type:'get',
                url: '{{URL::to('suggested_contacts')}}',
                data:{id:lastID, checkbox_val:checkbox_value},                beforeSend:function(){
                    $('.load-more-more').show();
                },
                success:function(html){

                    $('.load-more-more').remove();
                    $('#postSuggest').append(html);

                    $("button").click(function(e) {
                                e.preventDefault();
                                
                                    var me = $(this);
                                    var user_id = me.val();

                                    if(user_id){
                                        
                                        $.ajax({
                                        type: "get",
                                        url: '{{URL::to('sendrequest')}}',
                                        data: { 
                                            'id': user_id
                                        },
                                        success: function(result) {
                                          me.hide();
                                        $('#request_title'+user_id).html("<b>Request Sent</b>");    
                                        },
                                        error: function(result) {
                                            alert('error');
                                        }
                                    });
                                        
                                    }else{
                                       
                                    }
                                });
                        }
                    });
                }
           
        });

 });

$(document).ready(function(){ 

    if ($('#all_contacts').is(":checked"))
            {
                var checkbox_value = "1";
                }else{
                    var checkbox_value = "0";
                }

            $.ajax({
                        type: "get",
                        url: '{{URL::to('suggested_few_contacts')}}',
                        data:{checkbox_val: checkbox_value},
                        success: function(result) {
                          $('#postSuggest').html(result);

                          $("button").click(function(e) {
                                e.preventDefault();
                                
                                    var me = $(this);
                                    var user_id = me.val();

                                    if(user_id){
                                        $.ajax({
                                        type: "get",
                                        url: '{{URL::to('sendrequest')}}',
                                        data: { 
                                            'id': user_id
                                        },
                                        success: function(result) {
                                          me.hide();
                                        $('#request_title'+user_id).html("<b>Request Sent</b>");    
                                        },
                                        error: function(result) {
                                            alert('error');
                                        }
                                    });
                                    }else{
                                       
                                    }
                            });
                        },
                        error: function(result) {
                            alert('error');
                        }
                    });
     });

/*..........................................................................*/

 $(".chb").change(function() {
    $(".chb").prop('checked', false);
    $(this).prop('checked', true);
});

 $(".chb").change(function() {
    $(".chb").not(this).prop('checked', false);
});


 $('#accepted_contacts').change(function () {
    $.ajax({
                        type: "get",
                        url: '{{URL::to('accepted_contacts')}}',
                        success: function(result) {
                          $('#postSuggest').html(result);
                        },
                        error: function(result) {
                            alert('error');
                        }
                    });

        });

 $('#all_contacts').change(function () {

    if ($('#all_contacts').is(":checked"))
            {
                var checkbox_value = "1";
                }else{
                    var checkbox_value = "0";
                }
    
    $.ajax({
                        type: "get",
                        url: '{{URL::to('suggested_few_contacts')}}',
                        data:{checkbox_val: checkbox_value},
                        success: function(result) {
                          $('#postSuggest').html(result);

                          $("button").click(function(e) {
                                e.preventDefault();
                                
                                    var me = $(this);
                                    var user_id = me.val();

                                    if(user_id){
                                        $.ajax({
                                        type: "get",
                                        url: '{{URL::to('sendrequest')}}',
                                        data: { 
                                            'id': user_id
                                        },
                                        success: function(result) {
                                          me.hide();
                                        $('#request_title'+user_id).html("<b>Request Sent</b>");    
                                        },
                                        error: function(result) {
                                            alert('error');
                                        }
                                    });
                                    }else{
                                       
                                    }
                            });
                        },
                        error: function(result) {
                            alert('error');
                        }
                    });

});

 </script>   

@endsection