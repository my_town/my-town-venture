@extends('layouts.new_MyTown')
@section('content')
@push('head')

 <script src="vendor/jquery/jquery.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>         
<script src="js/form_validation.js"></script> 

        <section class="shopping-cart-wrp">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-9 col-md-8 col-sm-12">
                        <div class="shopping-block">
                            <h2>Shopping Cart</h2>

    @foreach ($cart as $item)
    <?php $product=App\Models\Product::where('id',$item->id)->first();?>
                            <div class="shopping-box">
                                <div class="row">
                                    <div class="col-md-3 col-sm-4">
                                        <div class="box-img">
                                            <div class="form-group form-check">
                                                <input type="checkbox" name="" class="form-control" />
                                            </div>
                <img src="{{url('/')}}/storage/images/products/{{$product->product_image}}" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-7">
                                        <div class="shopping-info">
                                            <h4>{{$item->name}}</h4>
                                            <?php 

                                        $string = strip_tags($product->description);
                                            if (strlen($string) > 100) {

                                              
                                                $stringCut = substr($string, 0, 100);
                                                $endPoint = strrpos($stringCut, ' ');
                                               
                                                $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                                $string .= '... <a href="/this/story">Read More</a>';
                                            }
                                           

                                            ?>
                                            <p><?php echo $string;?></p>
                                            <label>In Stock</label>
                                            <p><span>Color :</span>Marble</p>
                                            <p><span>Size :</span>27cm</p>
                                            <ul>
                                                

                                                <li>
                                                    

<form method="POST" action="/cart/remove" id={{'removeForm' . $item->id}}>
                @csrf
            <input type="hidden" name="item_id" value={{ $item->getUniqueId() }} >
    <a href="javascript:$({{'removeForm' . $item->id}}).submit();">Delete</a>
            </form>


                                                </li>
    @if (Auth::user())
    <li><a href="#">Save for Later</a></li>
    @endif
                                                <li><a href="#">See More Like this</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-4">
                                        <div class="form-group">
                    
<form method="post" action="/cart/update_quantity" 
id={{'updateForm'. $item->id}}>
{{ csrf_field() }}
        <input type="number" value="{{$item->quantity}}" name="quantity" id = "qq" onchange="({{'updateForm' . $item->id}}).submit();" class="form-control" min="1" />
                <input type="hidden" name="product_id" value={{ $product->id }} >
                <input type="hidden" name="item_id" value={{ $item->getUniqueId() }}>
                <input type="hidden" name="name" value={{ $item->name }}>
                <input type="hidden" name="price" value={{ $product->actual_rate }}>
    </form>
                    
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-4">
                                        <div class="price">
                                            <h5>₹ {{ $item->quantity*$product->actual_rate }}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             @endforeach


                           <!--  <div class="shopping-box">
                                <div class="row">
                                    <div class="col-md-3 col-sm-4">
                                        <div class="box-img">
                                            <div class="form-group form-check">
                                                <input type="checkbox" name="" class="form-control" />
                                            </div>
                                            <img src="assets/img/image2.png" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-7">
                                        <div class="shopping-info">
                                            <h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
                                            <label class="red">Out of Stock</label>
                                            <p><span>Color :</span>Marble</p>
                                            <p><span>Size :</span>27cm</p>
                                            <ul>
                                                <li><a href="#">Delete</a></li>
                                                <li><a href="#">Save for Later</a></li>
                                                <li><a href="#">See More Like this</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-4">
                                        <div class="form-group">
                                            <input type="number" placeholder="1" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-4">
                                        <div class="price">
                                            <h5>Rs 200</h5>
                                        </div>
                                    </div>
                                </div>
                            </div> -->


                          <!--   <div class="shopping-box">
                                <div class="row">
                                    <div class="col-md-3 col-sm-4">
                                        <div class="box-img">
                                            <div class="form-group form-check">
                                                <input type="checkbox" name="" class="form-control" />
                                            </div>
                                            <img src="assets/img/image3.png" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-7">
                                        <div class="shopping-info">
                                            <h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
                                            <label>In Stock</label>
                                            <p><span>Color :</span>Marble</p>
                                            <p><span>Size :</span>27cm</p>
                                            <ul>
                                                <li><a href="#">Delete</a></li>
                                                <li><a href="#">Save for Later</a></li>
                                                <li><a href="#">See More Like this</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-4">
                                        <div class="form-group">
                                            <input type="number" placeholder="1" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-4">
                                        <div class="price">
                                            <h5>Rs 200</h5>
                                        </div>
                                    </div>
                                </div>
                            </div> -->


                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-12">
                        <div class="price-block">
                            <h2>Price Details</h2>

                            <ul>

    @foreach ($cart as $item)
    <?php $product=App\Models\Product::where('id',$item->id)->first();?>
                <li>{{$item->name}} (item {{$item->quantity}})<span>₹ {{ $item->quantity*$product->actual_rate }}</span></li>
    @endforeach
                <li class="total">Total Balance <span>₹{{$total}}</span></li>
                            </ul>
                            <!-- <a href="#" class="buy-button"></a> -->

@if(Auth::user())


<a href="{{ url('/save_shoppers_orders') }}" class="buy-button">Proceed to Checkout</a>

@else

<div style="padding-top: 20px;">
<!-- <form method="POST" action="{{ url('/save_orders') }}" id={{'placeForm'}}>  -->
<p class="guest"><a href="{{ url('/save_guest_orders') }}">Checkout As Guest?</a>
    <br>
 <span class="small">Want to checkout fast? Use our Guest checkout</span>
<br>OR<br><a href="{{ url('/save_shoppers_orders') }}">Login as Shopper?</a>
<br>OR<br><a href="{{ url('/new_register') }}">Register as Shopper?</a></p>
<p>Delivery to <span>Lorem Ipsum simply</span></p>
</div>

@endif       

        <!-- <form method="POST" action="{{ url('/save_orders') }}" id={{'placeForm'}}> -->
            <!-- @csrf -->
<?php
    //$item_count=Melihovv\ShoppingCart\Facades\ShoppingCart::count();
    //if($item_count>0){?>

 <!-- <button onclick="javascript:$({{'placeForm'}}).submit();" class="buy-button">Proceed to Checkout</button> -->

    <?php    
   // }
    ?>
       </div>
       <!--  </form>
                            <p>Delivery to <span>Lorem Ipsum simply</span></p> -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End shopping-cart -->

        <!-- ======= Footer ======= -->
        <footer id="footer">
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 footer-contact">
                            <h3>MyTown</h3>
                            <p>
                                A108 Adam Street <br />
                                New York, NY 535022<br />
                                United States <br />
                                <br />
                                <strong>Phone:</strong> +1 5589 55488 55<br />
                                <strong>Email:</strong> info@example.com<br />
                            </p>
                        </div>

                        <div class="col-lg-2 col-md-6 footer-links">
                            <h4>Useful Links</h4>
                            <ul>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
                            </ul>
                        </div>

                        <div class="col-lg-3 col-md-6 footer-links">
                            <h4>Our Services</h4>
                            <ul>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
                            </ul>
                        </div>

                        <div class="col-lg-4 col-md-6 footer-newsletter">
                            <h4>Join Our Newsletter</h4>
                            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                            <form action="" method="post"><input type="email" name="email" /><input type="submit" value="Subscribe" /></form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container d-lg-flex py-4">
                <div class="mr-lg-auto text-center text-lg-left">
                    <div class="copyright">
                        &copy; Copyright <strong><span>MyTown</span></strong>. All Rights Reserved
                    </div>
                </div>
                <div class="social-links text-center text-lg-right pt-3 pt-lg-0">
                    <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                    <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                    <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                    <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                    <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                </div>
            </div>
        </footer>
        <!-- End Footer -->

        <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

        <!-- Vendor JS Files -->
        <!-- <script src="/vendor/jquery/jquery.min.js"></script> -->
        <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="/vendor/jquery.easing/jquery.easing.min.js"></script>
        <script src="/vendor/php-email-form/validate.js"></script>
        <script src="/vendor/jquery-sticky/jquery.sticky.js"></script>
        <script src="/vendor/venobox/venobox.min.js"></script>
        <script src="/vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="/vendor/isotope-layout/isotope.pkgd.min.js"></script>
        <script src="/vendor/aos/aos.js"></script>

        <!-- Template Main JS File -->
        <script src="/js/js/main.js"></script>
   @endsection
