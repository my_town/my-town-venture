@push('head')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://kit.fontawesome.com/e3dc723f7b.js" crossorigin="anonymous"></script>
<script>
    $(document).ready( function () {
    $('#business').DataTable({
        "columnDefs": [{
            "targets":[4],
            "orderable":false,
             "className": 'dt-body-right'
        }]
    });
} );
</script>
@endpush

<x-app-layout>
    <div class="py-12">
    <!--<div style="margin-bottom:20px;width:100%;text-align:center;"><h3>Orders</h3></div>-->
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
            
		        <div class="p-6 sm:px-20 bg-white border-b border-gray-200">
		            <div class="mt-6 text-gray-900">
		                <div class="table-responsive">
                            <table id="business" class="display">
                                <thead>
                                    <tr>
                                        <th>Buyer</th>
                                        <th>Order date</th>
                                        <th>Total price</th>   
                                        <th>Actions</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($orders as $order)
                                    <tr>
                                        <td>{{$order->user->name}}<br>{{$order->user->place}}</td>
                                        <td>{{ $order->created_at }}</td>
                                        <td>{{ $order->total_price }}</td>
                                        <td>
                                            <a href="{{ url('/admin/order/'.$order->id) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            <a href="{{ url('/admin/orders/delete/'.$order->id) }}" onclick="return(confirm('Are you sure you want to delete order-'+'<?php echo $order->id;?>'+'?'))"><i class="fas fa-trash-alt"></i></a>  
                                        </td>  
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
		                </div><!-- table-responsive -->
		            </div><!-- mt-6 -->
		        </div><!-- p-6 -->
            </div>
        </div>
    </div>
</x-app-layout>