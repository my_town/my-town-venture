@extends('layouts.my-town-app')
@section('content')
@push('head')
<!--Stylesheet--------------------------->
<link rel="stylesheet" href="css/style.css"/>
<!--Fav-icon------------------------------>
<link rel="shortcut icon" href="images/fav-icon.png"/>
<!--poppins-font-family------------------->
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
<!--using-Font-Awesome-------------------->
<script src="https://kit.fontawesome.com/c8e4d183c2.js" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://kit.fontawesome.com/e3dc723f7b.js" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
a{
    text-decoration: none;
}
#testimonials{
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    width:100%;
}
.testimonial-heading{
    letter-spacing: 1px;
    margin: 30px 0px;
    padding: 10px 20px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
}
.testimonial-heading h1{
    font-size: 2.2rem;
    font-weight: 500;
    background-color: #202020;
    color: #ffffff;
    padding: 10px 20px;
}
.testimonial-heading span{
    font-size: 1.3rem;
    color: #252525;
    margin-bottom: 10px;
    letter-spacing: 2px;
    text-transform: uppercase;
}
.testimonial-box-container{
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
    width:100%;
}
.testimonial-box{
    width:700px;
    box-shadow: 2px 2px 30px rgba(0,0,0,0.1);
    background-color: #ffffff;
    padding: 20px;
    margin: 15px;
    cursor: pointer;
}
.profile-img{
    width:50px;
    height: 50px;
    border-radius: 50%;
    overflow: hidden;
    margin-right: 10px;
}
.profile-img img{
    width: 100%;
    height: 100%;
    object-fit: cover;
    object-position: center;
}
.profile{
    display: flex;
    align-items: center;
}
.name-user{
    display: flex;
    flex-direction: column;
}
.name-user strong{
    color: #3d3d3d;
    font-size: 1.1rem;
    letter-spacing: 0.5px;
}
.name-user span{
    color: #979797;
    font-size: 0.8rem;
}
.demo{
    display: inline;
}
.reviews{
    color: #f9d71c;
}
.box-top{
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 20px;
}
.client-comment p{
    font-size: 0.9rem;
    color: #4b4b4b;
}
.testimonial-box:hover{
    transform: translateY(-10px);
    transition: all ease 0.3s;
}
 
@media(max-width:1060px){
    .testimonial-box{
        width:45%;
        padding: 10px;
    }
}
@media(max-width:790px){
    .testimonial-box{
        width:100%;
    }
    .testimonial-heading h1{
        font-size: 1.4rem;
    }
}
@media(max-width:340px){
    .box-top{
        flex-wrap: wrap;
        margin-bottom: 10px;
    }
    .reviews{
        margin-top: 10px;
    }
}
::selection{
    color: #ffffff;
    background-color: #252525;
}
</style>
<script>
   $(document).ready(function(){
                $('#show_post_form').on('click',function(){
                    $('#post-box').show();
                });

                $('#hide_post_form').on('click',function(){
                    $('#post-box').hide();
                });
            });
</script>
<script>
         $(function() {
            <?php 
                $name=DB::table('businesses')->pluck('name');
                $place=DB::table('businesses')->pluck('address');
                $final=array();
                for ($i =0; $i < sizeof($name); $i++){
                    $final[$i]=$name[$i].", ".$place[$i];  
                }
                
            ?>
            var availableTutorials = <?php echo json_encode($final); ?>;
            $( "#business" ).autocomplete({
               source: availableTutorials
            });
         });
</script>

@endpush

<div class="box1" id="post-box">
    <div class="jumbotron"> 
        <div class="card">
            <label id="hide_post_form" class="close-btn fas fa-times"></label>
            <div class="card-body">
                <h5 class="card-title">Add your review</h5>
                <form action="{{ url('/posts/add_post') }}" method="POST">
                    @csrf
                    <div class = "mb-3">
                        <label for = "automplete-1" class="form-label">Search name of business: </label>
                        <input type="text" name="name" class="form-control" id = "business">
                    </div>
                    <div class = "mb-3">
                        <label>Review/Experience</label>
                        <textarea class="textarea" id="description" placeholder="Add your review or experience" name="experience"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="greybg content-box">
    <div class="container">
        <div class="row paddingbottom3">
            <div class="col-8 col-12-medium">
                <div class="tabcontent" style="display:block;">
                <label class="show-btn" id="show_post_form" ><i class="fa fa-plus" aria-hidden="true"></i></label>
                    <!--Testimonials------------------->
                    <section id="testimonials">
                        <!--testimonials-box-container------>
                        <div class="testimonial-box-container">
                        @foreach($posts as $post)
                            <?php 
                                $user=App\Models\User::where('id',$post->user_id)->first();
                                $business=App\Models\Business::where('id',$post->business_id)->first();
                            ?>
                            <!--BOX-1-------------->
                            <div class="testimonial-box">
                                <!--top------------------------->
                                <div class="box-top">
                                    <!--profile----->
                                    <div class="profile">
                                        <!--img---->
                                        <div class="profile-img">
                                            <img src="/images/profile.png" />
                                        </div>
                                        <!--name-and-username-->
                                        <div class="name-user">
                                            <strong>{{$user->name}}</strong>
                                            <span>{{$user->email}}</span>
                                        </div>
                                    </div>
                                    <!--reviews------>
                                    <div class="demo">
                                        <h5>{{$business->name}}</h5>
                                        <div class="reviews">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i><!--Empty star-->
                                        </div>
                                    </div>
                                </div>
                                <!--Comments---------------------------------------->
                                <div class="client-comment">
                                    <p>{{$post->experience}}</p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </section>
                </div>
            </div>
            <div class="col-4 col-12-medium">
                <div class="whitebg padding2 margintop3">
                    <h5>4 Visitors also viewing this</h5>
                    <div class="colored">
                        <ul>
                            <li>Pratiksha</li>
                            <li>John Doe</li>
                        </ul>
                    </div>
                </div>
                <div class="whitebg padding2 margintop2">
                    <h5><img src="/images/ad-300x250.jpg" width="300" height="250" alt=""/></h5>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection