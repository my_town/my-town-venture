@extends('layouts.my-town-app')


@section('content')
<div class="padding-tb-2" style="background: #F8F9FA;">
    <div class="container">
        <div class="row marginbottom1">
            <div class="col-12 col-12-medium">
                <!-- <div class="small marginbottom1">Kothrud, Pune, India</div> -->
                <h3>You searched for <?php echo $_GET['user_query'];?></h3>
                @foreach($businesses as $business)
                <div class="tabcontent marginbottom1" style="display:block;">
                    <div class="row">
                        <div class="col-3 col-12-medium"><a href="{{ url('biz-profile/'.$business->id) }}">
                        @if(empty($business->logo))
                        <img width="152" height="152" src="/images/logo-gray.jpg"> 
                        @else 
                        <img src="{{ asset('/storage/logo/'.$business->logo) }}" width="181" height="150" alt=""/>
                        @endif
                        </a>
                        </div>
                        <div class="col-9 col-12-medium">
                            <div class="row">
                                <div class="col-9 col-12-medium">
                                    <h3><a href="{{ url('biz-profile/'.$business->id) }}" style="text-decoration:none;">{{$business->name}}</a></h3>
                                    <div class="small bolder"><img src="images/location.png" width="14" height="20" alt="" class="vmiddle">&nbsp;&nbsp;{{ $business->address }}</div>
                                </div>
                                <div class="col-3 col-12-medium">
                                    <div class="small bolder"><img src="images/user.png" width="16" height="16" alt="" class="vmiddle">&nbsp;&nbsp;1000 Contacts</div>
                                </div>
                            </div>
                            <div class="margintop1">{{ $business->description }} </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <!--<div class="tabcontent marginbottom1" style="display:block;">
                    <div class="row">
                        <div class="col-3 col-12-medium"><img src="images/store-photo-th.png" width="181" height="120" alt=""/>
                        </div>
                        <div class="col-9 col-12-medium">
                            <div class="row">
                                <div class="col-9 col-12-medium">
                                    <h3>Chacha Halwai</h3>
                                    <div class="small bolder"><img src="images/location.png" width="14" height="20" alt="" class="vmiddle">&nbsp;&nbsp;Kothrud, Pune, India</div>
                                </div>
                                <div class="col-3 col-12-medium">
                                    <div class="small bolder"><img src="images/user.png" width="16" height="16" alt="" class="vmiddle">&nbsp;&nbsp;1000 Contacts</div>
                                </div>
                            </div>
                            <div class="margintop1">Making way for a hearty meal is Chacha Halwai in Pune. This place is synonymous with delicious food that can satiate all food cravings. </div>
                        </div>
                    </div>
                </div>
                <div class="tabcontent marginbottom1" style="display:block;">
                    <div class="row">
                        <div class="col-3 col-12-medium"><img src="images/store-photo-th.png" width="181" height="120" alt=""/>
                        </div>
                        <div class="col-9 col-12-medium">
                            <div class="row">
                                <div class="col-9 col-12-medium">
                                    <h3>Chacha Halwai</h3>
                                    <div class="small bolder"><img src="images/location.png" width="14" height="20" alt="" class="vmiddle">&nbsp;&nbsp;Kothrud, Pune, India</div>
                                </div>
                                <div class="col-3 col-12-medium">
                                    <div class="small bolder"><img src="images/user.png" width="16" height="16" alt="" class="vmiddle">&nbsp;&nbsp;1000 Contacts</div>
                                </div>
                            </div>
                            <div class="margintop1">Making way for a hearty meal is Chacha Halwai in Pune. This place is synonymous with delicious food that can satiate all food cravings. </div>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
</div>

@endsection