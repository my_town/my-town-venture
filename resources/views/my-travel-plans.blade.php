@extends('layouts.my-town-app')
@section('content')
@push('head')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://kit.fontawesome.com/e3dc723f7b.js" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
    $('#my_travel_plan').DataTable( {
        "columnDefs": [{
            "targets":[3],
            "orderable":false,
            "className": 'dt-body-right'
        }]
    } );
} );
</script>
@endpush
<script>
   $(document).ready(function(){
                $('#showTravelPlanForm').on('click',function(){
                    $('#travel-plan-box').show();
                });

                $('#hide_travel_plan_form').on('click',function(){
                    $('#travel-plan-box').hide();
                });
            });
</script>

<div class="box1" id="travel-plan-box">

    <div class="jumbotron"> 
        <div class="card">
            <label id="hide_travel_plan_form" class="close-btn fas fa-times"></label>
            <div class="card-body" >
                <h5 class="card-title">Travel Plan Details</h5>
                <form action="{{ url('my-travel-plans/add_travel_plan') }}" method="POST">
                    @csrf
                    <div class="mb-3">
                        <label class="form-label">Date:</label>
                        <input type="date" class="form-control" id="date" name="date">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">From:</label>
                        <input type="text" class="form-control" id="from" name="from">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">To:</label>
                        <input type="text" class="form-control" id="to" name="to">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="greybg content-box">
    <div class="container">
        <div class="row paddingbottom3">
            <div class="col-8 col-12-medium">
                <div id="Tab1" class="tabcontent" style="display:block;">

                <label class="show-btn" id="showTravelPlanForm" ><i class="fa fa-plus" aria-hidden="true"></i></label>

                <div class="p-6 sm:px-20 bg-white border-b border-gray-200">                
		<div class="mt-6 text-gray-900">
		<div class="table-responsive">
        @if(!empty($my_travel_plans))
        <table id="my_travel_plan" class="display">
            <thead>
                <tr>
                    <th>DATE</th>
                    <th>FROM</th>      
                    <th>TO</th>
                    <th>ACTIONS</th>
                </tr>
            </thead>
            <tbody>
                @foreach($my_travel_plans as $my_travel_plan)
                <tr>
                    <td>{{ $my_travel_plan->date }}</td>
                    <td>{{ $my_travel_plan->from }}</td>
                    <td>{{ $my_travel_plan->to }}</td>
                    <td><a href = "my-travel-plans/delete/{{$my_travel_plan->id}}"><i class="fas fa-trash-alt"></a></i></td> 
                
                
                
                </tr>
                @endforeach
            </tbody>
            </table>
            @endif
		</div><!-- table-responsive -->
		</div><!-- mt-6 -->
		</div><!-- p-6 -->
           <p>&nbsp;</p>
                </div>
                <div id="Tab4" class="tabcontent">
                    <div class="row marginbottom1 padding-0">
                        <div class="col-6 col-12-medium">
                            <h3>Customer reviews</h3>
                        </div>
                        <div class="col-6 col-12-medium">
                            <div class="floatright"><span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                    </div>
                    <p class="text-muted">56 reviews</p>
                    <hr class="marginbottom2 margintop2">
                    <p>The best sweet shop in Kothrud. The food quality and taste is just beyond words. Absolute value for money. Anytime good for parties and occasion.</p>
                    <div class="marginbottom1"><img src="/images/profile-pic-review.jpg" width="36" height="36" alt="" class="marginright2 vmiddle"><span class="bolder">Amit Sharma</span></div>
                    <div class="text-muted"><span class="marginright2 small bolder">11 Likes</span><span class="marginright2 small bolder">3 Comments</span></div>
                    <div class="text-muted marginbottom1"><span class="marginright2 small bolder"><i class="far fa-thumbs-up marginright1"></i>Likes</span><span class="marginright2 small bolder"><i class="far fa-comment marginright1"></i>Comments</span></div>
                    <div class="row">
                        <div class="col-1"><img src="/images/profile-pic-review.jpg" width="36" height="36" alt="" class="marginright2 vmiddle"></div>
                        <div class="col-11 col-12-medium">
                            <textarea rows="1" placeholder="Write your comment" tabindex="1" width="100%"></textarea>
                        </div>
                    </div>
                    <hr class="marginbottom2 margintop2">
                </div>
            </div>
            <div class="col-4 col-12-medium">
                <div class="whitebg padding2 margintop3">
                    <h5>4 Visitors also viewing this</h5>
                    <div class="colored">
                        <ul>
                            <li>Pratiksha</li>
                            <li>John Doe</li>
                        </ul>
                    </div>
                </div>
                <div class="whitebg padding2 margintop2">
                    <h5><img src="/images/ad-300x250.jpg" width="300" height="250" alt=""/></h5>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
