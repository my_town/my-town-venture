@extends('layouts.new_MyTown')
@section('content')
@push('head')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

<!-- <script src="//code.jquery.com/jquery-1.12.1.js"></script>  --> 
<!-- <link href="vendor/aos/aos.css" rel="stylesheet"> -->
<!-- <script src="https://code.jquery.com/jquery-2.1.4.js"></script> -->

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="js/form_validation.js"></script>  


<script>

$(function (){
    $("#locations_search").autocomplete({

      source :function( request, response ) {
        $.ajax({
           url: "{{ url('/location/search') }}",
           dataType: "json",
           data: {
              q: request.term 
           },
           success: function( data ) {
               response($.map(data.predictions, function(item) {
                     return {
                         label : item.description,
                         value : item.place_id
                     };
               }));
           }
        });
       },
       select: function (event, ui) {
             $("#locations_search").val(ui.item.label);
             $("#places_id").val(ui.item.value);
             return false;
       },
       focus: function(event, ui) {
        $("#locations_search").val(ui.item.label);
        return false;
      },
   });
});
</script>

<style>
.ui-autocomplete-loading { background:url('/images/loading.gif') no-repeat right center }

.error{
    color: red;
}
</style>
@endpush

<!-- ======= Hero Section ======= -->
<div id="carouselExampleIndicators" class="carousel slide hp-slider" data-ride="carousel">
 
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block1 w-100" src="{{url('/')}}/images/thank-you-slide.jpeg" alt="First slide">
      <!--<button type="button" class="btn btn-primary btn-lg" style="position: absolute; top: 130px; right: 50px; "><a href="purohit_products.html">Buy Products</a></button> -->   
    </div>
  
  </div>
</div>
<!-- End Hero -->

<!-- ======= Clients Section ======= -->
<section id="clients" class="clients">
   <div class="container aos-init aos-animate" data-aos="fade-up">
    <div style="margin-bottom: 20px; "><h5>Mithai Shops - Pune, India</h5></div>
        <div class="owl-carousel clients-carousel owl-loaded owl-drag">
        <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-2405px, 0px, 0px); transition: all 0.25s ease 0s; width: 3700px;"><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/haldirams.png" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/mithas.png" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/mulchand-sweets.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/kakahalwai.png" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/karachi.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/bhavnagri.jpg" alt=""></div><div class="owl-item" style="width: 185px;"><a href="business_owner_profile_view.html"><img src="{{url('/')}}/images/products/purohit.jpg" alt=""></a></div><div class="owl-item" style="width: 185px;"><img src="{{url('/')}}/images/products/chitale.png" alt=""></div><div class="owl-item" style="width: 185px;"><img src="{{url('/')}}/images/products/haldirams.png" alt=""></div><div class="owl-item" style="width: 185px;"><img src="{{url('/')}}/images/products/mithas.png" alt=""></div><div class="owl-item" style="width: 185px;"><img src="{{url('/')}}/images/products/mulchand-sweets.jpg" alt=""></div><div class="owl-item" style="width: 185px;"><img src="{{url('/')}}/images/products/kakahalwai.png" alt=""></div><div class="owl-item" style="width: 185px;"><img src="{{url('/')}}/images/products/karachi.jpg" alt=""></div><div class="owl-item active" style="width: 185px;"><img src="{{url('/')}}/images/products/bhavnagri.jpg" alt=""></div><div class="owl-item cloned active" style="width: 185px;"><a href="business_owner_profile_view.html"><img src="{{url('/')}}/images/products/purohit.jpg" alt=""></a></div><div class="owl-item cloned active" style="width: 185px;"><img src="{{url('/')}}/images/products/chitale.png" alt=""></div><div class="owl-item cloned active" style="width: 185px;"><img src="{{url('/')}}/images/products/haldirams.png" alt=""></div><div class="owl-item cloned active" style="width: 185px;"><img src="{{url('/')}}/images/products/mithas.png" alt=""></div><div class="owl-item cloned active" style="width: 185px;"><img src="{{url('/')}}/images/products/mulchand-sweets.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/kakahalwai.png" alt=""></div></div></div></div>

  </div>
</section>
<!-- End Clients Section -->

<!-- ======= Clients Section ======= -->
<section id="clients" class="clients">
   <div class="container aos-init aos-animate" data-aos="fade-up">
    <div style="margin-bottom: 20px; "><h5>Kala Niketan - Bandra, Mumbai, India - <a href="#">Visit Store</a></h5></div>
        <div class="owl-carousel clients-carousel owl-loaded owl-drag">
        <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-1480px, 0px, 0px); transition: all 0.25s ease 0s; width: 3700px;"><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree3.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree4.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree5.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree6.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree7.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree8.jpg" alt=""></div><div class="owl-item" style="width: 185px;"><a href="#"><img src="{{url('/')}}/images/products/saree1.jpg" alt=""> <span class="text-small">Rs. 1000</span></a></div><div class="owl-item" style="width: 185px;"><img src="{{url('/')}}/images/products/saree2.jpg" alt=""></div><div class="owl-item active" style="width: 185px;"><img src="{{url('/')}}/images/products/saree3.jpg" alt=""></div><div class="owl-item active" style="width: 185px;"><img src="{{url('/')}}/images/products/saree4.jpg" alt=""></div><div class="owl-item active" style="width: 185px;"><img src="{{url('/')}}/images/products/saree5.jpg" alt=""></div><div class="owl-item active" style="width: 185px;"><img src="{{url('/')}}/images/products/saree6.jpg" alt=""></div><div class="owl-item active" style="width: 185px;"><img src="{{url('/')}}/images/products/saree7.jpg" alt=""></div><div class="owl-item active" style="width: 185px;"><img src="{{url('/')}}/images/products/saree8.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><a href="#"><img src="{{url('/')}}/images/products/saree1.jpg" alt=""> <span class="text-small">Rs. 1000</span></a></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree2.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree3.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree4.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree5.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree6.jpg" alt=""></div></div></div></div>

  </div>
</section>
<!-- End Clients Section -->

<!-- ======= Values Section ======= -->
<section id="values" class="values">
      <div class="container">

        <div class="row">
          <div class="col-md-6 d-flex align-items-stretch aos-init aos-animate" data-aos="fade-up">
            <div class="card" style="background-image: url({{url('/')}}/images/values-1.jpg);">
              <div class="card-body">
                <h5 class="card-title"><a href="">Home Town - Pune, India</a></h5>
                <p class="card-text"></p>
            <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Enter</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch mt-4 mt-md-0 aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
            <div class="card" style="background-image: url({{url('/')}}/images/artifacts.jpg);">
              <div class="card-body">
                <h5 class="card-title"><a href="">The Bombay Store - Mumbai, India</a></h5>
                <p class="card-text"></p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Enter  Store</a></div>
              </div>
            </div>

          </div>
          <div class="col-md-6 d-flex align-items-stretch mt-4 aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">
            <div class="card" style="background-image: url({{url('/')}}/images/furniture.jpg);">
              <div class="card-body">
                <h5 class="card-title"><a href="">Furniture: <i class="icofont-location-pin"></i>London, UK</a></h5>
                <p class="card-text"> </p>
               <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Visit Store</a></div> 
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch mt-4 aos-init aos-animate" data-aos="fade-up" data-aos-delay="300">
            <div class="card" style="background-image: url({{url('/')}}/images/chocolates.jpg);">
              <div class="card-body">
                <h5 class="card-title"><a href="">Chocolates: <i class="icofont-location-pin"></i>Basel, Switzerland</a></h5>
                <p class="card-text"></p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Visit Store</a></div>
              </div>
            </div>
          </div>
        </div>

  </div>
</section>
<!-- End Values Section -->

<!-- ======= Clients Section ======= -->
<section id="clients" class="clients">
   <div class="container aos-init aos-animate" data-aos="fade-up">
    <div style="margin-bottom: 20px; "><h5>Kala Niketan - Bandra, Mumbai, India - <a href="#">Visit Store</a></h5></div>
        <div class="owl-carousel clients-carousel owl-loaded owl-drag">
        <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-1480px, 0px, 0px); transition: all 0.25s ease 0s; width: 3700px;"><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree3.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree4.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree5.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree6.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree7.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree8.jpg" alt=""></div><div class="owl-item" style="width: 185px;"><a href="#"><img src="{{url('/')}}/images/products/saree1.jpg" alt=""> <span class="text-small">Rs. 1000</span></a></div><div class="owl-item" style="width: 185px;"><img src="{{url('/')}}/images/products/saree2.jpg" alt=""></div><div class="owl-item active" style="width: 185px;"><img src="{{url('/')}}/images/products/saree3.jpg" alt=""></div><div class="owl-item active" style="width: 185px;"><img src="{{url('/')}}/images/products/saree4.jpg" alt=""></div><div class="owl-item active" style="width: 185px;"><img src="{{url('/')}}/images/products/saree5.jpg" alt=""></div><div class="owl-item active" style="width: 185px;"><img src="{{url('/')}}/images/products/saree6.jpg" alt=""></div><div class="owl-item active" style="width: 185px;"><img src="{{url('/')}}/images/products/saree7.jpg" alt=""></div><div class="owl-item active" style="width: 185px;"><img src="{{url('/')}}/images/products/saree8.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><a href="#"><img src="{{url('/')}}/images/products/saree1.jpg" alt=""> <span class="text-small">Rs. 1000</span></a></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree2.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree3.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree4.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree5.jpg" alt=""></div><div class="owl-item cloned" style="width: 185px;"><img src="{{url('/')}}/images/products/saree6.jpg" alt=""></div></div></div></div>

  </div>
</section>
<!-- End Clients Section -->
@endsection