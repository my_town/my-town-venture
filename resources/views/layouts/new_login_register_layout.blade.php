<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>MyTown - Homepage</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="images/img/favicon.png" rel="icon">
  <link href="images/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="vendor/venobox/venobox.css" rel="stylesheet">
  <link href="vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="css/css/style2.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Flexor - v2.4.1
  * Template URL: https://bootstrapmade.com/flexor-free-multipurpose-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
  @stack('head')
</head>

<body>
@section('header')
<!-- ======= Top Bar ======= -->
<section id="topbar" class="d-none d-lg-block">
    <div class="container d-flex">
	    <div class="logo mr-auto">
            <a href="/"><img src="images/img/mytown-logo.png" alt="MyTown Logo"></a>        <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
        </div>
        <div class="contact-info mr-auto">
            <ul> 
                <li>
                    <input type="text" class="search-form" id="name" placeholder="Enter Location e.g. India, Mumbai">
                    <input type="text" class="search-form" placeholder="Search Product or Services"> <i class="icofont-search"></i>
                </li>
	        </ul>
        </div>
        <div class="cta">
            @guest
	            <a href="/new_register" class="scrollto">Sign Up</a>
            @endguest 
            @auth
				@php
					$place_ar = explode(",",@\Auth::user()->place );
				@endphp
                                
                <a href="/dashboard" class="button-outline-gray marginright1">{{ (\Auth::user()->name)}}, {{ $place_ar[0] }}</a>&nbsp;&nbsp;
            @endauth
            @guest 
                <a href="/new_login" class="scrollto">Login</a>
            @endguest 
            @auth
                <a href="{{ route('logout') }}" class="button-solid marginright1" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            @endauth 
	    </div>
    </div>
</section>
<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

    <!-- Vendor JS Files -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/jquery.easing/jquery.easing.min.js"></script>
    <script src="vendor/php-email-form/validate.js"></script>
    <script src="vendor/jquery-sticky/jquery.sticky.js"></script>
    <script src="vendor/venobox/venobox.min.js"></script>
    <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
    <script src="vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="vendor/aos/aos.js"></script>

    <!-- Template Main JS File -->
    <script src="js/js/main.js"></script>
  @show
        
<div>@yield('content')</div>


</body>

</html>