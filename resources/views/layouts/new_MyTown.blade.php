<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>MyTown - Homepage</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{url('/')}}/images/img/favicon.png" rel="icon">
  <link href="images/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{url('/')}}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{url('/')}}/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="{{url('/')}}/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="{{url('/')}}/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="{{url('/')}}/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="{{url('/')}}/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{url('/')}}/css/css/style2.css" rel="stylesheet">

  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">  
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" defer></script>  
<script src="https://code.jquery.com/jquery-2.1.4.js"></script>

  <script>
$(function (){
    $("#location_search").autocomplete({
      source :function( request, response ) {
        $.ajax({
           url: "{{ url('/location/search') }}",
           dataType: "json",
           data: {
              q: request.term 
           },
           success: function( data ) {
               response($.map(data.predictions, function(item) {
                     return {
                         label : item.description,
                         value : item.place_id
                     };
               }));
           }
        });
       },
       select: function (event, ui) {
             $("#location_search").val(ui.item.label);
             $("#place_id").val(ui.item.value);
             return false;
       },
       focus: function(event, ui) {
        $("#location_search").val(ui.item.label);
        $("#place_id").val(ui.item.value);
        return false;
      },
   });
});


$(function (){
$( "#product_search" ).autocomplete({
        source: function( request, response ) {
          // Fetch data
          $.ajax({
            url: "{{ url('/product/search') }}",
            type: 'get',
            dataType: "json",
            data: {
               search: request.term
            },
            success: function( data ) {
               response( data );
            }
          });
        },
        select: function (event, ui) {
           // Set selection
           $('#product_search').val(ui.item.label); // display the selected text
           $('#productid').val(ui.item.value); // save selected id to input
           return false;
        },

       focus: function(event, ui) {
        $("#product_search").val(ui.item.label);
        return false;
            },
      });

    });
</script>
  <!-- =======================================================
  * Template Name: Flexor - v2.4.1
  * Template URL: https://bootstrapmade.com/flexor-free-multipurpose-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
  @stack('head')

</head>

<body>
@section('header')
<!-- ======= Top Bar ======= -->
<section id="header" class="topbar d-none d-lg-block">
    <div class="container1 d-flex">
        <div class="logo mr-auto" >
            <a href="{{url('/')}}"><img src="{{url('/')}}/images/img/mytown-logo.png" alt="MyTown Logo" ></a></br>

    
             @if (Auth::user())
                <span class="category-icon-shopper"><img src="{{url('/')}}/images/img/shop_icon.png" alt=""/> Business</span>
            @else
              <span class="category-icon-shopper"><img src="{{url('/')}}/images/img/shopper_icon.png" alt=""/> Shopper</span>
            @endif()


            
            

                    <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
        </div>

       <!--  <form class="form-inline  my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Enter Location e.g. Kothrud, Pune, India" aria-label="Enter Location e.g. Kothrud, Pune, India">  
            <input class="form-control mr-sm-2" type="search" placeholder="Search Contacts, Product or Service Business" aria-label="Search Contacts, Product or Service Business">
            <button class="btn btn-primary my-2 my-sm-0" type="submit">Search</button>
        </form> -->

<form class="form-inline my-2 my-lg-0" action="{{ url('/search_result') }}" method="get">
    <!-- {{ csrf_field() }} -->
<input class="form-control mr-sm-2" id="location_search" type="text" name="location_search"  autofocus autocomplete="location_search" placeholder="Enter Location">
<input id="place_id" type="hidden" name="place_id"  value="">

          <!-- <input class="form-control mr-sm-2" type="search" placeholder="Enter Location" aria-label="Enter Location"> -->  

<input class="form-control mr-sm-2" type="text" id="product_search" name="product_search"
  autofocus autocomplete="location_search" placeholder="Product Type or Service Type" aria-label="Product Type or Service Type">
 <input id="productid" type="hidden" name="productid" value="">

      <button class="btn btn-primary my-2 my-sm-0" type="submit">Search</button>
    
    </form>



        <!--<div class="contact-info mr-auto">
            <ul> 
                <li>
                    <a href="#"> <i class="icofont-shopping-cart" style="padding-right: 30px; font-size: 30px;"></i></a>  
                    <a href="#"><i class="icofont-bell-alt"> <em class="number5">0</em></i></a>
                </li>
                <li class="cta"><a href="register.html" class="scrollto">Sign Up</a></li>
                <li class="cta"> <a href="login.html" class="scrollto">Login</a></li>
           </ul>
        
      </div>-->
     
        <div class="contact-info mr-auto">
            
            <ul>
                 @guest
                <li>
                    @if (Auth::user())
                    <a href="{{ url('/cart') }}"><i class="icofont-shopping-cart" style="margin-right: 30px; font-size: 30px;"></i></a>
                    @else
                    <a href="{{ url('/cart') }}"><i class="icofont-shopping-cart" style="margin-right: 30px; font-size: 30px;">
                        <em class="number5">
                            <?php
                        Melihovv\ShoppingCart\Facades\ShoppingCart::restore(Session::get('uuid'));
                    $item_count=Melihovv\ShoppingCart\Facades\ShoppingCart::count();
                    print_r($item_count);
                            ?>
                        </em>
                    </i></a>

                    @endif


                    @if (Auth::user())
                        <?php
                            $notification_count = App\Http\Controllers\UserController::getNotificationCount(Auth::user()->id);
                        ?>
                    @else
                        <?php
                            $notification_count = 0;
                        ?>
                    @endif()

                    @if(@$notification_count>0)
                        <a href="{{ url('/notification_details') }}"><i class="icofont-bell-alt"> <em class="number5">
                    @elseif(@$notification_count<1)
                        <a href=""><i class="icofont-bell-alt"> <em class="number5"> 
                    @else
                        <a href="{{ url('/new_login') }}"><i class="icofont-bell-alt"> <em class="number5">
                    @endif
                    @if (Auth::user())
                        <?php 
                            echo $notification_count; 
                        ?>

                    @endif()0
                    </em></i></a>
                </li>
                @endguest

            @guest
                <li>
                    <!-- <a href="/new_register" class="scrollto">Sign Up</a> -->

                    <span class="user-name"> 
      <p class="guest">Welcome Guest!<br>
        <a href="{{url('/new_register')}}" style="color:#1a237e">SIGN UP</a>| 
        <a href="{{url('/new_login')}}" style="color:#1a237e">LOGIN</a></p>
  </span>

                </li>
            @endguest
            @auth
                @php
                    $place_ar = explode(",",@\Auth::user()->place );
                @endphp 
                <li>
                    <a href="{{ url('/cart') }}"><i class="icofont-shopping-cart" style="margin-right: 30px; font-size: 30px;">
                        <em class="number5">
                          <?php $x=0; $item_count=0;?>  
                    @if (Auth::user())
                        <?php
                        $x=Auth:: user()->businesses->first();
                                        Melihovv\ShoppingCart\Facades\ShoppingCart::restore(Session::get('uuid'));
                    $item_count= Melihovv\ShoppingCart\Facades\ShoppingCart::count();

                    print_r($item_count); 
                        ?>
                    @endif()
                    </em></i></a>
                    @if (Auth::user())
                        <?php
                            $notification_count = App\Http\Controllers\UserController::getNotificationCount(Auth::user()->id);
                        ?>
                    @else
                        <?php
                            $notification_count = 0;
                        ?>
                    @endif()

                    @if(@$notification_count>0)
                        <a href="{{ url('/notification_details') }}"><i class="icofont-bell-alt"> <em class="number5">
                    @else
                        <a href=""><i class="icofont-bell-alt"> <em class="number5"> 
                    @endif
                    @if (Auth::user())
                        <?php 
                            echo $notification_count; 
                        ?>
                    @endif()
                    </em></i></a>
  
                    <span class="user-name dropdown"> 
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" style="color: #1a237e;">
                            @if (Auth::user())
                                <p>{{ Auth::user()->name }}<br>{{ $place_ar[0] }}
                             @if(isset($place_ar[1]))   
                                 ,{{ substr($place_ar[1], 0, 100) }}
                              @endif()   
                              @if(isset($place_ar[2]))   
                              ,<br>{{ $place_ar[2] }}</p>
                            @endif()   
                            @if(isset($place_ar[3]))   
                            ,<br>{{ $place_ar[3] }}</p>
                          @endif()   
                            @endif()  
                        </a>
                        <div class="dropdown-menu">
                            <div class="menu-line">
                                <ul>
                                    <strong>Your Lists</strong>
                                    <li><a href="#">Create a Wish List</a></li>
                                    <li><a href="#">Create a Wish List</a></li>
                                    <li><a href="#">Create a Wish List</a></li>
                                </ul>
        
                                <ul style="border-right: none;">
                                    <strong>Your Account</strong>
                                    <li><a href="#">Your Account</a></li>
                                    <li><a href="#">Your Orders</a></li>
                                    <li>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </span>
                </li>
                @endauth
            @guest 
            <!-- <li class="cta"><a href="/new_login" class="scrollto">Login</a></li> -->
            @endguest 
            </ul>
        </div>
    </div>
</section>


<!-- ======= Header ======= -->
<!-- biz-owner nav; changes needed -->
@if(Auth::user() && Auth::user()->businesses->count() > 0)

<!-- ======= Header ======= -->
<header class="topbar2">
    <div class="container d-flex">
        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li><a href="#">Dashboard</a></li>
                <li><a href="#">Business Profile</a></li>
                <li><a href="#portfolio">Sales History</a></li>
                <li><a href="#team">Posts</a></li>
            </ul>
        </nav><!-- .nav-menu -->
    </div>
</header><!-- End Header -->

<!-- shopper nav -->
@elseif (Auth::user())


<header class="topbar2">
    <div class="container d-flex">
        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <!-- <li><a href="{{ url('/my_feed') }}">Feed</a></li>
                <li><a href="{{ url('/contacts') }}">Contacts</a></li>
                <li><a href="{{ url('/business-contacts') }}">Business Contacts</a></li>
                <li><a href="#portfolio">Purchase History</a></li>
                <li><a href="#team">Posts</a></li>
                <li><a href="#pricing">My Travel Plans</a></li> -->
                <li><a href="{{ url('/my_feed') }}">Feed</a></li>
                <li><a href="{{ url('/contacts') }}">Contacts</a></li>
                <li><a href="{{ url('/business-contacts') }}">Business Contacts</a></li>
                <li><a href="#portfolio">Purchase History</a></li>            
                <li><a href="#team">Posts</a></li>
                <li class="pad-l"><a href="#team">My Shipments</a></li>
                <li><a href="#pricing">My Travel Plans</a></li>
            </ul>
        </nav><!-- .nav-menu -->
    </div>
</header><!-- End Header -->
@else


  <header class="topbar2">
    <div class="container d-flex">
        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li class="active"><a href="{{ url('/') }}"><i class="icofont-home"></i> Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#services">How It Works</a></li>
                <li><a href="#team">My Network</a></li>
                <li><a href="#pricing">Create Your Profile</a></li>
                <li><a href="#portfolio">Contact Us</a></li>
            </ul>
        </nav><!-- .nav-menu -->
    </div>
</header><!-- End Header -->
@endif

@show

<div>@yield('content')</div>

@section('footer')
    <!-- ======= Footer ======= -->
    <footer id="footer">
        <!-- <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 footer-contact">
                        <h3>MyTown</h3>
                        <p>
                            A108 Adam Street <br>
                            New York, NY 535022<br>
                            United States <br><br>
                            <strong>Phone:</strong> +1 5589 55488 55<br>
                            <strong>Email:</strong> info@example.com<br>
                        </p>
                    </div>

                    <div class="col-lg-2 col-md-6 footer-links">
                        <h4>Useful Links</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Our Services</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-4 col-md-6 footer-newsletter">
                        <h4>Join Our Newsletter</h4>
                        <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                        <form action="" method="post">
                            <input type="email" name="email"><input type="submit" value="Subscribe">
                        </form>
                    </div>

                </div>
            </div>
        </div> -->

        

       <!--  <div class="container d-lg-flex py-4">

            <div class="mr-lg-auto text-center text-lg-left">
                <div class="copyright">
                    &copy; Copyright <strong><span>MyTown</span></strong>. All Rights Reserved
                </div> -->
                <!-- <div class="credits">
                    All the links in the footer should remain intact. -->
                    <!-- You can delete the links only if you purchased the pro version. -->
                    <!-- Licensing information: https://bootstrapmade.com/license/ -->
                    <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/flexor-free-multipurpose-bootstrap-template/ 
                    Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
                </div>-->
       
    </footer><!-- End Footer -->

    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

    <!-- Vendor JS Files -->
    <!-- <script src="vendor/jquery/jquery.min.js"></script> -->
    <script src="{{url('/')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="{{url('/')}}/vendor/jquery.easing/jquery.easing.min.js"></script>
    <script src="{{url('/')}}/vendor/php-email-form/validate.js"></script>
    <script src="{{url('/')}}/vendor/jquery-sticky/jquery.sticky.js"></script>
    <script src="{{url('/')}}/vendor/venobox/venobox.min.js"></script>
    <script src="{{url('/')}}/vendor/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{url('/')}}/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="{{url('/')}}/vendor/aos/aos.js"></script>

    <!-- Template Main JS File -->
    <script src="{{url('/')}}/js/js/main.js"></script>
    @show
</body>

</html>
