<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>MyTown - Thank you</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{url('/')}}/images/img/favicon.png" rel="icon">
    <link href="images/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{url('/')}}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{url('/')}}/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="{{url('/')}}/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="{{url('/')}}/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="{{url('/')}}/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="{{url('/')}}/vendor/aos/aos.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <!-- <link href="{{url('/')}}/css/css/style2_up.css" rel="stylesheet"> -->
    <link href="{{url('/')}}/css/css/style2.css" rel="stylesheet">
  
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <!-- =======================================================
    * Template Name: Flexor - v2.4.1
    * Template URL: https://bootstrapmade.com/flexor-free-multipurpose-bootstrap-template/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
    <style>
  
        .get-started-icon {
            font-size: 24px;
            background: #ffce00;
            padding: 14px;
            color: #fff;
            border-radius: 50px;
            position: relative;
            z-index: 5;
            box-shadow: 10px 2px 15px rgba(0, 0, 0, 0.1);
        }

        .btn-get-started {
            font-family: "Raleway", sans-serif;
            font-weight: 400;
            font-size: 16px;
            letter-spacing: 1px;
            display: inline-block;
            transition: 0.5s;
            margin-left: -10px;
            padding: 8px 26px 8px 26px;
            color: #ffffff;
            background: #ffce00;
            border-radius: 0 50px 50px 0;
            position: relative;
            z-index: 4; 
        }

        .btn-get-started:hover {
            color: #262228;
        }

        .img-fluid {
            width: 100%;
   
        }

        .bg-products{ border:1px solid #ffbd83; padding:0; margin-right:0px; margin-bottom: 5px;}
    </style>
</head>

<body>
@section('header')
  <!-- ======= Top Bar ======= -->
<section id="header" class="topbar d-none d-lg-block">
    <div class="container1 d-flex">
        <div class="logo mr-auto" >
            <a href="/"><img src="{{url('/')}}/images/img/mytown-logo.png" alt="MyTown Logo" ></a>        <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
        </div>
    
        <form class="form-inline  my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Enter Location e.g. India, Mumbai" aria-label="Enter Location e.g. India, Mumbai">  
            <input class="form-control mr-sm-2" type="search" placeholder="Search Product or Services" aria-label="Search Product or Services">
            <button class="btn btn-primary my-2 my-sm-0" type="submit">Search</button>
        </form>
     
       <div class="contact-info mr-auto">
            <ul> 
                <li>
                    <a href="#"> <i class="icofont-shopping-cart"></i></a> 
                    <a href="#"><i class="icofont-bell-alt"><em class="number5">0</em></i></a>
                    <span class="user-name dropdown"> 
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                            <p>Hello Aarti</p>
                            <h2>Pune,MH,India</h2>
                        </a>
                        <div class="dropdown-menu">
                            <div class="menu-line">
                                <ul>
                                    <strong>Your Lists</strong>
                                    <li><a href="">Create a Wish List</a></li>
                                    <li><a href="">Create a Wish List</a></li>
                                    <li><a href="">Create a Wish List</a></li>
                                </ul>
        
                                <ul>
                                    <strong>Your Lists</strong>
                                    <li><a href="">Create a Wish List</a></li>
                                    <li><a href="">Create a Wish List</a></li>
                                    <li><a href="">Create a Wish List</a></li>
                                </ul>
                            </div>
                        </div>
                    </span>
                </li>
            </ul>
        </div>
    </div>
</section>

<!-- ======= Header ======= -->
<header class="topbar2">
    <div class="container d-flex">
        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li><a href="index.html">Feed</a></li>
                <li><a href="#about">Contacts</a></li>
                <li><a href="#services">Business Contacts</a></li>
                <li><a href="#portfolio">Purchase History</a></li>
                <li><a href="#team">Posts</a></li>
                <!--<li class="pad-l"><a href="#pricing">My Travel Plans</a></li>-->
            </ul>
        </nav><!-- .nav-menu -->
    </div>
</header><!-- End Header -->
  @show

<div>@yield('content')</div>

@section('footer')
    
 <!-- ======= Footer ======= -->
 <footer id="footer">

<div class="footer-top">
  <div class="container">
    <div class="row">

      <div class="col-lg-3 col-md-6 footer-contact">
        <h3>MyTown</h3>
        <p>
          A108 Adam Street <br>
          New York, NY 535022<br>
          United States <br><br>
          <strong>Phone:</strong> +1 5589 55488 55<br>
          <strong>Email:</strong> info@example.com<br>
        </p>
      </div>

      <div class="col-lg-2 col-md-6 footer-links">
        <h4>Useful Links</h4>
        <ul>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
        </ul>
      </div>

      <div class="col-lg-3 col-md-6 footer-links">
        <h4>Our Services</h4>
        <ul>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
        </ul>
      </div>

      <div class="col-lg-4 col-md-6 footer-newsletter">
        <h4>Join Our Newsletter</h4>
        <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
        <form action="" method="post">
          <input type="email" name="email"><input type="submit" value="Subscribe">
        </form>
      </div>

    </div>
  </div>
</div>

<div class="container d-lg-flex py-4">

  <div class="mr-lg-auto text-center text-lg-left">
    <div class="copyright">
      &copy; Copyright <strong><span>MyTown</span></strong>. All Rights Reserved
    </div>
     <!-- <div class="credits">
     All the links in the footer should remain intact. -->
      <!-- You can delete the links only if you purchased the pro version. -->
      <!-- Licensing information: https://bootstrapmade.com/license/ -->
      <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/flexor-free-multipurpose-bootstrap-template/ 
      Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
    </div>-->
  </div>
  <div class="social-links text-center text-lg-right pt-3 pt-lg-0">
    <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
    <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
    <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
    <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
    <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
  </div>
</div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

<!-- Vendor JS Files -->
<script src="{{url('/')}}/vendor/jquery/jquery.min.js"></script>
<script src="{{url('/')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="{{url('/')}}/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="{{url('/')}}/vendor/php-email-form/validate.js"></script>
<script src="{{url('/')}}/vendor/jquery-sticky/jquery.sticky.js"></script>
<script src="{{url('/')}}/vendor/venobox/venobox.min.js"></script>
<script src="{{url('/')}}/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="{{url('/')}}/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="{{url('/')}}/vendor/aos/aos.js"></script>

<!-- Template Main JS File -->
<script src="{{url('/')}}/js/js/main.js"></script>
@show
</body>

</html>