<!DOCTYPE html>
<html lang="en">
    <head>
        <title>My Town</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="/css/main.css" />
        <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
        <link rel="manifest" href="/favicon/site.webmanifest">
        <link rel="stylesheet" href="/css/fontawesome-all.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">  
        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <!-- Scripts -->
        <!--<script src="{{ mix('js/app.js') }}" defer></script>-->
    @stack('head')

  </head>
<body class="is-preload homepage">
    <div id="page-wrapper">
        <!-- Header -->
        <div id="home-bg">
            <header>
                <!-- Logo -->
                <div class="container">
                    <div class="row">
                        <div class="col-3 col-12-medium">
                            <div id="logo">
                                <a href="/"><img src="/images/mytown-logo.png" width="127" height="43" alt="Art My Breath"/></a>
                            </div>
                        </div>
                        <div class="col-9 col-12-medium">
                        </div>
                        <div class="col-6 col-12-medium textcenter marginauto marginbottom2 margintop2">
                	<main>
                	{{ $slot }}
                	</main>
                        </div>
                    </div>
				<div class="row margintop1 marginbottom1">
				</div>
              </div>
            </header>
        </div>
        <div id="footer-wrapper">
            <footer id="footer" class="container">
                <div class="row">
                    <div class="col-12 col-12-medium">
                        <div class="footerlinks">
                            <ul>
                                <li><a href="#">About</a> </li>
                                <li><a href="#">How it Works</a> </li>
                                <li><a href="#">Contact</a> </li>
                                <li><a href="#">My Network</a> </li>
                                <li><a href="#">Create Your Profile</a> </li>
                            </ul>
                        </div>
                        <div class="social">
                            <ul>
                                <li><a href="#"><i class="fab fa-twitter fa-lg"></i></a> </li>
                                <li><a href="#"><i class="fab fa-facebook-square fa-lg"></i></a> </li>
                                <li><a href="#"><i class="fab fa-instagram fa-lg"></i></a> </li>
                            </ul>
                        </div>
                        <p>© 2021 MyTown. All Rights Reserved.</p>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <!-- Scripts -->
</body>
</html>
