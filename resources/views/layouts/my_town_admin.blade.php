<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
    <link rel="stylesheet" href="{{ url('/') }}/css/bootstrap.min.css" />
    <link href="{{ url('/') }}/css/font-awesome.min.css" rel="stylesheet" />
    <link href="{{ url('/') }}/css/sidemenu.css" rel="stylesheet" />
    <link href="{{ url('/') }}/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/style_admin.css" />
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/iconfonts/feather/feather.css" />
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/responsive.css" />
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap4.min.css" />
      

    <title>Dashboard</title>
</head>


<body class="main-body leftmenu">

    <!-- Switcher -->

    <!-- End Switcher -->

    <!-- Loader -->
    {{-- <div id="global-loader">
                    <img src="https://www.spruko.com/demo/dashpro/Dashpro/assets/img/loader.svg" class="loader-img" alt="Loader">
                </div> --}}
    <!-- End Loader -->


    <!-- Page -->
    <div class="page">

        <!-- Sidemenu -->
        <div class="main-sidebar main-sidebar-sticky side-menu">
            <div class="sidemenu-logo">
                <a class="main-logo">
                    <img src="{{ url('/') }}/images/dashboard/mytown-logo.png">
                </a>
            </div>
            <div class="main-sidebar-body">
                <ul class="nav">
                    {{-- <li class="active"><a data-toggle="tab" href="#home">Home</a></li>  --}}
                    <li class="nav-item active show" style="width: 100%">
                        <a class="nav-link" id="tab0-tab" data-toggle="tab" href="#tab0" role="tab" aria-controls="tab1" aria-selected="true">

                            <i class="fas fa-briefcase sidemenu-icon"></i>
                            <span class="sidemenu-label">Summary
                            </span>
                        </a>
                    </li>
                  <li class="nav-item">
						<a class="nav-link with-sub" href="#"><i class="fas fa-user sidemenu-icon"></i><span
								class="sidemenu-label">Users</span><i class="angle fe fe-chevron-right"></i></a>
						<ul class="nav-sub">
							<li class="nav-sub-item">
								<a class="nav-sub-link"  id="tab2-tab" data-toggle="tab" href="#tab1" role="tab" aria-controls="tab2" aria-selected="true">Businesses</a>
							</li>
							<li class="nav-sub-item">
								<a class="nav-sub-link"  id="tab3-tab" data-toggle="tab" href="#tab2" role="tab" aria-controls="tab3" aria-selected="true">Shoppers</a>
							</li>
						
						</ul>
					</li>
                    {{--  <li class="nav-item" style="width: 100%">
                        <a class="nav-link " id="tab3-tab" data-toggle="tab" href="#tab3" role="tab" aria-controls="tab3" aria-selected="true"><i class="fas fa-briefcase sidemenu-icon"></i><span
                                class="sidemenu-label">Service Businesses</span></a>
                    </li>  --}}
                    <li class="nav-item" style="width: 100%">
                        <a class="nav-link " id="tab4-tab" data-toggle="tab" href="#tab4" role="tab" aria-controls="tab4" aria-selected="true"><i class="fab fa-codepen sidemenu-icon"></i><span
                                class="sidemenu-label">Products</span></a>
                    </li>
                    <li class="nav-item" style="width: 100%">
                        <a class="nav-link " id="tab5-tab" data-toggle="tab" href="#tab5" role="tab" aria-controls="tab5" aria-selected="true"><i class="fas fa-shopping-cart sidemenu-icon"></i><span
                                class="sidemenu-label">Orders</span></a>
                    </li>

                    <li class="nav-item" style="width: 100%">
                        <a class="nav-link" id="tab6-tab" data-toggle="tab" href="#tab6" role="tab" aria-controls="tab6" aria-selected="true"><i class="fas fa-truck sidemenu-icon"></i><span
                                class="sidemenu-label">Shipments</span></a>

                    </li>






                </ul>
            </div>
        </div>
        <!-- End Sidemenu -->

        <!-- Main Header-->
        <div class="main-header side-header sticky">
            <div class="container-fluid">
                <div class="main-header-left">
                    <a class="main-header-menu-icon" href="#" id="mainSidebarToggle"><span></span></a>
                </div>
                <div class="admin-k"><img src="{{ url('/') }}/images/admin-icon.png"> Admin Dashboard</div>
                <div class="main-header-center">
                    <!-- 	<div class="responsive-logo">
                                    <a href="index.html"><img src="assets/images/mobile-mytowon.png" class="mobile-logo" alt="logo"></a>
                                    <a href="index.html"><img src="../../assets/img/brand/logo.png" class="mobile-logo-dark" alt="logo"></a>
                                </div> -->
                    <div class="input-group">
                        <div class="mt-0">
                            <form class="form-inline">
                                <div class="search-element">
                                    <input type="search" class="form-control header-search" placeholder="Search…"
                                        aria-label="Search" tabindex="1">
                                    <button class="btn" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="main-header-right">

                    <div class="dropdown main-header-notification">
                        <a class="nav-link icon" href="#">
                            <i class="header-icons"><svg xmlns="http://www.w3.org/2000/svg" height="24"
                                    viewBox="0 0 24 24" width="24">
                                    <path d="M0 0h24v24H0V0z" fill="none" />
                                    <path
                                        d="M12 22c1.1 0 2-.9 2-2h-4c0 1.1.9 2 2 2zm6-6v-5c0-3.07-1.63-5.64-4.5-6.32V4c0-.83-.67-1.5-1.5-1.5s-1.5.67-1.5 1.5v.68C7.64 5.36 6 7.92 6 11v5l-2 2v1h16v-1l-2-2zm-2 1H8v-6c0-2.48 1.51-4.5 4-4.5s4 2.02 4 4.5v6z" />
                                </svg></i>
                            <span class="badge badge-danger nav-link-badge">4</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow  animated p-0">
                            <div class="notifications-menu">
                                <a class="dropdown-item d-flex p-3 border-bottom rounded-top " href="#">
                                    <span
                                        class="avatar avatar-md mr-3 fs-20 align-self-center cover-image bg-primary brround">
                                        <i class="fe fe-upload"></i>
                                    </span>
                                    <div>
                                        <span class="font-weight-bold text-dark"> New Business Owner </span>
                                        {{-- <div class="small text-muted d-flex">
                                            5 hour ago
                                        </div> --}}
                                    </div>
                                </a>
                                <a class="dropdown-item d-flex p-3 border-bottom" href="#">
                                    <span
                                        class="avatar avatar-md  fs-20 mr-3 align-self-center cover-image bg-teal brround">
                                        <i class="fe fe-arrow-up-circle"></i>
                                    </span>
                                    <div>
                                        <span class="font-weight-bold text-dark"> New Shopper</span>
                                        {{-- <div class="small text-muted d-flex">
                                            20 mins ago
                                        </div> --}}
                                    </div>
                                </a>
                                <a class="dropdown-item d-flex p-3 border-bottom" href="#">
                                    <span
                                        class="avatar avatar-md fs-20 mr-3 align-self-center cover-image bg-info brround">
                                        <i class="fe fe-shopping-bag"></i>
                                    </span>
                                    <div>
                                        <span class="font-weight-bold text-dark"> New Order</span>
                                        {{-- <div class="small text-muted d-flex">
                                            1 hour ago
                                        </div> --}}
                                    </div>
                                </a>
                                {{-- <a class="dropdown-item d-flex p-3 border-bottom" href="#">
                                    <span
                                        class="avatar avatar-md mr-3 fs-20 align-self-center cover-image bg-pink brround">
                                        <i class="fe fe-database"></i>
                                    </span>
                                    <div>
                                        <span class="font-weight-bold text-dark">Server Rebooted</span>
                                        <div class="small text-muted d-flex">
                                            2 hour ago
                                        </div>
                                    </div>
                                </a> --}}
                            </div>
                            {{-- <a href="#" class="dropdown-item text-center notifications-menu1">View all Notification</a> --}}
                        </div>
                    </div>

                    <div class="dropdown main-profile-menu">
                        <a class="d-flex" href="#">
                            <span class="main-img-user"><img alt="avatar"
                                    src="{{ url('/') }}/images/dashboard/user.jpg"></span>
                        </a>
                        <div class="dropdown-menu">
                            <div class="header-navheading">
                                {{-- <h6 class="main-notification-title">{{ Auth::user()->name }}</h6> --}}
                                <p class="main-notification-text">Admin</p>
                            </div>
                            <a class="dropdown-item border-top" href="#">
                                <i class="fe fe-user"></i> My Profile
                            </a>
                            <a class="dropdown-item" href="#">
                                <i class="fe fe-edit"></i> Edit Profile
                            </a>
                            <a class="dropdown-item" href="#">
                                <i class="fe fe-settings"></i> Account Settings
                            </a>
                            <a class="dropdown-item" href="#">
                                <i class="fe fe fe-unlock"></i> Lock screen
                            </a>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fe fe-power"></i> Sign Out
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                    <button class="navbar-toggler navresponsive-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fe fe-more-vertical header-icons navbar-toggler-icon"></i>
                    </button><!-- Navresponsive closed -->
                </div>
            </div>
        </div>
        <!-- End Main Header-->


        <!-- Main Content-->
        @yield('content')

        <!-- End Main Content-->

        <!-- Main Footer-->
        <div class="main-footer text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <span>Copyright © 2021 </span>
                    </div>
                </div>
            </div>
        </div>
        <!--End Footer-->

     
    </div>

    @yield('modal')

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="{{url('/')}}/js/popper.min.js"></script>
        <script src="{{url('/')}}/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="{{url('/')}}/js/aos.js"></script>
        <script src="{{url('/')}}/js/main.js"></script> 
    {{-- <script src="{{url('/')}}/js/popper.min.js"></script>
        <script src="{{url('/')}}/js/bootstrap.min.js"></script> 
        <script type="text/javascript" src="{{url('/')}}/js/aos.js"></script>
        <script src="{{url('/')}}/js/main.js"></script> --}}
    <script src="{{ url('/') }}/js/sidemenu.js"></script>
    <script src="{{ url('/') }}/js/custom.js"></script>
    <script src="{{ url('/') }}/plugins/select2/js/select2.min.js"></script>
    <script src="{{ url('/') }}/js/select2.js"></script>
       <script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap4.min.js"></script>
<script>
    $('.nav-item').click(function(){
       $('.nav-item').removeClass('active');
        $(this).addClass('active');
      

    });
    $('.nav-sub-item').click(function(){
        $('.nav-sub-item').removeClass('active');
        $(this).addClass('active');
    });
</script>
<script>
  $(document).ready(function(){
                var table =$('#datatable').DataTable();
                  var productTable =$("#productTable").DataTable();
                  var shopperDataTable= $("#shopperDataTable").DataTable();
                table.on('click','.delete',function(){
                    $('#Delete_Product').modal('show');

                });
                $('#datatable tbody').on( 'click', 'tr', function () {
                    var data= table.row( this ).data() ;
                   // alert(data);
                  
                   
                    $('#deleteProductForm').attr('action','/deleteBusiness/'+data[0]);
                   

                } );
        
            });

</script>
<script>
    function showTab(id)
    {
        document.getElementById('main').innerHTML=document.getElementById(id).innerHTML;
    }
</script>
</body>

</html>
