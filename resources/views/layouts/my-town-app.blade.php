<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Profile</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />

        <meta name="_token" content="{{ csrf_token() }}">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
        <link rel="stylesheet" href="/css/main.css" />
        <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
        <link rel="manifest" href="/favicon/site.webmanifest">
        <link rel="stylesheet" href="/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="/css/biz_registration_form.css">
        <link rel="stylesheet" href="/css/cart.css">

        <!-- Scripts for products upload -->
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    

        <script>
            $(document).ready(function(){
                $('#show_product').on('click',function(){
                    $('#product-box').show();
                });

                $('#hide_product').on('click',function(){
                    $('#product-box').hide();
                });
            });
            $(document).ready(function(){
                $('#show_photo').on('click',function(){
                    $('#photo-box').show();
                });
                $('#hide_photo').on('click',function(){
                    $('#photo-box').hide();
                });
            });
            $(document).ready(function(){
                $('#show_service').on('click',function(){
                    $('#service-box').show();
                });

                $('#hide_service').on('click',function(){
                    $('#service-box').hide();
                });
            });
            $(document).ready(function(){
                $('#show_banner').on('click',function(){
                    $('#banner-box').show();
                });

                $('#hide_banner').on('click',function(){
                    $('#banner-box').hide();
                });
            });
            $(document).ready(function(){
                $('#show_logo').on('click',function(){
                    $('#logo-box').show();
                });

                $('#hide_logo').on('click',function(){
                    $('#logo-box').hide();
                });
            });
            $(document).ready(function(){
                $('#show_tag').on('click',function(){
                    $('#tag-box').show();
                });

                $('#hide_tag').on('click',function(){
                    $('#tag-box').hide();
                });
            });

            function showMenu(){
                document.getElementById('menuDiv').style.display="block";
            }


            window.onload=function(){

            var hideDiv=document.getElementById('menuDiv');
            document.onclick=function(div){
                if(div.target.id !== 'menuDiv' && div.target.id!=='profile_icon'){
                    hideDiv.style.display="none";
                     }
                };

            };






        </script>
        
        <!-- Styles for products upload -->
        <style>
            .card-body{
                padding: 30px;
            }
            .show-btn{
                font-size: 20px;
                font-weight: 500;
                margin-bottom: 10px;
                padding: 5px;
                background-color: white;
                border-color: white;
                cursor: pointer;
            }
            .box1{
                display: none;
                width: 50%;
                position: absolute;
                top: 35%;
                left: 50%;
                transform: translate(-50%, -15%);
                box-shadow: 0 0 8px rgba(0,0,0,0.1);
                z-index: 1;
            }
            .close-btn{
                position: absolute;
                right: 20px;
                top: 15px;
                font-size: 18px;
                cursor: pointer;
            }
        </style>
        @stack('head')
    </head>
<body class="is-preload homepage">
    <div id="page-wrapper">
        @section('header')
        <!-- Header -->
        <div id="header-wrapper">
            <header id="header" class="container">
                <!-- Logo -->
                <div class="container">
                    <div class="row">
                        <div class="col-2 col-12-medium">
                            <div id="logo">
                                <a href="/"><img src="/images/mytown-logo.png" width="127" height="43" alt="Art My Breath"/></a>
                            </div>
                        </div>
                        <div class="col-3 col-12-medium">
                            <div class="inputContainer">
                                <form method="get" action="/biz-search" id="Form">
                                    @csrf
                                    <i class="fa fa-map-marker-alt searchicon"> </i>
                                    <input id="myInput" name="user_query" class="Field" type="text" placeholder="Enter Location e.g.India , Mumbai" />
                                    <button style="display:none;" id="myBtn" onclick="javascript:$(Form).submit();">Button</button>
                                </form>
                                <script>
                                    var input = document.getElementById("myInput");
                                    input.addEventListener("keyup", function(event) {
                                        if (event.keyCode === 13) {
                                            event.preventDefault();
                                            document.getElementById("myBtn").click();
                                        }
                                    });
                                </script>
                            </div>
                        </div>
                        <div class="col-4 col-12-medium">
                            <div class="inputContainer">
                                <i class="fas fa-search searchicon"></i>
                                <input class="Field" type="text" placeholder="Search Product or Services" />
                            </div>
                        </div>

                        <div class="col-3 col-12-medium">
                            <div class="floatright">
                            <div style="display:flex;">
                                <?php $x=0; $item_count=0;?>
                                @if(!empty(Auth::user()->id))
                                    <?php
                                        $x=Auth:: user()->businesses->first();
                                        Melihovv\ShoppingCart\Facades\ShoppingCart::restore(\Auth::user()->id);
                                        $item_count=Melihovv\ShoppingCart\Facades\ShoppingCart::count();
                                    ?>
                                   
                                    @if(!is_null(Auth::user()->profile_photo_path))
                                        <img src="/images/profile.png" id="profile_icon" onclick="showMenu()" width="30" height="30" alt="Art My Breath">&nbsp;&nbsp;
                                    @else
                                        <h5 style="cursor:pointer;margin-top:5px;line-height: 1.3;" id="profile_icon" onclick="showMenu()">{{ Auth::user()->name }}</h5>
                                    @endif
                                    <!--<img src="/images/profile.png" width="36" height="36" alt="Art My Breath"/>&nbsp;&nbsp;-->

		                            <div class="notification">
                                        <div class="count">
                                            <?php 
                                                echo $item_count;
                                            ?>
                                        </div>

                                	    <div class="carticon">
									        <a href="{{ url('/cart') }}"><img src="/images/cart1.png" width="30px" height="30px"></a>
									    </div>
                                    </div>


                                    <div class="notification">
                                        <div class="count">
                                            <?php 

                            echo App\Http\Controllers\UserController::getNotificationCount(Auth::user()->id);
                            
                                            ?>

                                        </div>

                                        <div class="carticon">
                                            
                                            <!-- <img src="/images/bell.png" width="30px" height="30px" onclick="showMenu()"/> -->

                                          <a href="{{ url('/notification_details') }}">  <img src="/images/bell.png" width="30px" height="30px" id="notification_icon"/></a>
                                          
                                        </div>
                                    </div>


                                @else
                                    <a href="/login" class="button-solid marginright1" style="margin-left:-40px;">Login</a>
                                @endif
                            </div>

                                <div class="sublist1" id="menuDiv">
					                <ul>
						                <li><a href="/user/profile">Account Information</a>
                                        </li>
                                        
                                        @if ($x)
                                            <li><a href="{{ url('biz-profile/'.$x->id) }}">Business Profile</a></li>
                                        @else
                                            <li><a href="biz-registration">Business Profile</a></li>
                                        @endif
                                        <li>
                                            <form method="POST" action="{{ route('logout') }}">
                                                @csrf
                                                <x-jet-responsive-nav-link href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                    this.closest('form').submit();">
                                                    {{ __('Log Out') }}
                                                </x-jet-responsive-nav-link>
                                            </form>
                                        </li>
                                        <!--<li><a href="#">Log Out</a></li>-->
					                </ul>	
				                </div>
                            </div>
                        </div>

                    </div>
                </div>


                <!-- Nav -->
            </header>
        </div>
        <!-- <div class="br.clear"></div>-->
        
        @if(Auth::user() &&  Auth::user()->hasRole('admin'))
        <div class="topnav" id="myAdmin">
            <div class="container">
                <a href="/dashboard" class="{{ Request::is('dashboard')? 'active' : '' }}">Dashboard</a>
                <a href="/admin/usermanagement" class="{{ Request::is('admin/usermanagement')? 'active' : '' }}">Users</a>
                <a href="/admin/businesses" class="{{ Request::is('admin/businesses')? 'active' : '' }}">Businesses</a>
                <a href="/admin/orders" class="{{ Request::is('admin/orders')? 'active' : '' }}">Orders</a>
                <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                <i class="fa fa-bars"></i>
                </a>
            </div>
        </div>

        @elseif (Auth::user() && Auth::user()->businesses->count() > 0)
        <div class="topnav" id="bizownerNav">
            <div class="container">
                <a href="/biz_owner/dashboard" class="{{ Request::is('biz_owner/dashboard')? 'active' : '' }}">Dashboard</a>
                <?php $business=App\Models\Business::where('owner',Auth::user()->id)->first();?>
                <a href="{{url('biz-profile/'.$business->id)}}" class="{{ Request::is('biz-profile')? 'active' : '' }}">Business Profile</a>
                <a href="/biz_owner/feed" class="{{ Request::is('biz_owner/feed')? 'active' : '' }}">Feed</a>
                <a href="/biz_owner/contacts" class="{{ Request::is('biz_owner/contacts')? 'active' : '' }}">Contacts</a>
                <a href="/biz_owner/suppliers" class="{{ Request::is('biz_owner/suppliers')? 'active' : '' }}">Suppliers</a>
                <a href="/biz_owner/sales" class="{{ Request::is('biz_owner/sales-history')? 'active' : '' }}">Sales History</a>
                <a href="javascript:void(0);" class="icon" onclick="MyFunction()">
                <i class="fa fa-bars"></i>
                </a>
            </div>
        </div>
        
        @else
       @guest 
       <div class="topnav" id="myTopnav">
            <div class="container">
            <a></a>
            </div>
        </div>
        @endguest 
        @auth
       <div class="topnav" id="myTopnav">
            <div class="container">
                <a href="/my_feed" class="{{ Request::is('my_feed')? 'active' : '' }}">Feed</a>
                <a href="/contacts" class="{{ Request::is('contacts')? 'active' : '' }}">Contacts</a>
                <a href="/business-contacts" class="{{ Request::is('business-contacts')? 'active' : '' }}">Business Contacts</a>
                <a href="/purchase-history" class="{{ Request::is('purchase-history')? 'active' : '' }}">Purchase History</a>
                <a href="/posts" class="{{ Request::is('posts')? 'active' : '' }}">Posts</a>
                <a href="/my-travel-plans" class="{{ Request::is('my-travel-plans')? 'active' : '' }}">My Travel Plans</a>
                <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                <i class="fa fa-bars"></i>
                </a>
            </div>
        </div>
        @endauth
       @endif                                            
        
        @show
        <script>
            function myFunction() {
                var x = document.getElementById("myTopnav");
                if (x.className === "topnav") {
                    x.className += " responsive";
                } else {
                    x.className = "topnav";
                }
            }
        </script>

        <div>@yield('content')</div>


        @section('footer')
        <div id="footer-wrapper">
            <footer id="footer" class="container">
                <div class="row">
                    <div class="col-12 col-12-medium">
                        <div class="footerlinks">
                            <ul>
                                <li><a href="#">About</a></li>
                                <li><a href="#">How it Works</a></li>
                                <li><a href="#">Contact</a></li>
                                <li><a href="#">My Network</a></li>
                                <li><a href="#">Create Your Profile</a></li>
                            </ul>
                        </div>
                        <div class="social">
                            <ul>
                                <li><a href="#"><i class="fab fa-twitter fa-lg"></i></a></li>
                                <li><a href="#"><i class="fab fa-facebook-square fa-lg"></i></a></li>
                                <li><a href="#"><i class="fab fa-instagram fa-lg"></i></a></li>
                            </ul>
                        </div>
                        <p>© 2021 MyTown. All Rights Reserved.</p>
                    </div>
                </div>
            </footer>
        </div>
        @show
    </div>
    <!-- Scripts -->
</body>
</html>
