<!DOCTYPE html>
<html lang="en">
    <head>
        <title>My Town</title>
        <meta charset="utf-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="/css/main.css" />
        <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
        <link rel="manifest" href="/favicon/site.webmanifest">
        <link rel="stylesheet" href="/css/fontawesome-all.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">  
<script src="//code.jquery.com/jquery-1.12.1.js"></script>  
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>  

<script>
$(function(){
    $("#biz_location_search").autocomplete({
      source :function( request, response ) {
        $.ajax({
           url: "{{ url('/biz-location/search') }} ",
           dataType: "json",
           data: {
              q: request.term 
           },
           success: function( data ) {
               response($.map(data, function(item) {
                     return {
                         label : item,
                         value : item
                     };
               }));
           }
        });
       },
       select: function (event, ui) {
             $("#biz_location_search").val(ui.item.label);
             return false;
       }
   });
});

$("#biz_location_search").on("keyup", function(event) {
	if (event.keyCode === 13) {
   		event.preventDefault();
   		document.getElementById("myBtn").click();
	}
});
$("#product_search").on("keyup", function(event) {
   if (event.keyCode === 13) {
   		event.preventDefault();
   		document.getElementById("myBtn").click();
   }
});
</script>
<style>
.ui-autocomplete-loading { background:url('/images/loading.gif') no-repeat right center }
</style>
@stack('head')
  </head>
<body class="is-preload homepage">
    <div id="page-wrapper">
        <!-- Header -->
        <div id="home-bg">
            <header>
                <!-- Logo -->
                <div class="container">
                    <div class="row">
                        <div class="col-3 col-12-medium">
                            <div id="logo">
                                <a href="/"><img src="/images/mytown-logo.png" width="127" height="43" alt="Art My Breath"/></a>
                            </div>
                        </div>
                        <div class="col-9 col-12-medium">
                            <div class="floatright">
                                @guest  
                                <a href="/register" class="button-outline-gray marginright1">Sign Up</a>&nbsp;&nbsp;
                                @endguest 
                                @auth
								@php
									//$place_ar = explode(",",@\Auth::user()->place );
									$place = \App\Location::getTown(@\Auth::user()->place_id);
								@endphp
                                
                                <a href="/dashboard" class="button-outline-gray marginright1">{{ (\Auth::user()->name)}}, {{ $place }}</a>&nbsp;&nbsp;
                                @endauth
                                @guest 
                                
                                <a href="/login" class="button-solid marginright1">Login</a>

                                @endguest 
                                @auth
                                <a href="{{ route('logout') }}" class="button-solid marginright1" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                                @endauth
                            </div>
                        </div>
                        <div class="col-12 col-12-medium textcenter marginauto marginbottom2 margintop2">
                            <h1>Global Platform<br>for Shopping</h1>
                        </div>
                    </div>
                       <form method="get" action="/biz-search" id="homeForm">
                    <div class="row col-75-center marginauto margintop3-minus">
                        <div class="col-6 col-12-medium">
                            <div class="inputContainer">
                                    @csrf
                                    <i class="fa fa-map-marker-alt searchicon"> </i>
                                    <input id="biz_location_search" name="user_query" class="Field" type="text" required placeholder="Enter Location e.g. India, Mumbai" />
                                    <button style="display:none;" id="myBtn" onclick="javascript:$(homeForm).submit();">Button</button>
                            </div>
                        </div>
                        <div class="col-6 col-12-medium">
                            <div class="inputContainer">
                                <i class="fas fa-search searchicon"></i>
                                <input class="Field" name="product_search" type="text" placeholder="Search Product or Services" />
                            </div>
                        </div>
                    </div>
                       </form>
                    <div class="row col-12-center marginauto">
                    <div class="col-2 col-12-medium textcenter">
                            <h3 class="colored">{{ $towns }}</h3>
                            <div>Towns</div>
                        </div>
                        <div class="col-2 col-12-medium textcenter">
                            <h3 class="colored">{{ $cities }}</h3>
                            <div>Cities</div>
                        </div>
                        <div class="col-2 col-12-medium textcenter">
                            <h3 class="colored">{{ $countries }}</h3>
                            <div>Countries</div>
                        </div>
                        <div class="col-2 col-12-medium textcenter">
                            <h3 class="colored">{{ $businesses }}</h3>
                            <div>Businesses</div>
                        </div>
                        <div class="col-2 col-12-medium textcenter">
                            <h3 class="colored">{{ $shoppers }}</h3>
                            <div>Shoppers</div>
                        </div>
                        <div class="col-2 col-12-medium textcenter">
                            <h3 class="colored">{{ $products }}</h3>
                            <div>Products</div>
                        </div>
                    </div>
            	<div class="textcenter margintop1"><strong>Own a business?</strong></div>
				<div class="row margintop1 marginbottom1">
        				<div class="col-4 col-12-medium textcenter">
            				<div class="circle pop-biz-reg-form" style="cursor:pointer;">1</div>
            				<h3 class="margintop1">Sign Up</h3>
					@guest
                            <div><label id="show-pop-over" class="pop-biz-reg-form" style="color:blue; cursor:pointer; padding:0;">Create</label> a profile of your business on MyTown</div>
					@endguest
					@auth
            				<div>You have completed signing up!</div>
					@endauth
        				</div>
        				<div class="col-4 col-12-medium textcenter">
            				<div class="circle">2</div>
					@guest
            				<h3 class="margintop1">Get Listed on MyTown</h3>
            				<div>Add products and services</div>
					@endguest
					@auth
            				<h3 class="margintop1">Your Business on MyTown</h3>
					@if(Auth::user()->businesses->count() == 0)
            				<div><a href="/biz-registration">Create</a> a profile of your business</div>
					@else
            				<div>You have created a <a href="/biz-profile/{{ Auth::user()->businesses->first()->id }}" target="_new">profile</a> of your business! Go to the <a href="/biz_owner/dashboard">dashboard</a>.</div>
					@endif
					@endauth
        				</div>
        				<div class="col-4 col-12-medium textcenter">
            				<div class="circle">3</div>
            				<h3 class="margintop1">Grow Your Business</h3>
            				<div>Get global exposure!</div>
        				</div>
				</div>
                <!-- Nav -->
              </div>
            </header>
        </div>
            	@guest
				@yield('content')
            	@endguest
        <div id="footer-wrapper">
            <footer id="footer" class="container">
                <div class="row">
                    <div class="col-12 col-12-medium">
                        <div class="footerlinks">
                            <ul>
                                <li><a href="#">About</a> </li>
                                <li><a href="#">How it Works</a> </li>
                                <li><a href="#">Contact</a> </li>
                                <li><a href="#">My Network</a> </li>
                                <li><a href="#">Create Your Profile</a> </li>
                            </ul>
                        </div>
                        <div class="social">
                            <ul>
                                <li><a href="#"><i class="fab fa-twitter fa-lg"></i></a> </li>
                                <li><a href="#"><i class="fab fa-facebook-square fa-lg"></i></a> </li>
                                <li><a href="#"><i class="fab fa-instagram fa-lg"></i></a> </li>
                            </ul>
                        </div>
                        <p>© 2021 MyTown. All Rights Reserved.</p>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <!-- Scripts -->
</body>
</html>
