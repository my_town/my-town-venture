<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
        <link rel="stylesheet" href="{{url('/')}}/css/bootstrap.min.css" />
        <link href="{{url('/')}}/css/icofont.min.css" rel="stylesheet" />
        <link href="{{url('/')}}/css/boxicons.min.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="{{url('/')}}/css/aos.css" />
        <link rel="stylesheet" type="text/css" href="{{url('/')}}/css/style.css" />
        <link rel="stylesheet" type="text/css" href="{{url('/')}}/css/responsive.css" />
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap4.min.css" />
        <title>Dashboard</title>
    </head>
    <body class="dashboard-wrpper">
        <div class="main-wrpper owners-dashboard">
            <div class="fix-dashheader">
                <section id="header" class="topbar d-none d-lg-block">
                    <div class="container1 d-flex">
                        <div class="logo mr-auto">
                            <a href="index.html"><img src="{{url('/')}}/images/dashboard/mytown-logo.png" alt="MyTown Logo" /></a>
                            @if (Auth::user())
                                <span class="category-icon-shopper" style="color: #ff4500;"><img src="{{url('/')}}/images/dashboard/head1.png" alt="" /> Business</span>
                            @else
                               dddd
                            @endif()
                            
                            <!-- Uncomment below if you prefer to use an image logo -->
                            <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
                        </div>

                        <form class="form-inline my-2 my-lg-0" action="search_result" method="get">
                            <input class="form-control mr-sm-2" type="text" id="location_search" name="location_search" required autofocus autocomplete="location_search" aria-label="Enter Location" placeholder="Enter Location"/>
                            <input id="place_id" type="hidden" name="place_id" required value="">
                            <input class="form-control mr-sm-2" type="text" id="product_search" name="product_search" required autofocus autocomplete="location_search" placeholder="Product Type or Service Type" aria-label="Product Type or Service Type"/>
                            <input id="productid" type="hidden" name="productid" required value="">
                            <button class="btn btn-primary my-2 my-sm-0" type="submit">Search</button>
                        </form>

                        @php
                            $place_ar = explode(",", @\Auth::user()->place);
                        @endphp 

                        <div class="contact-info mr-auto">
                            <ul>
                                <li>
                                    <a href="#"> <i class="icofont-shopping-cart" style="padding-right: 30px; font-size: 30px;"></i></a>

                                    @if (Auth::user())
                                        <?php
                                            $notification_count = App\Http\Controllers\UserController::getNotificationCount(Auth::user()->id);
                                        ?>
                                    @else
                                        <?php
                                            $notification_count = 0;
                                        ?>
                                    @endif()

                                    @if(@$notification_count>0)
                                        <a href="{{ url('/notification_details') }}"><i class="icofont-bell-alt"> <em class="number5">
                                    @else
                                        <a href=""><i class="icofont-bell-alt"> <em class="number5"> 
                                    @endif
                                    @if (Auth::user())
                                        <?php 
                                            echo $notification_count; 
                                        ?>
                                    @endif()
                                    </em></i></a>

                                    <span class="user-name dropdown">
                                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                                            <p>
                                                <!-- Aarti Kore<br />
                                                Kharadi, Pune<br />
                                                India -->
                                                @if (Auth::user())
                                                    <p>{{ Auth::user()->name }}<br>{{ $place_ar[0] }},
                                                        @if (isset($place_ar[1]))
                                                        {{ $place_ar[1] }},<br>   
                                                        @endif

                                                        @if (isset($place_ar[2]))
                                                        {{ $place_ar[2] }} 
                                                        @endif
                                                      </p>
                                                @endif()
                                            </p>
                                        </a>
                                        <div class="dropdown-menu">
                                            <div class="menu-line">
                                                <ul>
                                                    <strong>Your Lists</strong>
                                                    <li><a href="">Create a Wish List</a></li>
                                                    <li><a href="">Create a Wish List</a></li>
                                                    <li><a href="">Create a Wish List</a></li>
                                                </ul>

                                                <ul style="border-right: none;">
                                                    <strong>Your Account</strong>
                                                    <li><a href="#">Your Account</a></li>
                                                    <li><a href="#">Your Orders</a></li>
                                                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            {{ csrf_field() }}
                                                        </form></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </section>

                <!-- ======= Header ======= -->
                <header class="topbar2">
                    <div class="container d-flex">
                        <nav class="nav-menu d-none d-lg-block">
                            <ul>
                                <li><a href="index.html">Feed</a></li>
                                <li><a href="#about">Contacts</a></li>
                                <li><a href="#services">Business Contacts</a></li>
                                <li><a href="#portfolio">Purchase History</a></li>
                                <li><a href="#team">Posts</a></li>
                                <li class="pad-l"><a href="#">My Dashboard</a></li>
                            </ul>
                        </nav>
                        <!-- .nav-menu -->
                    </div>
                </header>
                <!-- End Header -->
            </div>

            <div class="left-sidebar">
                <div class="main-sidebar-body">
                    <ul class="nav nav-tabs" id="nav-tab" role="tablist">
                        <li>
                            <a class="nav-link active" id="tab0-tab" data-toggle="tab" href="#tab0" role="tab" aria-controls="tab1" aria-selected="true">
                                <div class="tab-img">
                                    <img class="img1" src="{{url('/')}}/images/dashboard/ic1.png" />
                                    <img class="img2" src="{{url('/')}}/images/dashboard/ic1-hover.png" />
                                </div>
                                <span>Business owner's Summary</span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" id="tab1-tab" data-toggle="tab" href="#tab1" role="tab" aria-controls="tab1" aria-selected="true">
                                <div class="tab-img">
                                    <img class="img1" src="{{url('/')}}/images/dashboard/ic1.png" />
                                    <img class="img2" src="{{url('/')}}/images/dashboard/ic1-hover.png" />
                                </div>
                                <span>Shoppers</span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" id="tab2-tab" data-toggle="tab" href="#tab2" role="tab" aria-controls="tab2" aria-selected="false">
                                <div class="tab-img">
                                    <img class="img1" src="{{url('/')}}/images/dashboard/ic2.png" />
                                    <img class="img2" src="{{url('/')}}/images/dashboard/ic2-hover.png" />
                                </div>
                                <span>Orders</span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" id="tab3-tab" data-toggle="tab" href="#tab3" role="tab" aria-controls="tab3" aria-selected="false">
                                <div class="tab-img">
                                    <img class="img1" src="{{url('/')}}/images/dashboard/ic3.png" />
                                    <img class="img2" src="{{url('/')}}/images/dashboard/ic3-hover.png" />
                                </div>
                                <span>Shipment Details </span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" id="tab4-tab" data-toggle="tab" href="#tab4" role="tab" aria-controls="tab4" aria-selected="false">
                                <div class="tab-img">
                                    <img class="img1" src="{{url('/')}}/images/dashboard/ic4.png" />
                                    <img class="img2" src="{{url('/')}}/images/dashboard/ic4-hover.png" />
                                </div>
                                <span>Edit Products List</span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" id="tab5-tab" data-toggle="tab" href="#tab5" role="tab" aria-controls="tab5" aria-selected="false">
                                <div class="tab-img">
                                    <img class="img1" src="{{url('/')}}/images/dashboard/ic5.png" />
                                    <img class="img2" src="{{url('/')}}/images/dashboard/ic5-hover.png" />
                                </div>
                                <span>Monthly Sales</span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" id="tab6-tab" data-toggle="tab" href="#tab6" role="tab" aria-controls="tab6" aria-selected="false">
                                <div class="tab-img">
                                    <img class="img1" src="{{url('/')}}/images/dashboard/ic6.png" />
                                    <img class="img2" src="{{url('/')}}/images/dashboard/ic6-hover.png" />
                                </div>
                                <span>People in Store</span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" id="tab7-tab" data-toggle="tab" href="#tab7" role="tab" aria-controls="tab7" aria-selected="false">
                                <div class="tab-img">
                                    <img class="img1" src="{{url('/')}}/images/dashboard/ic7.png" />
                                    <img class="img2" src="{{url('/')}}/images/dashboard/ic7-hover.png" />
                                </div>
                                <span>View My Business Profile 
                                    {{-- <a href="{{ route('biz-profile/'.$business->id.'') }}"></a> --}}
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            @yield('content')
        </div>

        @yield('modal')

        <script src="{{url('/')}}/js/jquery-1.10.2.min.js"></script>
        <script src="{{url('/')}}/js/popper.min.js"></script>
        <script src="{{url('/')}}/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="{{url('/')}}/js/aos.js"></script>
        <script src="{{url('/')}}/js/main.js"></script>
       <script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap4.min.js"></script>
        <script type="text/javascript">
            /* Responsive Nav js **/
            $(".mainSidebarToggle").on("click", function () {
                $(".dashboard-wrpper").toggleClass("expand-menu-open");
            });

            $(".close-menus").on("click", function () {
                $(".dashboard-wrpper").removeClass("expand-menu-open");
            });

            $(document).ready(function(){
                var table =$('#datatable').DataTable();
              
                $('#datatable tbody').on( 'click', 'tr', function () {
                    var data= table.row( this ).data() ;
                   // var id =data[1];
                     $('#product_id').val(data[1]);
                    $('#image_div').prepend(data[2]);
                    $('#product_desc').val(data[3]);
                    $('#product_qty').val(data[4]);
                    $('#product_rate').val(data[5]);
                    // $url = "{{route('addProduct.update',"+ id +")}}";
                    // document.getElementById("editProductForm").action= $url;
                 $('#editProductForm').attr('action','/addProduct/'+data[0]);
table.on('click','.edit',function(){
                   $('#Edit_Product').modal('show');

                });
                    
                } );
        
            });
            $(document).ready(function(){
                var table =$('#datatable').DataTable();
                table.on('click','.delete',function(){
                    $('#Delete_Product').modal('show');

                });
                $('#datatable tbody').on( 'click', 'tr', function () {
                    var data= table.row( this ).data() ;
                    var id =data[1];
                    $('#deleteProductForm').attr('action','/addProduct/'+data[0]);
                   
                } );
        
            });

            $(document).ready(function(){
                const chooseFile = document.getElementById("product_image");
                    const imgPreview = document.getElementById("image_div");

                    chooseFile.addEventListener("change", function () {
                    getImgData();
                    });

                    function getImgData() {
                    const files = chooseFile.files[0];
                    if (files) {
                        const fileReader = new FileReader();
                        fileReader.readAsDataURL(files);
                        fileReader.addEventListener("load", function () {
                        imgPreview.style.display = "block";
                        imgPreview.innerHTML = '<img src="' + this.result + '" height="100" width="100" overflow: hidden;/>';
                        });    
                    }
                    }
            });
        </script>
        <script>
        $(document).ready(function(){
$('.edit').on(click)

        });

//           $(document).ready(function(){

//                 $('#Edit_Product').submit(function(e){
//       //  e.preventDefault(e);
//       // var form_data = new FormData(document.getElementById(this));
//         // $('#product_id').val(data[1]);
//         //             $('#image_div').prepend(data[2]);
//         //             $('#product_desc').val(data[3]);
//         //             $('#product_qty').val(data[4]);
//         //             $('#product_rate').val(data[5]);

// var token = $('meta[name="csrf-token"]').attr('content');
//             var myForm=document.getElementById('editProductForm');
// var form_data = new FormData(myForm);
//                 var formURL ='update_product_data';
//                   $.ajax({
//                 method: "POST",
//                 url: formURL,
//                 data:form_data,
//                 headers: { 'X-CSRF-TOKEN': token },
//                 processData: false,
//                 contentType: false,
//                 success:function(data) {
//                     alert(data);
//                  },
//                 error: function(jqXHR, textStatus, errorThrown) {
//                    alert("Error");
//                 }
//                   });

//             });
//           });
        </script>
    </body>
</html>
