<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link href="css/icofont.min.css" rel="stylesheet" />
    <link href="css/boxicons.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/aos.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/responsive.css" />
    <title>Dashboard</title>
</head>

<body class="dashboard-wrpper">
    <div class="main-wrpper owners-dashboard">
        <div class="fix-dashheader">
            <section id="header" class="topbar d-none d-lg-block">
                <div class="container1 d-flex">
                    <div class="logo mr-auto">
                        <a href="index.html"><img src="images/mytown-logo.png" alt="MyTown Logo" /></a>
                        <span class="category-icon-shopper" style="color: #ff4500;"><img src="images/shopper_icon.png"
                                alt="" /> Shopper</span>
                        <!-- Uncomment below if you prefer to use an image logo -->
                        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
                    </div>

                    <form class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2" type="search" placeholder="Enter Location"
                            aria-label="Enter Location" />
                        <input class="form-control mr-sm-2" type="search" placeholder="Product Type or Service Type"
                            aria-label="Product Type or Service Type" />
                        <button class="btn btn-primary my-2 my-sm-0" type="submit">Search</button>
                    </form>

                    <div class="contact-info mr-auto">
                        <ul>
                            <li>
                                <a href="#"> <i class="icofont-shopping-cart"
                                        style="padding-right: 30px; font-size: 30px;"></i></a>
                                <a href="#">
                                    <i class="icofont-bell-alt"> <em class="number5">0</em></i>
                                </a>

                                <span class="user-name dropdown">
                                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                                        <p>
                                            Aarti Kore<br />
                                            Kharadi, Pune<br />
                                            India
                                        </p>
                                    </a>
                                    <div class="dropdown-menu">
                                        <div class="menu-line">
                                            <ul>
                                                <strong>Your Lists</strong>
                                                <li><a href="">Create a Wish List</a></li>
                                                <li><a href="">Create a Wish List</a></li>
                                                <li><a href="">Create a Wish List</a></li>
                                            </ul>

                                            <ul style="border-right: none;">
                                                <strong>Your Account</strong>
                                                <li><a href="#">Your Account</a></li>
                                                <li><a href="#">Your Orders</a></li>
                                                <li><a href="">Logout</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>

            <!-- ======= Header ======= -->
            <header class="topbar2">
                <div class="container d-flex">
                    <nav class="nav-menu d-none d-lg-block">
                        <ul>
                            <li><a href="index.html">Feed</a></li>
                            <li><a href="#about">Contacts</a></li>
                            <li><a href="#services">Business Contacts</a></li>
                            <li><a href="#portfolio">Purchase History</a></li>
                            <li><a href="#team">Posts</a></li>
                            <li class="pad-l"><a href="#team">My Shipments</a></li>
                            <li><a href="#pricing">My Travel Plans</a></li>
                        </ul>
                    </nav>
                    <!-- .nav-menu -->
                </div>
            </header>
            <!-- End Header -->
        </div>

        <div class="left-sidebar">
            <div class="main-sidebar-body">
                <ul class="nav nav-tabs" id="nav-tab" role="tablist">
                    <li>
                        <a class="nav-link active" id="tab1-tab" data-toggle="tab" href="#tab1" role="tab"
                            aria-controls="tab1" aria-selected="true">
                            <div class="tab-img">
                                <i class="fas fa-user"></i>
                            </div>
                            <span>My Contacts</span>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" id="tab1-tab" data-toggle="tab" href="#tab1" role="tab"
                            aria-controls="tab1" aria-selected="true">
                            <div class="tab-img">
                                <i class="fas fa-user-tie"></i>

                            </div>
                            <span>My Business Contacts</span>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" id="tab2-tab" data-toggle="tab" href="#tab2" role="tab"
                            aria-controls="tab2" aria-selected="false">
                            <div class="tab-img">
                                <i class="fas fa-shopping-basket"></i>
                            </div>
                            <span>My Purchases</span>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" id="tab3-tab" data-toggle="tab" href="#tab3" role="tab"
                            aria-controls="tab3" aria-selected="false">
                            <div class="tab-img">
                                <i class="fas fa-truck"></i>

                            </div>
                            <span>My Shipments</span>
                        </a>
                    </li>
                    <!--   <li>
                            <a class="nav-link" id="tab4-tab" data-toggle="tab" href="#tab4" role="tab" aria-controls="tab4" aria-selected="false">
                                <div class="tab-img">
                                    <img class="img1" src="images/ic4.png" />
                                    <img class="img2" src="images/ic4-hover.png" />
                                </div>
                                <span>Edit Products List</span>
                            </a>
                        </li>-->
                    <li>
                        <a class="nav-link" id="tab5-tab" data-toggle="tab" href="#tab5" role="tab"
                            aria-controls="tab5" aria-selected="false">
                            <div class="tab-img">
                                <i class="far fa-credit-card"></i>
                            </div>
                            <span>My Expenses</span>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" id="tab6-tab" data-toggle="tab" href="#tab6" role="tab"
                            aria-controls="tab6" aria-selected="false">
                            <div class="tab-img">


                                <i class="fas fa-globe"></i>
                            </div>
                            <span>My Locations</span>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" id="tab7-tab" data-toggle="tab" href="#tab7" role="tab"
                            aria-controls="tab7" aria-selected="false">
                            <div class="tab-img">
                                <i class="fas fa-sign-out-alt"></i>
                            </div>
                            <span>Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="right-sldier">
            <div class="top-header">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <ul class="left-links">
                            <li>
                                <a href="#" class="mainSidebarToggle"><i class="far fa-bars"></i></a>
                            </li>
                            <li>
                                <img src="images/admin-icon.png" alt="" />
                                Shopper's Dashboard
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <ul class="admin-info">
                            <li>
                                <form>
                                    <div class="form-group">
                                        <input type="text" class="form-control" />
                                        <button type="submit"><i class="far fa-search"></i></button>
                                    </div>
                                </form>
                            </li>

                            <li class="dropdown notification-drop">
                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-bell"></i>
                                </button>
                                <!-- <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <div class="notifications-menu">
                                            <a class="dropdown-item d-flex p-3 border-bottom rounded-top" href="#">
                                                <span class="avatar avatar-md mr-3 fs-20 align-self-center cover-image bg-primary brround">
                                                    <i class="fa fa-upload"></i>
                                                </span>
                                                <div>
                                                    <span class="font-weight-bold text-dark"> New file Uploaded </span>
                                                    <div class="small text-muted d-flex">
                                                        5 hour ago
                                                    </div>
                                                </div>
                                            </a>
                                            <a class="dropdown-item d-flex p-3 border-bottom" href="#">
                                                <span class="avatar avatar-md fs-20 mr-3 align-self-center cover-image bg-teal brround">
                                                    <i class="fas fa-chevron-circle-up"></i>
                                                </span>
                                                <div>
                                                    <span class="font-weight-bold text-dark"> Account Updated</span>
                                                    <div class="small text-muted d-flex">
                                                        20 mins ago
                                                    </div>
                                                </div>
                                            </a>
                                            <a class="dropdown-item d-flex p-3 border-bottom" href="#">
                                                <span class="avatar avatar-md fs-20 mr-3 align-self-center cover-image bg-info brround">
                                                    <i class="fa fa-shopping-bag"></i>
                                                </span>
                                                <div>
                                                    <span class="font-weight-bold text-dark"> Order's Recevied</span>
                                                    <div class="small text-muted d-flex">
                                                        1 hour ago
                                                    </div>
                                                </div>
                                            </a>
                                            <a class="dropdown-item d-flex p-3 border-bottom" href="#">
                                                <span class="avatar avatar-md mr-3 fs-20 align-self-center cover-image bg-pink brround">
                                                    <i class="fa fa-database"></i>
                                                </span>
                                                <div>
                                                    <span class="font-weight-bold text-dark">Server Rebooted</span>
                                                    <div class="small text-muted d-flex">
                                                        2 hour ago
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <a href="#" class="dropdown-item text-center notifications-menu1">View all Notification</a>s
                                    </div> -->
                            </li>
                            <li class="dropdown user-drop">
                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="images/user.jpg" align="" />
                                </button>
                                <!-- <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div> -->
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="center-main-blocker">
                <div class="tab-content" id="nav-tabContent">

                    <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab1-tab">
                        <div class="dash-action-lst">
                            <ul>
                                <li>
                                    <a href="#">
                                        <div class="dash-box-in">
                                            <h3>My Contacts</h3>
                                            <h2>1,50,000</h2>
                                            <div class="dash-icbx">
                                                <img src="images/dash-ic1.png" alt="" />
                                            </div>
                                            <img src="images/img-1.png" alt="" class="dash-shp" />
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="dash-box-in">
                                            <h3>My Business Contacts</h3>
                                            <h2>1,50,000</h2>
                                            <div class="dash-icbx">
                                                <img src="images/dash-ic2.png" alt="" />
                                            </div>
                                            <img src="images/img-1.png" alt="" class="dash-shp" />
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="dash-box-in">
                                            <h3>My Purchases</h3>
                                            <h2>1,50,000</h2>
                                            <div class="dash-icbx">
                                                <img src="images/dash-ic3.png" alt="" />
                                            </div>
                                            <img src="images/img-1.png" alt="" class="dash-shp" />
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="dash-box-in">
                                            <h3>My Shipments</h3>
                                            <h2>1,50,000</h2>
                                            <div class="dash-icbx">
                                                <img src="images/dash-ic4.png" alt="" />
                                            </div>
                                            <img src="images/img-1.png" alt="" class="dash-shp" />
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="dash-box-in">
                                            <h3>My Expenses</h3>
                                            <h2>1,50,000</h2>
                                            <div class="dash-icbx">
                                                <img src="images/dash-ic5.png" alt="" />
                                            </div>
                                            <img src="images/img-1.png" alt="" class="dash-shp" />
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="dash-box-in">
                                            <h3>My Locations</h3>
                                            <h2>1,50,000</h2>
                                            <div class="dash-icbx">
                                                <img src="images/dash-ic6.png" alt="" />
                                            </div>
                                            <img src="images/img-1.png" alt="" class="dash-shp" />
                                        </div>
                                    </a>
                                </li>


                            </ul>
                        </div>
                        <div class="prolst-wrpbx">
                            <div class="pro-had">
                                <div class="row">
                                    <div class="col-md-5 col-sm-5">
                                        <div class="pro-hedbing">
                                            <h3>Lorem Ipsum is simply dummy</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-sm-7">
                                        <div class="sort-lst-bx">
                                            <ul>
                                                <li>
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option>1-10 Entries</option>
                                                            <option>1-50 Entries</option>
                                                        </select>
                                                        <div class="sort-ic">
                                                            <img src="images/exprot.png" alt="" />
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option>Filter Name List</option>
                                                            <option>1-50 Entries</option>
                                                        </select>
                                                        <div class="sort-ic">
                                                            <img src="images/filter.png" alt="" />
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="protable-wrp">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <p>Buyer</p>
                                                </th>
                                                <th>
                                                    <p>Order Date</p>
                                                </th>
                                                <th>
                                                    <p>Total Price</p>
                                                </th>
                                                <th>
                                                    <p>Actions</p>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <p>Ketan Kulkarni Karvengar, Pune, Maharashtra, India</p>
                                                </td>
                                                <td>
                                                    <p>2021-08-08 09:53:11</p>
                                                </td>
                                                <td>
                                                    <p>1</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table"><i
                                                                class="fas fa-edit"></i></a><a href="#"
                                                            class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Ketan Kulkarni Karvengar, Pune, Maharashtra, India</p>
                                                </td>
                                                <td>
                                                    <p>2021-08-08 09:53:11</p>
                                                </td>
                                                <td>
                                                    <p>1</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table"><i
                                                                class="fas fa-edit"></i></a><a href="#"
                                                            class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Ketan Kulkarni Karvengar, Pune, Maharashtra, India</p>
                                                </td>
                                                <td>
                                                    <p>2021-08-08 09:53:11</p>
                                                </td>
                                                <td>
                                                    <p>1</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table"><i
                                                                class="fas fa-edit"></i></a><a href="#"
                                                            class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Ketan Kulkarni Karvengar, Pune, Maharashtra, India</p>
                                                </td>
                                                <td>
                                                    <p>2021-08-08 09:53:11</p>
                                                </td>
                                                <td>
                                                    <p>1</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table"><i
                                                                class="fas fa-edit"></i></a><a href="#"
                                                            class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Ketan Kulkarni Karvengar, Pune, Maharashtra, India</p>
                                                </td>
                                                <td>
                                                    <p>2021-08-08 09:53:11</p>
                                                </td>
                                                <td>
                                                    <p>1</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table"><i
                                                                class="fas fa-edit"></i></a><a href="#"
                                                            class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Ketan Kulkarni Karvengar, Pune, Maharashtra, India</p>
                                                </td>
                                                <td>
                                                    <p>2021-08-08 09:53:11</p>
                                                </td>
                                                <td>
                                                    <p>1</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table"><i
                                                                class="fas fa-edit"></i></a><a href="#"
                                                            class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab2-tab">
                        <div class="dash-action-lst">
                            <ul>
                                <li>
                                    <a href="#">
                                        <div class="dash-box-in">
                                            <h3>Shoppers</h3>
                                            <h2>1,50,000</h2>
                                            <div class="dash-icbx">
                                                <img src="images/dash-ic1.png" alt="" />
                                            </div>
                                            <img src="images/img-1.png" alt="" class="dash-shp" />
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="dash-box-in">
                                            <h3>Orders</h3>
                                            <h2>1,50,000</h2>
                                            <div class="dash-icbx">
                                                <img src="images/dash-ic2.png" alt="" />
                                            </div>
                                            <img src="images/img-1.png" alt="" class="dash-shp" />
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="dash-box-in">
                                            <h3>Shipment Details</h3>
                                            <h2>1,50,000</h2>
                                            <div class="dash-icbx">
                                                <img src="images/dash-ic3.png" alt="" />
                                            </div>
                                            <img src="images/img-1.png" alt="" class="dash-shp" />
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="dash-box-in">
                                            <h3>Products</h3>
                                            <h2>1,50,000</h2>
                                            <div class="dash-icbx">
                                                <img src="images/dash-ic4.png" alt="" />
                                            </div>
                                            <img src="images/img-1.png" alt="" class="dash-shp" />
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="dash-box-in">
                                            <h3>Monthly Sales</h3>
                                            <h2>1,50,000</h2>
                                            <div class="dash-icbx">
                                                <img src="images/dash-ic5.png" alt="" />
                                            </div>
                                            <img src="images/img-1.png" alt="" class="dash-shp" />
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="dash-box-in">
                                            <h3>People in Store</h3>
                                            <h2>1,50,000</h2>
                                            <div class="dash-icbx">
                                                <img src="images/dash-ic6.png" alt="" />
                                            </div>
                                            <img src="images/img-1.png" alt="" class="dash-shp" />
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="prolst-wrpbx">
                            <div class="pro-had">
                                <div class="row">
                                    <div class="col-md-5 col-sm-5">
                                        <div class="pro-hedbing">
                                            <h3>Lorem Ipsum is simply dummy</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-sm-7">
                                        <div class="sort-lst-bx">
                                            <ul>
                                                <li>
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option>1-10 Entries</option>
                                                            <option>1-50 Entries</option>
                                                        </select>
                                                        <div class="sort-ic">
                                                            <img src="images/exprot.png" alt="" />
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option>Filter Name List</option>
                                                            <option>1-50 Entries</option>
                                                        </select>
                                                        <div class="sort-ic">
                                                            <img src="images/filter.png" alt="" />
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="protable-wrp">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <p>Name</p>
                                                </th>
                                                <th>
                                                    <p>Email Address</p>
                                                </th>
                                                <th>
                                                    <p>Phone</p>
                                                </th>
                                                <th>
                                                    <p>Town</p>
                                                </th>
                                                <th>
                                                    <p>Registered</p>
                                                </th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <p>Andrew Young</p>
                                                </td>
                                                <td>
                                                    <p><a href="mailto:andrewyoung@gmail.com">andrewyoung@gmail.com</a>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p><a href="tel:9898998989">98989 98989</a></p>
                                                </td>
                                                <td>
                                                    <p>Pune, Maharashtra, India</p>
                                                </td>
                                                <td>
                                                    <p>21-07-02</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table"><i
                                                                class="fas fa-edit"></i></a><a href="#"
                                                            class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Andrew Young</p>
                                                </td>
                                                <td>
                                                    <p><a href="mailto:andrewyoung@gmail.com">andrewyoung@gmail.com</a>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p><a href="tel:9898998989">98989 98989</a></p>
                                                </td>
                                                <td>
                                                    <p>Pune, Maharashtra, India</p>
                                                </td>
                                                <td>
                                                    <p>21-07-02</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table"><i
                                                                class="fas fa-edit"></i></a><a href="#"
                                                            class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Andrew Young</p>
                                                </td>
                                                <td>
                                                    <p><a href="mailto:andrewyoung@gmail.com">andrewyoung@gmail.com</a>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p><a href="tel:9898998989">98989 98989</a></p>
                                                </td>
                                                <td>
                                                    <p>Pune, Maharashtra, India</p>
                                                </td>
                                                <td>
                                                    <p>21-07-02</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table"><i
                                                                class="fas fa-edit"></i></a><a href="#"
                                                            class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Andrew Young</p>
                                                </td>
                                                <td>
                                                    <p><a href="mailto:andrewyoung@gmail.com">andrewyoung@gmail.com</a>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p><a href="tel:9898998989">98989 98989</a></p>
                                                </td>
                                                <td>
                                                    <p>Pune, Maharashtra, India</p>
                                                </td>
                                                <td>
                                                    <p>21-07-02</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table"><i
                                                                class="fas fa-edit"></i></a><a href="#"
                                                            class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Andrew Young</p>
                                                </td>
                                                <td>
                                                    <p><a href="mailto:andrewyoung@gmail.com">andrewyoung@gmail.com</a>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p><a href="tel:9898998989">98989 98989</a></p>
                                                </td>
                                                <td>
                                                    <p>Pune, Maharashtra, India</p>
                                                </td>
                                                <td>
                                                    <p>21-07-02</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table"><i
                                                                class="fas fa-edit"></i></a><a href="#"
                                                            class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Andrew Young</p>
                                                </td>
                                                <td>
                                                    <p><a href="mailto:andrewyoung@gmail.com">andrewyoung@gmail.com</a>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p><a href="tel:9898998989">98989 98989</a></p>
                                                </td>
                                                <td>
                                                    <p>Pune, Maharashtra, India</p>
                                                </td>
                                                <td>
                                                    <p>21-07-02</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table"><i
                                                                class="fas fa-edit"></i></a><a href="#"
                                                            class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Andrew Young</p>
                                                </td>
                                                <td>
                                                    <p><a href="mailto:andrewyoung@gmail.com">andrewyoung@gmail.com</a>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p><a href="tel:9898998989">98989 98989</a></p>
                                                </td>
                                                <td>
                                                    <p>Pune, Maharashtra, India</p>
                                                </td>
                                                <td>
                                                    <p>21-07-02</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table"><i
                                                                class="fas fa-edit"></i></a><a href="#"
                                                            class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Andrew Young</p>
                                                </td>
                                                <td>
                                                    <p><a href="mailto:andrewyoung@gmail.com">andrewyoung@gmail.com</a>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p><a href="tel:9898998989">98989 98989</a></p>
                                                </td>
                                                <td>
                                                    <p>Pune, Maharashtra, India</p>
                                                </td>
                                                <td>
                                                    <p>21-07-02</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table"><i
                                                                class="fas fa-edit"></i></a><a href="#"
                                                            class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Andrew Young</p>
                                                </td>
                                                <td>
                                                    <p><a href="mailto:andrewyoung@gmail.com">andrewyoung@gmail.com</a>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p><a href="tel:9898998989">98989 98989</a></p>
                                                </td>
                                                <td>
                                                    <p>Pune, Maharashtra, India</p>
                                                </td>
                                                <td>
                                                    <p>21-07-02</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table"><i
                                                                class="fas fa-edit"></i></a><a href="#"
                                                            class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Andrew Young</p>
                                                </td>
                                                <td>
                                                    <p><a href="mailto:andrewyoung@gmail.com">andrewyoung@gmail.com</a>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p><a href="tel:9898998989">98989 98989</a></p>
                                                </td>
                                                <td>
                                                    <p>Pune, Maharashtra, India</p>
                                                </td>
                                                <td>
                                                    <p>21-07-02</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table"><i
                                                                class="fas fa-edit"></i></a><a href="#"
                                                            class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Andrew Young</p>
                                                </td>
                                                <td>
                                                    <p><a href="mailto:andrewyoung@gmail.com">andrewyoung@gmail.com</a>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p><a href="tel:9898998989">98989 98989</a></p>
                                                </td>
                                                <td>
                                                    <p>Pune, Maharashtra, India</p>
                                                </td>
                                                <td>
                                                    <p>21-07-02</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table"><i
                                                                class="fas fa-edit"></i></a><a href="#"
                                                            class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Andrew Young</p>
                                                </td>
                                                <td>
                                                    <p><a href="mailto:andrewyoung@gmail.com">andrewyoung@gmail.com</a>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p><a href="tel:9898998989">98989 98989</a></p>
                                                </td>
                                                <td>
                                                    <p>Pune, Maharashtra, India</p>
                                                </td>
                                                <td>
                                                    <p>21-07-02</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table"><i
                                                                class="fas fa-edit"></i></a><a href="#"
                                                            class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="tab4" role="tabpanel" aria-labelledby="tab4-tab">
                        <div class="dash-action-lst">
                            <ul>
                                <li>
                                    <a href="#">
                                        <div class="dash-box-in">
                                            <h3>Shoppers</h3>
                                            <h2>1,50,000</h2>
                                            <div class="dash-icbx">
                                                <img src="images/dash-ic1.png" alt="" />
                                            </div>
                                            <img src="images/img-1.png" alt="" class="dash-shp" />
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="dash-box-in">
                                            <h3>Orders</h3>
                                            <h2>1,50,000</h2>
                                            <div class="dash-icbx">
                                                <img src="images/dash-ic2.png" alt="" />
                                            </div>
                                            <img src="images/img-1.png" alt="" class="dash-shp" />
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="dash-box-in">
                                            <h3>Shipment Details</h3>
                                            <h2>1,50,000</h2>
                                            <div class="dash-icbx">
                                                <img src="images/dash-ic3.png" alt="" />
                                            </div>
                                            <img src="images/img-1.png" alt="" class="dash-shp" />
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="dash-box-in">
                                            <h3>Products</h3>
                                            <h2>1,50,000</h2>
                                            <div class="dash-icbx">
                                                <img src="images/dash-ic4.png" alt="" />
                                            </div>
                                            <img src="images/img-1.png" alt="" class="dash-shp" />
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="dash-box-in">
                                            <h3>Monthly Sales</h3>
                                            <h2>1,50,000</h2>
                                            <div class="dash-icbx">
                                                <img src="images/dash-ic5.png" alt="" />
                                            </div>
                                            <img src="images/img-1.png" alt="" class="dash-shp" />
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="dash-box-in">
                                            <h3>People in Store</h3>
                                            <h2>1,50,000</h2>
                                            <div class="dash-icbx">
                                                <img src="images/dash-ic6.png" alt="" />
                                            </div>
                                            <img src="images/img-1.png" alt="" class="dash-shp" />
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="prolst-wrpbx">
                            <div class="pro-had">
                                <div class="row">
                                    <div class="col-md-5 col-sm-5">
                                        <div class="pro-hedbing">
                                            <h3>Lorem Ipsum is simply dummy</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-sm-7">
                                        <div class="sort-lst-bx">
                                            <ul>
                                                <li>
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option>1-10 Entries</option>
                                                            <option>1-50 Entries</option>
                                                        </select>
                                                        <div class="sort-ic">
                                                            <img src="images/exprot.png" alt="" />
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option>Filter Name List</option>
                                                            <option>1-50 Entries</option>
                                                        </select>
                                                        <div class="sort-ic">
                                                            <img src="images/filter.png" alt="" />
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="protable-wrp information-tbl">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <p>Products</p>
                                                </th>
                                                <th>
                                                    <p>Description</p>
                                                </th>
                                                <th>
                                                    <p>Rate</p>
                                                </th>
                                                <th>
                                                    <p>Discount</p>
                                                </th>
                                                <th>
                                                    <p>Actions</p>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <p class="tab-img"><img src="images/tab-img-01.jpg"
                                                            alt="img" /></p>
                                                </td>
                                                <td>
                                                    <p>
                                                        Lorem Ipsum is simply dummy text of the printing and typesetting
                                                        industry. Lorem Ipsum has been the industry's standard dummy
                                                        text ever since the 1500s, when an unknown printer
                                                        took a galley of type and scrambled it to make a type specimen
                                                        book.
                                                    </p>
                                                </td>
                                                <td>
                                                    <p>Rs.200</p>
                                                </td>
                                                <td>
                                                    <p>Rs.20</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table" data-toggle="modal"
                                                            data-target="#myModal"><i class="fas fa-edit"></i></a><a
                                                            href="#" class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p class="tab-img"><img src="images/tab-img-01.jpg"
                                                            alt="img" /></p>
                                                </td>
                                                <td>
                                                    <p>
                                                        Lorem Ipsum is simply dummy text of the printing and typesetting
                                                        industry. Lorem Ipsum has been the industry's standard dummy
                                                        text ever since the 1500s, when an unknown printer
                                                        took a galley of type and scrambled it to make a type specimen
                                                        book.
                                                    </p>
                                                </td>
                                                <td>
                                                    <p>Rs.200</p>
                                                </td>
                                                <td>
                                                    <p>Rs.20</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table" data-toggle="modal"
                                                            data-target="#myModal"><i class="fas fa-edit"></i></a><a
                                                            href="#" class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p class="tab-img"><img src="images/tab-img-01.jpg"
                                                            alt="img" /></p>
                                                </td>
                                                <td>
                                                    <p>
                                                        Lorem Ipsum is simply dummy text of the printing and typesetting
                                                        industry. Lorem Ipsum has been the industry's standard dummy
                                                        text ever since the 1500s, when an unknown printer
                                                        took a galley of type and scrambled it to make a type specimen
                                                        book.
                                                    </p>
                                                </td>
                                                <td>
                                                    <p>Rs.200</p>
                                                </td>
                                                <td>
                                                    <p>Rs.20</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table" data-toggle="modal"
                                                            data-target="#myModal"><i class="fas fa-edit"></i></a><a
                                                            href="#" class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p class="tab-img"><img src="images/tab-img-01.jpg"
                                                            alt="img" /></p>
                                                </td>
                                                <td>
                                                    <p>
                                                        Lorem Ipsum is simply dummy text of the printing and typesetting
                                                        industry. Lorem Ipsum has been the industry's standard dummy
                                                        text ever since the 1500s, when an unknown printer
                                                        took a galley of type and scrambled it to make a type specimen
                                                        book.
                                                    </p>
                                                </td>
                                                <td>
                                                    <p>Rs.200</p>
                                                </td>
                                                <td>
                                                    <p>Rs.20</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table" data-toggle="modal"
                                                            data-target="#myModal"><i class="fas fa-edit"></i></a><a
                                                            href="#" class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p class="tab-img"><img src="images/tab-img-01.jpg"
                                                            alt="img" /></p>
                                                </td>
                                                <td>
                                                    <p>
                                                        Lorem Ipsum is simply dummy text of the printing and typesetting
                                                        industry. Lorem Ipsum has been the industry's standard dummy
                                                        text ever since the 1500s, when an unknown printer
                                                        took a galley of type and scrambled it to make a type specimen
                                                        book.
                                                    </p>
                                                </td>
                                                <td>
                                                    <p>Rs.200</p>
                                                </td>
                                                <td>
                                                    <p>Rs.20</p>
                                                </td>
                                                <td>
                                                    <p>
                                                        <a href="#" class="edit-table" data-toggle="modal"
                                                            data-target="#myModal"><i class="fas fa-edit"></i></a><a
                                                            href="#" class="delete-table"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    </p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">...</div>
                    <div class="tab-pane fade" id="tab5" role="tabpanel" aria-labelledby="tab5-tab">...</div>
                    <div class="tab-pane fade" id="tab6" role="tabpanel" aria-labelledby="tab6-tab">...</div>
                    <div class="tab-pane fade" id="tab7" role="tabpanel" aria-labelledby="tab7-tab">...</div>
                </div>
            </div>

            <!-- ======= Footer ======= -->
            <footer id="footer">
                <div class="footer-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 footer-contact">
                                <h3>MyTown</h3>
                                <p>
                                    A108 Adam Street <br />
                                    New York, NY 535022<br />
                                    United States <br />
                                    <br />
                                    <strong>Phone:</strong> +1 5589 55488 55<br />
                                    <strong>Email:</strong> info@example.com<br />
                                </p>
                            </div>

                            <div class="col-lg-2 col-md-6 footer-links">
                                <h4>Useful Links</h4>
                                <ul>
                                    <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                                    <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
                                    <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
                                    <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
                                    <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
                                </ul>
                            </div>

                            <div class="col-lg-3 col-md-6 footer-links">
                                <h4>Our Services</h4>
                                <ul>
                                    <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
                                    <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
                                    <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
                                    <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
                                    <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
                                </ul>
                            </div>

                            <div class="col-lg-4 col-md-6 footer-newsletter">
                                <h4>Join Our Newsletter</h4>
                                <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                                <form action="" method="post"><input type="email" name="email" /><input type="submit"
                                        value="Subscribe" /></form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container d-lg-flex py-4">
                    <div class="mr-lg-auto text-center text-lg-left">
                        <div class="copyright">
                            &copy; Copyright <strong><span>MyTown</span></strong>. All Rights Reserved
                        </div>
                    </div>
                    <div class="social-links text-center text-lg-right pt-3 pt-lg-0">
                        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                    </div>
                </div>
            </footer>
            <!-- End Footer -->

            <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
        </div>
    </div>

    <div class="modal fade products-pop" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="prd-pop-details">
                        <h4 class="modal-title">Add Products</h4>
                        <p>Products</p>

                        <form>
                            <div class="form-group">
                                <div class="file-upd">
                                    <input type="file" id="real-file" hidden="hidden" class="int-file" />
                                    <button type="button" id="custom-button" class="int-btn">Browse</button>
                                    <span id="custom-text" class="int-text">No file chosen, yet.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="file-upd">
                                    <input type="file" id="real-file" hidden="hidden" class="int-file" />
                                    <button type="button" id="custom-button" class="int-btn">Browse</button>
                                    <span id="custom-text" class="int-text">No file chosen, yet.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="file-upd">
                                    <input type="file" id="real-file" hidden="hidden" class="int-file" />
                                    <button type="button" id="custom-button" class="int-btn">Browse</button>
                                    <span id="custom-text" class="int-text">No file chosen, yet.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="file-upd">
                                    <input type="file" id="real-file" hidden="hidden" />
                                    <button type="button" id="custom-button" class="int-btn">Browse</button>
                                    <span id="custom-text" class="int-text">No file chosen, yet.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="file-upd">
                                    <input type="file" id="real-file" hidden="hidden" />
                                    <button type="button" id="custom-button" class="int-btn">Browse</button>
                                    <span id="custom-text" class="int-text">No file chosen, yet.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="file-upd">
                                    <input type="file" id="real-file" hidden="hidden" />
                                    <button type="button" id="custom-button" class="int-btn">Browse</button>
                                    <span id="custom-text" class="int-text">No file chosen, yet.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" type="text"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Rate</label>
                                <input type="text" class="form-control" type="text" />
                            </div>
                            <div class="form-group">
                                <label>Discount</label>
                                <input type="text" class="form-control" type="text" />
                            </div>
                            <div class="form-group">
                                <button href="#" class="btn-main" type="submit">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="js/jquery-3.5.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/aos.js"></script>
    <script src="js/main.js"></script>
    <script type="text/javascript">
        /* Responsive Nav js **/
        $(".mainSidebarToggle").on("click", function() {
            $(".dashboard-wrpper").toggleClass("expand-menu-open");
        });

        $(".close-menus").on("click", function() {
            $(".dashboard-wrpper").removeClass("expand-menu-open");
        });
    </script>
</body>

</html>
