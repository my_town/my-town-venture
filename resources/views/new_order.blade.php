@extends('layouts.new_MyTown')
@section('content')
@push('head')        <!-- End Header -->

<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="_base_url" content="{{ url('/update_perticular_item') }}">
<script src="{{ asset('/vendor/jquery/jquery.min.js') }}"></script>
<!-- <script src=""></script> -->
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="{{ asset('/js/form_validation.js') }}"></script>

<script type="text/javascript">
    
    $(document).on('change','#same-address',function(){
        if($(this).is(":checked")){
        $('#extra_address').hide();
      }
      else{
        $('#extra_address').show();
      }
    });

</script> 


        <!-- shopping-cart -->
       <section class="checkout-wrp">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-9 col-md-8 col-sm-12">
                       <div class="checkout-box">
                            <div class="orange-head">
                               <h3>Order Information</h3>
                            </div>



<?php 

//echo $users->place_id;
/*foreach ($users as $key => $value) {
    echo $value->name;
}*/




$items=json_decode($orders->items);

if(empty($items))
{
    echo "Record is not available.";
}else{?>
                            <div class="chakout-tbl">
                                <div class="tbl-box table-responsive">
                                    <table class="table">
                                        <tbody>
    <?php $sum = 0;?>
   @foreach ($items as $item)
        <?php $sum += $item->price*$item->quantity; ?>
    <?php $product=App\Models\Product::where('id',$item->id)->first();?>
                                            <tr>
                                                <td>
                                                    <div class="prd-img">
    <img src="{{ asset('/storage/images/products/'.$product->product_image) }}" alt=""/>{{$item->name}}
                                                    </div>
                                                </td>
                                                <td>
    <input type="number" name="qty_value" min="1" 
    item_id="{{$item->id}}" 
    order_id="{{$orders->id}}"
    total_price="{{ $sum }}" 
    value="{{ $item->quantity }}" id="number_quantity_{{$item->id}}" class="form-control">
                                                </td>
                                                <td>
            <span>₹{{ $item->price*$item->quantity }}</span>
                                                </td>
                                                <td>
                                                
        <form action="{{ url('/remove_perticular_item') }}" id="form1" method="post">
            @csrf
        <input type="hidden" name="item" value="<?php echo $item->id;?>"/>
        <input type="hidden" name="order_id" value="<?php echo $orders->id;?>"/>
            <!-- <input type="submit" value="X" name="submit" /> -->
        <button class="btn btn-link" role="link" type="submit" name="submit" value="X">X</button>

                                                </form>
                                                
                                                </td>
                                            </tr>
    @endforeach
                                            
                                        </tbody>
                                    </table>
                                </div>
                                <!-- <ul>
                                    <li><a href="#">Previous</a></li>
                                    <li><a href="#">Next</a></li>
                                </ul> -->
                            </div>

                            <?php } ?>
                       </div>

                       <div class="checkout-box">
                            <div class="orange-head">
                               <h3>Billing Address</h3>
                            </div>
                            <div class="checkout-frm">
                        
        <form action="{{ url('/final_order') }}" method="post" enctype="multipart/form-data" id="billing_details" autocomplete="off">
            @csrf


    <input type="hidden" name="order_id" value="<?php echo $orders->id;?>"/>
    <input type="hidden" name="total_amount" value="<?php echo $sum;?>"/>
                        
                                    <div class="row">
        <?php if(Auth::user()){?>

                            <div class="col-md-12 col-sm-12" style="margin:20px">
                                    <b>Name:-</b> <?php echo $users->name; ?></br>
                            <b>Phone Number:-</b> <?php echo $users->phone; ?></br>
                            <b>Email Address:-</b> <?php echo $users->email; ?></br>
                            <b>Address:-</b> <?php echo $users->place; ?>
                        </br></br></br>


        
            <input type="checkbox"  id="same-address">
            <label for="vehicle1">Shipping Address is same as Billing Address</label>
                                        
                                        </div>



                    <?php }else{?>


                    <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
             <input type="text" name="first_name" placeholder="First Name :" class="form-control" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                <input type="text" name="last_name" placeholder="Last Name :" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
            <input type="text" name="phone" placeholder="Phone Number :" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
            <input type="text" name="email" placeholder="Email Addres :" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group">
            <textarea class="form-control" name="address" rows="3" placeholder="Address :" required></textarea>
                                            </div>
                                        </div>
                                         <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
            <input type="text" name="country" placeholder="Country :" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                <input type="text" name="state" placeholder="State :" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                <input type="text" name="zip_code" placeholder="Zip/Postal Code :" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="same-address">
                                              <label class="custom-control-label" for="same-address">Shipping Address is same as Billing Address</label>
                                            </div>
                                        </div> 



                <?php }?>
                                       
                                  
                                           </div>

                                        <span id="extra_address">

                                            <div class="orange-head">
                               <h3>Shipping Address</h3>
                            </div>

                                            <div class="row">

                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
<input type="text" name="first_name2" placeholder="First Name :" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
<input type="text" name="last_name2" placeholder="Last Name :" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
<input type="text" name="phone2" placeholder="Phone Number :" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
<input type="text" name="email2" placeholder="Email Addres :" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group">
<textarea class="form-control" name="address2" rows="3" placeholder="Address :"></textarea>
                                            </div>
                                        </div>
                                         <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
<input type="text" name="country2" placeholder="Country :" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
<input type="text" name="state2" placeholder="State :" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
<input type="text" name="zip_code2" placeholder="Zip/Postal Code :" class="form-control">
                                            </div>
                                        </div>

                                    </div>

                                    </span>


                                    </div>
                             <!--    </form> -->
                                <!-- <ul>
                                    <li><a href="#">Previous</a></li>
                                    <li><a href="#">Next</a></li>
                                </ul> -->
                         
                       </div>

                        <div class="checkout-box">
                            <div class="orange-head">
                               <h3>Payment Options</h3>
                            </div>
                            <div class="payment-wrp">
                            <!--     <form> -->
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12 col-sm-12">
                                            <div class="radio payment-card">
                                              <label>  <input type="radio" name="optradio" value="credit_card" checked>
                                              Credit Card</label>
                                              <img src="{{url('images/payment1.png')}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-8 col-sm-12">
                                            <div class="form-group month-group">
                                                <label>Expiry Date</label>
                                                <select class="form-control" name="month">
                                                    <option>Month</option>
                                                    <option value="January">January</option>
                                                    <option value="February">February</option>
                                                    <option value="March">March</option>
                                                    <option value="April">April</option>
                                                    <option value="May">May</option>
                                                    <option value="June">June</option>
                                                    <option value="July">July</option>
                                                    <option value="August">August</option>
                                                    <option value="September">September</option>
                                                    <option value="October">October</option>
                                                    <option value="November">November</option>
                                                    <option value="December">December</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <select class="form-control" name="year">
                                                    <option>Year</option>
                                                    <option value="2021">2021</option>
                                                    <option value="2022">2022</option>
                                                    <option value="2023">2023</option>
                                                    <option value="2024">2024</option>
                                                    <option value="2025">2025</option>
                                                    <option value="2026">2026</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12">
                                            <div class="form-group card-input">
                                                <input type="text" placeholder="Card Number" class="form-control" name="card_number">
                                            <img src="{{url('images/payment2.png')}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <input type="text" placeholder="Security code" class="form-control" name="security_code">
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6 col-sm-12">
                                            <div class="radio payment-card">
                                              <label>  <input type="radio" name="optradio" value="paypal">
                                              Paypal</label>
                                              <img src="{{url('images/payment3.png')}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="custom-control custom-checkbox">
                                              <input type="checkbox" class="custom-control-input" id="conditions" required>
                                              <label class="custom-control-label" for="conditions">By Clicking the button you agree to the Terms and Conditions </label>
                                            </div>
                                        </div>
                                    </div>
                               
                            </div>
                       </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-sm-12">
                        <div class="price-block">
                            <h2>Price Details</h2>

                            <ul>
<?php
if(empty($items))
{
    echo "Record is not available";
}else{?>                                
    @foreach ($items as $item)
    <?php $product=App\Models\Product::where('id',$item->id)->first();?>
                            <li>{{$item->name}} (item {{$item->quantity}})<span>₹{{ $item->price*$item->quantity }}</span></li>
    @endforeach
<?php } ?>
    <!-- 
                                <li class="charges">Delivery Charges<span>Rs. 80</span></li> -->
<?php
if(empty($items))
{
    //echo "Success";
}else{ ?> 

<li class="total">SHIPPING CHARGES<span>₹ 80</span></li>
<li class="total">Total Balance <span>₹{{ $sum + 80 }}</span></li>

<?php } ?>
                            </ul>
                              <div class="promo">
                                <label>Add promo Code</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Add Promo code">
                                    <a href="#" class="add-button">ADD</a>
                                </div>
                            </div>

                           <!--  <a href="#" class="buy-button">Checkout</a> -->
                    <input type="submit" class="buy-button" name="submit" value="Checkout"/>
                            <p>Delivery to <span>Lorem Ipsum simply</span></p>
                        </div>
                    </div>
                     </form>


                </div>
            </div>
       </section>
        <!-- End shopping-cart -->

        <!-- ======= Footer ======= -->
        <footer id="footer">
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 footer-contact">
                            <h3>MyTown</h3>
                            <p>
                                A108 Adam Street <br />
                                New York, NY 535022<br />
                                United States <br />
                                <br />
                                <strong>Phone:</strong> +1 5589 55488 55<br />
                                <strong>Email:</strong> info@example.com<br />
                            </p>
                        </div>

                        <div class="col-lg-2 col-md-6 footer-links">
                            <h4>Useful Links</h4>
                            <ul>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
                            </ul>
                        </div>

                        <div class="col-lg-3 col-md-6 footer-links">
                            <h4>Our Services</h4>
                            <ul>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
                            </ul>
                        </div>

                        <div class="col-lg-4 col-md-6 footer-newsletter">
                            <h4>Join Our Newsletter</h4>
                            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                            <form action="" method="post"><input type="email" name="email" /><input type="submit" value="Subscribe" /></form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container d-lg-flex py-4">
                <div class="mr-lg-auto text-center text-lg-left">
                    <div class="copyright">
                        &copy; Copyright <strong><span>MyTown</span></strong>. All Rights Reserved
                    </div>
                </div>
                <div class="social-links text-center text-lg-right pt-3 pt-lg-0">
                    <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                    <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                    <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                    <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                    <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                </div>
            </div>
        </footer>
        <!-- End Footer -->

        <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

        <!-- Vendor JS Files -->
        <!-- <script src="assets/vendor/jquery/jquery.min.js"></script> -->
        <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="/vendor/jquery.easing/jquery.easing.min.js"></script>
        <script src="/vendor/php-email-form/validate.js"></script>
        <script src="/vendor/jquery-sticky/jquery.sticky.js"></script>
        <script src="/vendor/venobox/venobox.min.js"></script>
        <script src="/vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="/vendor/isotope-layout/isotope.pkgd.min.js"></script>
        <script src="/vendor/aos/aos.js"></script>

        <!-- Template Main JS File -->
        <script src="{{ asset('/js/main.js') }}"></script>

<script type="text/javascript">
    $(":input[name=qty_value]").bind('keyup mouseup', function () {
        var item_id = $(this).attr("item_id");
        var order_id = $(this).attr("order_id");
        var quantity = $('#number_quantity_'+item_id).val();
        var price = $(this).attr("total_price");
        var token = $('meta[name="csrf-token"]').attr('content');
        var APP_URL = $('meta[name="_base_url"]').attr('content');
        /*alert(order_id);*/
        $.ajax({
                method: "POST",
                url: APP_URL,
                data:{'order_id':order_id,'item_id':item_id,'quantity':quantity,'price':price},
                headers: { 'X-CSRF-TOKEN': token },
                success:function(data) {
                    window.location.reload();
                    /*alert("quantity :-"+ data);*/
            },
                error: function(jqXHR, textStatus, errorThrown) {
                   alert("Error");
                }
            });
        return false;

});
</script>

@endsection

    