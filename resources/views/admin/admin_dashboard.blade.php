@extends('layouts.my_town_admin')
@section('content')
    <div class="main-content side-content pt-0 padding-top">

        <div class="center-main-blocker">
            <div class="tab-content" id="nav-tabContent">

                <!-- Row -->
                <div class="tab-pane fade show active" id="tab0" role="tabpanel" aria-labelledby="tab1-tab">

                     
                    <div class="card-body-two card overflow-hidden bg-orange" onclick="showTab('tab1')">
                        <div class="d-flex clearfix">
                            <div class="text-left">
                                <p class="mb-0 text-white fs-24">Businesses</p>
                                <h1 class="mb-0 text-white fs-26">{{ $businessCount }}</h1>

                            </div>
                            <div class="ml-auto">
                                <span class="bg-primary-bll icon-service text-white ">
                                    <img src="{{ url('/') }}/images/dashboard/home-icon4.png">
                                </span>
                            </div>
                        </div>
                        <img src="{{ url('/') }}/images/dashboard/img-1.png" alt="img" class="img-card-circle">
                    </div>
                    <div class="card-body-two card overflow-hidden bg-red" onclick="showTab('tab2')">
                        <div class="d-flex clearfix">
                            <div class="text-left">
                                <p class="mb-0 text-white fs-24">Shoppers</p>
                                <h1 class="mb-0 text-white fs-26">{{ $shopperCount }}</h1>

                            </div>
                            <div class="ml-auto">
                                <span class="bg-primary-bll icon-service text-white ">
                                    <img src="{{ url('/') }}/images/dashboard/home-icon5.png">
                                </span>
                            </div>
                        </div>
                        <img src="{{ url('/') }}/images/dashboard/img-1.png" alt="img" class="img-card-circle">
                    </div>
                    <div class="card-body-two card overflow-hidden bg-black-7"  onclick="showTab('tab4')">
                        <div class="d-flex clearfix">
                            <div class="text-left">
                                <p class="mb-0 text-white fs-24">Products</p>
                                <h1 class="mb-0 text-white fs-26">{{ $businessCount }}</h1>

                            </div>
                            <div class="ml-auto">
                                <span class="bg-primary-bll icon-service text-white ">
                                    <img src="{{ url('/') }}/images/dashboard/home-icon6.png">
                                </span>
                            </div>
                        </div>
                        <img src="{{ url('/') }}/images/dashboard/img-1.png" alt="img" class="img-card-circle">
                    </div>


                    <div class="card-body-two card overflow-hidden bg-secondary-gradient">
                        <div class="d-flex clearfix">
                            <div class="text-left">
                                <p class="mb-0 text-white fs-24">Towns</p>
                                <h1 class="mb-0 text-white fs-26">{{ $townCount }}</h1>

                            </div>
                            <div class="ml-auto">
                                <span class="bg-primary-bll icon-service text-white ">
                                    <img src="{{ url('/') }}/images/dashboard/home-icon.png">
                                </span>
                            </div>
                        </div>
                        <img src="{{ url('/') }}/images/dashboard/img-1.png" alt="img" class="img-card-circle">
                    </div>
                    <div class="card-body-two card overflow-hidden bg-success-gradient">
                        <div class="d-flex clearfix">
                            <div class="text-left">
                                <p class="mb-0 text-white fs-24">Cities</p>
                                <h1 class="mb-0 text-white fs-26">{{ $cityCount }}</h1>

                            </div>
                            <div class="ml-auto">
                                <span class="bg-primary-bll icon-service text-white ">
                                    <img src="{{ url('/') }}/images/dashboard/home-icon2.png">
                                </span>
                            </div>
                        </div>
                        <img src="{{ url('/') }}/images/dashboard/img-1.png" alt="img" class="img-card-circle">
                    </div>
                    <div class="card-body-two card overflow-hidden bg-purple-gradient">
                        <div class="d-flex clearfix">
                            <div class="text-left">
                                <p class="mb-0 text-white fs-24">Countries</p>
                                <h1 class="mb-0 text-white fs-26">{{ $countryCount }}</h1>

                            </div>
                            <div class="ml-auto">
                                <span class="bg-primary-bll icon-service text-white ">
                                    <img src="{{ url('/') }}/images/dashboard/home-icon3.png">
                                </span>
                            </div>
                        </div>
                        <img src="{{ url('/') }}/images/dashboard/img-1.png" alt="img" class="img-card-circle">
                    </div>
                   
                    <!-- End Row -->

                    <!-- ROW-2 -->
                    <div id="main">
                         
                    </div>
                 
                    <!-- ROW-2 END -->
                </div>
                <!-- Row -->
                <div class="tab-pane fade" id="tab1" role="tabpanel" aria-labelledby="tab1-tab">



                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body-one">
                                <div class="row">
                                    <div class="col-sm-5 col-md-62 col-lg-5 col-xl-5">
                                        <h6 class="main-content-label mb-1">Businesses</h6>
                                    </div>
                                
                                </div>
                                <div class="table-responsive border">
                                    <table class="table  text-nowrap text-md-nowrap table-striped mg-b-0"
                                        style="overflow-x: hidden" id="datatable">
                                        <thead>
                                            <tr>
                                                <th style="color:white">Business Id</th>
                                                <th style="color:white">Business Name</th>
                                                <th style="color:white">Town, City, Country</th>
                                                <th style="color:white">Phone Number</th>
                                                <th style="color:white">Email Id</th>
                                                <th style="color:white">Business Type</th>
                                                <th style="color:white">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
 
                                            @foreach ($business as $businessOwner)
                                                <tr>
                                                    <td >
                                                        {{ $businessOwner->id }}
                                                    </td>
                                                    {{--  <td>
                                                        {{ '#' . $businessOwner->id }}
                                                    </td>  --}}
                                                    <td>
                                                        {{ $businessOwner->name }}
                                                    </td>
                                                    <td>
                                                        {{ $businessOwner->address }}
                                                    </td>
                                                    <td>
                                                        {{ $businessOwner->phone_number1 }}
                                                    </td>
                                                    <td>
                                                        {{ $businessOwner->email_address }}
                                                    </td>
                                                    <td>
                                                        {{ $businessOwner->id }}
                                                    </td>
                                                    <td>
                                                        <a href="#" class="edit-table delete"><i
                                                                class="far fa-trash-alt"></i></a>
                                                                <a href="#" class="edit-table delete"><i class="fas fa-edit"></i></a>
                                                    </td>
                                                </tr>
                                               
                                            @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <!-- ROW-2 END -->
                </div>
                <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab2-tab">


                    <div>
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body-one">
                                    <div class="row">
                                        <div class="col-sm-5 col-md-62 col-lg-5 col-xl-5">
                                            <h6 class="main-content-label mb-1">Shoppers</h6>
                                        </div>
                                        {{--  <div class="col-sm-7 col-md-7 col-lg-7 col-xl-7">
                                            <div class="row">
                                                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group select2-lg section-selete">
                                                        <!-- <div class="exsport"><img src="assets/images/exprot.png"></div> -->
                                                        <select name="country" class="form-control select-lg select2">
                                                            <option value="">Large Select</option>
                                                            <option value="cz">Czech Republic</option>
                                                            <option value="de">Germany</option>
                                                            <option value="pl">Poland</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group select2-lg select2-lg-one section-selete">
                                                        <select name="country" class="form-control select-lg select1">
                                                            <option value="">Large Select</option>
                                                            <option value="cz">Czech Republic</option>
                                                            <option value="de">Germany</option>
                                                            <option value="pl">Poland</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>  --}}
                                    </div>
                                    <div class="table-responsive border">
                                        <table class="table  text-nowrap text-md-nowrap table-striped mg-b-0"
                                            style="overflow-x: hidden" id="shopperDataTable">
                                            <thead>
                                                <tr>
                                                    <th style="color:white;">Shopper Id</th>
                                                    <th style="color:white;">Shopper Name</th>
                                                    <th style="color:white;">Town, City, Country</th>
                                                    <th style="color:white;">Phone number</th>
                                                    <th style="color:white;">Email</th>
                                                    <th style="color:white;">Action</th>
                                                    {{-- <th>Order Id</th>
                                                    <th>Inventory Status</th> --}}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($shoppers as $shopper)
                                                    <tr>
                                                        <td>
                                                            {{ '#' . $shopper->shopperId }}
                                                        </td>
                                                        <td>
                                                            {{ $shopper->name }}
                                                        </td>
                                                        <td>
                                                            {{ $shopper->address }}
                                                        </td>
                                                        <td>
                                                            {{ $shopper->phone_number }}
                                                        </td>
                                                        <td>
                                                            {{ $shopper->email }}
                                                        </td>
                                                        
                                                        <td>
                                                            <a href="#" class="edit-table delete"><i
                                                                    class="far fa-trash-alt"></i></a>
                                                                    <a href="#" class="edit-table delete"><i class="fas fa-edit"></i></a>
                                                        </td>
                                                    </tr>

                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ROW-2 END -->
                </div>
                <div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">


                    <div>
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body-one">
                                    <div class="row">
                                        <div class="col-sm-5 col-md-62 col-lg-5 col-xl-5">
                                            <h6 class="main-content-label mb-1">Service Business Details</h6>
                                        </div>
                                        <div class="col-sm-7 col-md-7 col-lg-7 col-xl-7">
                                            <div class="row">
                                                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group select2-lg section-selete">
                                                        <!-- <div class="exsport"><img src="assets/images/exprot.png"></div> -->
                                                        <select name="country" class="form-control select-lg select2">
                                                            <option value="">Large Select</option>
                                                            <option value="cz">Czech Republic</option>
                                                            <option value="de">Germany</option>
                                                            <option value="pl">Poland</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group select2-lg select2-lg-one section-selete">
                                                        <select name="country" class="form-control select-lg select1">
                                                            <option value="">Large Select</option>
                                                            <option value="cz">Czech Republic</option>
                                                            <option value="de">Germany</option>
                                                            <option value="pl">Poland</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="table-responsive border">
                                        <table class="table  text-nowrap text-md-nowrap table-striped mg-b-0">
                                            <thead>
                                                <tr>
                                                    <th>Device Id</th>
                                                    <th>Device Name</th>
                                                    <th>Serial number</th>
                                                    <th>Procurement date</th>
                                                    <th>Manufacturing date</th>
                                                    <th>Country</th>
                                                    <th>Inventory Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>Joan Powell</td>
                                                    <td>Associate Developer</td>
                                                    <td>$450,870</td>
                                                    <td>Joan Powell</td>
                                                    <td>Associate Developer</td>
                                                    <td>$450,870</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>Gavin Gibson</td>
                                                    <td>Account manager</td>
                                                    <td>$230,540</td>
                                                    <td>Joan Powell</td>
                                                    <td>Associate Developer</td>
                                                    <td>$450,870</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td>Julian Kerr</td>
                                                    <td>Senior Javascript Developer</td>
                                                    <td>$55,300</td>
                                                    <td>Joan Powell</td>
                                                    <td>Associate Developer</td>
                                                    <td>$450,870</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">4</th>
                                                    <td>Cedric Kelly</td>
                                                    <td>Accountant</td>
                                                    <td>$234,100</td>
                                                    <td>Joan Powell</td>
                                                    <td>Associate Developer</td>
                                                    <td>$450,870</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">5</th>
                                                    <td>Samantha May</td>
                                                    <td>Junior Technical Author</td>
                                                    <td>$43,198</td>
                                                    <td>Joan Powell</td>
                                                    <td>Associate Developer</td>
                                                    <td>$450,870</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ROW-2 END -->
                </div>
                <div class="tab-pane fade" id="tab4" role="tabpanel" aria-labelledby="tab4-tab">


                    <div>
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body-one">
                                    <div class="row">
                                        <div class="col-sm-5 col-md-62 col-lg-5 col-xl-5">
                                            <h6 class="main-content-label mb-1">Product Details</h6>
                                        </div>
                                       
                                    </div>
                                    <div class="table-responsive border">
                                        <table class="table  text-nowrap text-md-nowrap table-striped mg-b-0"
                                            style="overflow-x: hidden" id="productTable">
                                            <thead>
                                                <tr>
                                                    <th style="color:white;">Bussiness Id</th>
                                                    <th style="color:white;">Product Id</th>
                                                    <th style="color:white;">Product Name</th>
                                                    <th style="color:white;">Product Description</th>
                                                    <th style="color:white;">Product Quantity</th>
                                                    <th style="color:white;">Rates</th>
                                                    {{-- <th>Country</th>
                                                    <th>Inventory Status</th> --}}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($products as $product)
                                                    <tr>
                                                        <td>{{ '#' . $product->business_id }}</td>
                                                        <td>{{ $product->productId }}</td>
                                                        <td>{{ $product->name }}</td>
                                                        <td>{{ $product->description }}</td>
                                                        <td>{{ $product->product_count }}</td>
                                                        <td>{{ $product->actual_rate }}</td>
                                                    </tr>

                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ROW-2 END -->
                </div>
                <div class="tab-pane fade" id="tab5" role="tabpanel" aria-labelledby="tab5-tab">


                    <div>
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body-one">
                                    <div class="row">
                                        <div class="col-sm-5 col-md-62 col-lg-5 col-xl-5">
                                            <h6 class="main-content-label mb-1">Order Details</h6>
                                        </div>
                                        <div class="col-sm-7 col-md-7 col-lg-7 col-xl-7">
                                            <div class="row">
                                                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group select2-lg section-selete">
                                                        <!-- <div class="exsport"><img src="assets/images/exprot.png"></div> -->
                                                        <select name="country" class="form-control select-lg select2">
                                                            <option value="">Large Select</option>
                                                            <option value="cz">Czech Republic</option>
                                                            <option value="de">Germany</option>
                                                            <option value="pl">Poland</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group select2-lg select2-lg-one section-selete">
                                                        <select name="country" class="form-control select-lg select1">
                                                            <option value="">Large Select</option>
                                                            <option value="cz">Czech Republic</option>
                                                            <option value="de">Germany</option>
                                                            <option value="pl">Poland</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="table-responsive border">
                                        <table class="table  text-nowrap text-md-nowrap table-striped mg-b-0"
                                            style="overflow-x: hidden">
                                            <thead>
                                                <tr>
                                                    <th>Order Id</th>
                                                    <th>Business Owner Id</th>
                                                    <th>Business Owner Name</th>
                                                    <th>Product Id</th>
                                                    <th>Product Name</th>
                                                    <th>Shopper's Name</th>
                                                    <th>From: Town Name</th>
                                                    <th>To: Town Name</th>
                                                    <th>Product Cost</th>
                                                    <th>Order Placed Date & Time</th>
                                                    <th>Order Delivery Date & Time</th>
                                                    {{-- <th>Inventory Status</th> --}}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($orders as $order)
                                                    <tr>
                                                        <td>
                                                            {{ '#' . $order->order_id }}
                                                        </td>
                                                        <td>
                                                            {{ '#' . $order->id }}
                                                        </td>
                                                        <td>
                                                            {{ $order->name }}
                                                        </td>
                                                        <td>
                                                            {{ $order->productId }}
                                                        </td>
                                                        <td>
                                                            {{ $order->productName }}
                                                        </td>
                                                        <td>
                                                            {{ $order->first_name . ' ' . $order->last_name }}
                                                        </td>
                                                        <td>
                                                            {{ $order->address }}
                                                        </td>
                                                        <td>
                                                            {{ $order->shAddress }}
                                                        </td>
                                                        <td>
                                                            {{ $order->cost * $order->product_count }}
                                                        </td>
                                                        <td>
                                                            {{ $order->created_at }}
                                                        </td>
                                                        <td>
                                                            <p><?php
                                                            $time = strtotime($order->created_at);
                                                            echo $final = date('Y-m-d h:i:s', strtotime('+1 month', $time));
                                                            ?></p>

                                                        </td>
                                                    </tr>

                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ROW-2 END -->
                </div>
                <div class="tab-pane fade" id="tab6" role="tabpanel" aria-labelledby="tab6-tab">


                    <div>
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body-one">
                                    <div class="row">
                                        <div class="col-sm-5 col-md-62 col-lg-5 col-xl-5">
                                            <h6 class="main-content-label mb-1">Shipment Details</h6>
                                        </div>
                                        <div class="col-sm-7 col-md-7 col-lg-7 col-xl-7">
                                            <div class="row">
                                                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group select2-lg section-selete">
                                                        <!-- <div class="exsport"><img src="assets/images/exprot.png"></div> -->
                                                        <select name="country" class="form-control select-lg select2">
                                                            <option value="">Large Select</option>
                                                            <option value="cz">Czech Republic</option>
                                                            <option value="de">Germany</option>
                                                            <option value="pl">Poland</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group select2-lg select2-lg-one section-selete">
                                                        <select name="country" class="form-control select-lg select1">
                                                            <option value="">Large Select</option>
                                                            <option value="cz">Czech Republic</option>
                                                            <option value="de">Germany</option>
                                                            <option value="pl">Poland</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="table-responsive border">
                                        <table class="table  text-nowrap text-md-nowrap table-striped mg-b-0"
                                            style="overflow-x: hidden">
                                            <thead>
                                                <tr>
                                                    <th>Order Id</th>
                                                    <th>Business Id</th>
                                                    <th>Shopper Name</th>
                                                    <th>From: Town Name</th>
                                                    <th>To: Town Name</th>
                                                    <th>Order Delivery Date & Time</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($orderShipment as $shipment)
                                                    <tr>
                                                        <td>
                                                            {{ '#' . $shipment->orderId }}
                                                        </td>
                                                        <td>
                                                            {{ '#' . $shipment->id }}
                                                        </td>
                                                        <td>
                                                            {{ $shipment->first_name . ' ' . $shipment->last_name }}
                                                        </td>
                                                        <td>
                                                            {{ $shipment->address }}
                                                        </td>
                                                        <td>
                                                            {{ $shipment->shAddress }}
                                                        </td>
                                                        <td>
                                                            <p><?php
                                                            $time = strtotime($shipment->created_at);
                                                            echo $final = date('Y-m-d h:i:s', strtotime('+1 month', $time));
                                                            ?></p>

                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ROW-2 END -->
                </div>

            </div>
        </div>
    </div>
@endsection
 @section('modal')
{{--  start Delete product  --}}
 <div class="modal fade products-pop" id="Delete_Product">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-body">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="prd-pop-details">
                      <h4 class="modal-title">Delete Products</h4>
                     
                      <form  id="deleteProductForm" action="\addProduct" method="POST">
                      
                      {{ csrf_field() }}
                      {{ method_field('DELETE') }}
                      <input type="hidden" name="_method" value="DELETE"/>
                      <p>Are you sure?.. You want to delete data</p>
                        <input type="hidden" name="businessId" value="{{  Session::get('business_id')}}">
                          <div class="form-group">
                              <button href="#" class="btn-main" type="submit" id="updateProduct">Delete</button>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
{{-- end Delete Product--}}
@endsection