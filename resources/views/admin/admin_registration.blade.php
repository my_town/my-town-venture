@extends('layouts.new_MyTown')
@section('content')
    @push('head')
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

        <!-- <script src="//code.jquery.com/jquery-1.12.1.js"></script>  -->
        <!-- <link href="vendor/aos/aos.css" rel="stylesheet"> -->
        <!-- <script src="https://code.jquery.com/jquery-2.1.4.js"></script> -->

        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <script src="js/form_validation.js"></script>


        <script>
            $(function() {
                $("#locations_search").autocomplete({

                    source: function(request, response) {
                        $.ajax({
                            url: "{{ url('/location/search') }}",
                            dataType: "json",
                            data: {
                                q: request.term
                            },
                            success: function(data) {
                                response($.map(data.predictions, function(item) {
                                    return {
                                        label: item.description,
                                        value: item.place_id
                                    };
                                }));
                            }
                        });
                    },
                    select: function(event, ui) {
                        $("#locations_search").val(ui.item.label);
                        $("#places_id").val(ui.item.value);
                        return false;
                    },
                    focus: function(event, ui) {
                        $("#locations_search").val(ui.item.label);
                        return false;
                    },
                });
            });
        </script>

        <style>
            .ui-autocomplete-loading {
                background: url('/images/loading.gif') no-repeat right center
            }

            .error {
                color: red;
            }

        </style>
    @endpush

    <!-- ======= About Section ======= -->
    <section id="about" class="about login-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
                    <div class="card card0 border-0 login-page">
                        <div class="row d-flex">

                            <div class="col-lg-3">
                                <div class="card2 my-4 px-3">
                                    <div class="row px-3 justify-content-center mt-4 mb-5">
                                        <img src="images/img/login-clipart.png" class="image">
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-9">
                                <div class="card2 card border-0 px-4 py-5">
                                    <div class="row mb-4 px-3">
                                        <h5 class="mb-0 mr-4 mt-2">Let us Register you as a Shopper</h5>
                                    </div>
                                    <form method="POST" action="{{ route('user_register') }}"
                                        enctype="multipart/form-data" id="register" autocomplete="off">
                                        @csrf
                                        @if (request()->input('biz') == 1)
                                            <h3>Step 1 - Let's create your account.</h3>
                                        @endif

                                        <div class="row px-3">
                                            <label class="mb-1" for="location_search">
                                                <h6 class="mb-0 text-sm">Tell us about your Town<span
                                                        style="color:red">*</span></h6>
                                            </label>

                                            <input class="mb-4" id="locations_search" type="text"
                                                name="locations_search" required autofocus autocomplete="locations_search"
                                                placeholder="Start typing your address. We will grab details like your Town, Country etc.">
                                            <input id="places_id" type="hidden" name="places_id" required value="">
                                        </div>

                                        <div class="row px-3">
                                            <label class="mb-1" for="name">
                                                <h6 class="mb-0 text-sm">Your Name<span style="color:red">*</span></h6>
                                            </label>
                                            <input id="name" class="mb-4" type="text" name="name"
                                                :value="old('name')" required autocomplete="off" placeholder="Name">
                                        </div>

                                        <div class="row px-3">
                                            <label class="mb-1" for="phone">
                                                <h6 class="mb-0 text-sm">Mobile number<span style="color:red">*</span>
                                                </h6>
                                            </label>
                                            <input id="phone" class="mb-4" type="text" name="phone"
                                                :value="old('phone')" required autocomplete="off"
                                                placeholder="Mobile number">
                                        </div>

                                        <div class="row px-3">
                                            <label class="mb-1" for="email">
                                                <h6 class="mb-0 text-sm">Email address (This will be used as your
                                                    login-ID.)<span style="color:red">*</span></h6>
                                            </label>
                                            <input id="email" class="mb-4" type="email" name="email"
                                                :value="old('email')" required autocomplete="off"
                                                placeholder="Email address">
                                        </div>

                                        <div class="row px-3">
                                            <label class="mb-1" for="profile_photo">
                                                <h6 class="mb-0 text-sm">Profile Photo (Optional)</h6>
                                            </label>
                                            <input class="mb-4" input type="file" name="profile_photo"
                                                id="profile_photo" placeholder="Upload your profile photo">
                                        </div>

                                        <div class="row px-3">
                                            <label class="mb-1" for="password">
                                                <h6 class="mb-0 text-sm">Password<span style="color:red">*</span></h6>
                                            </label>
                                            <input class="mb-4" id="password" type="password" name="password"
                                                required autocomplete="new-password" autocomplete="off"
                                                placeholder="Enter password">
                                        </div>

                                        <div class="row px-3">
                                            <label class="mb-1" for="password_confirmation">
                                                <h6 class="mb-0 text-sm">Confirm Password<span style="color:red">*</span>
                                                </h6>
                                            </label>
                                            <input class="mb-4" id="password_confirmation" type="password"
                                                name="password_confirmation" required autocomplete="off"
                                                placeholder="Confirm password">
                                        </div>

                                        <div class="row mb-3 px-3"><button type="submit"
                                                class="btn btn-blue text-center">Register</button></div>

                                        <div class="row mb-4 px-3">
                                            <small class="font-weight-bold"><a href="admin/login"
                                                    class="text-danger">Already Registered?</a></small>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="bg-blue py-4">
                            <div class="row px-3">
                                <small class="ml-4 ml-sm-5 mb-2"> &copy; Copyright <strong><span>MyTown</span></strong>. All
                                    Rights Reserved</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End About Section -->

@endsection
