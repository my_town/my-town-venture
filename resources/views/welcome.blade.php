
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css"> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
<script src="//code.jquery.com/jquery-1.12.1.js"></script>  
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 


<script>
$(function(){
    $("#location_search").autocomplete({
      source :function( request, response ) {
        $.ajax({
           url: "/location/search",
           dataType: "json",
           data: {
              q: request.term 
           },
           success: function( data ) {
               response($.map(data.predictions, function(item) {
                     return {
                         label : item.description,
                         value : item.place_id
                     };
               }));
           }
        });
       },
       select: function (event, ui) {
             $("#location_search").val(ui.item.label);
             $("#place_id").val(ui.item.value);
             return false;
       }
   });
});

</script>

<script>
$(function(){
    $("#business_location_search").autocomplete({
      source :function( request, response ) {
        $.ajax({
           url: "/location/search",
           dataType: "json",
           data: {
              q: request.term 
           },
           success: function( data ) {
               response($.map(data.predictions, function(item) {
                     return {
                         label : item.description,
                         value : item.place_id
                     };
               }));
           }
        });
       },
       select: function (event, ui) {
             $("#business_location_search").val(ui.item.label);
             $("#business_place_id").val(ui.item.value);
             return false;
       }
   });
});

</script>

<script>
$(document).ready(function(){

	$('.pop-biz-reg-form').on('click', function(){
        $('#pop-over').show();
	});
    $('#hide-pop-over').on('click',function(){
        $('#pop-over').hide();
    });

});
</script>
<style>
	#pop-over{
		overflow:scroll;
	}
    .contain{
        display:none;
           width:540px;
           height:700px;
           top:50%;
           left:50%;
           transform:translate(-50%,-50%);
           background:white;
           border-radius:5px;
           position:absolute;
           padding:20px
       }
       .contain form .form-box .title{
            font-size: 24px;
            font-weight: 700;
            margin-bottom: 10px;
            color: black;
            text-transform: uppercase;
            text-align: center;
        }
       .contain form .form-box{
            width:450px;
            margin:10px;
            position:absolute;
            transition:0.5s;
        }
        .contain form .form-box .input_field{
             margin-bottom: 15px;
            display: flex;
            align-items: center;
        }
        .contain form .form-box .btn-box{
            width:100%;
            margin:30px auto;
            text-align:center;
        }
        .contain form .form-box .input_field label{
            width: 200px;
            color: black;
            margin-right: 10px;
            font-size: 14px;
            font-weight: 500;
        }
        .contain form .form-box .input_field .input1,
        .contain form .form-box .input_field .textarea{
            width: 100%;
            outline: none;
            border: 1px solid #d5dbd9;
            font-size: 15px;
            padding: 8px 10px;
            border-radius: 3px;
            transition: all 0.3s ease;
        }
        .contain form .form-box .input_field .textarea{
            resize: none;
            height: 125px;
        }
        .contain form .form-box .input_field .input1:focus,
        .contain form .form-box .input_field .textarea:focus{
            border: 1px solid #FF002B;
        }
        .contain form .form-box .btn-box .btn{
            width: 80px;
            padding: 8px 10px;
            font-size: 15px;
            border: 0;
            background: #FF002B;
            color: #fff;
            cursor: pointer;
            border-radius: 3px;
            outline: none;
        }
        .contain form .form-box .input_field:last-child{
            margin-bottom: 0;
        }
        .contain form .form-box .input_field .btn:hover{
            background: #B3001E;
        }
        @media(max-width:420px){
        .contain form .form-box .input_field{
            flex-direction: column;
            align-items: flex-start;
        }
        .contain form .form-box .input_field label{
            margin-bottom: 5px;
        }
    }

.ui-autocomplete-loading { background:url('/images/loading.gif') no-repeat right center }
input[type=radio]
{
  -webkit-appearance:checkbox;
}
</style>
 
<div class="bgcolored">
    <div class="container">
        <div class="row">
            <div class="col-7 col-12-medium">
                <h2 class="margintop1 textwhite">20+ years of experience</h2>
                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eget risus ac quam rhoncus lacinia. Etiam convallis tortor quis enim egestas varius. Sed suscipit ex mi, at sagittis elit mollis in. Vivamus vel pulvinar felis, quis mattis ipsum. Integer eget quam orci. In commodo purus at nulla.</div>
                <a href="#" class="button-solid-white margintop1">Learn more</a>
            </div>
            <div class="col-5 col-12-medium">
                <img src="/images/experience.svg" alt="" width="422" height="355"/>
            </div>
        </div>
    </div>
</div>


<div class="contain" id="pop-over">
    <label id="hide-pop-over" class="close-btn fas fa-times" style="float:right;"></label>
        <form action="/biz-registration" class="btn-submit" method="POST" enctype="multipart/form-data">
            @csrf

			
            <div class="form-box" id="form1">
                <div class="title">Let us create a profile of your business</div>
                <div class="input_field">
                    <label>Your Name</label>
                    <input type="text" class="input1" id="name" name="name"  autocomplete="off" placeholder="Enter your name">
                </div>
                <div class="input_field">
                    <label for="location_search">Town</label>
                    <input type="text" name="location_search" id="location_search" class="input1"  autofocus autocomplete="location_search" placeholder="Start typing the name of your town">
                    <input id="place_id" type="hidden" name="place_id" required value=""/>
                </div>
                <div class="input_field">
                    <label>Email</label>
                    <input type="email" class="input1" id="email" name="email" autocomplete="off" placeholder="Enter your email address">
                </div>
                <div class="input_field">
                    <label>Phone</label>
                    <input type="text" name="phone" id="phone" class="input1"  autocomplete="off" placeholder="Enter your mobile number">
                </div>
                <div class="input_field">
                    <label>Password</label>
                    <input type="password" name="password" id="password" class="input1"  autocomplete="off" placeholder="Enter strong password">
                </div>
                <div class="input_field">
                    <label>Confirm Password</label>
                    <input type="password" name="password_confirmation" id="password_confirmation" class="input1"  autocomplete="off" placeholder="Confirm your password">
                </div>
                <div class="btn-box">
                    <button type="button" class="btn" id="next1">Next</button>
                </div>
            </div>
			-->

            <!--<div class="form-box" id="form2">
                <div class="title1">Let us create profile of your business</div>
                <div class="input_field">
                    <label>Name of your business</label>
                    <input type="text" class="input1" id="business_name" name="business_name" placeholder="Name of your business" autocomplete="off">
                </div>
				<div>
                    Business Offering Type
				</div>
                <div class="input_field">
                    <input type="radio" name="offering_type" value="service" /> Service
                    <input type="radio" name="offering_type" value="product" /> Product
                    <input type="radio" name="offering_type" value="product_service" /> Product and Service both
                </div>
                <div class="input_field">
                    <label>Address</label>
                    <input type="text" class="input1" id="business_location_search" name="business_location_search" value="" required autofocus autocomplete="business_location_search" placeholder="Address of your business">
                    <input type="hidden" id="business_place_id" name="business_place_id" value="" />
                </div>
                <div class="input_field">
                    <label>Owner/Business Email address</label>
                    <input type="email" class="input1" id="business_email" name="business_email" placeholder="Email address of your business" autocomplete="off">
                </div>
                <div class="input_field">
                    <label>Password</label>
                    <input type="password" name="password" id="password" class="input1"  autocomplete="off" placeholder="Enter strong password">
                </div>
                <div class="input_field">
                    <label>Phone number1</label>
                    <input type="text" class="input1" id="business_phone1" name="business_phone1" placeholder= "Phone number of your business" autocomplete="off">
                </div>
                <div class="input_field">
                    <label>Phone number2</label>
                    <input type="text" class="input1" id="business_phone2" name="business_phone2" placeholder="Another phone number of your business" autocomplete="off">
                </div>
                <div class="input_field">
                    <label>Logo</label>
                    <input type="file" class="input1" id="logo" name="logo" placeholder="Add your company logo" autocomplete="off">
                </div>
                <div class="input_field">
                    <label>Banner Image</label>
                    <input type="file" class="input1" id="banner_image" name="banner_image" placeholder="Add banner image for your business profile" autocomplete="off">
                </div>
                <div class="input_field">
                    <label>Description</label>
                    <textarea class="textarea" id="description" placeholder="Add short description about your business" name="description"></textarea>
                </div>
                <div class="btn-box">
                    <!--<button type="button" class="btn" id="back1">Back</button>-->
                    <!--<button type="submit" class="btn">Submit</button>
                </div>
            </div>

                        
        </form>

    </div>

<script type="text/javascript">

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/*
$(".btn-submit").click(function(e){

    e.preventDefault();

    var name = $("input[name=name]").val();
    var email = $("input[name=email]").val();
    var password = $("input[name=password]").val();
    var location_search = $("input[name=location_search]").val();
    var phone = $("input[name=phone]").val();
    var place_id = $("input[name=place_id]").val();

    var business_name = $("input[name=business_name]").val();
    var business_location_search = $("input[name=business_location_search]").val();
    var business_place_id = $("input[name=business_place_id]").val();
    var business_email = $("input[name=business_email]").val();
    var business_phone1 = $("input[name=business_phone1]").val();
    var business_phone2 = $("input[name=business_phone2]").val();
    var logo = $("input[name=logo]").val();
    var banner_image = $("input[name=banner_image]").val();
    var description = $("input[name=description]").val();

    var url = '{{ url('/') }}';

    $.ajax({
       url:url,
       method:'POST',
       data:{
              name:name,
              email:email,
              password:password,
              location_search:location_search
              phone:phone,
              place_id:place_id,
              business_name:business_name,
              business_phone1:business_phone1,
              business_phone2:business_phone2,
              business_email:business_email,
              logo:logo,
              banner_image:banner_image,
              description:description
              business_place_id:business_place_id,
              business_location_search:business_location_search,
            },
        success: function (x) {
            if(x){
        window.location.href ="";
        }
        },
       error:function(error){
          console.log(error)
       }
    });
}); 
*/
</script>-->

@extends('layouts.index_welcome')
@section('content')


<!-- ======= Hero Section ======= -->
<div id="carouselExampleIndicators" class="carousel slide hp-slider" data-ride="carousel" >
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
	    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
    </ol>

    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="images/img/hp_slide2.jpg" alt="First slide">
	        <div class="carousel-caption d-none d-md-block">
                <!--<h1>Welcome to MyTown</h1>
                <p>The world is at your doorstep!</p>-->
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/img/hp_slide3_2.jpg" alt="Third slide">
	        <div class="carousel-caption d-none d-md-block">
                <!--<h1>London</h1>
                <p>Visit any store from any town, any city or any country, it's a shopping experience!</p>-->
            </div>
        </div>
	    <!--<div class="carousel-item">
            <img class="d-block w-100" src="images/img/New_york.jpg" alt="Fourth slide">
	        <div class="carousel-caption d-none d-md-block">
                <h1>New York</h1>
                <p>Visit any store from any town, any city or any country, it's a shopping experience!</p>
            </div>
        </div>-->
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div><!-- End Hero -->



<main id="main">
    <section id="why-us" class="why-us">
        <div class="container">

            <div class="row" >
                <div class="col-xl-4 col-lg-5" data-aos="fade-up">
                    <div class="content text-center">
                        <h4>Register your Business</h4>
                        <p>
                            In 3 simple steps!  <i class="icofont-arrow-right"></i>
                        </p>
                        <div class="text-center">
              
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-7 d-flex">
                    <div class="icon-boxes d-flex flex-column justify-content-center">
                        <div class="row">
                            <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                                <div class="icon-box mt-4 mt-xl-0" style="background-color:#D5D28C; ">		
				                    <i class="bx">1</i> 
                                    <h4> SIGN UP</h4>
                                    <p><a href="{{ url('/biz-registration') }}">Create</a> a profile of your business on MyTown</p>
                                </div>
                            </div>
                            <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
                                <div class="icon-box mt-4 mt-xl-0" style="background-color:#B98E8F; ">
                                    <i class="bx">2</i> 
                                    <h4>GET LISTED ON MYTOWN</h4>
                                    <p>Add products and services</p>
                                </div>
                            </div>
                            <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
                                <div class="icon-box mt-4 mt-xl-0" style="background-color:#F64D71 ">
                                    <i class="bx">3</i>
                                    <h4> GROW YOUR BUSINESS</h4>
                                    <p>Get global exposure!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End Why Us Section -->


    <!-- ======= Pricing Section ======= -->
    <section id="pricing" class="pricing">
        <div class="container">

            <div class="section-title">
                <h2 data-aos="fade-up">20+ Years of Experience</h2>
                <!-- <p data-aos="fade-up">Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>-->
            </div>

            <div class="row">

                <div class="col-lg-2 col-md-6" data-aos="fade-up">
                    <div class="box">
             
                        <h4>{{ $towns }}<br>
                        <span>Towns</span></h4>
                    </div>
                </div>

                <div class="col-lg-2 col-md-6" data-aos="fade-up">
                    <div class="box">
             
                        <h4>{{ $cities }}<br>
                        <span>Cities</span></h4>
                    </div>
                </div>

                <div class="col-lg-2 col-md-6" data-aos="fade-up">
                    <div class="box">
             
                        <h4>{{ $countries }}<br>
                        <span>Countries</span></h4>
                    </div>
                </div>

                <div class="col-lg-2 col-md-6" data-aos="fade-up">
                    <div class="box">
             
                        <h4>{{ $businesses }}<br>
                        <span>Businesses</span></h4>
                    </div>
                </div>
		        <div class="col-lg-2 col-md-6" data-aos="fade-up">
                    <div class="box">
             
                        <h4>{{ $shoppers }}<br>
                        <span>Shoppers</span></h4>
                    </div>
                </div>
		        <div class="col-lg-2 col-md-6" data-aos="fade-up">
                    <div class="box">
             
                        <h4>{{ $businesses }}<br>
                        <span>Products</span></h4>
                    </div>
                </div>
		  

            </div>

        </div>
    </section><!-- End Pricing Section -->
	


    <!-- ======= About Section ======= -->
    <section id="about" class="about video-box section-bg">
        <div class="container">

            <div class="row">
                <div class="col-xl-5 col-lg-6 video-box d-flex justify-content-center align-items-stretch" data-aos="fade-right">
                    <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>          
                </div>
                <div class="col-xl-7 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5">
                    <h4 data-aos="fade-up">About us</h4>
                    <h3 data-aos="fade-up">Enim quis est voluptatibus aliquid consequatur fugiat</h3>
                    <p data-aos="fade-up">Esse voluptas cumque vel exercitationem. Reiciendis est hic accusamus. Non ipsam et sed minima temporibus laudantium. Soluta voluptate sed facere corporis dolores excepturi. Libero laboriosam sint et id nulla tenetur. Suscipit aut voluptate.</p>

                    <div class="icon-box" data-aos="fade-up">
                        <div class="icon"><i class="bx bx-fingerprint"></i></div>
                        <h4 class="title"><a href="">Lorem Ipsum</a></h4>
                        <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
                    </div>

                    <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                        <div class="icon"><i class="bx bx-gift"></i></div>
                        <h4 class="title"><a href="">Nemo Enim</a></h4>
                        <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
                    </div>

                    <div class="icon-box" data-aos="fade-up" data-aos-delay="200">
                        <div class="icon"><i class="bx bx-atom"></i></div>
                        <h4 class="title"><a href="">Dine Pad</a></h4>
                        <p class="description">Explicabo est voluptatum asperiores consequatur magnam. Et veritatis odit. Sunt aut deserunt minus aut eligendi omnis</p>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End About Section -->
	

    <!-- ======= Services Section ======= -->
    <section id="services" class="services section-bg">
        <div class="container">

            <div class="section-title" data-aos="fade-up">
                <h2>How It Works</h2>
                <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-6" data-aos="fade-up">
                    <div class="icon-box">
                        <div class="icon"><i class="icofont-businessman"></i></div>
                        <h4 class="title"><a href="">Lorem Ipsum</a></h4>
                        <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="100">
                    <div class="icon-box">
                        <div class="icon"><i class="icofont-chart-bar-graph"></i></div>
                        <h4 class="title"><a href="">Dolor Sitema</a></h4>
                        <p class="description">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="200">
                    <div class="icon-box">
                        <div class="icon"><i class="icofont-coins"></i></div>
                        <h4 class="title"><a href="">Sed ut perspiciatis</a></h4>
                        <p class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End Services Section -->

    <!-- ======= Values Section ======= -->
    <section id="values" class="values">
        <div class="container">

            <div class="row">
                <div class="col-md-6 d-flex align-items-stretch mt-4" data-aos="fade-up" data-aos-delay="200">
                    <div class="card" style="background-image: url(images/img/values-3.jpg);">
                        <div class="card-body">
                            <h5 class="card-title"><a href="">Our Mission</a></h5>
                            <p class="card-text">Nemo enim ipsam voluptatem quia voluptas sit aut odit aut fugit, sed quia magni dolores.</p>
                            <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 d-flex align-items-stretch mt-4" data-aos="fade-up" data-aos-delay="300">
                    <div class="card" style="background-image: url(images/img/values-4.jpg);">
                        <div class="card-body">
                            <h5 class="card-title"><a href="">Our Vision</a></h5>
                            <p class="card-text">Nostrum eum sed et autem dolorum perspiciatis. Magni porro quisquam laudantium voluptatem.</p>
                            <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End Values Section -->

    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials">
        <div class="container" data-aos="fade-up">

            <div class="owl-carousel testimonials-carousel">

                <div class="testimonial-item">
                    <img src="images/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
                    <h3>Saul Goodman</h3>
                    <h4>Ceo &amp; Founder</h4>
                    <p>
                        <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                        Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                        <i class="bx bxs-quote-alt-right quote-icon-right"></i>           
                    </p>
                </div>

                <div class="testimonial-item">
                    <img src="images/img/testimonials/testimonials-2.jpg" class="testimonial-img" alt="">
                    <h3>Sara Wilsson</h3>
                    <h4>Designer</h4>
                    <p>
                        <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                        Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
                        <i class="bx bxs-quote-alt-right quote-icon-right"></i>            
                    </p>
                </div>

                <div class="testimonial-item">
                    <img src="images/img/testimonials/testimonials-3.jpg" class="testimonial-img" alt="">
                    <h3>Jena Karlis</h3>
                    <h4>Store Owner</h4>
                    <p>
                        <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                        Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.
                        <i class="bx bxs-quote-alt-right quote-icon-right"></i>            
                    </p>
                </div>

                <div class="testimonial-item">
                    <img src="images/img/testimonials/testimonials-4.jpg" class="testimonial-img" alt="">
                    <h3>Matt Brandon</h3>
                    <h4>Freelancer</h4>
                    <p>
                        <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                        Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
                        <i class="bx bxs-quote-alt-right quote-icon-right"></i>            
                    </p>
                </div>

                <div class="testimonial-item">
                    <img src="images/img/testimonials/testimonials-5.jpg" class="testimonial-img" alt="">
                    <h3>John Larson</h3>
                    <h4>Entrepreneur</h4>
                    <p>
                        <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                        Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid.
                        <i class="bx bxs-quote-alt-right quote-icon-right"></i>            
                    </p>
                </div>

            </div>

        </div>
    </section><!-- End Testimonials Section -->


    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
        <div class="container">

            <div class="section-title">
                <h2 data-aos="fade-up">Contact</h2>
                <p data-aos="fade-up">Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
            </div>

            <div class="row justify-content-center">

                <div class="col-xl-3 col-lg-4 mt-4" data-aos="fade-up">
                    <div class="info-box">
                        <i class="bx bx-map"></i>
                        <h3>Our Address</h3>
                        <p>A108 Adam Street, New York, NY 535022</p>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-4 mt-4" data-aos="fade-up" data-aos-delay="100">
                    <div class="info-box">
                        <i class="bx bx-envelope"></i>
                        <h3>Email Us</h3>
                        <p>info@example.com<br>contact@example.com</p>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 mt-4" data-aos="fade-up" data-aos-delay="200">
                    <div class="info-box">
                        <i class="bx bx-phone-call"></i>
                        <h3>Call Us</h3>
                        <p>+1 5589 55488 55<br>+1 6678 254445 41</p>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center" data-aos="fade-up" data-aos-delay="300">
                <div class="col-xl-9 col-lg-12 mt-4">
                    <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                        <div class="form-row">
                            <div class="col-md-6 form-group">
                                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                <div class="validate"></div>
                            </div>
                            <div class="col-md-6 form-group">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                                <div class="validate"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                            <div class="validate"></div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                            <div class="validate"></div>
                        </div>
                        <div class="mb-3">
                            <div class="loading">Loading</div>
                            <div class="error-message"></div>
                            <div class="sent-message">Your message has been sent. Thank you!</div>
                        </div>
                        <div class="text-center"><button type="submit">Send Message</button></div>
                    </form>
                </div>
            </div>
        </div>
    </section><!-- End Contact Section -->

</main><!-- End #main -->

  @endsection

    
