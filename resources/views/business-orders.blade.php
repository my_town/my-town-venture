@extends('layouts.my-town-app')
@section('content')
@push('head')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://kit.fontawesome.com/e3dc723f7b.js" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
    $('#businesses').DataTable( {
        "columnDefs": [{
            "targets":[4],
            "orderable":false,
             "className": 'dt-body-right'
        }]
    } );
} );
</script>
@endpush


<div class="greybg content-box">
    <div class="container">
        <div class="row paddingbottom3">
            <div class="col-8 col-12-medium">
                <div class="tabcontent" style="display:block;">
                    <div class="p-6 sm:px-20 bg-white border-b border-gray-200">                
		                <div class="mt-6 text-gray-900">
		                    <div class="table-responsive">
                                <table id="businesses" class="display">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Customer</th>
                                            <th>Product Name</th>  
                                            <th>Quantity</th>    
                                            <th>Total Cost</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $order)
                                        <?php 
                                            $items=json_decode($order->order->items);
                                            $user=App\Models\User::find($order->order->user_id);
                                        ?>
                                        @foreach($items as $item)
                                            @if($item->id==$order->product_id)
                                                <tr>
                                                    <th>{{ $order->created_at }}</th>
                                                    <th>{{ $user->name }}<br>{{ $user->place }}</th> 
                                                    <th>{{ $item->name }}</th> 
                                                    <th>{{ $item->quantity }}</th>      
                                                    <th>{{ $order->cost * $item->quantity }}</th>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endforeach
                                    </tbody>
                                </table>
                            </div><!-- table-responsive -->
		                </div><!-- mt-6 -->
		            </div><!-- p-6 -->
                </div>   
            </div>

            <div class="col-4 col-12-medium">
                <div class="whitebg padding2 margintop3">
                    <h5>4 Visitors also viewing this</h5>
                    <div class="orange">
                        <ul>
                            <li>Pratiksha</li>
                            <li>John Doe</li>
                        </ul>
                    </div>
                </div>
                <div class="whitebg padding2 margintop2">
                    <h5>4 Visitors also viewing this</h5>
                    <div class="orange">
                        <ul>
                            <li>Pratiksha</li>
                            <li>John Doe</li>
                        </ul>
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>

@endsection