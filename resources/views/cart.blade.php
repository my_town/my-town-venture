@extends('layouts.my-town-app')


@section('content')
<div class="shopping-cart">
    <!-- Title -->
    <div class="heading">
        <i class="fa fa-shopping-bag " ></i>
        Shopping Bag
    </div>
    @foreach ($cart as $item)
    <?php $product=App\Models\Product::where('id',$item->id)->first();?>
    <!-- Product #1 -->
    <div class="item">
        <div class="image99">
            <img src="{{ asset('/storage/products/'.$product->product_image) }}" alt="" width="150" height="150" />
            <div class="total-price">{{ $product->business->name }}</div>
        </div>
 
        <div class="description">
            <span>{{$item->name}}</span>
            <span>{{$product->description}}</span>
        </div>
 
        <div class="quantity">
            <form method="post" action="/cart/update_quantity" id={{'updateForm' . $item->id}}>

                @csrf
                <label>Choose a Quantity:</label>
                <!--<input type="number" name="quantity" min="1" max="10" style="border:1px solid red;">-->
                <select name="quantity" id = "qq" onchange="({{'updateForm' . $item->id}}).submit();">
                    <option value={{ $item->quantity }}>{{ $item->quantity }}</option> 
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>

                <input type="hidden" name="product_id" value={{ $product->id }} >
                <input type="hidden" name="item_id" value={{ $item->getUniqueId() }}>
                <input type="hidden" name="name" value={{ $item->name }}>
                <input type="hidden" name="price" value={{ $item->price }}>
                <!--<button class="update-btn" onclick="javascript:$({{'updateForm' . $item->id}}).submit();">Update</button>-->
                <!-- <a href="javascript:$({{'updateForm' . $item->id}}).submit();">Update</a> -->
            </form>
        </div>
        <div class="total-price">₹{{ $item->price }}</div>
        <div class="total-price">
            <form method="POST" action="/cart/remove" id={{'removeForm' . $item->id}}>
                @csrf
                <input type="hidden" name="item_id" value={{ $item->getUniqueId() }} >
                <a href="javascript:$({{'removeForm' . $item->id}}).submit();" class="button-sm marginleft1"><i class="fas fa-trash-alt"></i></a>
            </form>
        </div>
    </div>
    @endforeach
    <div class="cart-total">Total: {{$total}}</div>
    <div class="last-div">
        <form method="POST" action="{{ url('/save_orders') }}" id={{'placeForm'}}>
            @csrf
            <button onclick="javascript:$({{'placeForm'}}).submit();" class="place-btn">Place Order</button></div>
        </form>
    </div>
</div>

@endsection