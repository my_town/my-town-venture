@extends('layouts.new_MyTown')
@section('content')

 <style>
  
 .get-started-icon {
  font-size: 24px;
  background: #ffce00;
  padding: 14px;
  color: #fff;
  border-radius: 50px;
  position: relative;
  z-index: 5;
  box-shadow: 10px 2px 15px rgba(0, 0, 0, 0.1);
}

 .btn-get-started {
  font-family: "Raleway", sans-serif;
  font-weight: 400;
  font-size: 16px;
  letter-spacing: 1px;
  display: inline-block;
  transition: 0.5s;
  margin-left: -10px;
  padding: 8px 26px 8px 26px;
  color: #ffffff;
  background: #ffce00;
  border-radius: 0 50px 50px 0;
  position: relative;
  z-index: 4; 
}

 .btn-get-started:hover {
color: #262228;
}

.img-fluid {
    width: 100%;
   
}

.bg-products{ border:1px solid #ffbd83; padding:0; margin-right:0px; margin-bottom: 5px;}
      
     .search-results {
    padding: 20px 0px;
    background-color: #FFF6E9;
    margin-bottom: 50px;
}

.pagination {
   justify-content: center !important;
}

  </style>

<!-- ======= Header ======= -->
 <div class="container srace-wrapper">
<div class="row search-results">
   
    <div class="col-md-12 col-md-3">
        <h5><?php echo $_GET['product_search'];?> in <?php echo $_GET['location_search'];?></h5>

                <?php
                    foreach ($information as $key => $details) {
                        ?>
        
        <section class="search-result-item">
            <a class="image-link" href="#"><!-- <img class="image" src="assets/img/business_profiles/purohit2.jpeg"> -->
                <img class="image" src="{{url('/')}}/images/default_my_business.jpg" />
            </a>
            <div class="search-result-item-body">
                <div class="row">
                    <div class="col-sm-9">
                        <h4 class="search-result-item-heading"><a href="#"><?php echo $details->business_name;?></a> </h4>
                        <p class="info"><i class=" icofont-location-pin"></i>
                            <?php
                                echo $details->business_address;
                        ?></p>
                        <p class="description"><?php
                            echo $details->business_description;
                        ?></p>
                    </div>
                    <div class="col-sm-3 text-align-center">
                        <p class="info-contacts"><i class="icofont-ui-user"></i> 1000 Contacts</p><a class="btn btn-primary btn-info btn-sm" href="{{ url('biz-profile/' . $details->businesses_id) }}">Visit Store</a>
                    </div>
                </div>
            </div>
        </section>

        <?php
}
        ?>

        <!-- 
         <section class="search-result-item">
            <a class="image-link" href="#"><img class="image" src="assets/img/businesses/1623065414_sports.jpg">
            </a>
            <div class="search-result-item-body">
                <div class="row">
                    <div class="col-sm-9">
                        <h4 class="search-result-item-heading"><a href="#">Pune Sports</a> </h4>
                        <p class="info"><i class=" icofont-location-pin"></i>Baner, Pune, India</p>
                        <p class="description">You will never know exactly how something will go until you try it. You can think three hundred times and still have no precise result.</p>
                    </div>
                    <div class="col-sm-3 text-align-center">
                        <p class="info-contacts"><i class="icofont-ui-user"></i> 800 Contacts</p><a class="btn btn-primary btn-info btn-sm" href="#">Visit Store</a>
                    </div>
                </div>
            </div>
        </section>
       <section class="search-result-item">
            <a class="image-link" href="#"><img class="image" src="assets/img/businesses/1623066189_bicycle logo.jpg">
            </a>
            <div class="search-result-item-body">
                <div class="row">
                    <div class="col-sm-9">
                        <h4 class="search-result-item-heading"><a href="#">Pune Bicycles</a> </h4>
                        <p class="info"><i class=" icofont-location-pin"></i>Kothrud, Pune, India</p>
                        <p class="description">You will never know exactly how something will go until you try it. You can think three hundred times and still have no precise result.</p>
                    </div>
                    <div class="col-sm-3 text-align-center">
                        <p class="info-contacts"><i class="icofont-ui-user"></i> 700 Contacts</p><a class="btn btn-primary btn-info btn-sm" href="#">Visit Store</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="search-result-item">
            <a class="image-link" href="#"><img class="image" src="assets/img/businesses/1623068600_paint shop logo.png">
            </a>
            <div class="search-result-item-body">
                <div class="row">
                    <div class="col-sm-9">
                        <h4 class="search-result-item-heading"><a href="#">Pune Paintings</a> </h4>
                        <p class="info"><i class=" icofont-location-pin"></i>Kothrud, Pune, India</p>
                        <p class="description">You will never know exactly how something will go until you try it. You can think three hundred times and still have no precise result.</p>
                    </div>
                    <div class="col-sm-3 text-align-center">
                        <p class="info-contacts"><i class="icofont-ui-user"></i> 500 Contacts</p><a class="btn btn-primary btn-info btn-sm" href="#">Visit Store</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="search-result-item">
            <a class="image-link" href="#"><img class="image" src="assets/img/businesses/1623069218_music store logo.jpg">
            </a>
            <div class="search-result-item-body">
                <div class="row">
                    <div class="col-sm-9">
                        <h4 class="search-result-item-heading"><a href="#">Pune Music Store</a> </h4>
                        <p class="info"><i class=" icofont-location-pin"></i>Kothrud, Pune, India</p>
                        <p class="description">You will never know exactly how something will go until you try it. You can think three hundred times and still have no precise result.</p>
                    </div>
                    <div class="col-sm-3 text-align-center">
                        <p class="info-contacts"><i class="icofont-ui-user"></i> 200 Contacts</p><a class="btn btn-primary btn-info btn-sm" href="#">Visit Store</a>
                    </div>
                </div>
            </div>
        </section>
         <section class="search-result-item">
            <a class="image-link" href="#"><img class="image" src="assets/img/businesses/logo-gray.jpg">
            </a>
            <div class="search-result-item-body">
                <div class="row">
                    <div class="col-sm-9">
                        <h4 class="search-result-item-heading"><a href="#">Carving IT Pvt.Ltd.</a> </h4>
                        <p class="info"><i class=" icofont-location-pin"></i>Kothrud, Pune, India</p>
                        <p class="description">You will never know exactly how something will go until you try it. You can think three hundred times and still have no precise result.</p>
                    </div>
                    <div class="col-sm-3 text-align-center">
                        <p class="info-contacts"><i class="icofont-ui-user"></i> 100 Contacts</p><a class="btn btn-primary btn-info btn-sm" href="#">Visit Store</a>
                    </div>
                </div>
            </div>
        </section> -->
       
    </div>
    
</div>
</div>

 </div>
      <section id="blog" class="blog">
    
            {!! $information->appends(['location_search'=>$_GET['location_search'],'product_search'=>$_GET['product_search'],'productid'=>$_GET['productid']])->links("pagination::bootstrap-4") !!}
          
    </section>
<!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>MyTown</h3>
            <p>
              A108 Adam Street <br>
              New York, NY 535022<br>
              United States <br><br>
              <strong>Phone:</strong> +1 5589 55488 55<br>
              <strong>Email:</strong> info@example.com<br>
            </p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Services</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Join Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="container d-lg-flex py-4">

      <div class="mr-lg-auto text-center text-lg-left">
        <div class="copyright">
          &copy; Copyright <strong><span>MyTown</span></strong>. All Rights Reserved
        </div>
         <!-- <div class="credits">
         All the links in the footer should remain intact. -->
          <!-- You can delete the links only if you purchased the pro version. -->
          <!-- Licensing information: https://bootstrapmade.com/license/ -->
          <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/flexor-free-multipurpose-bootstrap-template/ 
          Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>-->
      </div>
      <div class="social-links text-center text-lg-right pt-3 pt-lg-0">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>
    </div>

  @endsection