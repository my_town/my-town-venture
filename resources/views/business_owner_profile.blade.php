@extends('layouts.new_MyTown')
@section('content')
@push('head')
<!-- new_sign_up_thank_you_layout -->

<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

<!-- <script src="//code.jquery.com/jquery-1.12.1.js"></script>  --> 
<!-- <link href="vendor/aos/aos.css" rel="stylesheet"> -->
<!-- <script src="https://code.jquery.com/jquery-2.1.4.js"></script> -->

<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="{{url('/')}}/js/form_validation.js"></script>  


<script>

$(function (){
    $("#locations_search").autocomplete({

      source :function( request, response ) {
        $.ajax({
           url: "{{ url('/location/search') }}",
           dataType: "json",
           data: {
              q: request.term 
           },
           success: function( data ) {
               response($.map(data.predictions, function(item) {
                     return {
                         label : item.description,
                         value : item.place_id
                     };
               }));
           }
        });
       },
       select: function (event, ui) {
             $("#locations_search").val(ui.item.label);
             $("#places_id").val(ui.item.value);
             return false;
       },
       focus: function(event, ui) {
        $("#locations_search").val(ui.item.label);
        return false;
      },
   });
});
</script>

<style>
  .ui-autocomplete-loading { background:url('{{url('/')}}/images/loading.gif') no-repeat right center }

  .error{
      color: red;
  }

  .get-started-icon {
      font-size: 24px;
      background: #ffce00;
      padding: 14px;
      color: #fff;
      border-radius: 50px;
      position: relative;
      z-index: 5;
      box-shadow: 10px 2px 15px rgba(0, 0, 0, 0.1);
  }

  .btn-get-started {
      font-family: "Raleway", sans-serif;
      font-weight: 400;
      font-size: 16px;
      letter-spacing: 1px;
      display: inline-block;
      transition: 0.5s;
      margin-left: -10px;
      padding: 8px 26px 8px 26px;
      color: #ffffff;
      background: #ffce00;
      border-radius: 0 50px 50px 0;
      position: relative;
      z-index: 4;
  }

  .btn-get-started:hover {
      color: #262228;
  }

  .img-fluid {
      width: 100%;
  }

  .bg-products {
      border: 1px solid #ffbd83;
      padding: 0;
      margin-right: 0px;
      margin-bottom: 5px;
  }
</style>
@endpush

<!-- ======= Business Profile Section ======= -->
<div class="container-fluid srace-wrapper business-wrpper">
    <div class="row">

        <div class="col-lg-12 col-md-12 col-sm-12" id="line1">

            <strong>
                <h3 class="location"><span><i class='fas fa-map-marker-alt' style='font-size:10px;color:red'></i>
                    </span>
                    {{-- {{$business->address}} --}}
                    {{ $placeId }}
                </h3>
            </strong>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="<?php echo Session::get('tab_count') == '1' ? 'nav-link active' : 'nav-link'; ?>" id="tab1-tab" data-toggle="tab" href="#tab1" role="tab"
                        aria-controls="tab1" aria-selected="true">OVERVIEW</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="<?php echo Session::get('tab_count') == '2' ? 'nav-link active' : 'nav-link'; ?>" id="tab2-tab" data-toggle="tab" href="#tab2" role="tab"
                        aria-controls="tab2" aria-selected="false">POSTS</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="<?php echo Session::get('tab_count') == '3' ? 'nav-link active' : 'nav-link'; ?>" id="tab3-tab" data-toggle="tab" href="#tab3" role="tab"
                        aria-controls="tab3" aria-selected="false">PRODUCTS</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="<?php echo Session::get('tab_count') == '4' ? 'nav-link active' : 'nav-link'; ?>" id="tab4-tab" data-toggle="tab" href="#tab4" role="tab"
                        aria-controls="tab4" aria-selected="false">VIDEO TOUR</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="<?php echo Session::get('tab_count') == '5' ? 'nav-link active' : 'nav-link'; ?>" id="tab5-tab" data-toggle="tab" href="#tab5" role="tab"
                        aria-controls="tab5" aria-selected="false">SPECIALS</a>
                </li>
            </ul>
        </div>
        <div class="col-sm-12 col-md-3 col-lg-3" id="line2">
            <div class="connection-section">
                <div class="business-left">
                    <div class="title-cot">
                        <h2 style="font-size: 22px;">{{ $business->name }}</h2>
                    </div>
                    <div class="fillter-two">
                        <!-- <div class="fillter-sweet"><img src="{{ url('/') }}/images/{{ $business->banner_image }}" /></div> -->
                        <img src="{{ url('/') }}/images/banner/{{ $business->banner_image }}"
                            alt="" height="205px" width="205px">
                        <a href="#" class="connect-store">Connect with the Store</a>
                    </div>
                </div>
                <div class="business-left ">
                    <div class="title-cot bg-color">
                        <h2>People in the store</h2>
                    </div>
                    <div class="fillter-three">
                        <ul>
                            <li>
                                <div class="ptstore-pic-people"><img src="{{ url('/') }}/images/avatar1.png" />
                                </div>
                                <h2><a href="">Ketan Kulkarni Sukla</a></h2>
                                <p>Pune</p>
                                <a href="#">Connect</a>
                            </li>
                            <li>
                                <div class="ptstore-pic-people"><img src="{{ url('/') }}/images/avatar1.png" />
                                </div>
                                <h2><a href="">Ketan Kulkarni Sukla</a></h2>
                                <p>Pune</p>
                                <a href="#">Connect</a>
                            </li>
                            <li>
                                <div class="ptstore-pic-people"><img src="{{ url('/') }}/images/avatar1.png" />
                                </div>
                                <h2><a href="">Ketan Kulkarni Sukla</a></h2>
                                <p>Pune</p>
                                <a href="#">Connect</a>
                            </li>
                            <li>
                                <div class="ptstore-pic-people"><img src="{{ url('/') }}/images/avatar1.png" />
                                </div>
                                <h2><a href="">Ketan Kulkarni Sukla</a></h2>
                                <p>Pune</p>
                                <a href="#">Connect</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6" id="line3">
            <div class="tab-content" id="myTabContent">
                <div class="<?php echo Session::get('tab_count') == '1' ? 'tab-pane fade show active' : 'tab-pane fade'; ?>" id="tab1" role="tabpanel" aria-labelledby="tab1-tab">
                    <div class="tab-info-box">
                        <!-- <h2>Overview</h2> -->
                        <div class="row">
                            <!-- <div class="col-md-4 col-sm-12">
                                <img src="{{ url('/') }}/images/{{ $business->banner_image }}" alt="">
                                <img src="{{ url('/') }}/images/default_my_business.jpg" alt="">
                            </div> -->
                            <div class="col-md-12 col-sm-12">
                                {{-- <p class="location">
                              <strong>  <span><i class="fas fa-map-marker-alt"></i></span>
                                {{$business->address}}</strong>
                            </p> --}}
                                <p>{{ $business->description }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="<?php echo Session::get('tab_count') == '2' ? 'tab-pane fade show active' : 'tab-pane fade'; ?>" id="tab2" role="tabpanel" aria-labelledby="tab2-tab">
                    <div class="tab-info-box">
                        <!--  <h2>Posts</h2> -->

                        @foreach ($posts as $post)
                            <div class="post-container">
                                <div class="post-titel-ber">
                                    <div class="post-pic-people"><img src="{{ url('/') }}/images/avatar1.png" />
                                    </div>
                                    <h4><a href="">Ketan Kulkarni Sukla</a></h4>
                                    <p>Pune</p>
                                    <span><i class="far fa-clock"></i> 10 mins ago</span>
                                </div>
                                <div class="post-contt">
                                    <h2><a href="">Lorem ipsum dolor sit amet consectetur adipisicing elit.</a></h2>
                                    <p>
                                        {{ $post->experience }}
                                    </p>
                                </div>
                                <div class="shear-ber">
                                    <ul>
                                        <li>
                                            <a href=""><i class="far fa-thumbs-up"></i> Like</a>
                                        </li>
                                        <li>
                                            <a href=""><i class="far fa-comment-dots"></i> Comment</a>
                                        </li>
                                        <li>
                                            <a href=""><i class="fas fa-share-alt"></i> Share</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="<?php echo Session::get('tab_count') == '3' ? 'tab-pane fade show active' : 'tab-pane fade'; ?>" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">
                    <div class="tab-info-box">
                        <!-- <h2>PRODUCTS</h2> -->

                        <div class="row">
                            @foreach ($products as $product)
                                @if ($product->product_count > 0)


                                    <div class="col-sm-12 col-md-6 col-lg-3">
                                        <div class="video-products">
                                            <div class="proimg">
                                                <img src="{{ url('/') }}/images/products/{{ $product->product_image }}"
                                                    alt="" height="185px" />
                                            </div>
                                            <div class="info">
                                                <h3><span>{{ $product->name }}</span></h3>
                                                <h5>Price Rs. {{ $product->actual_rate }}</h5>
                                                {{-- <p><span class="new">Save Rs. 10</span></p> --}}
                                            </div>

                                            <form action="{{ route('become-a-customer') }}" method="post">
                                                {{ csrf_field() }}

                                                <div class="vbtn">

                                                    <input type="hidden" name="product_id"
                                                        value="{{ $product->id }}" />
                                                    <input type="hidden" name="business_id"
                                                        value="{{ $business->id }}" />

                                                    <span class="qty">
                                                        <label>Qty :</label>
                                                        <select class="form-select" name="quantity"
                                                            aria-label="Default select example">
                                                            <option selected="" value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                        </select>
                                                    </span>


                                                    <!--  <input type="submit" name="submit" value="Add to Cart"/> -->
                                                    <a href="javascript:void(0)" class="login-button">Add to Cart</a>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                    </div>
                </div>
                <div class="<?php echo Session::get('tab_count') == '4' ? 'tab-pane fade show active' : 'tab-pane fade'; ?>" id="tab4" role="tabpanel" aria-labelledby="tab4-tab">
                    <div class="tab-info-box">
                        <!-- <h2>Video Tour</h2> -->
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="video-box">
                                    <video width="150" controls>
                                        <source
                                            src="{{ url('/') }}/videos/{{ $business->embedded_video_code }}"
                                            type="video/ogg">
                                    </video>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="<?php echo Session::get('tab_count') == '5' ? 'tab-pane fade show active' : 'tab-pane fade'; ?>" id="tab5" role="tabpanel" aria-labelledby="tab5-tab">
                    <div class="row">
                        @foreach ($products as $product)
                            <div class="col-sm-12 col-md-6 col-lg-3">
                                <div class="video-products">
                                    <div class="proimg">
                                        <img src="{{ url('/') }}/images/{{ $product->product_image }}" alt="" />
                                    </div>
                                    <div class="info">
                                        <h3><span>{{ $product->name }}</span></h3>
                                        <h5>Price Rs. {{ $product->actual_rate }}</h5>
                                        {{-- <p><span class="new">Save Rs. 10</span></p> --}}
                                    </div>
                                    <div class="vbtn">
                                        <a href="#">Add to Cart</a>
                                    </div>
                                    <span class="qty">
                                        <label>Qty :</label>
                                        <select class="form-select" aria-label="Default select example">
                                            <option selected="">1</option>
                                            <option value="1">2</option>
                                            <option value="2">3</option>
                                            <option value="3">4</option>
                                        </select>
                                    </span>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
        <div class="col-sm-12 col-md-3 col-lg-3" id="line4">
            <div class="right-section-bt">
                <div class="tag-section">
                    <h2>Business</h2>
                </div>
                <div class="business-logo-ft">
                    <ul>
                        <li>
                            <div class="bu-logo">
                                <img src="{{ url('/') }}/images/clients/client-3.png" />
                            </div>

                            <div class="life-j">
                                <h2><a href="#">Life Group Pvt Ltd</a></h2>
                                <p>Medicine Pharmacy</p>
                                <p>Pune</p>
                            </div>
                        </li>
                        <li>
                            <div class="bu-logo">
                                <img src="{{ url('/') }}/images/clients/client-3.png" />
                            </div>

                            <div class="life-j">
                                <h2><a href="#">Life Group Pvt Ltd</a></h2>
                                <p>Medicine Pharmacy</p>
                                <p>Pune</p>
                            </div>
                        </li>
                        <li>
                            <div class="bu-logo">
                                <img src="{{ url('/') }}/images/clients/client-3.png" />
                            </div>

                            <div class="life-j">
                                <h2><a href="#">Life Group Pvt Ltd</a></h2>
                                <p>Medicine Pharmacy</p>
                                <p>Pune</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End Business Profile Section -->
@endsection
