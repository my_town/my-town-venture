@extends('layouts.new_MyTown')
@section('content')
    @push('head')
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" type="text/css" href="/css/css/easy-responsive-tabs.css " />
        <!-- Favicons -->
        <link href="/images/favicon.png" rel="icon" />
        <link href="/images/apple-touch-icon.png" rel="apple-touch-icon" />
        <!--   <link rel="stylesheet" href="./assets/css/fontawesome/css/all.css" /> -->
        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
            rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap"
            rel="stylesheet" />
        <!-- Vendor CSS Files -->
        <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="/vendor/icofont/icofont.min.css" rel="stylesheet" />
        <link href="/vendor/boxicons/css/boxicons.min.css" rel="stylesheet" />
        <link href="/vendor/venobox/venobox.css" rel="stylesheet" />
        <link href="/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet" />
        <link href="/vendor/aos/aos.css" rel="stylesheet" />
        <!-- Template Main CSS File -->
        <!--    <link href="assets/css/style2.css" rel="stylesheet" />
         -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
                integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous">
        </script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <script src="js/form_validation.js"></script>
        <style>
            .get-started-icon {
                font-size: 24px;
                background: #ffce00;
                padding: 14px;
                color: #fff;
                border-radius: 50px;
                position: relative;
                z-index: 5;
                box-shadow: 10px 2px 15px rgba(0, 0, 0, 0.1);
            }

            .btn-get-started {
                font-family: "Raleway", sans-serif;
                font-weight: 400;
                font-size: 16px;
                letter-spacing: 1px;
                display: inline-block;
                transition: 0.5s;
                margin-left: -10px;
                padding: 8px 26px 8px 26px;
                color: #ffffff;
                background: #ffce00;
                border-radius: 0 50px 50px 0;
                position: relative;
                z-index: 4;
            }

            .btn-get-started:hover {
                color: #262228;
            }

            .img-fluid {
                width: 100%;
            }

            .bg-products {
                border: 1px solid #ffbd83;
                padding: 0;
                margin-right: 0px;
                margin-bottom: 5px;
            }

            .login-page .bg-blue {
                color: #fff;
                background-color: #1A237E;
            }

            .error {
                color: red;
            }

            .custom-file-upload {
                border: 1px solid #ccc;
                display: inline-block;
                padding: 6px 12px;
                cursor: pointer;
            }
            input {
  outline: 0;
  border-width: 0 0 2px;
  border-color: blue
}
input:focus {
  border-color: green
}
        </style>
        <script>
            function editText()
            {
                if($('input').is('[readonly]'))
                {
                    $('input').attr('readonly', false);
                }
            }
            $(function() {
                $("#locations_search").autocomplete({

                    source: function(request, response) {
                        $.ajax({
                            url: "{{ url('/location/search') }}",
                            dataType: "json",
                            data: {
                                q: request.term
                            },
                            success: function(data) {
                                response($.map(data.predictions, function(item) {
                                    return {
                                        label: item.description,
                                        value: item.place_id
                                    };
                                }));
                            }
                        });
                    },
                    select: function(event, ui) {
                        $("#locations_search").val(ui.item.label);
                        $("#places_id").val(ui.item.value);
                        return false;
                    },
                    focus: function(event, ui) {
                        $("#locations_search").val(ui.item.label);
                        return false;
                    },
                });
            });
            /// for business offering type
            function selectOnlyThis(id) {
                for (var i = 1; i <= 3; i++) {
                    document.getElementById(i).checked = false;
                    document.getElementsByClassName("select_" + i).disabled = true; //not working currently

                }
                document.getElementById(id).checked = true;
                document.getElementsByClassName("select_" + id).disabled = false;
            }

            // for next button click
            function nextButtonCick(id, tabId) {


                var ele = document.getElementById(tabId);
                ele.classList.add("active");
                var element = document.getElementById("tab_" + id);

                element.classList.remove("active");

            }

            //video uploading

            function open_file() {
                document.getElementById('input_file').click();
            }


            $(document).ready(function() {
                $('#input_file').change(function(e) {
                    var fileName = e.target.files[0].name;
                    // alert('The file "' + fileName +  '" has been selected.');
                    //$('#video_code').val(fileName);
                    //alert(this.files[0].size);
                    document.getElementById('video_code').value = fileName;
                });
            });
            //video uploading

            function open_image_file() {
                document.getElementById('input_image_file').click();
            }


            $(document).ready(function() {
                $('#input_image_file').change(function(e) {
                    var fileName = e.target.files[0].name;
                    // alert('The file "' + fileName +  '" has been selected.');
                    //$('#video_code').val(fileName);
                    //alert(this.files[0].size);
                    document.getElementById('image_code').value = fileName;
                });
            });
        </script>
        <!-- ======= About Section ======= -->
        <section id="about" class="about login-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
                        <div class="card card0 border-0 login-page">
                            <div class="row d-flex">

                                <div class="col-lg-3">
                                    <div class="card2 my-4 px-3">
                                        <div class="row px-3 justify-content-center mt-4 mb-5">
                                            <img src="images/img/login-clipart.png" class="image">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-9">
                                    <div class="card2 card border-0 px-4 py-5">
                                        <div class="row mb-4 px-3">
                                            <h5 class="mb-0 mr-4 mt-2">Your Profile Overview</h5>
                                        </div>
                                        
                                            <div class="row px-3">
                                                <label class="mb-1" for="name">
                                                    <h6 class="mb-0 text-sm">Your Business Name</h6>
                                                </label>
                                                <input id="name" class="mb-4" type="text" name="name" placeholder="Name" style="outline:0;border-width: 0 0 2px; border-color: blue;" readonly>
                                            </div>
                                            <div class="row px-3">
                                                <label class="mb-1" for="name">
                                                    <h6 class="mb-0 text-sm">Service or Product Name</h6>
                                                </label>
                                                <input id="name" class="mb-4" type="text" name="name" placeholder="Name" style="outline:0; border-width: 0 0 2px; border-color: blue;" readonly>
                                            </div>
                                            <div class="row px-3">
                                                <label class="mb-1" for="name">
                                                    <h6 class="mb-0 text-sm">Street Name</h6>
                                                </label>
                                                <input id="name" class="mb-4" type="text" name="name" placeholder="Name" style="outline:0; border-width: 0 0 2px; border-color: blue;" readonly>
                                            </div>

                                            <div class="row px-3">
                                                <label class="mb-1" for="location_search">
                                                    <h6 class="mb-0 text-sm">Town Name</h6>
                                                </label>

                                                <input class="mb-4" id="locations_search" type="text"
                                                    name="locations_search" required autofocus autocomplete="locations_search"
                                                    placeholder="Start typing your Town Name" style="outline:0; border-width: 0 0 2px; border-color: blue;" readonly>
                                                <input id="places_id" type="hidden" name="places_id" required value="" >
                                            </div>

                                            <div class="row px-3">
                                                <label class="mb-1" for="name">
                                                    <h6 class="mb-0 text-sm">Email address</h6>
                                                </label>
                                                <input id="name" class="mb-4" type="text" name="name" placeholder="Name" style="outline:0; border-width: 0 0 2px; border-color: blue;" readonly>
                                            </div>

                                            <div class="row px-3">
                                                <label class="mb-1" for="phone">
                                                    <h6 class="mb-0 text-sm">Mobile number
                                                    </h6>
                                                </label>
                                                <input id="phone" class="mb-4" type="text" name="phone" placeholder="Mobile number" style="outline:0; border-width: 0 0 2px; border-color: blue;" readonly>
                                            </div>
                                            <div class="row px-3">
                                                <label class="mb-1" for="phone">
                                                    <h6 class="mb-0 text-sm">Business Discription 
                                                    </h6>
                                                </label>
                                                {{-- <input id="phone" class="mb-4" type="text" name="phone" placeholder="Mobile number" style="outline:0; border-width: 0 0 2px; border-color: blue;" readonly>
                                             --}}
                                             <textarea name="" id="" cols="30" rows="3"></textarea>
                                            </div>

                                            

                                            <div class="row mb-3 px-3"><button 
                                                    class="btn btn-blue text-center" style="margin:15px;border-radius: 8px;" onclick="editText();">Edit</button> 
                                                    <button type="submit" class="btn btn-blue text-center" style="margin:15px;border-radius: 8px;">Save & Continue</button>
                                            </div>


                                       
                                    </div>
                                </div>
                            </div>
                            <div class="bg-blue py-4">
                                <div class="row px-3">
                                    <small class="ml-4 ml-sm-5 mb-2"> &copy; Copyright <strong><span>MyTown</span></strong>. All
                                        Rights Reserved</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- End About Section -->

    @endsection
