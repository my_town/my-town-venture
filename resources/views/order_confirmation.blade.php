@extends('layouts.new_MyTown')
@section('content')
@push('head')

<?php $items=json_decode($orders->items);
    
?>
<section class="checkout-wrp">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12">
                       <div class="checkout-box">
                            <div class="orange-head">
                               <h3>Congratulations! You have successfully placed your order.</h3>
								
								
                            </div>
					     <div class="order-confirm">
                            <p>Thank you for your purchase! Click here to print a copy of your order <a href="#">confirmation and invoice</a>.</p>
							 
							 <div class="shopping-box">
                                <div class="row">
                                    <div class="col-md-6 col-sm-7">
                                        <div class="box-img">
                                            <p><strong>Order ID: </strong>#<?php echo $orders->id;?><br>
<strong>Transaction ID: </strong>#<?php echo(rand()); ?> </p>
											<p><strong>Payment Method:</strong>

<br>
ICICI Bank Credit Card, <br>

 <?php  foreach ($billing_address as $key => $billing) {?>
    Visa <?php echo $billing->card_number;?>
 <?php }?>
</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-8">
                                        <div class="shopping-info">
											 <p>

                <?php
                if(count($shipping_address)==0){?>
                
            <strong>Shipping & Billing information:</strong><br>
                <?php  foreach ($billing_address as $key => $billing) {
                    echo $billing->first_name." ".$billing->last_name."</br>";

                    if(Auth::user()){
                        echo $billing->address."</br>";
                    }else{
                        echo $billing->state."</br>";
                        echo $billing->country.", ".$billing->zip_code."</br>";
                    }
                    
                    echo $billing->email."</br>";
                    echo $billing->phone."</br>";
                }?>

                <?php }else{?>
                    
                    <strong>Shipping information:</strong><br>
                <?php  foreach ($shipping_address as $key => $shipping) {
                    echo $shipping->first_name." ".$shipping->last_name."</br>";
                    echo $shipping->address.", ".$shipping->state."</br>";
                    echo $shipping->country.", ".$shipping->zip_code."</br>";
                    echo $shipping->email."</br>";
                    echo $shipping->phone."</br>";
                }?>

                </br><strong>Billing information:</strong><br>
                <?php  foreach ($billing_address as $key => $billing) {
                    echo $billing->first_name." ".$billing->last_name."</br>";
                    
                    if(Auth::user()){
                        echo $billing->address."</br>";
                    }else{
                        echo $billing->state."</br>";
                        echo $billing->country.", ".$billing->zip_code."</br>";
                    }
                    
                    echo $billing->email."</br>";
                    echo $billing->phone."</br>";
                }?>

                <?php } ?>
                </p>


                                         
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-4">
                                      
                                    </div>
                                    
                                </div>
                            </div>
							 	
							 <!-- <h5>Your cart is empty.</h5> -->
                    <p>
                        <a href="/"><strong>Continue shopping</strong></a>.
                    </p>
								
					       </div>
                       </div>

                       

                     
                    </div>



                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="price-block">
                            <h2>Order Summary</h2>

                            <ul>
                               <?php
if(empty($items))
{
    echo "Record is not available";
}else{?>                                
    @foreach ($items as $item)
    <?php $product=App\Models\Product::where('id',$item->id)->first();?>
                            <li>{{$item->name}} (item {{$item->quantity}})<span>₹{{ $item->price*$item->quantity }}</span></li>
    @endforeach
<?php } ?>

<!-- <li>Item 2 (3 Items) <span>Rs. 200</span></li>
<li>Item 3 (4 Items) <span>Rs. 200</span></li> -->
<li class="charges">Delivery Charges<span>Rs. 80</span></li>
<?php
if(empty($items)){//echo "Success";
}else{ ?> <li class="total">GRAND TOTAL <span>₹{{ $orders->total_price + 80 }}</span></li>
<?php } ?>
                            </ul>
                              <div class="promo">
								  <p><strong><a href="#">Download Invoice</a></strong></p>
                               
                            </div>
 
                      
                            
                        </div>
                    </div>


                </div>
				<section id="clients" class="clients">
      <div class="container" data-aos="fade-up">
<div style="margin-bottom: 20px; "><h5>Kala Niketan - Bandra, Mumbai, India - <a href="#">Visit Store</a></h5></div>
        <div class="owl-carousel clients-carousel">
          <a href="#"><img src="{{ url('images/saree1.jpg') }}" alt=""> <span class="text-small">Rs. 1000</span></a>
         <img src="{{ url('images/saree2.jpg') }} " alt="">
			<img src="{{ url('images/saree3.jpg') }} " alt="">
			<img src="{{ url('images/saree4.jpg') }} " alt="">
			<img src="{{ url('images/saree5.jpg') }} " alt="">
			<img src="{{ url('images/saree6.jpg') }} " alt="">
			<img src="{{ url('images/saree7.jpg') }} " alt="">
			<img src="{{ url('images/saree8.jpg') }} " alt="">
        </div>

      </div>
    </section>
				
				<section id="clients" class="clients">
      <div class="container" data-aos="fade-up">
<div style="margin-bottom: 20px; "><h5>Bestsellers in Artifacts, <i class="icofont-location-pin"></i>Melbourne, Australia</h5></div>
        <div class="owl-carousel clients-carousel">
          <img src="{{ url('/images/client-1.png') }}" alt="">
          <img src="{{ url('/images/client-2.png') }}" alt="">
          <img src="{{ url('/images/client-3.png') }}" alt="">
          <img src="{{ url('/images/client-4.png') }}" alt="">
          <img src="{{ url('/images/client-5.png') }}" alt="">
          <img src="{{ url('/images/client-6.png') }}" alt="">
          <img src="{{ url('/images/client-7.png') }}" alt="">
          <img src="{{ url('/images/client-8.png') }}" alt="">
        </div>

      </div>
    </section>
            </div>
       </section>

       <footer id="footer">
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 footer-contact">
                            <h3>MyTown</h3>
                            <p>
                                A108 Adam Street <br />
                                New York, NY 535022<br />
                                United States <br />
                                <br />
                                <strong>Phone:</strong> +1 5589 55488 55<br />
                                <strong>Email:</strong> info@example.com<br />
                            </p>
                        </div>

                        <div class="col-lg-2 col-md-6 footer-links">
                            <h4>Useful Links</h4>
                            <ul>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
                            </ul>
                        </div>

                        <div class="col-lg-3 col-md-6 footer-links">
                            <h4>Our Services</h4>
                            <ul>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
                            </ul>
                        </div>

                        <div class="col-lg-4 col-md-6 footer-newsletter">
                            <h4>Join Our Newsletter</h4>
                            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                            <form action="" method="post"><input type="email" name="email" /><input type="submit" value="Subscribe" /></form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container d-lg-flex py-4">
                <div class="mr-lg-auto text-center text-lg-left">
                    <div class="copyright">
                        &copy; Copyright <strong><span>MyTown</span></strong>. All Rights Reserved
                    </div>
                </div>
                <div class="social-links text-center text-lg-right pt-3 pt-lg-0">
                    <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                    <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                    <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                    <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                    <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                </div>
            </div>
        </footer>
      @endsection