@extends('layouts.my-town-app')
@section('content')
@push('head')


<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://kit.fontawesome.com/e3dc723f7b.js" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>


<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>

<script>
$(document).ready(function(){
    function alignModal(){
        var modalDialog = $(this).find(".modal-dialog");
        
        // Applying the top margin on modal to align it vertically center
        modalDialog.css("margin-top", Math.max(0, ($(window).height() - modalDialog.height()) / 2));
    }
    // Align modal when it is displayed
    $(".modal").on("shown.bs.modal", alignModal);
    
    // Align modal when user resize the window
    $(window).on("resize", function(){
        $(".modal:visible").each(alignModal);
    });   
});
</script>


<script>
    $(document).ready(function() {
    $('#contacts').DataTable({
        "oLanguage": {
        "sEmptyTable": "Contacts are not available."
    }

    });
} );
</script>
<script>
   $(document).ready(function(){
                $('#show_contact_form').on('click',function(){
                    $('#contact-box').show();
                });

                $('#hide_contact_form').on('click',function(){
                    $('#contact-box').hide();
                });
            });
</script>
<script>
         
         /*$(function() {
            <?php 
                $name=DB::table('users')->pluck('name');
                $place=DB::table('users')->pluck('place');
                $final=array();
                for ($i =0; $i < sizeof($name); $i++){
                    $final[$i]=$name[$i].", ".$place[$i];  
                }
                
            ?>
            var availableTutorials = <?php echo json_encode($final); ?>;
            $( "#automplete-1" ).autocomplete({
               source: availableTutorials
            });
         });*/

</script>

<style type="text/css">
    .card {
    
    top:0;
    left:0;
    z-index: 2000;
    
    background-color: white;
    border-radius:10px;
    border: 1px solid rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
    -moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
    box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
    -webkit-background-clip: padding-box;
    -moz-background-clip: padding-box;
    background-clip: padding-box;
}

.card .modal-header {
    margin:5px;
}

.card .modal-body {
    margin:5px 15px 5px 15px;
}

.card .modal-footer {
    margin:0 15px 15px 15px;
}

.card .modal-header .close {
 
  right: 10px;
  top: 10px;
  color: #999;
  line-height: 10px;
  cursor: pointer;
}

.footer {
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: red;
  color: white;
  text-align: center;
}


/* Important part */
.modal-dialog{
    overflow-y: initial !important
}
.suggested-modal-body, .business-modal-body{
    height: 60vh;
    overflow-y: auto;
}

@wuliwong

</style>

@endpush


<div class="box1" id="contact-box">
        <div class="card">

            <div class="modal-header" style="background:#ec0038">
        <h5 class="modal-title" style="color:white;">Send Request</h5>
         <label id="hide_contact_form" class="close-btn fas fa-times"></label>
         
        </button>
      </div>


           
            <div class="card-body">
                <!-- <h5 class="card-title">Contact Details</h5> -->
                
                <form action="{{ url('contacts/add_contact') }}" method="POST">
                    @csrf
                    <div class = "mb-3">
                        <label for = "automplete-1" class="form-label">Search by name or by town:- </label>

                         <input type="text" class="form-controller" id="search" name="search"></input>
                        <!-- <input type="text" name="name" class="form-control" id = "automplete-1" placeholder="Search by Name, Town, City or Country"> -->
                    </div>
                </form>

                <table class="table" id="search_result">
                              
                </table>


            </div>
        </div>
    </div>

<div class="row" style="padding-top: 20px;">

<div class="col-sm-3"></div>

<div class="col-sm-6">

<div class="alert alert-primary" role="alert" style="margin-top:20px">You do not have any contacts. Would you like to add any contact? 

<!-- <button type="button" class="show-btn btn btn-primary">Yes</button>
<a href="{{ url('/my_feed') }}"><button type="button" class="show-btn btn btn-primary">No</button></a> -->

<span style="margin-left:20px;margin-right: 20px;"><a href="#">Yes</a></span>/
    <span style="margin-left:10px"><a href="{{ url('/my_feed') }}">No</a></span>

</div>



</div>

</div>
<div class="col-sm-3"></div>
</div>



<div class="row">

    <div class="col-sm-4">
        
    </div>
    
    <div class="col-sm-2">

        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Business Contacts</button>

        <?php //print_r(Auth::user()->id);?>

    </div>
    
    <div class="col-sm-2">

        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#suggested">People you may know.</button>
    
    </div>

     <div class="col-sm-4">
        
    </div>

</div>


<div class="row" style="padding-top: 20px;">

<div class="col-sm-2"></div>
    <div class="col-sm-8">

        <?php
        $count = App\Http\Controllers\UserController::geContactCount();
       if($count){
        ?>

    <div class="table-responsive">
        <table id="contacts" class="display">
                    <thead>
                        <tr>
                        <th>ID</th>
                        <th>NAME</th>      
                        <th>PLACE</th>
                        <th>ACTIONS</th>
                    </tr>
                    </thead>
                        <tbody>
                            @foreach($contacts as $key => $details)
                                <tr>
                                <td>{{ $details->contact_id }}</td>
                                <td>{{ $details->user_name }}</td>
                                <td>{{ $details->user_place }}</td>
                                <td>Delete </td>
                                </tr>
                                        @endforeach
                                </tbody>
                         </table>
                    </div>

                    <?php
                }
                    ?>



                </div>
                <div class="col-sm-2"></div>


  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
    <div class="modal-header" style="background-color: #242e69;color: ghostwhite;">
          <h6 class="modal-title" style="color:white">Business Contacts</h6>
          
          <button type="button" style="color:white" class="close" data-dismiss="modal">&times;</button>
       
        </div>

        <div class="business-modal-body">
        
        <div class="jumbotron">
            <div class="col-12 col-12-medium" >

                <div id="postList">
                </div>
            
            </div>
            </div>

        </div>

        <div class="modal-footer"></div>
        
      </div>
    </div>
  </div>


  <div class="modal" id="suggested">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
    <div class="modal-header" style="background-color: #242e69;color: ghostwhite;">
          <h6 class="modal-title" style="color:white">Suggested Contacts</h6>
          
          <button type="button" style="color:white" class="close" data-dismiss="modal">&times;</button>
       
        </div>

        <div class="suggested-modal-body">
        
        <div class="jumbotron">
                <div id="postSuggest">
                </div>
            </div>

        </div>

        <div class="modal-footer"></div>
        
      </div>
    </div>
  </div>

<script type="text/javascript">

    $('#search').on('keyup',function(){

            $value=$(this).val();

            $.ajax({
            type : 'get',
            url : '{{URL::to('search')}}',
            data:{'search':$value},
            success:function(data){

            $('#search_result').html(data);
            
            $("button").click(function(e) {
                    e.preventDefault();

                        var me = $(this);
                        var user_id = me.val();
                            

                    $.ajax({
                        type: "get",
                        url: '{{URL::to('sendrequest')}}',
                        data: { 
                            'id': user_id
                        },
                        success: function(result) {
                          me.hide();
                        $('#request_title'+user_id).html("Request Sent");
                           
                        },
                        error: function(result) {
                            alert('error');
                        }
                    });
                });
            }
        });
    });

    </script>
    
    <script type="text/javascript">
    $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
    </script>
    
  
 <script type="text/javascript">

/*...................BusinessContacts..............................*/


$(document).ready(function(){
    $('.business-modal-body').scroll(function(){

         var lastID = $('.load-more').attr('lastID');

        if(($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight) && (lastID != 0))
        {
            $.ajax({
                type:'get',
                url: '{{URL::to('business_records')}}',
                data:'id='+lastID,
                beforeSend:function(){
                    $('.load-more').show();
                },
                success:function(html){
                    $('.load-more').remove();
                    $('#postList').append(html);

                    $("button").click(function(e) {
                                e.preventDefault();
                                
                                    var me = $(this);
                                    var user_id = me.val();

                                    if(user_id){
                                        
                                        $.ajax({
                                        type: "get",
                                        url: '{{URL::to('sendrequest')}}',
                                        data: { 
                                            'id': user_id
                                        },
                                        success: function(result) {
                                          me.hide();
                                        $('#request_title'+user_id).html("Request Sent");    
                                        },
                                        error: function(result) {
                                            alert('error');
                                        }
                                    });

                                    }else{
                                       
                                    }
                            });
                }
            });
        }
    });
});

$(document).ready(function(){  
            $.ajax({
                        type: "get",
                        url: '{{URL::to('business_few_records')}}',
                        success: function(result) {
                          $('#postList').html(result);
                           
                           $("button").click(function(e) {
                                e.preventDefault();
                                
                                    var me = $(this);
                                    var user_id = me.val();

                                    if(user_id){
                                        $.ajax({
                                        type: "get",
                                        url: '{{URL::to('sendrequest')}}',
                                        data: { 
                                            'id': user_id
                                        },
                                        success: function(result) {
                                          me.hide();
                                        $('#request_title'+user_id).html("Request Sent");    
                                        },
                                        error: function(result) {
                                            alert('error');
                                        }
                                    });
                                    }else{
                                       
                                    }
                            });

                        },
                        error: function(result) {
                            alert('error');
                        }
                    });


        });


/*................Suggested Contacts....................................*/

$(document).ready(function(){
    $('.suggested-modal-body').scroll(function(){
         var lastID = $('.load-more-more').attr('lastID');
        if(($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight) && (lastID != 0)){
            $.ajax({
                type:'get',
                url: '{{URL::to('suggested_contacts')}}',
                data:'id='+lastID,
                beforeSend:function(){
                    $('.load-more-more').show();
                },
                success:function(html){
                    $('.load-more-more').remove();
                    $('#postSuggest').append(html);

                    $("button").click(function(e) {
                                e.preventDefault();
                                
                                    var me = $(this);
                                    var user_id = me.val();

                                    if(user_id){
                                        
                                        $.ajax({
                                        type: "get",
                                        url: '{{URL::to('sendrequest')}}',
                                        data: { 
                                            'id': user_id
                                        },
                                        success: function(result) {
                                          me.hide();
                                        $('#request_title'+user_id).html("Request Sent");    
                                        },
                                        error: function(result) {
                                            alert('error');
                                        }
                                    });
                                        
                                    }else{
                                       
                                    }
                            });
                }
            });
        }
    });
});

$(document).ready(function(){  
            $.ajax({
                        type: "get",
                        url: '{{URL::to('suggested_few_contacts')}}',
                        success: function(result) {
                          $('#postSuggest').html(result);

                          $("button").click(function(e) {
                                e.preventDefault();
                                
                                    var me = $(this);
                                    var user_id = me.val();

                                    if(user_id){
                                        $.ajax({
                                        type: "get",
                                        url: '{{URL::to('sendrequest')}}',
                                        data: { 
                                            'id': user_id
                                        },
                                        success: function(result) {
                                          me.hide();
                                        $('#request_title'+user_id).html("Request Sent");    
                                        },
                                        error: function(result) {
                                            alert('error');
                                        }
                                    });
                                    }else{
                                       
                                    }
                            });
                        },
                        error: function(result) {
                            alert('error');
                        }
                    });
        });

/*..........................................................................*/

 </script>   




@endsection
