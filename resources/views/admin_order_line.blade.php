@push('head')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://kit.fontawesome.com/e3dc723f7b.js" crossorigin="anonymous"></script>
<script>
    $(document).ready( function () {
    $('#business').Table();
} );
</script>
<style>
.middle{
    width:75%;
    margin:10px auto;
}
table{
    width:100%;
}
.middle table thead tr th{
    border-bottom: 2.5px solid black;
    font-weight:bold;
}
.middle table tbody tr{
    padding: 10px;
    border-bottom: 1px solid grey;
    border-collapse: collapse;
}
.middle table tbody tr:last-child{
    border-bottom:none;
}
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
$(document).ready(function () {
$(".deleteConfirm").click(function () {
var answer = confirm("Are you sure you want to delete this record?");
return answer;
});
});
</script>
@endpush
<x-app-layout>
    <div class="table-responsive">
        <div class="middle">
            <table >
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Price per product</th>
                        <th>Quantity</th>   
                        <th>Total price</th>  
                        <th>Shop</th>  
                        <th>Actions</th> 
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $order)
                        <?php $items=json_decode($order->order->items);?>
                        @foreach($items as $item)
                            @if($item->id==$order->product_id)
                                <?php $product=App\Models\Product::where('id',$item->id)->first();?>
                                <tr>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->price }}</td>
                                    <td>{{ $item->quantity }}</td>
                                    <td>{{ $order->cost * $item->quantity }}</td>
                                    <td>{{ $product->business->name }}<br>{{ $product->business->address }}</td> 
                                    <td><a href="{{ url('/admin/order/delete/'.$order->id) }}" class="deleteConfirm"><i class="fas fa-trash-alt"></i></a></td>
                                </tr>
                            @endif
                        @endforeach
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</x-app-layout>