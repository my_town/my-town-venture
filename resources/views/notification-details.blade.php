@extends('layouts.my-town-app')
@section('content')
@push('head')




<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://kit.fontawesome.com/e3dc723f7b.js" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
    $('#businesses').DataTable( {
        "columnDefs": [{
            "targets":[4],
            "orderable":false,
             "className": 'dt-body-right'
        }]
    } );
} );
</script>
@endpush


<div class="greybg content-box">
    <div class="container">
        <div class="row paddingbottom3">
            <div class="col-8 col-12-medium">

                <b>Notfication Details:- </b>
                
                       <?php
                        foreach ($notifications as $key => $details) {
                        ?>
                            <div class="tabcontent" style="display:block;">
                                <div class="p-6 sm:px-20 bg-white border-b border-gray-200">
                        
                        <?php echo $details->user_name;?> wants to add you in his / her contact list.

                         <div align="right">
                        
                            <button id="notification_accept" 
                            value="<?php echo $details->notification_id.",".$details->sender_id.","."1"; ?>" class="btn btn-primary">Accept</button>

                            <button id="notification_cancel" 
                            value="<?php echo $details->notification_id.",".$details->sender_id.","."0"; ?>" class="btn btn-primary">Cancel</button>

                        </div>

                                </div></div>   
                        <?php }?>

               
		           
            </div>

            <div class="col-4 col-12-medium">
                <div class="whitebg padding2 margintop3">
                    <h5>4 Visitors also viewing this</h5>
                    <div class="orange">
                        <ul>
                            <li>Pratiksha</li>
                            <li>John Doe</li>
                        </ul>
                    </div>
                </div>
                <div class="whitebg padding2 margintop2">
                    <h5>4 Visitors also viewing this</h5>
                    <div class="orange">
                        <ul>
                            <li>Pratiksha</li>
                            <li>John Doe</li>
                        </ul>
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){

   
            $("button").click(function(e) {
                                e.preventDefault();
                                
                                    var me = $(this);
                                    var notification_id = me.val();

                                    if(notification_id){

                                        $.ajax({
                                        type: "get",
                                        url: '{{URL::to('accept_contact_request')}}',
                                        data: { 
                                            'id': notification_id
                                        },
                                        success: function(result) {
                                        location.reload();
                                        
                                        },
                                        error: function(result) {
                                            alert('error');
                                        }
                                    });

                                    }else{
                                       
                                    }
                            });
                });
</script>

@endsection