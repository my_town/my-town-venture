@extends('layouts.new_MyTown')
@section('content')

 <!-- ======= Breadcrumbs ======= -->
 <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">
        <ol>
          <li><a href="index.html">Kothrud</a></li>
          <li><a href="#">Pune</a></li>
		     <li><a href="#">India</a></li>
			   
        </ol>
       
      </div>
    </section><!-- End Breadcrumbs -->
   <!-- ======= Hero Section ======= -->
  <!--<div id="carouselExampleIndicators" class="carousel slide hp-slider" data-ride="carousel" >
 
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block1 w-100" src="assets/img/business_profiles/purohit2_nameboard.jpg" alt="First slide">
<button type="button" class="btn btn-primary btn-lg" style="position: absolute; top: 230px; right: 50px; "><a href="purohit_products.html">Buy Products</a></button>    
    </div>
  
  </div>
</div>--><!-- End Hero -->
  
  <main id="main" >
  
 


    <div class="container gedf-wrapper">
        <div class="row">
            <div class="col-md-3">
                

				<div class="card gedf-card">
                    <div class="card-body" >
                        <h5 class="card-title">Contacts <span class="rt-cart"><i class="icofont-location-pin"></i>Pune</span></h5>
						
                         <ul class="list-group list-group-flush"  >
                        <li class="list-group-item" >
						  
                            <div class="user-text-muted">
                              <span class="rounded-circle"><img src="images/img/avatar1.png" alt="" ></span> 
                              <h2><a href="#">Ketan Kulkarni</a> </h2>
                              <p>Pune</p>
                              <a href="" class="onnect-n">Connect</a>
                            </div>
							  <div class="user-text-muted">
                              <span class="rounded-circle"><img src="images/img/avatar1.png" alt="" ></span> 
                              <h2><a href="#">Ketan Kulkarni</a></h2>
                              <p>Pune</p>
                              <a href="" class="onnect-n">Connect</a>
                            </div>
							   <div class="user-text-muted">
                              <span class="rounded-circle"><img src="images/img/avatar1.png" alt="" ></span> 
                              <h2><a href="#">Ketan Kulkarni</a></h2>
                              <p>Pune</p>
                              <a href="" class="onnect-n">Connect</a>
                            </div>
                          
                        </li>
                      <!--  <li class="list-group-item">
                            <div class="h6 text-muted">Contacts</div>
                            <div class="h5">6758</div>
                        </li>-->
                     
                    </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 gedf-main" style="padding:0;">
			<div class="card gedf-card">
                    <div class="card-header">
					
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="mr-2">
                                  
                                </div>
                                <div class="ml-2">
                                    <div class="h5 m-0">Thank you!</div>
                                    <!--<div class="h7 text-muted">Sub Heading</div>-->                                </div>
                            </div>
                            <div>
                                <div class="dropdown">
                                    <button class="btn btn-link dropdown-toggle" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        
                                    </button>
                                   
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-body-two">
                       
                        <a class="card-link" href="#">
                            <h5 class="card-title-two">You have now registered as a Shopper</h5>
                        </a>

                        <p class="card-text">
                           If you would like to register your business, <a href="#">Register</a> now or continue as a Shopper.</p>
                           <div class="rt-bt6"><button type="submit" class="btn btn-blue text-center">Register Your Business</button> <button type="submit" class="btn btn-blue text-center">Explore Shopping</button></div>
                    </div>
                   
                </div>

                <!--- \\\\\\\Post-->
				
                
                <!-- Post /////-->

            <div >
                                  <!--- \\\\\\\Post-->
             
			   
                <!-- Post /////-->
				   <!--- \\\\\\\Post-->
              
			   
                <!-- Post /////-->
				   <!--- \\\\\\\Post-->
             
			   
                <!-- Post /////-->
                            </div>



            </div>
            <div class="col-md-3">
                
                <div class="card gedf-card">
                        <div class="card-body">
                            <h5 class="card-title">Businesses <span class="rt-cart"><i class="icofont-location-pin"></i>Pune</span></h5>
                            <div class="card-logo-ft">
                              <ul>
                                <li> 
                                  <div class="ct-logo"><img src="images/img/clients/client-1.png"></div>
                                  <div class="life-j"><h2><a href="#">Life Group Pvt Ltd</a></h2><p>Medicine Pharmacy</p><p>Pune</p></div>
                                </li>

                                 <li> 
                                  <div class="ct-logo"><img src="images/img/clients/client-2.png"></div>
                                  <div class="life-j"><h2><a href="#">Life Group Pvt Ltd</a></h2><p>Medicine Pharmacy</p><p>Pune</p></div>
                                </li>

                                 <li> 
                                  <div class="ct-logo"><img src="images/img/clients/client-4.png"></div>
                                  <div class="life-j"><h2><a href="#">Life Group Pvt Ltd</a></h2><p>Medicine Pharmacy</p><p>Pune</p></div>
                                </li>

                                 <li> 
                                  <div class="ct-logo"><img src="images/img/clients/client-5.png"></div>
                                  <div class="life-j"><h2><a href="#">Life Group Pvt Ltd</a></h2><p>Medicine Pharmacy</p><p>Pune</p></div>
                                </li>
                               
                              </ul>
                            </div>
                      </div>
                    </div>
            </div>
        </div>
    </div>

  
</div>
   


  

   

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container">

        <div class="row">
		

          <div class="col-lg-8 entries">
		 

            <!-- <article class="entry ">
			
                <div class="entry-img">
                <img src="assets/img/blog-1.jpg" alt="" class="img-fluid">
              </div>

           <h2 class="entry-title">
                <a href="blog-single.html">Dolorum optio tempore voluptas dignissimos cumque fuga qui quibusdam quia</a>
              </h2>-->

<!--<div class="col-md-12">
            <div class="tab" role="tabpanel">
                <!-- Nav tabs -->
              <!--  <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#Section1" aria-controls="home" role="tab" data-toggle="tab"><i class="icofont-user"></i> Overview</a></li>
                    <li role="presentation"><a href="#Section2" aria-controls="profile" role="tab" data-toggle="tab"><i class="icofont-photobucket"></i> Photos</a></li>
                    <li role="presentation"><a href="#Section3" aria-controls="messages" role="tab" data-toggle="tab"><i class="icofont-rss-feed"></i> Feed</a></li>
                </ul>
                <!-- Tab panes -->
              <!--  <div class="tab-content tabs">
                    <div role="tabpanel" class="tab-pane fade in active" id="Section1">
                        <h3>Overview</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras nec urna aliquam, ornare eros vel, malesuada lorem. Nullam faucibus lorem at eros consectetur lobortis. Maecenas nec nibh congue, placerat sem id, rutrum velit. Phasellus porta enim at facilisis condimentum. Maecenas pharetra dolor vel elit tempor pellentesque sed sed eros. Aenean vitae mauris tincidunt, imperdiet orci semper, rhoncus ligula. Vivamus scelerisque.</p>
						
						 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras nec urna aliquam, ornare eros vel, malesuada lorem. Nullam faucibus lorem at eros consectetur lobortis. Maecenas nec nibh congue, placerat sem id, rutrum velit. Phasellus porta enim at facilisis condimentum. Maecenas pharetra dolor vel elit tempor pellentesque sed sed eros. Aenean vitae mauris tincidunt, imperdiet orci semper, rhoncus ligula. Vivamus scelerisque.</p>
						 
						 
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="Section2">
                        <h3>Photos</h3>
                        <p><img src="assets/img/portfolio/portfolio-1.jpg" alt="" class="img-thumbnail"></p>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="Section3">
                        <h3>Feed</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras nec urna aliquam, ornare eros vel, malesuada lorem. Nullam faucibus lorem at eros consectetur lobortis. Maecenas nec nibh congue, placerat sem id, rutrum velit. Phasellus porta enim at facilisis condimentum. Maecenas pharetra dolor vel elit tempor pellentesque sed sed eros. Aenean vitae mauris tincidunt, imperdiet orci semper, rhoncus ligula. Vivamus scelerisque.</p>
                    </div>
                </div>
            </div>
        </div>
              

             


            </article>--><!-- End blog entry -->

            

            <!-- End blog comments -->
          </div>
          <!-- End blog entries list -->

      <!--    <div class="col-lg-4">
		  

            <div class="sidebar">

            <!--    <h3 class="sidebar-title">Search</h3>
            <div class="sidebar-item search-form">
                <form action="">
                  <input type="text">
                  <button type="submit"><i class="icofont-search"></i></button>
                </form>

              </div><!-- End sidebar search formn-->
			  

        <!--      <h4 class="sidebar-title">Contacts who visited</h4>
              <div class="sidebar-item categories">
                <ul>
                  <li><a href="#">Pratiksha</a></li>
                  <li><a href="#">Aarti Kore</a></li>
				   </ul>

              </div><!-- End sidebar categories-->

              

             

        </div><!-- End sidebar -->
			
			
			

      </div>

      </div>
	  </div>
    </section><!-- End Blog Section -->

  </main><!-- End #main -->
  @endsection