@extends('layouts.my-town-app')


@section('content')
<?php $items=json_decode($orders->items);?>
<div class="shopping-cart">
    <!-- Title -->
    <div class="heading" >
        <!--<i class="fa fa-shopping-bag " ></i>-->
        Order Summary
        <div style="display:inline;margin-left:60%;">{{ $orders->created_at }}</div>
    </div>
    @foreach ($items as $item)
    <?php $product=App\Models\Product::where('id',$item->id)->first();?>
    <!-- Product #1 -->
    <div class="item">
        <div class="image99">
            <img src="{{ asset('/storage/products/'.$product->product_image) }}" alt="" width="150" height="150" />
        </div>
 
        <div class="description">
            <span>{{$item->name}}</span>
            <span>{{$product->description}}</span>
        </div>
 
        <div class="quantity">
            <label>Quantity:{{ $item->quantity }}</label>
            
        </div>
 
        <div class="total-price">Price per item: ₹{{ $item->price }}</div>
        <div class="total-price">{{ $product->business->name }}</div>
        <div class="last-box">Total price: ₹{{ $item->price*$item->quantity }}</div>
        <div class="total-price">{{ $orders->created_at }}</div>
    </div>
    @endforeach
    <div class="cart-total">Total: {{ $orders->total_price }}</div>
</div>

@endsection
