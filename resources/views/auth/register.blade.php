<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
        </x-slot>

@push('head')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">  
<script src="//code.jquery.com/jquery-1.12.1.js"></script>  
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>  

<script>
$(function(){
    $("#location_search").autocomplete({
      source :function( request, response ) {
        $.ajax({
           url: "{{ url('/location/search') }}",
           dataType: "json",
           data: {
              q: request.term 
           },
           success: function( data ) {
               response($.map(data.predictions, function(item) {
                     return {
                         label : item.description,
                         value : item.place_id
                     };
               }));
           }
        });
       },
       select: function (event, ui) {
             $("#location_search").val(ui.item.label);
             $("#place_id").val(ui.item.value);
             return false;
       }
   });
});

</script>
<style>
.ui-autocomplete-loading { background:url('/images/loading.gif') no-repeat right center }
</style>

@endpush
        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('register') }}" autocomplete="off">
            @csrf
			@if(request()->input('biz') == 1)
			<h3>Step 1 - Let's create your account.</h3>
			@endif

            <div class="mt-4">
                <x-jet-label for="location_search" value="{{ __('Your town') }}" />
                <x-jet-input id="location_search" class="block mt-1 w-full" type="text" name="location_search" required autofocus autocomplete="location_search" placeholder="Start typing the name of your town" />
                <input id="place_id" type="hidden" name="place_id" required value=""/>
            </div>
            <div class="mt-4">
                <x-jet-label for="name" value="{{ __('Your name') }}" />
                <x-jet-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autocomplete="off" />
            </div>

            <div class="mt-4">
                <x-jet-label for="phone" value="{{ __('Your mobile phone number') }}" />
                <x-jet-input id="phone" class="block mt-1 w-full" type="text" name="phone" :value="old('phone')" required autocomplete="off"/>
            </div>
            <div class="mt-4">
                <x-jet-label for="email" value="{{ __('Your email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autocomplete="off"/>
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" autocomplete="off"/>
            </div>

            <div class="mt-4">
                <x-jet-label for="password_confirmation" value="{{ __('Confirm Password') }}" />
                <x-jet-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="off" />
            </div>

            @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
                <div class="mt-4">
                    <x-jet-label for="terms">
                        <div class="flex items-center">
                            <x-jet-checkbox name="terms" id="terms"/>

                            <div class="ml-2">
                                {!! __('I agree to the :terms_of_service and :privacy_policy', [
                                        'terms_of_service' => '<a target="_blank" href="'.route('terms.show').'" class="underline text-sm text-gray-600 hover:text-gray-900">'.__('Terms of Service').'</a>',
                                        'privacy_policy' => '<a target="_blank" href="'.route('policy.show').'" class="underline text-sm text-gray-600 hover:text-gray-900">'.__('Privacy Policy').'</a>',
                                ]) !!}
                            </div>
                        </div>
                    </x-jet-label>
                </div>
            @endif

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-jet-button class="ml-4">
                    {{ __('Register') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
