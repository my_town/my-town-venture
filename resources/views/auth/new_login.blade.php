@extends('layouts.index_welcome')
@section('content')
<!-- ======= About Section ======= -->

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="js/form_validation.js"></script>  


<style>
.text-red-600{
    color: red;
}
.error{
    color: red;
}
</style>
<section id="about" class="login-bg">
    <div class="container">
	    <div class="row">
            <div class="col-lg-9 px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
                <div class="card card0 border-0 login-page">
                    <div class="row d-flex">
                        <div class="col-lg-3">
                            <div class="card2 my-4 px-3">
                                <div class="row px-3 justify-content-center mt-4 mb-5">
                                    <img src="images/img/login-clipart.png" class="image"> 
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9">
                           
                            <div class="card2 card border-0 px-4 py-5">

                                <div class="row mb-4 px-3"> 

                          <!-- <p class="small alert-warning"><i class="icofont-close-circled "></i> Whoops! Something went wrong...these credentials do not match our records.</p> -->
                          
                    </div>


                              <x-jet-validation-errors class="mb-4" />

                                <div class="row mb-4 px-3">
					                <h5 class="mb-0 mr-4 mt-2">Login</h5>
                                </div>
                                @if (session('status'))
                                    <div class="mb-4 font-medium text-sm text-green-600">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <form method="POST" action="{{ route('login') }}" autocomplete="off" name="login" id="login">
                                    @csrf
                                <div class="row px-3"> 
                                    <label class="mb-1"><h6 class="mb-0 text-sm">Email Address</h6></label>
                                    <input class="mb-4" type="text" id="email" name="email" placeholder="Enter a valid email address"> 
                                </div>
                                <div class="row px-3">
                                    <label class="mb-1"><h6 class="mb-0 text-sm">Password</h6></label>
                                    <input type="password" id="password" name="password" placeholder="Enter password" required autocomplete="current-password">
                                </div>
                                <div class="row px-3 mb-4">
                                    <div class="custom-control custom-checkbox custom-control-inline">
                                        <input id="remember_me" name="remember" type="checkbox" class="custom-control-input">
                                        <label for="chk1" class="custom-control-label text-sm">Remember me</label>
                                    </div>
                                    @if (Route::has('password.request'))
                                        <a href="{{ route('password.request') }}" class="ml-auto mb-0 text-sm">Forgot Password?</a>
                                    @endif
                                </div>
                                <div class="row mb-3 px-3">
                                    <button type="submit" class="btn btn-blue text-center">Login</button>
                                </div>
                                </form>
                                <div class="row mb-4 px-3">
                                    <small class="font-weight-bold">Don't have an account? <a href="/new_register" class="text-danger">Register</a></small>
                                </div>

                              
                            </div>

                        </div>
                    </div>
                    <div class="bg-blue py-4">
                        <div class="row px-3">
                            <small class="ml-4 ml-sm-5 mb-2"> &copy; Copyright <strong><span>MyTown</span></strong>. All Rights Reserved</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</section><!-- End About Section -->
@endsection