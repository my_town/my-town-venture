@extends('layouts.new_MyTown')
@section('content')

<style>
.text-red-600{
    color: red;
}
</style>
<section id="about" class="login-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
                <div class="card card0 border-0 login-page">
                    <div class="row d-flex">
                        <div class="col-lg-3">
                            <div class="card2 my-4 px-3">
                                <div class="row px-3 justify-content-center mt-4 mb-5">
                                    <img src="images/img/login-clipart.png" class="image"> 
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9">
                           
                            <div class="card2 card border-0 px-4 py-5">

        <div class="mb-4 text-sm text-gray-600">
			<h3>Forgot password?</h3>
            {{ __('No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}
        </div>

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('password.email') }}">
            @csrf
            <div class="block">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="flex items-center justify-end mt-4">
               <!--  <x-jet-button>
                    {{ __('Email Password Reset Link') }}
                </x-jet-button> -->
                 <button type="submit" class="btn btn-blue text-center">Reset</button>
            </div>
        </form>
    </div>

                        </div>
                    </div>
                    <div class="bg-blue py-4">
                        <div class="row px-3">
                            <small class="ml-4 ml-sm-5 mb-2"> &copy; Copyright <strong><span>MyTown</span></strong>. All Rights Reserved</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- End About Section -->
@endsection
