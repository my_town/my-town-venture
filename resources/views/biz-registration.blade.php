@extends('layouts.my-town-app')
@push('head')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">  
<script src="//code.jquery.com/jquery-1.12.1.js"></script>  
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>  

<script>
$(function(){
    $("#location_search").autocomplete({
      source :function( request, response ) {
        $.ajax({
           url: "{{ url('/location/search') }}",
           dataType: "json",
           data: {
              q: request.term 
           },
           success: function( data ) {
               response($.map(data.predictions, function(item) {
                     return {
                         label : item.description,
                         value : item.place_id
                     };
               }));
           }
        });
       },
       select: function (event, ui) {
             $("#location_search").val(ui.item.label);
             $("#place_id").val(ui.item.value);
             return false;
       }
   });
});

</script>
<style>
.ui-autocomplete-loading { background:url('/images/loading.gif') no-repeat right center }
</style>
@endpush
@section('content')
<div class="greybg content-box">
    <div class="container">
        <div class="row paddingbottom3">
            <div class="col-8 col-12-medium">
                <div class="whitebg margintop3">
                    <div class="wrapper">
                    <div class="title">
					    @php
					        $referer = request()->headers->get('referer');
					    @endphp
					    @if(preg_match('/register\?biz=1/',$referer))
					        {{ __('Step 2: ') }}
					    @endif
					    Let us create profile of your business
                    </div>
                    <form class="form" action='biz-registration' method="POST" enctype="multipart/form-data" autocomplete="off">
                        @csrf
                        <div class="input_field">
                            <label>Address</label>
                            <input type="text" class="input1" id="location_search" name="location_search" value="" placeholder="Address of your business">
                            <input type="hidden" id="place_id" name="place_id" value="" />
                        </div>
                        <div class="input_field">
                            <label>Name</label>
                            <input type="text" class="input1" id="name" name="name" placeholder="Name of your business" autocomplete="off">
                        </div>
                        <div class="input_field">
                            <label>Phone number</label>
                            <input type="text" class="input1" id="phone_number1" name="phone_number1" placeholder= "Phone number of your business" autocomplete="off">
                        </div>
                        <div class="input_field">
                            <label>Phone number2</label>
                            <input type="text" class="input1" id="phone_number2" name="phone_number2" placeholder="Another phone number of your business" autocomplete="off">
                        </div>
                        <div class="input_field">
                            <label>Email address</label>
                            <input type="email" class="input1" id="email_address" name="email_address" placeholder="Email address of your business" autocomplete="off">
                        </div>
                        <div class="input_field">
                            <label>Logo</label>
                            <input type="file" class="input1" id="logo" name="logo" placeholder="Add your company logo" autocomplete="off">
                        </div>
                        <div class="input_field">
                            <label>Banner Image</label>
                            <input type="file" class="input1" id="banner_image" placeholder="Add banner image for your business profile" name="banner_image">
                        </div>
                        <div class="input_field">
                            <label>Description</label>
                            <textarea class="textarea" id="description" placeholder="Add short description about your business" name="description"></textarea>
                        </div>
                        <div class="input_field">
                            <button type="submit" class="btn">Submit</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
            <div class="col-4 col-12-medium">
                <div class="whitebg padding2 margintop3">
                    <h5>4 Visitors also viewing this</h5>
                    <div class="orange">
                        <ul>
                            <li>Pratiksha</li>
                            <li>John Doe</li>
                        </ul>
                    </div>
                </div>
                <div class="whitebg padding2 margintop2">
                    <h5>4 Visitors also viewing this</h5>
                    <div class="orange">
                        <ul>
                            <li>Pratiksha</li>
                            <li>John Doe</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
