@extends('layouts.new_MyTown')
@section('content')
    @push('head')
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/css/easy-responsive-tabs.css " />
        <!-- Favicons -->
        <link href="{{ url('/') }}/images/favicon.png" rel="icon" />
        <link href="{{ url('/') }}/images/apple-touch-icon.png" rel="apple-touch-icon" />
        <!--   <link rel="stylesheet" href="./assets/css/fontawesome/css/all.css" /> -->
        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
            rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap"
            rel="stylesheet" />
        <!-- Vendor CSS Files -->
        <link href="{{ url('/') }}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="{{ url('/') }}/vendor/icofont/icofont.min.css" rel="stylesheet" />
        <link href="{{ url('/') }}/vendor/boxicons/css/boxicons.min.css" rel="stylesheet" />
        <link href="{{ url('/') }}/vendor/venobox/venobox.css" rel="stylesheet" />
        <link href="{{ url('/') }}/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet" />
        <link href="{{ url('/') }}/vendor/aos/aos.css" rel="stylesheet" />
        <!-- Template Main CSS File -->
        <!--    <link href="assets/css/style2.css" rel="stylesheet" />
         -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
                integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous">
        </script>
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
        <script src="{{ url('/') }}/vendor/jquery/jquery.min.js"></script>
        <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <script src="{{ url('/') }}/js/form_validation.js"></script>
        <style>
            .get-started-icon {
                font-size: 24px;
                background: #ffce00;
                padding: 14px;
                color: #fff;
                border-radius: 50px;
                position: relative;
                z-index: 5;
                box-shadow: 10px 2px 15px rgba(0, 0, 0, 0.1);
            }

            .btn-get-started {
                font-family: "Raleway", sans-serif;
                font-weight: 400;
                font-size: 16px;
                letter-spacing: 1px;
                display: inline-block;
                transition: 0.5s;
                margin-left: -10px;
                padding: 8px 26px 8px 26px;
                color: #ffffff;
                background: #ffce00;
                border-radius: 0 50px 50px 0;
                position: relative;
                z-index: 4;
            }

            .btn-get-started:hover {
                color: #262228;
            }

            .img-fluid {
                width: 100%;
            }

            .bg-products {
                border: 1px solid #ffbd83;
                padding: 0;
                margin-right: 0px;
                margin-bottom: 5px;
            }

            .login-page .bg-blue {
                color: #fff;
                background-color: #1A237E;
            }

            .error {
                color: red;
            }

            .custom-file-upload {
                border: 1px solid #ccc;
                display: inline-block;
                padding: 6px 12px;
                cursor: pointer;
            }

        </style>
        <script>
            $(function() {
                $("#locations_search").autocomplete({

                    source: function(request, response) {
                        $.ajax({
                            url: "{{ url('/location/search') }}",
                            dataType: "json",
                            data: {
                                q: request.term
                            },
                            success: function(data) {
                                response($.map(data.predictions, function(item) {
                                    return {
                                        label: item.description,
                                        value: item.place_id
                                    };
                                }));
                            }
                        });
                    },
                    select: function(event, ui) {
                        $("#locations_search").val(ui.item.label);
                        $("#places_id").val(ui.item.value);
                        return false;
                    },
                    focus: function(event, ui) {
                        $("#locations_search").val(ui.item.label);
                        return false;
                    },
                });
            });
            /// for business offering type
            function selectOnlyThis(id,txtId) {
                for (var i = 1; i <= 3; i++) {
                    document.getElementById(i).checked = false;
                    document.getElementsByClassName('txtVal').readOnly=true;
                    
                }
                document.getElementById(id).checked = true;
                document.getElementById(txtId).readOnly = false;
            }

            // for next button click
            function nextButtonCick(id, tabId) {


                var ele = document.getElementById(tabId);
                ele.classList.add("active");
                var element = document.getElementById("tab_" + id);

                element.classList.remove("active");

            }

            //video uploading

            function open_file() {
                document.getElementById('input_file').click();
            }


            $(document).ready(function() {
                $('#input_file').change(function(e) {
                    var fileName = e.target.files[0].name;
                    // alert('The file "' + fileName +  '" has been selected.');
                    //$('#video_code').val(fileName);
                    //alert(this.files[0].size);
                    document.getElementById('video_code').value = fileName;
                });
            });
            //video uploading 

            function open_image_file() {
                document.getElementById('input_image_file').click();
            }


            $(document).ready(function() {
                $('#input_image_file').change(function(e) {
                    var fileName = e.target.files[0].name;
                    // alert('The file "' + fileName +  '" has been selected.');
                    //$('#video_code').val(fileName);
                    //alert(this.files[0].size);
                    document.getElementById('image_code').value = fileName;
                });
            });

            $(document).ready(function() {
                $.ajax({
                    url: 'service_product_list',
                    type: 'get',
                    success: function(data) {
                        var service = document.getElementById("service_list");
                        service.innerHTML = data;
                        var product = document.getElementById("product_list");
                        product.innerHTML = data;

                    }
                });


            });
        </script>

        <body class="main-business">
            <!-- My Profile -->
            <section class="business-profile-wrp">
                <div class="container">
                    <div class="business-pro-box drop-shad">
                        <div class="tab" role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist" id="myLinks">
                                <li role="presentation" class="tab_act active" id="tab_next_1"><a href="#Section1"
                                        aria-controls="home" role="tab" data-toggle="tab">Business Profile Details</a></li>
                                <li role="presentation" class="tab_act" id="tab_next_2"><a href="#Section2"
                                        aria-controls="profile" role="tab" data-toggle="tab">Add Products</a></li>
                                <li role="presentation" class="tab_act" id="tab_next_3"><a href="#Section3"
                                        aria-controls="messages" role="tab" data-toggle="tab">Add Video, Overview</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content" id="myTabs">
                                <div role="tabpanel" class="tab-pane fade in show active" id="Section1">
                                    <div class="prodata-frm">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <form id="section_1" name="section_1" action="" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <!--  <h6>Tell us about your Name</h6> -->
                                                        <input type="hidden" name="user_name" id="user_name"
                                                            class="form-control" placeholder="Your Name"
                                                            value="business_registration">
                                                    </div>
                                                    <div class="form-group">
                                                        <h6>Let Us Create Profile Of Your Business</h6>
                                                        <input type="text" name="business_name" id="business_name"
                                                            class="form-control" placeholder="Name of your business">
                                                    </div>
                                                    <div class="form-group">
                                                        <h6>Business Offering Type</h6>
                                                        <ul>
                                                            <li class="chekbox-bx">
                                                                <!-- <input type="checkbox" id="1" class="checkbox">
                                                                    <label></label> -->
                                                                <input type="checkbox" id="1" onClick="selectOnlyThis(this.id,'service_list_value')">

                                                                <label for="1">Service</label>
                                                                <input type="text" name="service_list_value"
                                                                    id="service_list_value" placeholder="Specify Service type"
                                                                    class="form-control select_2 txt_val" list="service_list" readonly>
                                                                <datalist id="service_list">

                                                                </datalist>

                                                            </li>
                                                            <li class="chekbox-bx">
                                                                <!-- <input type="checkbox" id="1" class="checkbox">
                                                                    <label></label> -->
                                                                <input type="checkbox" id="2" onClick="selectOnlyThis(this.id,'product_list_value')">


                                                                <label for="2">Product</label>
                                                                <input type="text" name="product_list_value"
                                                                    id="product_list_value" placeholder="Specify Product type"
                                                                    class="form-control select_2 txt_val" list="product_list" readonly>

                                                                <datalist id="product_list">

                                                                </datalist>
                                                            </li>
                                                            <li class="chekbox-bx">

                                                                <input type="checkbox" id="3" onClick="selectOnlyThis(this.id,'specify_product_service_type')">
                                                                <label for="3"></label>
                                                                Product and Service both
                                                                <div class="namebxfrm">
                                                                    <input type="text" name="specify_product_service_type"
                                                                        id="specify_product_service_type"
                                                                        placeholder="Specify Product and Service type"
                                                                        class="form-control select_2 txt_val" readonly>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="form-group">
                                                        <!--       <h6>Tell Us about your business</h6>
                                                            <div class="inbx">

                                    <input type="text" id="description" type="text" name="description" required autofocus autocomplete="locations_search" placeholder="Start typing the name of your town" class="form-control">
                                 -->
                                                        <h6>Business Town Name</h6>
                                                        <div class="inbx">
                                                            <input type="text" id="locations_search" type="text"
                                                                name="locations_search" required autofocus
                                                                autocomplete="locations_search"
                                                                placeholder="Start typing the name of your town"
                                                                class="form-control">
                                                            <input id="places_id" type="hidden" name="places_id" required
                                                                value="">
                                                        </div>
                                                        <div class="inbx">
                                                            <input type="text" name="address" id="address"
                                                                placeholder="Street Address" class="form-control">
                                                        </div>

                                                        <div class="inbx">
                                                            <input type="text" name="email" id="email"
                                                                placeholder="Email address of your Business"
                                                                class="form-control">
                                                        </div>
                                                        <div class="inbx">
                                                            <input type="password" name="passd" id="passd"
                                                                placeholder="Enter strong password" class="form-control">
                                                        </div>
                                                        <div class="inbx">
                                                            <input type="text" name="contact1" id="contact1"
                                                                placeholder="Phone number of your Business"
                                                                class="form-control">
                                                        </div>
                                                        <div class="inbx">
                                                            <input type="text" name="contact2" id="contact2"
                                                                placeholder="Another phone number" class="form-control">
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn-main float-rig" id="next_1"
                                                        onClick="nextButtonCick(this.id,'tab_next_2')">Next</button>
                                                    <!--  <button type="submit" class="btn btn-default">Submit</button>
         -->
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="Section2">
                                    <div class="prodata-frm">
                                        <form id="section_2" action="" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <h6>Add Products</h6>
                                                <div class="inbx">
                                                    <div class="row">
                                                        <div class="col-md-5 col-sm-5">
                                                            <div class="file-upd">
                                                                <input type="file" id="image1" name="image1"
                                                                    class="int-file form-control" accept="image/*" />
                                                            </div>
                                                        </div> 
                                                        <div class="col-md-2 col-sm-2">
                                                            <input type="text" name="count1" id="count1" placeholder="Count"
                                                                class="form-control">
                                                        </div>
                                                        <div class="col-md-3 col-sm-3">
                                                            <input type="text" name="description1" id="description1"
                                                                placeholder="Description" class="form-control">
                                                        </div>
                                                        <div class="col-md-2 col-sm-2">
                                                            <input type="text" name="rate1" id="rate1" placeholder="Rate"
                                                                class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="inbx">
                                                    <div class="row">
                                                        <div class="col-md-5 col-sm-5">
                                                            <div class="file-upd">
                                                                <input type="file" id="image2" name="image2"
                                                                    class="int-file form-control" accept="image/*" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2">
                                                            <input type="text" name="count2" id="count2" placeholder="Count"
                                                                class="form-control">
                                                        </div>
                                                        <div class="col-md-3 col-sm-3">
                                                            <input type="text" name="description2" id="description2"
                                                                placeholder="Description" class="form-control">
                                                        </div>
                                                        <div class="col-md-2 col-sm-2">
                                                            <input type="text" name="rate2" id="rate2" placeholder="Rate"
                                                                class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="inbx">
                                                    <div class="row">
                                                        <div class="col-md-5 col-sm-5">
                                                            <div class="file-upd">
                                                                <input type="file" id="image3" name="image3"
                                                                    class="int-file form-control" accept="image/*" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2">
                                                            <input type="text" name="count3" id="count3" placeholder="Count"
                                                                class="form-control">
                                                        </div>
                                                        <div class="col-md-3 col-sm-3">
                                                            <input type="text" name="description3" id="description3"
                                                                placeholder="Description" class="form-control">
                                                        </div>
                                                        <div class="col-md-2 col-sm-2">
                                                            <input type="text" name="rate3" id="rate3" placeholder="Rate"
                                                                class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="inbx">
                                                    <div class="row">
                                                        <div class="col-md-5 col-sm-5">
                                                            <div class="file-upd">
                                                                <input type="file" id="image4" name="image4"
                                                                    class="int-file form-control" accept="image/*" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2">
                                                            <input type="text" name="count4" id="count4" placeholder="Count"
                                                                class="form-control">
                                                        </div>
                                                        <div class="col-md-3 col-sm-3">
                                                            <input type="text" name="description4" id="description4"
                                                                placeholder="Description" class="form-control">
                                                        </div>
                                                        <div class="col-md-2 col-sm-2">
                                                            <input type="text" name="rate4" id="rate4" placeholder="Rate"
                                                                class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn-main float-rig" id="next_2"
                                                onClick="nextButtonCick(this.id,'tab_next_3')">Next</button>
                                        </form>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="Section3">
                                    <div class="prodata-frm">
                                        <form id="section_3" name="section_3" action="" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <h6>Add Business Profile picture </h6>
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-12">
                                                            <input type="file" name="input_image_file" id='input_image_file'
                                                                accept="image/*" hidden>
                                                            <input type="text" name="image_code" id="image_code"
                                                                class="form-control"
                                                                placeholder="Add your Business profile pic"
                                                                onClick="open_image_file()" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <h6>Embed your Video code</h6>
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-12">
                                                            <input type="file" name="input_file" id='input_file'
                                                                accept="video/*" hidden>
                                                            <input type="text" name="video_code" id="video_code"
                                                                class="form-control" placeholder="Embed your Video code"
                                                                onClick="open_file()" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <h6>Add Description about your business :</h6>
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12">
                                                        <textarea class="form-control1" name="video_description"
                                                            id="video_description" rows="5"
                                                            placeholder="Add Description about your business :"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn-main float-rig" id="next_3">Save</button>
                                        </form>
                                        {{-- <p><a href="#">View Profile</a> | <a href="#">Edit Profile</a> | <a href="#">Dashboard</a></p> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--     <div class="bg-blue py-4" style="background-color:#1A237E">
                                <div class="row px-3">
                                    <small class="ml-4 ml-sm-5 mb-2" style="color: white"> &copy; Copyright <strong><span>MyTown</span></strong>. All Rights Reserved</small>
                                </div>
                            </div> -->
                    </div>
                </div>
                <div class="py-4">
                    <div class="row px-3"> <small class="ml-4 ml-sm-5 mb-2"> &copy; Copyright
                            <strong><span>MyTown</span></strong>. All Rights Reserved</small> </div>
                </div>
            </section>
            <!-- <form id="section_22" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="file" name="photo" id="photo"/>
            <button type="submit" id="submit" class="btn-main float-rig">Next</button>
            </form> -->
            <!-- End My Profile -->
            <!-- ======= Footer ======= -->
            <!-- End Footer -->
            <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
            <!-- Vendor JS Files -->
            <script src="{{ url('/') }}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
            <script src="{{ url('/') }}/vendor/jquery.easing/jquery.easing.min.js"></script>
            <!-- <script src="assets/vendor/php-email-form/validate.js"></script> -->
            <script src="{{ url('/') }}/vendor/jquery-sticky/jquery.sticky.js"></script>
            <script src="{{ url('/') }}/vendor/venobox/venobox.min.js"></script>
            <script src="{{ url('/') }}/vendor/owl.carousel/owl.carousel.min.js"></script>
            <script src="{{ url('/') }}/vendor/isotope-layout/isotope.pkgd.min.js"></script>
            <script src="{{ url('/') }}/vendor/aos/aos.js"></script>
            <!-- Template Main JS File -->
            <script src="{{ url('/') }}/js/js/main.js"></script>
            <script src="{{ url('/') }}/js/easyResponsiveTabs.js"></script>
            <script type="text/javascript">
                /*$("#myTabs form").on('submit', function (e) {
              e.preventDefault();
              var linkHref = $(this).parents('.tab-pane').attr('id');
              $('#myLinks li a').removeClass('active');
              $('#myLinks li')
                .find('a[href="#' + linkHref + '"]')
                .parent()
                .next()
                .find('a')
                .tab('show')
                .addClass('active')
                .attr('data-toggle', 'tab');
                $('a.nav-link').not('.active').css('pointer-events', 'none');});
                $('a.nav-link').not('.active').css('pointer-events', 'none');*/



                $(document).ready(function() {

                    //Horizontal Tab
                    $("#parentHorizontalTab").easyResponsiveTabs({
                        type: "default", //Types: default, vertical, accordion
                        width: "auto", //auto or any width like 600px
                        fit: true, // 100% fit in a container
                        tabidentify: "hor_1", // The tab groups identifier
                        activate: function(event) {
                            // Callback function if tab is switched
                            var $tab = $(this);
                            var $info = $("#nested-tabInfo");
                            var $name = $("span", $info);
                            $name.text($tab.text());
                            $info.show();
                        },
                    });
                });

                /*const realFileBtn1 = document.getElementById("image1");
                const customBtn1 = document.getElementById("custom-button1");
                const customTxt1 = document.getElementById("custom-text1");

                 if(customBtn1){
                customBtn1.addEventListener("click", function() {
                  realFileBtn1.click();
                     });
                }

                realFileBtn1.addEventListener("change", function() {
                  if (realFileBtn1.value) {
                    customTxt1.innerHTML = realFileBtn1.value.match(
                      /[\/\\]([\w\d\s\.\-\(\)]+)$/
                    )[1];
                  } else {
                    customTxt1.innerHTML = "No file chosen, yet.";
                  }
                });*/

                /*....................................................*/

                /*const realFileBtn2 = document.getElementById("image2");
                const customBtn2 = document.getElementById("custom-button2");
                const customTxt2 = document.getElementById("custom-text2");

                 if(customBtn2){
                customBtn2.addEventListener("click", function() {
                  realFileBtn2.click();
                     });
                }

                realFileBtn2.addEventListener("change", function() {
                  if (realFileBtn2.value) {
                    customTxt2.innerHTML = realFileBtn2.value.match(
                      /[\/\\]([\w\d\s\.\-\(\)]+)$/
                    )[1];
                  } else {
                    customTxt2.innerHTML = "No file chosen, yet.";
                  }
                });*/
                /*......................................................*/

                /*const realFileBtn3 = document.getElementById("image3");
                const customBtn3 = document.getElementById("custom-button3");
                const customTxt3 = document.getElementById("custom-text3");

                 if(customBtn3){
                customBtn3.addEventListener("click", function() {
                  realFileBtn3.click();
                     });
                }

                realFileBtn3.addEventListener("change", function() {
                  if (realFileBtn3.value) {
                    customTxt3.innerHTML = realFileBtn3.value.match(
                      /[\/\\]([\w\d\s\.\-\(\)]+)$/
                    )[1];
                  } else {
                    customTxt3.innerHTML = "No file chosen, yet.";
                  }
                });*/

                /*.................................................*/

                /* const realFileBtn4 = document.getElementById("image4");
                 const customBtn4 = document.getElementById("custom-button4");
                 const customTxt4 = document.getElementById("custom-text4");

                  if(customBtn4){
                 customBtn4.addEventListener("click", function() {
                   realFileBtn4.click();
                      });
                 }

                 realFileBtn4.addEventListener("change", function() {
                   if (realFileBtn4.value) {
                     customTxt4.innerHTML = realFileBtn4.value.match(
                       /[\/\\]([\w\d\s\.\-\(\)]+)$/
                     )[1];
                   } else {
                     customTxt4.innerHTML = "No file chosen, yet.";
                   }
                 });*/

                /*...................................................*/
            </script>
        </body>
    @endsection
