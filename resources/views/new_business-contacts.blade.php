@extends('layouts.new_MyTown')
@section('content')
@push('head')
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<!-- ............................................................. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://kit.fontawesome.com/e3dc723f7b.js" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

<style>
  
 .get-started-icon {
  font-size: 24px;
  background: #ffce00;
  padding: 14px;
  color: #fff;
  border-radius: 50px;
  position: relative;
  z-index: 5;
  box-shadow: 10px 2px 15px rgba(0, 0, 0, 0.1);
}

 .btn-get-started {
  font-family: "Raleway", sans-serif;
  font-weight: 400;
  font-size: 16px;
  letter-spacing: 1px;
  display: inline-block;
  transition: 0.5s;
  margin-left: -10px;
  padding: 8px 26px 8px 26px;
  color: #ffffff;
  background: #ffce00;
  border-radius: 0 50px 50px 0;
  position: relative;
  z-index: 4; 
}

 .btn-get-started:hover {
color: #262228;
}
  </style>

  <style type="text/css">
   
.footer {
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: red;
  color: white;
  text-align: center;
}


/* Important part */
.modal-dialog{
    overflow-y: initial !important
}
.suggested-modal-body, .business-modal-body{
    height: 50vh;
    overflow-y: auto;
    overflow-x: hidden;
}

@wuliwong

</style>
@endpush

<!-- ======= Breadcrumbs ======= -->
<section id="breadcrumbs" class="breadcrumbs">
    <div class="container">
        <ol>
          <li><a href="index.html">Home</a></li>
          <li><a href="feed.html">Business Contacts</a></li>
        </ol>
    </div>
</section><!-- End Breadcrumbs -->

<main id="main">

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container">

        <div class="row">

          <div class="col-lg-8 entries">

            <br>

          <div class="card gedf-card">
                    <div class="card-header">
                    
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="mr-2">
                                  
                                </div>
                                <div class="ml-2">
                                    <div class="h5 m-0">You do not have any contacts. Would you like to add any contact?</div>
                                    <!--<div class="h7 text-muted">Sub Heading</div>-->                                </div>
                            </div>
                            <div>
                                <div class="dropdown">
                                    <button class="btn btn-link dropdown-toggle" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        
                                    </button>
                                   
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-body">
                       
                        <a class="card-link" href="#">
                            <h5 class="card-title"></h5>
                        </a>

                        <p class="card-text text-center">
                          <a href="#" class="btn btn-primary">Yes</a> <a href="{{ route('my_feed') }}" class="btn btn-primary">No</a></p>                     
                    </div>
                   
                </div>
            
             
        
            
            

            <!-- ======= Team Section ======= -->
    <section id="team" class="team section-bg-team">
      <div class="container">

        <div class="section-title">
          <h4 data-aos="fade-up">Contacts</h4>
           <ul style="list-style-type:none; padding: 0;"><li><input type="text" class="search-form" id="name" placeholder="Search Contacts by Location" style="width: 90%;"> <i class="icofont-search"></i></li> 
            </ul>
         <!-- <p data-aos="fade-up">Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>-->
        </div>

        <div class="business-modal-body">
           <div class="row" id="postList">
                </div>
         </div>

      </div>
    </section><!-- End Team Section -->
            <br>

            <!-- End blog comments -->

          </div><!-- End blog entries list -->

          <div class="col-lg-4">

            <div class="sidebar">

              <h3 class="sidebar-title">Search</h3>
              <div class="sidebar-item search-form">
                <form action="">
                  <input type="text" placeholder="Search Business Contacts">
                  <button type="submit"><i class="icofont-search"></i></button>
                </form>

              </div><!-- End sidebar search formn-->


              <h3 class="sidebar-title">Business Contacts</h3>
              <div class="sidebar-item recent-posts">
                <div class="post-item clearfix">
                  <img src="images/blog-recent-1.jpg" alt="">
                  <h4><a href="#">Nihil blanditiis at in nihil autem</a></h4>
                  <time datetime="2020-01-01">Town, City</time>
                </div>

                <div class="post-item clearfix">
                  <img src="images/blog-recent-2.jpg" alt="">
                  <h4><a href="#">Quidem autem et impedit</a></h4>
                  <time datetime="2020-01-01">Town, City</time>
                </div>

                <div class="post-item clearfix">
                  <img src="images/blog-recent-3.jpg" alt="">
                  <h4><a href="#">Id quia et et ut maxime similique occaecati ut</a></h4>
                  <time datetime="2020-01-01">Town, City</time>
                </div>

                <div class="post-item clearfix">
                  <img src="images/blog-recent-4.jpg" alt="">
                  <h4><a href="#">Laborum corporis quo dara net para</a></h4>
                  <time datetime="2020-01-01">Town, City</time>
                </div>

                <div class="post-item clearfix">
                  <img src="images/blog-recent-5.jpg" alt="">
                  <h4><a href="#">Et dolores corrupti quae illo quod dolor</a></h4>
                  <time datetime="2020-01-01">Town, City</time>
                </div>

              </div><!-- End sidebar recent posts-->

              

            </div><!-- End sidebar -->

          </div><!-- End blog sidebar -->

      </div>
      </div>
    </section><!-- End Blog Section -->

  </main><!-- End #main -->


  <script type="text/javascript">

    $('#search').on('keyup',function(){

            $value=$(this).val();

            $.ajax({
            type : 'get',
            url : '{{URL::to('search')}}',
            data:{'search':$value},
            success:function(data){

            $('#search_result').html(data);
            
            $("button").click(function(e) {
                    e.preventDefault();

                        var me = $(this);
                        var user_id = me.val();
                            

                    $.ajax({
                        type: "get",
                        url: '{{URL::to('sendrequest')}}',
                        data: { 
                            'id': user_id
                        },
                        success: function(result) {
                          me.hide();
                        $('#request_title'+user_id).html("Request Sent");
                           
                        },
                        error: function(result) {
                            alert('error');
                        }
                    });
                });
            }
        });
    });

    </script>
    
    <script type="text/javascript">
    $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
    </script>

 
<script type="text/javascript">
 
$(document).ready(function(){
    $('.business-modal-body').scroll(function(){
         var lastID = $('.load-more').attr('lastID');
        if(($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight) && (lastID != 0))
        {
            $.ajax({
                type:'get',
                url: '{{URL::to('business_records')}}',
                data:'id='+lastID,
                beforeSend:function(){
                    $('.load-more').show();
                },
                success:function(html){
                    $('.load-more').remove();
                    $('#postList').append(html);

                    $("button").click(function(e) {
                                e.preventDefault();
                                
                                    var me = $(this);
                                    var user_id = me.val();

                                    if(user_id){
                                        
                                        $.ajax({
                                        type: "get",
                                        url: '{{URL::to('sendrequest')}}',
                                        data: { 
                                            'id': user_id
                                        },
                                        success: function(result) {
                                          me.hide();
                                        $('#request_title'+user_id).html("Request Sent");    
                                        },
                                        error: function(result) {
                                            alert('error');
                                        }
                                    });

                                    }else{
                                       
                                    }
                            });
                }
            });
        }
    });
});

$(document).ready(function(){  

            $.ajax({
                        type: "get",
                        url: '{{URL::to('business_few_records')}}',
                        success: function(result) {
                          $('#postList').html(result);
                           
                           $("button").click(function(e) {
                                e.preventDefault();
                                
                                    var me = $(this);
                                    var user_id = me.val();

                                    if(user_id){
                                        $.ajax({
                                        type: "get",
                                        url: '{{URL::to('sendrequest')}}',
                                        data: { 
                                            'id': user_id
                                        },
                                        success: function(result) {
                                          me.hide();
                                        $('#request_title'+user_id).html("Request Sent");    
                                        },
                                        error: function(result) {
                                            alert('error');
                                        }
                                    });
                                    }else{
                                       
                                    }
                            });

                        },
                        error: function(result) {
                            alert('error');
                        }
                    });


        });

</script>

@endsection


