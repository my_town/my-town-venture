@extends('layouts.my-town-app')


@section('content')
@push('head')
<style>
.image-box{
    position: relative;
    width:1170px;
    height:380px;
}
.tag-box{
    width:100%;
    padding:10px 0px 20px 0px;
}
.logo-image-box{
    position: relative;
    width:152px;
    height:152px;
}
.logo-image-box .img,.image-box .img{
    display:block;
    width:100%;
}
.tags{
    position: relative;
    max-width:500px;
    padding:2px 15px;
    margin-right:5px;
    margin-bottom:5px;
    background-color:#ec0038;
    border-radius:3px;
    float:left;
    white-space: nowrap;
}
.tags h6{
    font-weight:500;
    color:white;
}
.logo-image-box .image_overlay,.image-box .image_overlay{
    position:absolute;
    top:0;
    left:0;
    width:100%;
    height:100%;
    background:rgba(0,0,0,0.4);
    color:white;
    display: flex;
    flex-direction:column;
    align-items:flex-end;
    justify-content: flex-end;
    opacity:0;
    transition:opacity 0.25s;
}
.tags .image_overlay{
    position:absolute;
    top:0;
    left:0;
    width:100%;
    height:100%;
    background:rgba(0,0,0,0.4);
    color:white;
    display: flex;
    align-items:flex-end;
    justify-content: flex-end;
    opacity:0;
    transition:opacity 0.25s;
    border-radius:3px;
}
.image_overlay--blur{
    backdrop-filter: blur(5px);
}
.image_overlay > *{
    /*transform: translateY(20px);*/
    transition: transform 0.25s;
}
.image_overlay:hover{
    opacity:1;
}
.update-btn{
    padding:5px;
    background:transparent;
    border-radius:5px 0px 0px 0px;
    border: 1px solid white; 
}
.cancel-btn{
    padding:6px;
    background:transparent;
}
</style>
<script>
/* unsupported
$(document).ready(function() {
  $("#bookmarkme").click(function() {
    if (window.sidebar) { // Mozilla Firefox Bookmark
      window.sidebar.addPanel(location.href,document.title,"");
    } else if(window.external) { // IE Favorite
      window.external.AddFavorite(location.href,document.title); }
    else if(window.opera && window.print) { // Opera Hotlist
      this.title=document.title;
      return true;
    }
  });
});
*/
</script>

@endpush
<!-- Banner -->
<div class="container">
    <div class="image-box">
        @if(empty($business->banner_image))
            <img class="img" src="/images/banner-gray2.png"> 
        @else 
            <img width="1170" height="380" class="img" alt="" src="{{ asset('/storage/banner/'.$business->banner_image) }}" />
        @endif
        @if(!empty(\Auth::user()->id) && $business->owner==\Auth::user()->id) 
        <div class="image_overlay image_overlay--blur">
            <a style="cursor:pointer;" id="show_banner"><div class="update-btn"><i class="far fa-edit"></i>Edit banner image</div></a>
        </div>
        @endif
    </div>
</div>
<div class="margintop2 marginbottom5">
    <div class="container">
        <div class="row marginbottom1">
            <div class="col-2 col-12-medium">
                <div style="margin-top: -4rem;">
                    <div class="logo-image-box">
                        @if(empty($business->logo))
                            <img width="152" height="152" src="/images/logo-gray.jpg"> 
                        @else 
                            <img width="152" height="152" class="img" alt=""src="{{ asset( '/storage/logo/'.$business->logo ) }}"/><!--<a style="cursor:pointer;" id="show_logo"></a>-->
                        @endif
                        @if(!empty(\Auth::user()->id) && $business->owner==\Auth::user()->id) 
                            <div class="image_overlay image_overlay--blur">
                                <a style="cursor:pointer;" id="show_logo"><div class="update-btn"><i class="far fa-edit"></i>Edit logo</div></a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-6 col-12-medium">
                <h3>{{ $business->name }}</h3>
                <div><img src="/images/location.png" width="14" height="20" alt="">&nbsp;&nbsp;{{ $town }}</div>
                <div class="colored"><img src="/images/user.png" width="16" height="16" alt=""/>&nbsp;&nbsp;{{ implode(', ',$business->tagNames()) }}</div>
            </div>
            <div class="col-4 col-12-medium">
                <div class="floatright">
                    <form method="POST" action="/business-contacts/add_bizcontact" id="businessForm">
                        @csrf
                        <input type="hidden" name="business_id" value={{ $business->id }}>
                    </form>
					@if(!Auth::user() || Auth::user()->id != $business->owner)
                    <a href="javascript:$(businessForm).submit();" class="button-solid marginright1">Add as Contact</a>
                    <a href="#" class="button-solid floatright">Chat with Us</a>
					@endif
                </div>
            </div>
        </div>
        <hr>
    </div>
</div>

<!-- Div for product addition -->
<div class="box1" id="product-box">
    <div class="jumbotron"> 
        <div class="card">
            <label id="hide_product" class="close-btn fas fa-times"></label>
            <div class="card-body" >
                <h5 class="card-title">Product Details</h5>
                <form method="POST" action="{{ url('biz-profile/'.$business->id.'/add_product') }}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="business_id" id="business_id" value={{$business->id}} >
                    <div class="mb-3">
                        <label class="form-label">Name:</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">SKU:</label>
                        <input type="text" class="form-control" id="sku" name="sku">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Price in USD:</label>
                        <input type="text" class="form-control" id="price_base_currency" name="price_base_currency">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Product Image:</label>
                        <input type="file" class="form-control" id="product_image" name="product_image" >
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Description:</label>
                        <textarea class="form-control" id="description" name="description" rows="4" cols="50"></textarea>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Div for services addition-->
<div class="box1" id="service-box">
    <div class="jumbotron"> 
        <div class="card">
            <label id="hide_service" class="close-btn fas fa-times"></label>
            <div class="card-body" >
                <h5 class="card-title">Services</h5>
                <form method="POST" action="{{ url('biz-profile/'.$business->id.'/add_service') }}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="business_id" id="business_id" value={{$business->id}} >
                    <div class="mb-3">
                        <label class="form-label">Title:</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Description:</label>
                        <textarea class="form-control" id="description" name="description" rows="4" cols="50"></textarea>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Div for photo addition -->

<div class="box1" id="photo-box">
    <div class="jumbotron"> 
        <div class="card">
            <label id="hide_photo" class="close-btn fas fa-times"></label>
            <div class="card-body" >
                <h5 class="card-title">Product Details</h5>
                <form method="POST" action="{{ url('biz-profile/'.$business->id.'/add_photo') }}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="business_id" id="business_id" value={{$business->id}} >
                    <div class="mb-3">
                        <label class="form-label">Photo:</label>
                        <input type="file" class="form-control" id="photo" name="photo" >
                    </div>   
                    <div class="mb-3">
                        <label class="form-label">Title:</label>
                        <input type="text" class="form-control" id="title" name="title">
                    </div>                 
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Div for banner update -->
<div class="box1" id="banner-box">
    <div class="jumbotron"> 
        <div class="card">
            <label id="hide_banner" class="close-btn fas fa-times"></label>
            <div class="card-body" >
                <h5 class="card-title">Banner Update</h5>
                <form method="POST" action="{{ url('biz-profile/'.$business->id.'/update_banner') }}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="business_id" id="business_id" value={{$business->id}} >
                    <div class="mb-3">
                        <label class="form-label">New Image:</label>
                        <input type="file" class="form-control" id="banner_image" name="banner_image" >
                    </div> 
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Div for logo update -->
<div class="box1" id="logo-box">
    <div class="jumbotron"> 
        <div class="card">
            <label id="hide_logo" class="close-btn fas fa-times"></label>
            <div class="card-body" >
                <h5 class="card-title">Logo Update</h5>
                <form method="POST" action="{{ url('biz-profile/'.$business->id.'/update_logo') }}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="business_id" id="business_id" value={{$business->id}} >
                    <div class="mb-3">
                        <label class="form-label">New Image:</label>
                        <input type="file" class="form-control" id="logo" name="logo" >
                    </div> 
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="greybg content-box">
    <div class="container">
        <div class="row paddingbottom3">
            <div class="col-8 col-12-medium">
                <div class="tab margintop3-minus">
					@php
						$settings = json_decode($business->settings);
					@endphp
                    <button class="tablinks active" onclick="openCity(event, 'Tab1')">Overview</button>
					@if(@$settings->offering_type == 'product' || @$settings->offering_type == 'product_service')
                    <button class="tablinks" onclick="openCity(event, 'Tab2')">Products</button>
					@endif
					@if(@$settings->offering_type == 'service' || @$settings->offering_type == 'product_service')
                    <button class="tablinks" onclick="openCity(event, 'Tab5')">Services</button>
					@endif
                    <button class="tablinks" onclick="openCity(event, 'Tab3')">Photos</button>
                    <button class="tablinks" onclick="openCity(event, 'Tab4')">Feed</button>
                </div>
                <div id="Tab1" class="tabcontent" style="display:block;">
                    <h3>About this place</h3>
                    <p>{{ $business->description }}</p>

                    <h3>Categories</h3>

                    @if(!empty(\Auth::user()->id) && $business->owner==\Auth::user()->id)
                    <div style="display:flex; width:100%; height:50px; padding:5px;">
                        <label class="show-btn"  id="show_tag"><i class="fa fa-plus" aria-hidden="true"></i></label>
                        <h4 style="color:grey; padding:5px; font-weight:400; font-size:18px ">Add Tags Here...</h4>
                    </div> 
                    <div style="display: none; position: absolute; top: 120%; left: 50%;transform: translate(-50%, -50%); width: 50%; box-shadow: 0 0 8px rgba(0,0,0,0.1); z-index: 1;" id="tag-box" >
                        <div class="jumbotron"> 
                            <div class="card">
                                <label id="hide_tag" class="close-btn fas fa-times"></label>
                                <div class="card-body" >
                                    <h5 class="card-title">Add Tags</h5>
                                    <form method="POST" action="{{ url('biz-profile/'.$business->id.'/add_tags') }}" enctype="multipart/form-data" id="addTagForm">
                                        @csrf
                                        <input type="hidden" name="business_id" id="business_id" value={{$business->id}} >
                                        <input type="text" placeholder="Comma separated list of categories" data-role="tagsinput" id="category_tags" name="category_tags" rows="10" cols="20"></textarea>
                                        <a href="javascript:$({{'addTagForm'}}).submit();" class="button-outline-sm marginleft1" style="margin-top:10px;margin-left:-2px;">Add</a>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    @endif
                    
                    <div class="tag-box">
                        @foreach($business->tagNames() as $category)
                            <div class="tags">
                                <h6>{{$category}}</h6>
                                @if(!empty(\Auth::user()->id) && $business->owner==\Auth::user()->id) 
                                    <div class="image_overlay">
                                        <a href="/biz-profile/{{ $business->id }}/remove-tag/{{$category }}" style="cursor:pointer;"><div class="cancel-btn"><i class="fas fa-times"></i></div></a>
                                    </div>
                                @endif
                            </div>
                        @endforeach
                    </div>    
                
                    <!--<table width="90%" border="1" class="table">
                        <tbody>
                            <tr>
                                <td width="31%"><strong>Store Type</strong></td>
                                <td width="69%">Grocery</td>
                            </tr>
                            <tr>
                                <td><strong>Store Type </strong></td>
                                <td>Grocery</td>
                            </tr>
                            <tr>
                                <td><strong>Store Type </strong></td>
                                <td>Grocery</td>
                            </tr>
                        </tbody>
                    </table>-->
                    <p>&nbsp;</p>
                </div>
                <div id="Tab2" class="tabcontent">
                    @if(!empty(\Auth::user()->id) && $business->owner==\Auth::user()->id) 
                    <label class="show-btn"  id="show_product"><i class="fa fa-plus" aria-hidden="true"></i></label>
                    @endif
                    <div class="row marginbottom1">
                        @foreach ($business->products as $product)
                        <div class="col-4 col-12-medium">
                            <div class="border1">
                                <img src="{{ asset('/storage/products/'.$product->product_image) }}" width="170" height="137" alt=""/>
                                <div class="small bolder marginleft1 marginbottom1"> {{ $product->name }}, {{ $product->sku }}</div>
                                    <div class="row textcenter">
                                        <form method="POST" action="/cart/add-item" id={{'product' . $product->id}}>
                                            @csrf
                                            <input type="hidden" name="product_id" value={{$product->id}} >
                                            <div class="marginbottom1">₹{{ $product->price_base_currency }}<a href="javascript:$({{'product' . $product->id}}).submit();" class="button-outline-sm marginleft1"><i class="fas fa-shopping-cart marginright1" aria-hidden="true"></i>Add</a></div>
                                        </form>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <!-- <div class="col-4 col-12-medium">
                            <div class="border1">
                                <img src="/images/bakarwadi.jpg" width="170" height="137" alt=""/>
                                <div class="small bolder marginleft1 marginbottom1"> Bakarwadi, 150gm</div>
                                <div class="row textcenter">
                                    <div class="marginbottom1">₹ 55<a href="#" class="button-outline-sm marginleft1"><i class="fas fa-shopping-cart marginright1" aria-hidden="true"></i>Add</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4 col-12-medium">
                            <div class="border1">
                                <img src="/images/bakarwadi.jpg" width="170" height="137" alt=""/>
                                <div class="small bolder marginleft1 marginbottom1"> Bakarwadi, 150gm</div>
                                <div class="row textcenter">
                                    <div class="marginbottom1">₹ 55<a href="#" class="button-outline-sm marginleft1"><i class="fas fa-shopping-cart marginright1" aria-hidden="true"></i>Add</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4 col-12-medium">
                            <div class="border1">
                                <img src="/images/bakarwadi.jpg" width="170" height="137" alt=""/>
                                <div class="small bolder marginleft1 marginbottom1"> Bakarwadi, 150gm</div>
                                <div class="row textcenter">
                                    <div class="marginbottom1">₹ 55<a href="#" class="button-outline-sm marginleft1"><i class="fas fa-shopping-cart marginright1" aria-hidden="true"></i>Add</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4 col-12-medium">
                            <div class="border1">
                                <img src="/images/bakarwadi.jpg" width="170" height="137" alt=""/>
                                <div class="small bolder marginleft1 marginbottom1"> Bakarwadi, 150gm</div>
                                <div class="row textcenter">
                                    <div class="marginbottom1">₹ 55<a href="#" class="button-outline-sm marginleft1"><i class="fas fa-shopping-cart marginright1" aria-hidden="true"></i>Add</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4 col-12-medium">
                            <div class="border1">
                                <img src="/images/bakarwadi.jpg" width="170" height="137" alt=""/>
                                <div class="small bolder marginleft1 marginbottom1"> Bakarwadi, 150gm</div>
                                <div class="row textcenter">
                                    <div class="marginbottom1">₹ 55<a href="#" class="button-outline-sm marginleft1"><i class="fas fa-shopping-cart marginright1" aria-hidden="true"></i>Add</a></div>
                                </div>
                            </div>
                        </div>-->
                    </div>
                </div>
                <div id="Tab3" class="tabcontent">
                    <label class="show-btn" id="show_photo"><i class="fa fa-plus" aria-hidden="true"></i></label>
                    <div class="row marginbottom1 paddingtop2">
                        @foreach ($business->photos as $photo)
                        <div class="col-4 col-12-medium paddingtop1">
                            <img src="{{ asset('/storage/photos/'.$photo->photo) }}" width="194" height="160" alt=""/>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div id="Tab4" class="tabcontent">
                    <div class="row marginbottom1 padding-0">
                        <div class="col-6 col-12-medium">
                            <h3>Customer reviews</h3>
                        </div>
                        <div class="col-6 col-12-medium">
                            <div class="floatright"><span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                    </div>
                    <p class="text-muted">56 reviews</p>
                    <hr class="marginbottom2 margintop2">
                    <p>The best sweet shop in Kothrud. The food quality and taste is just beyond words. Absolute value for money. Anytime good for parties and occasion.</p>
                    <div class="marginbottom1"><img src="/images/profile-pic-review.jpg" width="36" height="36" alt="" class="marginright2 vmiddle"><span class="bolder">Amit Sharma</span></div>
                    <div class="text-muted"><span class="marginright2 small bolder">11 Likes</span><span class="marginright2 small bolder">3 Comments</span></div>
                    <div class="text-muted marginbottom1"><span class="marginright2 small bolder"><i class="far fa-thumbs-up marginright1"></i>Likes</span><span class="marginright2 small bolder"><i class="far fa-comment marginright1"></i>Comments</span></div>
                    <div class="row">
                        <div class="col-1"><img src="/images/profile-pic-review.jpg" width="36" height="36" alt="" class="marginright2 vmiddle"></div>
                        <div class="col-11 col-12-medium">
                            <textarea rows="1" placeholder="Write your comment" tabindex="1" width="100%"></textarea>
                        </div>
                    </div>
                    <hr class="marginbottom2 margintop2">
                </div>
                <div id="Tab5" class="tabcontent">
                    @if(!empty(\Auth::user()->id) && $business->owner==\Auth::user()->id) 
                    <label class="show-btn" id="show_service"><i class="fa fa-plus" aria-hidden="true"></i></label><br>
                    @endif
                        @foreach ($business->services as $service)
                            <strong>{{ $service->name }}</strong>
                            <p>{{ $service->description }}</p>
                        @endforeach
                </div>
                
                <script>
                    function openCity(evt, cityName) {
                        var i, tabcontent, tablinks;
                        tabcontent = document.getElementsByClassName("tabcontent");
                        for (i = 0; i < tabcontent.length; i++) {
                            tabcontent[i].style.display = "none";
                        }
                        tablinks = document.getElementsByClassName("tablinks");
                        for (i = 0; i < tablinks.length; i++) {
                            tablinks[i].className = tablinks[i].className.replace(" active", "");
                        }
                        document.getElementById(cityName).style.display = "block";
                        evt.currentTarget.className += " active";
                    }
                </script>
            </div>
            <div class="col-4 col-12-medium">
                <div class="floatright margintop3-minus">
                @if(!Auth::user() || Auth::user()->id != $business->owner)
				<!--
				<a id="bookmarkme" rel="sidebar" href="#" title="Bookmark this page" class="button-outline marginright1"><i class="far fa-bookmark marginright1" aria-hidden="true"></i>Bookmark</a>
				-->
				<a href="https://www.google.com/maps/place/{{$business->address }}" target="_new" class="button-outline floatright"><i class="fa fa-map-marker-alt marginright1" aria-hidden="true"></i>Directions</a></div>
                @endif
                <div class="whitebg padding2 margintop3">
                    <h5>{{ $hits->count() }} recent visitors</h5>
                    <div class="colored">
                        <ul>
						@foreach($hits as $h)
                            <li>{{ $h->user->name }}</li>
						@endforeach
                        </ul>
                    </div>
                </div>
                <div class="whitebg padding2 margintop2">
                    <h5><img src="/images/ad-300x250.jpg" width="300" height="250" alt=""/></h5>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
