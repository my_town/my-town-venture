@extends('layouts.my-town-app')

@section('content')
<div class="greybg content-box">
    <div class="container">
        <div class="row paddingbottom3">
            <div class="col-8 col-12-medium">
                <div id="Tab1" class="tabcontent" style="display:block;">
    				<h3>Thank you for signing up!</h3>
    				<p>Own a business? <a href="/biz-registration">Register your business</a> or continue as a <a href="/my_feed">Shopper</a>.</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
