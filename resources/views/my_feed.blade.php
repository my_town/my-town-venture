@extends('layouts.new_MyTown')
@section('content')
@push('head')



<!-- ======= Breadcrumbs ======= -->
<section id="breadcrumbs" class="breadcrumbs">
    <div class="container">
        <ol>
          <li><a href="index.html">Home</a></li>
          <li><a href="feed.html">Feed</a></li>
        </ol>
    </div>
</section><!-- End Breadcrumbs -->


<main id="main">
    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 entries">
                    <!-- End blog entry -->
	                <div class="blog-author clearfix">
                        <img src="images/img/team/team-4.jpg" class="rounded-circle float-left" alt="">
                        <h4>Jane Smith</h4>
                        <div class="social-links">
                            <a href="https://twitters.com/#"><i class="icofont-twitter"></i></a>
                            <a href="https://facebook.com/#"><i class="icofont-facebook"></i></a>
                            <a href="https://instagram.com/#"><i class="icofont-instagram"></i></a>
                        </div>
                        <p> Visited the store yesterday. </p>
                        <p>Short description of the purchased items</p>
			            <p><span><i class="icofont-wall-clock"></i></span> Was live 15 minutes ago. <button type="button" class="btn btn-primary btn-sm">Connect with this Contact</button> <button type="button" class="btn btn-primary btn-sm">Visit Store</button></p>
			        </div><!-- End blog author bio -->
			
                    <div class="blog-author clearfix">
                        <img src="images/img/team/team-2.jpg" class="rounded-circle float-left" alt="">
                        <h4>Aarti Kore</h4>
                        <div class="social-links">
                            <a href="https://twitters.com/#"><i class="icofont-twitter"></i></a>
                            <a href="https://facebook.com/#"><i class="icofont-facebook"></i></a>
                            <a href="https://instagram.com/#"><i class="icofont-instagram"></i></a>
                        </div>
                        <p> Visited the store on Wednesday. </p>
                        <p>Short description of the purchased items</p>
			            <p><span><i class="icofont-wall-clock"></i></span> Was live 15 minutes ago. <button type="button" class="btn btn-primary btn-sm">Connect with this Contact</button> <button type="button" class="btn btn-primary btn-sm">Visit Store</button></p>
			        </div><!-- End blog author bio -->
			
			        <div class="blog-author clearfix">
                        <img src="images/img/team/team-1.jpg" class="rounded-circle float-left" alt="">
                        <h4>Ketan Kulkarni</h4>
                        <div class="social-links">
                            <a href="https://twitters.com/#"><i class="icofont-twitter"></i></a>
                            <a href="https://facebook.com/#"><i class="icofont-facebook"></i></a>
                            <a href="https://instagram.com/#"><i class="icofont-instagram"></i></a>
                        </div>
                        <p> Visited the store last week. </p>
                        <p>Short description of the purchased items</p>
			            <p><span><i class="icofont-wall-clock"></i></span> Was live 15 minutes ago. <button type="button" class="btn btn-primary btn-sm">Connect with this Contact</button> <button type="button" class="btn btn-primary btn-sm">Visit Store</button></p>
	                </div><!-- End blog author bio -->
		
			        <!-- ======= Team Section ======= -->
                    <section id="team" class="team section-bg-team">
                        <div class="container">

                            <div class="section-title">
                                <h4 data-aos="fade-up">Business Contacts in this area</h4>
		                        <ul style="list-style-type:none; padding: 0;">
                                    <li><input type="text" class="search-form" id="name" placeholder="Search Business" style="width: 90%;"> <i class="icofont-search"></i></li> 
			                    </ul>
                                <!-- <p data-aos="fade-up">Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>-->
                            </div>
            
                            <div class="row">
                                <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
                                    <div class="member">
                                        <div class="member-img">
                                            <img src="images/img/logo1.png" class="img-fluid" alt="">
                                            <span><i class="icofont-user orange"></i> 35 Contacts</span>
                                        </div>
                                        <div class="member-info">
                                            <h4>Business Name</h4>
				                            <p>Short Description Short Description  Short Description </p>
				                            <span>Area, Town, Country</span>
				                            <button type="button" class="btn btn-primary btn-sm">Visit Store</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
                                    <div class="member">
                                        <div class="member-img">
                                            <img src="images/img/logo3.jpg" class="img-fluid" alt="">
                                            <span><i class="icofont-user orange"></i> 35 Contacts</span>
                                        </div>
                                        <div class="member-info">
                                            <h4>Business Name</h4>
				                            <p>Short Description Short Description  Short Description </p>
				                            <span>Area, Town, Country</span>
				                            <button type="button" class="btn btn-primary btn-sm">Visit Store</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="fade-up">
                                    <div class="member">
                                        <div class="member-img">
                                            <img src="images/img/logo4.png" class="img-fluid" alt="">
                                            <span><i class="icofont-user orange"></i> 30 Contacts</span>
                                        </div>
                                        <div class="member-info">
                                            <h4>Business Name</h4>
				                            <p>Short Description Short Description  Short Description </p>
				                            <span>Kothrud, Pune, India</span>
				                            <button type="button" class="btn btn-primary btn-sm">Visit Store</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section><!-- End Team Section -->
                    <br>
                    <div class="blog-pagination" data-aos="fade-up">
                        <ul class="justify-content-center">
                            <li class="disabled"><i class="icofont-rounded-left"></i></li>
                            <li><a href="#">1</a></li>
                            <li class="active"><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#"><i class="icofont-rounded-right"></i></a></li>
                        </ul>
                    </div>            <!-- End blog comments -->
                </div><!-- End blog entries list -->

                <div class="col-lg-4">
                    <div class="sidebar">
                        <h3 class="sidebar-title">Search</h3>
                        <div class="sidebar-item search-form">
                            <form action="">
                                <input type="text" placeholder="Search Contacts by Location">
                                <button type="submit"><i class="icofont-search"></i></button>
                            </form>
                        </div><!-- End sidebar search formn-->


                        <h3 class="sidebar-title">People visiting the store</h3>
                        <div class="sidebar-item recent-posts">
                            <div class="post-item clearfix">
                                <img src="images/img/team/team-1.jpg" alt="">
                                <h4><a href="#">Walter White</a></h4>
                                <time datetime="2020-01-01">Area, Town, City</time>
                            </div>

                            <div class="post-item clearfix">
                                <img src="images/img/team/team-2.jpg" alt="">
                                <h4><a href="#">Sarah Johnson</a></h4>
                                <time datetime="2020-01-01">Area, Town, City</time>
                            </div>

                            <div class="post-item clearfix">
                                <img src="images/img/team/team-3.jpg" alt="">
                                <h4><a href="#">William Anderson</a></h4>
                                <time datetime="2020-01-01">Area, Town, City</time>
                            </div>
                        </div><!-- End sidebar recent posts-->
                    </div><!-- End sidebar -->
			        
                    <div class="sidebar">
                        <h3 class="sidebar-title">Advertisement</h3>
			            <img src="images/img/blog-6.jpg" class="img-fluid">
                        <!-- End sidebar recent posts-->
                    </div>
                </div><!-- End blog sidebar -->
            </div>
	    </div>
    </section><!-- End Blog Section -->

</main><!-- End #main -->

@endsection